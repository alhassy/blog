
\begin{verbatim}
%{{{ piecewise Fin
We can extend the notions of source and target to paths as follows,
\begin{spec} shoudl be under module old
  -- the starting vertex of the path in |G|
  srcₚ : ∀{n} {G : Graph} → Path' n G → V(G)
  srcₚ p = (p ᵥ) fzero

  -- the ending vertex of the path in |G|
  tgtₚ : ∀{n} {G : Graph} → Path' n G → V(G)
  tgtₚ {n} p = (p ᵥ) (fromℕ n)

  s≤s˘ : ∀{x y} → suc x ≤ suc y → x ≤ y
  s≤s˘ (s≤s hh) = hh

  if_then_else : ∀ {a b} {X : Set a} {Y : Set b} → Dec X → (X → Y) → (¬ X → Y) → Y
  if_then_else (yes p) t f = t p
  if_then_else (no ¬p) t f = f ¬p

  Reduce : ∀{n m} (i : Fin (n + m)) (i≮n : ¬ toℕ i < n) → Fin m
  Reduce = λ {n m} (i : Fin (n + m)) i≮n → reduce≥ {n} {m} i ((≰⇒> ⨾ s≤s˘) i≮n)
  PiecewiseFin : ∀ {n m} {ℓ} {Z : Set ℓ} → (Fin n → Z) → (Fin m → Z) → (Fin (n + m) → Z)
  PiecewiseFin {n} p q = λ i → if suc (toℕ i) ≤? n then fromℕ≤ ⨾ p else (Reduce i ⨾ q)
{-- to define an operation on |Fin (n + m)| it suffices to define it on |Fin n| and |Fin m|
PiecewiseFin {n} {m} p q i with (suc (toℕ i)) ≤? n
PiecewiseFin {n} {m} p q i | yes i<n = p (fromℕ≤ i<n)
PiecewiseFin {n} {m} p q i | no i≮n = let sn≤si = ≰⇒> i≮n in q (reduce≥ i (s≤s˘ sn≤si))
-- more generally,
-- | : ∀ {n m} {ℓ} (P : ∀{i} → Fin i → Set ℓ)|
-- | → ((fn : Fin n) → P fn) → ((fm : Fin m) → P fm) → ((fnm : Fin(n + m)) → P fnm)|
-}

  ⨾-cond : ∀ {a b c} {X : Set a} {Y : Set b} {Z : Set c}
        {d : Dec X} {t : X → Y} {f : ¬ X → Y} {h : Y → Z}
      → h (if d then t else f) ≡ if d then (t ⨾ h) else (f ⨾ h)
  ⨾-cond {d = yes p} = ≡-refl
  ⨾-cond {d = no ¬p} = ≡-refl

  cond-⨾ : ∀{a b c} {X : Set a} {Y : Set b} {Arg : Set c}
        {d : Arg → Dec X} {t : Arg → X → Y} {f : Arg → ¬ X → Y} {h : Arg → Arg}
      → (λ i → if d (h i) then t (h i) else (f (h i)))
      ≗ h ⨾ (λ i → if d i then t i else (f i))
  cond-⨾ {d = d} i with d i
  ...| yes p = ≡-refl
  ...| no p = ≡-refl


{- 
PiecewiseFin-cond : ∀{n m ℓ}{Z : Set ℓ}{p : Fin n → Z}{q : Fin m → Z}
             → PiecewiseFin p q
             ≗ λ i → if suc (toℕ i) ≤? n then fromℕ≤ ⨾ p else (Reduce i ⨾ q)
PiecewiseFin-cond {n = n} i with (suc (toℕ i)) ≤? n
...| yes i<n = ≡-refl
...| no i≮n = ≡-refl
-}

  PiecewiseFinFusion-⨾ : ∀ {n m ℓ ℓ'} {Z : Set ℓ} {Ω : Set ℓ'}
    {p : Fin n → Z} {q : Fin m → Z} {r : Z → Ω}
    → PiecewiseFin p q ⨾ r
    ≗ (λ i → if suc (toℕ i) ≤? n then fromℕ≤ ⨾ p ⨾ r else (Reduce i ⨾ q ⨾ r))
  PiecewiseFinFusion-⨾ {n} {p = p} {q} {r} i =
    ⨾-cond {d = suc (toℕ i) ≤? n} {fromℕ≤ ⨾ p} {Reduce i ⨾ q} {r}

  ⨾-PiecewiseFinFusion : ∀ {n m ℓ ℓ'} {Z : Set ℓ} {Ω : Set ℓ'}
    {p : Fin n → Z} {q : Fin m → Z} {r : Ω → Fin(n + m)}
    → r ⨾ PiecewiseFin p q
    ≗ (λ i → if suc (toℕ (r i)) ≤? n then fromℕ≤ ⨾ p else ((Reduce (r i) ⨾ q)))
  ⨾-PiecewiseFinFusion {n} {p = p} {q} {r} i = {!magic!}

%}}}
%{{{ Fin theory
-- cf |fromℕ≤-lemma| below
s≤s-fromℕ : ∀{i j n} (i<j : i < j) (i<n : i < n) (j<n : j < n)
  → fromℕ≤ (s≤s i<n) f< fromℕ≤ (s≤s j<n)
s≤s-fromℕ i<j i<n j<n =
      ≡-cong suc (toℕ-fromℕ≤ (s≤s i<n))
  ⟨≡≤⟩ (s≤s i<j
  ⟨≤≡˘⟩ (toℕ-fromℕ≤ (s≤s j<n)))
{- i.e:

  suc (toℕ (fromℕ≤ (s≤s i<n))) 
≡⟨ ≡-cong toℕ-fromℕ≤ ⟩
  suc (suc i)
≤⟨ s≤s i<j ⟩ 
  suc j
≡˘⟨ toℕ-fromℕ≤ ⟩ 
  toℕ (fromℕ≤ (s≤s j<n))
-}

-- |i < j < n ⇒ fromℕ≤ i<n f< fromℕ≤ j<n|
fromℕ≤-lemma : ∀{i j n} (i<j : i < j) (j<n : j < n)
       → fromℕ≤ (<-trans i<j j<n) f< fromℕ≤ j<n
fromℕ≤-lemma (s≤s i<j) (s≤s j<n) with <-trans (s≤s i<j) (s≤s j<n)
fromℕ≤-lemma (s≤s i<j) (s≤s j<n) | s≤s p =
      (≡-cong suc (toℕ-fromℕ≤ (s≤s p)))
  ⟨≡≤⟩ (s≤s i<j
  ⟨≤≡˘⟩ toℕ-fromℕ≤ (s≤s j<n))
{-
  suc (toℕ (fromℕ≤ (s≤s p))) 
≡⟨ ≡-cong suc toℕ-fromℕ≤ ⟩
  suc (suc .i)
≤⟨ s≤s i<j ⟩
  suc j
≡˘⟨ toℕ-fromℕ≤ ⟩ 
 toℕ (fromℕ≤ (s≤s j<n))
-}

low : ∀ {m n j} {i : Fin (n + m)}
  → toℕ i < j → (j<n : j < n) → Σ k ∶ Fin n • k f< (fromℕ≤ j<n)
low i<j j<n = fromℕ≤ (<-trans i<j j<n) , fromℕ≤-lemma i<j j<n

high : ∀{m n}{i j : Fin(m + n)} (i<j : i f< j) (m≤i : m ≤ toℕ i) (m≤j : m ≤ toℕ j)
  → reduce≥ i m≤i f< reduce≥ j m≤j
high {i = i} {j} i<j m≤i m≤j = {!not yet proven, if at all true!}
    
-- proven by following the recursion/matching done in the definition of monus
∸-shunting : {x y z : ℕ} → y ≤ (x + z) → (y ∸ x) ≤ z
∸-shunting {zero } {y    } {z} y≤z = y≤z
∸-shunting {suc x} {zero } {z} z≤n = z≤n
∸-shunting {suc x} {suc y} {z} (s≤s y≤x+z) = ∸-shunting {x} {y} {z} y≤x+z

suc-∸-dist : ∀ {x y} → x < y → suc y ∸ x ≡ suc(y ∸ x)
suc-∸-dist {zero} (s≤s z≤n) = ≡-refl
suc-∸-dist {suc x} (s≤s x<y) = suc-∸-dist x<y

suc-∸-dist≤ : ∀ {x y} → x ≤ suc y → suc y ∸ x ≡ suc(y ∸ x)
suc-∸-dist≤ {zero} z≤n = ≡-refl
suc-∸-dist≤ {suc .0} {zero} (s≤s z≤n) = {!!}
suc-∸-dist≤ {suc x} {suc y} (s≤s pf) = {!!}

-- assuming a certain proviso, we can shunt monus leftwards of |<|
∸-shunting< : {x y z : ℕ} → x < y → y < (x + z) → (y ∸ x) < z
∸-shunting< {x} {y} {z} x<y y<x+z = eq ⟨≡≤⟩ yx≤z
  where
    yx≤z : suc y ∸ x ≤ z
    yx≤z = ∸-shunting {x} {suc y} {z} y<x+z

    eq : suc (y ∸ x) ≡ suc y ∸ x
    eq = ≡-sym (suc-∸-dist {x} {y} x<y)

open import Data.Sum using (_⊎_ ; inj₁ ; inj₂)
NoContinuum : ∀ {x y : ℕ} → x ≤ y → y ≤ suc x → (x ≡ y) ⊎ (y ≡ suc x)
NoContinuum {zero } {.0} z≤n z≤n = inj₁ ≡-refl
NoContinuum {zero } {.1} z≤n (s≤s z≤n) = inj₂ ≡-refl
NoContinuum {suc x} {.0} () z≤n
NoContinuum {suc m} {._} (s≤s p) (s≤s q) with NoContinuum p q
...| inj₁ x=y  = inj₁ (≡-cong suc x=y)
...| inj₂ y=sx = inj₂ (≡-cong suc y=sx)
%}}}
%{{{ working(?) ⨾-vertex and ⨾-edge
⨾vertex : ∀{n m G} (p : Path' n G) (q : Path' m G) → V [ n + m ] → V G
⨾vertex {n} {m} {G} p q i = h i
  where h : (x : Fin (suc (n + m))) → V G
        h = PiecewiseFin {suc n} {m} {ℓ₀} {V G} ( p ᵥ ) (λ i →  (q ᵥ) (` i) )

⨾edge : ∀{n m G} (p : Path' n G) (q : Path' m G) → E [ n + m ] → E G
⨾edge {n} {m} {G} p q j = h j
  where h : (x : Fin (n + m)) → E G
        h = PiecewiseFin {n} {m} {ℓ₀} {E G} (p ₑ) (q ₑ)

srcpres : ∀{n m G} (p : Path' n G) (q : Path' m G) → ⨾edge p q ⨾ src G ≗ src [ n + m ] ⨾ ⨾vertex p q
srcpres {n} {m} {G} p q j = wts
  where
    e : (x : Fin (n + m)) → E G
    e = ⨾edge p q
    v : (x : Fin (suc (n + m))) → V G
    v = ⨾vertex p q
    lhs : V G
    lhs = src G (e j)

    e-fusion : e ⨾ src G
          ≗ (λ i → if suc (toℕ i) ≤? n then fromℕ≤ ⨾ (p ₑ) ⨾ src G else (Reduce i ⨾ q ₑ ⨾ src G))
    e-fusion = PiecewiseFinFusion-⨾ {p = p ₑ} {q ₑ} {src G}

    v-fusion : {!src [ n + m ] ⨾ v
             ≗ (λ i → if suc (toℕ (src [ n + m ] i)) ≤? n
                       then {! fromℕ≤ ⨾ (p ᵥ) !} else {!(Reduce (src [ n + m ] i) ⨾ q)!})!}
    v-fusion = ⨾-PiecewiseFinFusion {p = p ᵥ} {λ i →  (q ᵥ) (` i)} {src [ n + m ]}

    rhs : V G
    rhs = v (src [ n + m ] j)

    wts : lhs ≡ rhs
    wts = {!!}
\end{spec}
%}}}
%{{{ ⨾vertex and ⨾edge
⨾vertex {n} {m = zero} p q (i , i<n+0) rewrite (+-right-identity n) = (p ᵥ) (i , i<n+0)
  -- the rewrite forces |i<n+0 : i < n|
⨾vertex {n} {m = suc m} p q (i , i<n+sm) rewrite (≡-trans (+-comm n (suc m)) (≡-cong suc (+-comm m n))) with (suc i) ≤? n
...| yes i<n = (p ᵥ) (i , i<n)
...| no i≮n = (q ᵥ) (i ∸ n , s≤s (∸-shunting {n} {i} {m} (s≤s˘ i<n+sm)))
      -- the rewrite forces |i<n+sm : suc i ≤ suc(n + m)|
      -- I should learn how to do this in a better way

⨾edge : ∀{n m G} (p : Path n G) (q : Path m G) → E [ n + m ] → E G
⨾edge {n} {m = zero} p q (j , sj<n+0) rewrite (+-right-identity n) = (p ₑ) (j , sj<n+0)
⨾edge {n} {m = suc m} {G} p q (j , sj<n+sm)
  rewrite (≡-trans (+-comm n (suc m)) (≡-cong suc (+-comm m n))) with (suc(suc j)) ≤? n | (suc j) ≤? (suc n)
  -- now |sj<n+sm : suc j < suc (n + m)|
...| yes sj<n | _ = (p ₑ) (j , sj<n)
...| no sj≮n | yes j<sn = {!!}
  where yo : n ≤ suc j
        yo = s≤s˘(≰⇒> sj≮n)

        oy : j ≤ n
        oy = s≤s˘ j<sn

        j<n+m : j < n + m
        j<n+m = s≤s˘ sj<n+sm

        oh : j ≡ n → n < n + m
        oh j=n = ≡-sym (≡-cong suc j=n) ⟨≡≤⟩ j<n+m

        uh : ∀{x} → suc x ∸ x ≡ suc 0
        uh = {!!}

        ho : j ≡ n → 0 < m
        ho j=n = ≡-sym (uh {n}) ⟨≡≤⟩ ∸-shunting {n} {suc n} {m} (oh j=n)

        oh2 : n ≡ suc j → j < suc j + m
        oh2 n=sj = j<n+m ⟨≤≡˘⟩ cong₂ _+_ (≡-sym n=sj) ≡-refl

        ho2 : n ≡ suc j → 0 ≤ m -- no duh!
        ho2 n=sj with ∸-shunting {j} {suc j} {suc m} (oh2 n=sj ⟨≤≡˘⟩ +-suc j m) 
        ...| sj-j≤sm = s≤s˘ (≡-sym (uh {j})  ⟨≡≤⟩ sj-j≤sm)

        ssh : n ≡ suc j → suc (suc j) ≤ suc n
        ssh n=sj = ≤-reflexive (≡-sym( ≡-cong suc n=sj ))

        ssj≤n : suc (suc j) ≤ n
        ssj≤n = {!!}

        0<m : 0 < m
        0<m = {!!}

        ee : E G
        ee with NoContinuum oy yo
        ee | inj₁ j=n = (q ₑ) (0 , s≤s (ho j=n))
        ee | inj₂ n=sj = (q ₑ) (0 , s≤s 0<m)


...| no _ | no j≮sn = (q ₑ) (j ∸ n , s≤s (∸-shunting< {n} {j} {m} n<j j<n+m))
  where 
        n<j : n < j
        n<j = s≤s˘(≰⇒> j≮sn)

        j<n+m : j < n + m
        j<n+m = s≤s˘ sj<n+sm

-- j < suc n , suc j ≤ suc n

_⨾⨾_ : ∀{n m : ℕ}{G : Graph} (p : Path (suc n) G) (q : Path (suc m) G) → srcₚ p ≡ tgtₚ q
  → Path (suc n + suc m) G
_⨾⨾_ {n} {m} {G} p q pf = {!!}
    where
      eedge :  E [ suc (n + suc m) ] → E G
      eedge (j , s≤s j<n+sm)
        rewrite (≡-trans (+-comm n (suc m)) (≡-cong suc (+-comm m n))) with (suc(suc j)) ≤? (suc n)
      ...| yes j<n = (p ₑ) (j , j<n)
      ...| no j≮n = (q ₑ) (j ∸ n , s≤s (∸-shunting< {n} {j} {m} {!!} {! j<n+sm!}) ) -- s≤s (s≤s (∸-shunting {n} {j} {m} (s≤s˘ j<n+sm))))
      -- the rewrite forces |j<n+sm : suc j ≤ suc(n + m)|
      -- I should learn how to do this in a better way


suc (j ∸ n) ≤ m


_⨾⨾_ : ∀{n m : ℕ}{G : Graph} (p : Path (suc n) G) (q : Path (suc m) G) → srcₚ p ≡ tgtₚ q
  → Path (suc n + suc m) G
_⨾⨾_ {n} {m} {G} p q pf = record {
    vertex = PiecewiseFin (p ᵥ) (q ᵥ)  -- |: V [ n + m ] → V G|
  ; edge = v
    -- λ x → let (i , j , i<j) = x in PiecewiseFin {n} {m} (λ z → (p ₑ) ({!!} , {!!}) ) {!!} j
    -- |: E [ n + m ] → E G|
  ; src-preservation = {!!}
  ; tgt-preservation = {!!} }
  where v : E [ suc n + suc m ] → E G
        v (i , j , i<j) with (suc (toℕ j)) ≤? (suc n) | (suc (toℕ i)) ≤? n
        v (i , j , i<j) | yes j<n | _ = let (i' , i'<j) = low i<j j<n in
                                    (p ₑ) (i' , (fromℕ≤ j<n , i'<j))
        v (i , j , i<j) | no j≮n | yes i<n = {!!}
        -- given : i < n , i < j , n < j
        -- reducing j : Fin(n+m) to j' : Fin m
        -- now j' < m
        -- injecting i : Fin n to i₀ : Fin(n + m) , since n ≤ n + m
        -- now n < i₀
        -- reducing i₀ : Fin(n+m) to i' : Fin m
        -- remains to show i' < j'
          where i' : Fin n
                i' = fromℕ≤ i<n
                n<j : n < toℕ j
                n<j = s≤s˘(≰⇒> j≮n)
                j' : Fin (suc m)
                j' = reduce≥ {suc n} {suc m} j n<j
                j'<m : (toℕ j') < suc m
                j'<m = bounded j'
                n<j' : n < toℕ j'
                n<j' = {!!}
                n<m : n < suc m
                n<m = {!!} -- <-trans n<j j<m
        v (i , j , i<j) | no j≮n | no i≮n =
          (q ₑ) ((reduce≥ i {!s≤s˘ (≰⇒> i≮n)!}) , ((reduce≥ j (s≤s˘ j>n))
          , {!!} )) -- {! high i<j (≰⇒> (λ z → i≮n (s≤s z))) (≰⇒> (λ z → j≮n (s≤s z))))!})
          where j>n : suc (suc n) ≤ suc (toℕ j)
                j>n = ≰⇒> j≮n

%}}}
%{{{ below all works!

open import Relation.Binary
open DecTotalOrder Data.Nat.decTotalOrder using () renaming (total to ℕ-total ; trans to ≤-trans)

AtMost : ℕ → Set
AtMost n = Σ i ∶ ℕ • i Data.Nat.< n

_≤ℕ_ = Data.Nat._≤_
-- proven by following the recursion/matching done in the definition of monus
hello : (x y z : ℕ) -- → x ≤ℕ y
  → y ≤ℕ (x + z) → (y Data.Nat.∸ x) ≤ℕ z
hello Data.Nat.zero y z y≤z = y≤z
hello (suc x) Data.Nat.zero z Data.Nat.z≤n = Data.Nat.z≤n
hello (suc x) (suc y) z (Data.Nat.s≤s y≤x+z) = hello x y z y≤x+z

helper : ∀{i m n z} → n ≤ℕ i → suc i ≤ℕ (n + m) → ((suc i) Data.Nat.∸ n) ≤ℕ z → suc(i Data.Nat.∸ n) ≤ℕ z
helper {n = Data.Nat.zero} n≤i i<n+m i<z = i<z
helper {Data.Nat.zero} {n = suc n} () _ _
helper {suc i} {n = suc m₁} (Data.Nat.s≤s n≤i) (Data.Nat.s≤s i<n+m) i+1-n≤z = helper n≤i i<n+m i+1-n≤z

yoo : ∀ n m → (AtMost n → Set) → (AtMost m → Set) → (AtMost (n + m) → Set)
yoo Data.Nat.zero m p q (i , i<m) = q (i , i<m)
yoo (suc n) Data.Nat.zero p q (i , Data.Nat.s≤s i<n+m) = p (Data.Nat.zero , Data.Nat.s≤s Data.Nat.z≤n)
yoo (suc n) m p q (Data.Nat.zero , Data.Nat.s≤s i<n+m) = p (Data.Nat.zero , Data.Nat.s≤s Data.Nat.z≤n)
yoo (suc n) m p q (suc i , Data.Nat.s≤s i<n+m) with ℕ-total i n
yoo (suc n) m p q (suc i , Data.Nat.s≤s i<n+m) | inj₁ x = p (i , Data.Nat.s≤s x)
yoo (suc n) m p q (suc i , Data.Nat.s≤s i<n+m) | inj₂ n≤i = q (suc i Data.Nat.∸ suc n , helper n≤i i<n+m (hello n (suc i) m i<n+m))

%}}}
%{{{ working theory
\begin{code}
module Path-defintion-1 (G : Graph₀) where

  open Graph₀ G using (src ; tgt)
  E = Graph₀.E
  V = Graph₀.V

  infixr 5 cons
  syntax cons v e ps s t = v ⟶[ s , e , t ]⟶ ps -- v goes, by e due to pf, to v'

  -- we want well-formed paths
  mutual
    data Path : Set where
      _!   : (v : V G) → Path
      cons : (v : V G) (e : E G) (ps : Path) .(s : v ≡ src e) .(t : tgt e ≡ head ps) → Path

    head : Path → V G
    head (v !) = v
    head (v ⟶[ s , e , t ]⟶ ps) = v

  -- the dots ensure proofs are irrelevant for equality
  eq-path : ∀{v v' e e' ps ps'} .{s} .{s'} .{t} .{t'}
    → v ≡ v' → e ≡ e' → ps ≡ ps'
    → cons v e ps s t ≡ cons v' e' ps' s' t'
  eq-path ≡-refl ≡-refl ≡-refl = ≡-refl

  syntax eq-path p q r = ≡-Path p ⟶[ q ]⟶ r

  -- |head| picks the start of a path, we can also pick the end of a path
  end : Path → V G
  end (v !) = v
  end (v ⟶[ s , e , t ]⟶ ps) = end ps

  mutual
    cat : (p q : Path) .(coh : end p ≡ head q) → Path
    cat (v !) q _ = q
      -- we know |v ≅ head q| due to |coh|, but do not care about the particular proof
    cat (v ⟶[ s , e , t ]⟶ ps) q coh = v ⟶[ s , e , t ⟨≡≡⟩ head-cat {ps} ]⟶ cat ps q coh

    head-cat : {p q : Path} {coh : end p ≡ head q} → head p ≡ head(cat p q coh)
    head-cat {._ !} {coh = ≡-refl} = ≡-refl
    head-cat {cons v _ _ _ _} {_} {_} = ≡-refl

  -- as usual, a new syntax ;-)
  infixr 5 cat
  syntax cat p q cohere = p ⨾[ cohere ] q
  end-cat : {p q : Path} {coh : end p ≡ head q} → end(cat p q coh) ≡ end q
  end-cat {.(head q) !} {q} {coh = ≡-refl} = ≡-refl
  end-cat {v ⟶[ s , e , t ]⟶ ps} {q} {coh} = end-cat {ps} {q} {coh}

  !-cat : ∀ {v q} (coh : v ≡ head q) → v ! ⨾[ coh ] q ≡ q
  !-cat {.(head q)} {q} ≡-refl = ≡-refl

  cat-! : ∀ {p v} (coh : end p ≡ v) → p ⨾[ coh ] (v !) ≡ p
  cat-! {v !} ≡-refl = ≡-refl
  cat-! {cons v e p s t} ≡-refl = eq-path ≡-refl ≡-refl (cat-! {p} {end p} ≡-refl)

  -- is this operation associative?
  cat-assoc : (p q r : Path) (coh₁ : end p ≡ head q) (coh₂ : end q ≡ head r)
    → let coh₂' = end-cat {p} {q} {coh₁ } ⟨≡≡⟩ coh₂
           ; coh₁' = coh₁ ⟨≡≡⟩ head-cat {q} {r} {coh₂}
       in  (p ⨾[ coh₁ ] q) ⨾[ coh₂'  ] r ≡ p ⨾[ coh₁' ] q ⨾[ coh₂ ] r
  cat-assoc (.(head q) !) q r ≡-refl coh₂
    rewrite (!-cat {head q} {cat q r coh₂} ((head-cat {q} {r} {coh₂}))) = ≡-refl
  cat-assoc (v ⟶[ s , e , t ]⟶ ps) q r coh₁ coh₂ =
     ≡-Path ≡-refl ⟶[ ≡-refl ]⟶ cat-assoc ps q r coh₁ coh₂

  -- morphisms
  record _⇝_ (x y : V G) : Set where -- \squigarrowright
    constructor _#_#_
    field
      path   : Path
      start  : head path ≡ x
      finish : end path  ≡ y

  open _⇝_ renaming (path to _₀ ; start to _ₕ ; finish to _ₑ) -- h and e for head and end
  -- (I wanted to use s and t, but subscript t is not supported :/ )

  -- handy dandy helper
  cohere : {x y z : V G} (p : x ⇝ y) (q : y ⇝ z) → end (p ₀) ≡ head (q ₀)
  cohere p q = p ₑ ⟨≡≡˘⟩ q ₕ

  -- say two path-morphisms are the same if the have identical path-components
  infix 2 _≈_
  _≈_ : {x y : V G} → (p q : x ⇝ y) → Set
  p ≈ q = p ₀  ≡  q ₀

  -- composition
  infixr 5 _⨾_
  _⨾_ : {x y z : V G} → x ⇝ y → y ⇝ z → x ⇝ z
  p ⨾ q = record {
      path = p ₀ ⨾[ coh ] q ₀
    ; start  = (≡-sym (head-cat {p ₀} {q ₀} {coh})) ⟨≡≡⟩ p ₕ
    ; finish = end-cat  {p ₀} {q ₀} {coh} ⟨≡≡⟩ q ₑ }
    where
      coh : end (p ₀) ≡ head (q ₀)
      coh = cohere p q

  -- composition is associtive
  assoc : {x y z ω : V G} {p : x ⇝ y} {q : y ⇝ z} {r : z ⇝ ω} → (p ⨾ q) ⨾ r ≈ p ⨾ q ⨾ r 
  assoc {p = p} {q} {r} = cat-assoc (p ₀) (q ₀) (r ₀) (cohere p q) (cohere q r)

  -- identities
  Id : {x : V G} → x ⇝ x
  Id {x} = record { path = x ! ; start = ≡-refl ; finish = ≡-refl }

  -- proof that they are identities
  leftId : {x y : V G} {p : x ⇝ y} → Id {x} ⨾ p ≈ p
  leftId {x} {y} {p} = !-cat {x} {p ₀} (cohere Id p)

  rightId : {x y : V G} {p : x ⇝ y} → p ⨾ Id {y} ≈ p
  rightId {x} {y} {p} = cat-! {p ₀} {y} (cohere p Id)
\end{code}

%}}}
%{{{ Path-definition-3-full 
\begin{code}
module Path-definition-3-full (G : Graph {ℓ₀} {ℓ₀}) where
  
  open Graph G hiding(V)
  open Graph using (V)

  -- handy dandy syntax
  infixr 5 cons
  syntax cons v ps e = v ⟶[ e ]⟶ ps -- v goes, by e, onto path ps

  -- we want well-formed paths
  mutual
    data Path : Set where
      _!   : (v : V G) → Path
      cons : (v : V G) (ps : Path) (e : v ⟶ head ps) → Path

    head : Path → V G
    head (v !) = v
    head (v ⟶[ e ]⟶ ps) = v

  -- just as |head| picks the start of a path, we can also pick the end of a path
  end : Path → V G
  end (v !) = v
  end (v ⟶[ e ]⟶ ps) = end ps

  -- How do we treat propositonal equailty over paths?
  ≅-path : {v v' : V G} (v≈v' : v ≡ v')
                 {ps ps' : Path} (ps≈ps' : ps ≡ ps') 
                 {e : v ⟶ head ps} {e' : v' ⟶ head ps'}
                 (e'≈e : e' ≅ e)                          -- note the heterogeneous equality
                 → (v ⟶[ e ]⟶ ps) ≡ (v' ⟶[ e' ]⟶ ps')
  ≅-path ≡-refl ≡-refl ≅-refl = ≡-refl

  syntax ≅-path v≈v' e≈e' ps≈ps' = ≅-Path v≈v' ⟶[ e≈e' ]⟶ ps≈ps'
\end{code}
One last time, we illustrate how things could have gone wrong if we had used |Path'| instead
---which relied on |Graph₀|---, and at the same time illustrate the troubles of using
propositional equality when types differ:
\begin{spec}  
  ≡-path' :   {v v' : V G} (v≈v' : v ≡ v')
                   {e e' : E G} (e≈e' : e ≡ e')
                   {ps ps' : Path} (ps≈ps' : ps ≡ ps')
                   {s : v ≡ src e} {s' : v' ≡ src e'}
                   (s'≈s : s' ≡ subst₂ (λ x y → x ≡ src y) v≈v' e≈e' s)
                   {t : tgt e ≡ head ps} {t' : tgt e' ≡ head ps'}
                   (t'≈t : t' ≡ subst₂ (λ x y → tgt x ≡ head y) e≈e' ps≈ps' t)
                   → (v ⟶[ s , e , t ]⟶ ps) ≡ (v' ⟶[ s' , e' , t' ]⟶ ps')
  ≡-path' ≡-refl ≡-refl ≡-refl ≡-refl ≡-refl = ≡-refl
\end{spec}
Observe that we can avoid the usage of the |subst₂|'s by using
|Relation.Binary.HeterogeneousEquality|.
However, since both instances, in this case, are equailty of equality proofs they are immediately
achieved, in practice, by using proof-irrelevance of propositonal equality. However, with this
observation in-mind we go back to the data-type |Path'| and dot the proof-obligations, thereby
declaring them irrelevant for equality. Then we can instead use:
\begin{spec}
  ≡-path' : {v v' : V G} (v≈v' : v ≡ v')
               {e e' : E G} (e≈e' : e ≡ e')
               {ps ps' : Path} (ps≈ps' : ps ≡ ps')
               → ∀ .{s} .{s'} .{t} .{t'}
               → (v ⟶[ s , e , t ]⟶ ps) ≡ (v' ⟶[ s' , e' , t' ]⟶ ps')
  ≡-path' ≡-refl ≡-refl ≡-refl = ≡-refl
\end{spec}
For more on irrelevance, see
\url{http://wiki.portal.chalmers.se/agda/pmwiki.php?n=ReferenceManual.Irrelevance}.

Anyhow, we no longer discuss, or dismiss, |Path'|.

\begin{code}
  -- we can concatenate two paths if they cohere at those ends
  mutual
    cat : (p q : Path) (coh : end p ≡ head q) → Path
    cat (v !) q _ = q
    cat (v ⟶[ e ]⟶ ps) q coh = v ⟶[ e ⟫ head-cat {ps} {q} {coh} ]⟶ cat ps q coh

    head-cat : {p q : Path} {coh : end p ≡ head q} → head p ≡ head(cat p q coh)
    head-cat {._ !} {coh = ≡-refl} = ≡-refl
    head-cat {cons v _ _} {_} {_} = ≡-refl

  -- as usual, a new syntax ;-)
  infixr 5 cat
  syntax cat p q cohere = p ++[ cohere ] q
\end{code}

I attempted to place a dot on |coh|, |cat|, but I needed to use it in |head-cat| and needed
to pattern match on |coh| for the base case. Had I succeeded in dotting |coh|, equality of
catenations would ignore the proofs. Now I must make do with a combinator, using proof-irrelevance
of heterogeneous propositional equality.
\begin{code}
  ≡-++ : {p p' q q' : Path}
         {coh : end p ≡ head q} {coh' : end p' ≡ head q'}
        → p ≡ p' → q ≡ q'
        → p ++[ coh ] q ≡ p' ++[ coh' ] q'
  ≡-++ {coh = coh} {coh'} ≡-refl ≡-refl rewrite (≡-uip coh coh') = ≡-refl
\end{code}
Using |≅-++| we can show equality of compositions irrespective of the coherence proofs,
provided that there are proofs.


Finally, do we have a dual to |head-cat|?
\begin{code}
  end-cat : {p q : Path} {coh : end p ≡ head q} → end(cat p q coh) ≡ end q
  end-cat {.(head q) !} {q} {coh = ≡-refl} = ≡-refl
  end-cat {v ⟶[ e ]⟶ ps} {q} {coh} = end-cat {ps} {q} {coh}
\end{code}

Just as list concatenation has a unit, does this path concatenation also.
Well, the simplest canadiate lying around is |_!|, let's try it!
\begin{code}
  !-++ : ∀ {v q} (coh : v ≡ head q) → v ! ++[ coh ] q ≡ q
  !-++ {.(head q)} {q} ≡-refl = ≡-refl
\end{code}
Sweet! That was easier than thought!
\begin{code}
  ++-! : ∀ {p v} (coh : end p ≡ v) → p ++[ coh ] (v !) ≡ p
  ++-! {v !} {.v} ≡-refl = ≡-refl
  ++-! {v ⟶[ e ]⟶ ps} {.(end ps)} ≡-refl
    = ≅-path ≡-refl (++-! {ps} {end ps} ≡-refl) (≅-⟫ (head-cat {ps}) )
\end{code}

Is this operation associative?
\begin{code}
  cat-assoc : (p q r : Path) (coh₁ : end p ≡ head q) (coh₂ : end q ≡ head r)
    → let coh₂' = end-cat {p} {q} {coh₁ } ⟨≡≡⟩ coh₂
           ; coh₁' = coh₁ ⟨≡≡⟩ head-cat {q} {r} {coh₂}
       in  (p ++[ coh₁ ] q) ++[ coh₂'  ] r ≡ p ++[ coh₁' ] q ++[ coh₂ ] r  -- main line, read this
  cat-assoc (.(head q) !) q r ≡-refl coh₂
    rewrite (!-++ {head q} {cat q r coh₂} ((head-cat {q} {r} {coh₂}))) = ≡-refl
  cat-assoc (v ⟶[ e ]⟶ ps) q r coh₁ coh₂ =
     ≅-path ≡-refl (cat-assoc ps q r coh₁ coh₂) (⟫-++-++ v ps q r e coh₁ coh₂)
     where
     
      lemma : ∀ qq rr cc →  (head-cat {(head qq) !} {cat qq rr cc} {head-cat {qq} {rr} {cc}})
           ≅ (head-cat {cat (head qq !) qq ≡-refl} {rr} {end-cat {head qq !}{qq} {≡-refl} ⟨≡≡⟩ cc})
      lemma (.(head rr) !) rr ≡-refl = ≅-refl
      lemma (cons v qq e₁) rr cc = ≅-refl

      -- loosely, | ⟫H (p ++ q ++ r) ≅ ⟫H (p ++ q) ⟫H ((p ++ q) ++ r) |
      ⟫-++-++  : ∀ x p q r (e : x ⟶ (head p))
       → (coh₁ : end p ≡ head q)
       → (coh₂ : end q ≡ head r) →
          e ⟫ (head-cat {p} {cat q r coh₂} {coh₁ ⟨≡≡⟩ (head-cat {q} {r} {coh₂})})
       ≅ (e ⟫ (head-cat {p} {q} {coh₁}))
            ⟫ (head-cat {cat p q coh₁} {r} {end-cat {p} {q} {coh₁} ⟨≡≡⟩ coh₂})
      ⟫-++-++ x (.(head q) !) q r e ≡-refl coh₂ = ≅-cong (λ xx → e ⟫ xx) (lemma q r coh₂)
      ⟫-++-++ x (cons v p e) q r e₁ coh₁ coh₂ = ≅-refl
\end{code}

It seems that we have a category, more-or-less.
Let's rephrase things that way.
\begin{code}
  -- morphisms
  record _⇝_ (x y : V G) : Set where -- \squigarrowright
    constructor _#_#_
    field
      path   : Path
      start  : head path ≡ x
      finish : end path  ≡ y

  open _⇝_ renaming (path to _₀ ; start to _ₕ ; finish to _ₑ) -- h and e for head and end
  -- (I wanted to use s and t, but subscript t is not supported :/ )

  -- handy dandy helper
  cohere : {x y z : V G} (p : x ⇝ y) (q : y ⇝ z) → end (p ₀) ≡ head (q ₀)
  cohere p q = p ₑ ⟨≡≡˘⟩ q ₕ
\end{code}

We'd like to say two path-morphisms are equivalent if they have identical path-components.
However, rather than make this a definition, we can prove that equality of path-morphisms
coincides with equality of their path-components! This is due to the proof-irrelevance of |_≡_|:
\begin{code}
  ≡-from-path : {x y : V G} {p q : x ⇝ y} → p ₀ ≡ q ₀ → p ≡ q
  ≡-from-path {x} {y} {p # ps # pf} {q # qs # qf} eq
    with ≡-uip ps (≡-subst (λ r → head r ≡ x) (≡-sym eq) qs)
       | ≡-uip pf (≡-subst (λ r → end r ≡ y) (≡-sym eq) qf)
  ≡-from-path {x} {y} {p # qs # qf} {.p # .qs # .qf} ≡-refl | ≡-refl | ≡-refl = ≡-refl
\end{code}
The converse is immediate,
\begin{code}
  ≡-from-path˘ : {x y : V G} {p q : x ⇝ y} → p ≡ q  → p ₀ ≡ q ₀
  ≡-from-path˘ {x} {y} {p} {.p} ≡-refl = ≡-refl
\end{code}

It is not always the case that this occurs. If we want to occur, irrespective of the other fields
admit irrelevance, we can just tag them as irrelevant as discussed in
http://wiki.portal.chalmers.se/agda/agda.php?n=ForkedReferenceManual.Records#Irrelevantfields

\begin{code}
  -- composition
  infixr 5 _⨾_
  _⨾_ : {x y z : V G} → x ⇝ y → y ⇝ z → x ⇝ z
  p ⨾ q = record {
      path = p ₀ ++[ coh ] q ₀
    ; start  = (≡-sym (head-cat {p ₀} {q ₀} {coh})) ⟨≡≡⟩ p ₕ
    ; finish = end-cat  {p ₀} {q ₀} {coh} ⟨≡≡⟩ q ₑ }
    where
      coh : end (p ₀) ≡ head (q ₀)
      coh = cohere p q
  -- composition is associtive
  assoc : {x y z ω : V G} {p : x ⇝ y} {q : y ⇝ z} {r : z ⇝ ω} → (p ⨾ q) ⨾ r ≡ p ⨾ q ⨾ r 
  assoc {p = p} {q} {r} = ≡-from-path goal
    where coh₁ : end (p ₀) ≡ head (q ₀)
          coh₁ = cohere p q

          coh₂ : end (q ₀) ≡ head (r ₀)
          coh₂ = cohere q r

          coh₂' : end (p ₀ ++[ coh₁ ] q ₀) ≡ head (r ₀)
          coh₂' = (end-cat {p ₀} {q ₀} {coh₁} ⟨≡≡⟩ (q ₑ)) ⟨≡≡˘⟩ (r ₕ)

          coh₁' : end (p ₀) ≡ head (q ₀ ++[ coh₂ ] r ₀)
          coh₁' = (p ₑ) ⟨≡≡˘⟩ (≡-sym (head-cat {q ₀} {r ₀} {coh₂}) ⟨≡≡⟩ (q ₕ))

          coh₂'' : end (p ₀ ++[ coh₁ ] q ₀) ≡ head (r ₀)
          coh₂'' = end-cat {p ₀} {q ₀} {coh₁ } ⟨≡≡⟩ coh₂

          coh₁'' : end (p ₀) ≡ head (q ₀ ++[ coh₂ ] r ₀)
          coh₁'' = coh₁ ⟨≡≡⟩ head-cat {q ₀} {r ₀} {coh₂}

          raw-assoc : (p ₀ ++[ coh₁ ] q ₀) ++[ coh₂'' ] r ₀
                     ≡ p ₀ ++[ coh₁'' ] q ₀ ++[ coh₂ ] r ₀
          raw-assoc = cat-assoc (p ₀) (q ₀) (r ₀) (cohere p q) (cohere q r)

          goal : (p ₀ ++[ coh₁ ] q ₀) ++[ coh₂' ] r ₀
              ≡  p ₀ ++[ coh₁' ] q ₀ ++[ coh₂ ] r ₀
          goal rewrite (≡-uip coh₁' coh₁'') | (≡-uip coh₂' coh₂'') = raw-assoc

{-
The rewrite simply establishes a type-casting:
        ∀ {p q r c₁ c₂ c₁' c₁'' c₂' c₂''}
     → (p ++[ c₁ ] q) ++[ c₂'  ] r ≡ p ++[ c₁'  ] q ++[ c₂ ] r
     → (p ++[ c₁ ] q) ++[ c₂'' ] r ≡ p ++[ c₁'' ] q ++[ c₂ ] r

-}
\end{code}

\begin{code}
  -- identities
  Id : {x : V G} → x ⇝ x
  Id {x} = record { path = x ! ; start = ≡-refl ; finish = ≡-refl }

  -- proof that they are identities
  leftId : {x y : V G} {p : x ⇝ y} → Id {x} ⨾ p ≡ p
  leftId {x} {y} {p} = ≡-from-path ( !-++ {x} {p ₀} (cohere Id p) )

  rightId : {x y : V G} {p : x ⇝ y} → p ⨾ Id {y} ≡ p
  rightId {x} {y} {p} = ≡-from-path (++-! {p ₀} {y} (cohere p Id) )
\end{code}

finally the category of paths over a graph
\begin{code}
  PathCategory : Category
  PathCategory =
    record
      { Obj = V G
      ; _⟶_ = _⇝_
      ; _⨾_ = _⨾_
      ; assoc = λ {x y z ω p q r} → assoc {x} {y} {z} {ω} {p} {q} {r}
      ; Id = Id
      ; leftId = leftId
      ; rightId = rightId
      }
\end{code}
%}}}

\end{verbatim}

% Local Variables:
% folded-file: t
% eval: (fold-set-marks "%{{{ " "%}}}")
% eval: (fold-whole-buffer)
% fold-internal-margins: 0
% end:


