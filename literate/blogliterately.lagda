\documentclass{article}

%{{{ header
\usepackage[hmargin=20mm,top=12mm,bottom=20mm]{geometry}

% unicode stuff
%% \usepackage[utf8x]{inputenc}
%% \usepackage{ucs}

\usepackage{multicol}
\usepackage[round]{natbib} % replaces {harvard}
\usepackage{etex}

\usepackage{hyperref}

%%%%%% for lhs2TeX
%include agda.fmt
%include /media/musa/Data/RATH-Agda/trunk/RathAgdaChars.sty
%%%%%%
\def\fcmp{\fatsemi}

\begin{document}

 \centerline{\sc \large Notes on Dependently-typed programming with Agda}
%% \vspace{.5pc}
%% \centerline{\sc Musa Alhassy}
%% \centerline{\it (rough draft)}
%% \vspace{2pc}
%% These rough notes are what I am learning about X
%% \tableofcontents
%}}}

%%% READ ME
%%
%% generate PDF: NAME=blogliterately ; lhs2TeX --agda $NAME.lagda > $NAME.tex && pdflatex $NAME.tex
%%
%% generate markdown for use with Hakyll: NAME=blogliterately; fromliterate $NAME
%%%

---
title: Blogging Literately: markdown from literate agda
date: January 12, 2016
tags: meta , agda , monoids
---

This post discusses how we achieved writing our articles using literate-style
coding and having the results transformed into markdown for processing with
Hakyll. Afterwards, an example is shown ---in fact, this post is a literate
document realized by our scripts below.

\begin{code}
module blogliterately where
\end{code}

\tableofcontents

%{{{ problem statement

\section{Problem Statement}

Ideally, I would like to write my blog posts as mostly-literate documents in-case I desire, say, to
produce PDFs from them. I would like to write
\begin{verbatim}

~~~~~~~
    ---
    title: my literate document
    date: August 20, 1991
    ---
    english and latex
    english and latex
    begin{code}
      lots of math, i.e., agda
    end{code}
    more english and latex
~~~~~~~

\end{verbatim}
and have that translate to a |.markdown| file for Hakyll to use.

% I cannot use a %-symbol above since everything after it is removed
% by my script below.
%
% lines beginning with a \ are deleted using script below
%
% note that I couldn't use the fence-blocks ```, since script below translates ` to '.
% also the ~ fencing requires a new line before and after, hence the latex comments

One of my main usages for LaTeX comments is to use them to fold, or organize, the structure
of my document. I use \href{https://bitbucket.org/alhassy/emacs/src}{folding-mode.el} for Emacs.
%}}}

%{{{ problem solution
\section{Problem Solution}

As done in the first post, we write a script that converts the code-block delimiters
from the literate style to the so-called fenced-style of Pandoc, then move the resulting
file to the |blog/posts| directory -we keep the literate files in their own directory
|blog/literate|. Along the way we translate LaTeX forward-quotes to markdown single-quotes,
ignore LaTeX comments, savagely delete any line containing a LaTeX command (i.e., a backslash),
and transform literate in-line code-delimiters %% i.e., |...|
into markdown back-ticks.

Again, the major advantages of such a translation is that it'd be easier for me to produce
PDF's from blog-posts and I'd use the same literate-agda syntax I'm accustomed to.
Also, I tend to view my work in PDF form, and so this will be a pleasant familiarity for me.

Since I want this page to be the end-result of the script, the code would be stripped of many
lines since it involves LaTeX control ---in the patterns being searched for deletion.
Hence, the reader must be content with a link to the script instead:
\href{https://bitbucket.org/alhassy/blog/raw/ac801720f0f6ca639cdcf2eb8506db6a7d30abb5/literate/fromLiterate_script.sh}{fromliterate},
or this embedding:
\begin{verbatim}
<script src="https://bitbucket.org/alhassy/blog/src/dbea95c5520c4a4fd7f35194e90f041fad644c7f/literate/fromLiterate_script.sh?embed=t"/>
\end{verbatim}

Running |fromliterate| on the
\href{https://bitbucket.org/alhassy/blog/src/ac801720f0f6ca639cdcf2eb8506db6a7d30abb5/literate/blogliterately.lagda?at=master&fileviewer=file-view-default}{literate source} file of this page
produces
\href{https://bitbucket.org/alhassy/blog/raw/ac801720f0f6ca639cdcf2eb8506db6a7d30abb5/posts/blogliterately.markdown}{markdown}
which is used to generate
this page. The same literate source leads to
\href{https://bitbucket.org/alhassy/blog/raw/dbea95c5520c4a4fd7f35194e90f041fad644c7f/literate/blogliterately.pdf}{PDF}
by running
|NAME=blogliterately ; lhs2TeX --agda $NAME.lagda > $NAME.tex && pdflatex $NAME.tex| at the
command-line. Admittedly, the PDF has room for improvement, but with
little effort can be made clear ---emphasis on `little'!--- then again, I might incorporate more
changes into the script when I need them.
Perhaps most importantly, I can edit the code within Emacs
by |C-c C-l| and prepare it for presentation in the same file!

The rest of the article is just an example of our script's power.

%}}}

%{{{ solution example usage
<a name="monoids"/>
\section{Example Usage :: What does it mean: `lists with concatenation form a monoid'?}

To understand the phrase we need to understand what lists are,
what concatenation is, and what a monoid is. A monoid is a set
with an operation such that certain equations hold --wait to talk of
equations, we need to know what equality means. Let's begin there!

   %{{{ Equality is an `easy one'
\subsection{Equality is an `easy one'}

It's a type |x ≡ y| for any |x , y| of the same type,
\begin{code}
data _≡_ {T : Set} : (x y : T) → Set where
\end{code}
such that any element |x| is equal to itself
\begin{code}
  ≡-refl : ∀{x} → x ≡ x
\end{code}
and there are no other ways to construct equality proofs besides this.

Let's take equality to be an infix operation with a relatively low binding,
\begin{code}
infix 5 _≡_
\end{code}

An important property is `substitution of equals for equals', or Leibniz Principle,
\begin{code}
≡-cong : ∀{A B} {x y : A} (f : A → B) → x ≡ y → f x ≡ f y
≡-cong f ≡-refl = ≡-refl
\end{code}
What this says is that given |p : x ≡ y| we must have |p| is |≡-refl|
since that is the only way to construct an equality proof. But if |p| is
|≡-refl| the necessarily |x| and |y| are the same item --`definitionally equal'.
But then |f x| and |f y| are also definitionally equal and so |≡-refl| is a valid
proof and we are done.

We want to avoid such paragraphs of English expaining a single line of code.
So we introduce combinators for reflexitivity and transitivity of equality:
\begin{code}
infix 8 _∎
_∎ : ∀{A} (x : A) → x ≡ x
x ∎ = ≡-refl

infixr 5 _≡⟨_⟩_
_≡⟨_⟩_ : ∀{A} (x : A) {y z : A} → x ≡ y → y ≡ z → x ≡ z
y ≡⟨ ≡-refl ⟩ ≡-refl = ≡-refl
\end{code}
Why the funky-looking names? Keep reading ;)

Before you move on, test your comprehension by proving a restatement of Leibniz Principle:
those that share all properties are necessarily identical.

|exercise : ∀{A} {x y : A} → (given : ∀ {P : A → Set} → P x → P y) → x ≡ y|
% solution: |given {P = λ e → x ≡ e} ≡-refl|.
%}}}

   %{{{ Monoids are one-sorted structures
\subsection{Monoids are one-sorted structures}

The suffix -oid is usually taken to mean sorted or typed,
as in `ringoid' and `groupoid', and the prefix mono- means one.
So monoid means, more-or-less, one-typed
---the generalization is called a category ;)

The notion of a type arises from the observation that all functions
|A → A| with function-composition and the identity function form a
monoid.

Enough shenanigans! What is a monoid? It is a type
\savecolumns
\begin{code}
record Monoid : Set₁ where
 field
\end{code}

that consists of a set
\begin{code}
  𝒮 : Set  -- slash-McS is the agda-input sequence in emacs
  -- looks really nice in the PDF ;) check it out!
\end{code}

with a particular element
\begin{code}
  0₊ : 𝒮
\end{code}

and a binary operation on that set
\begin{code}
  _⊕_ : 𝒮 → 𝒮 → 𝒮
\end{code}

such that the operation is associative
\begin{code}
  ⊕-assoc : ∀{x y z} → (x ⊕ y) ⊕ z ≡ x ⊕ (y ⊕ z)
\end{code}

and it has |0₊| as a unit
\begin{code}
  rightId  : ∀{x} → x  ⊕ 0₊ ≡ x
  leftId : ∀{y} → 0₊ ⊕  y ≡ y
\end{code}
%}}}

   %{{{ We can list it!
\subsection{We can list it!}

A `list of type A' is a type
\begin{code}
data List (A : Set) : Set where
\end{code}
that has an `empty list'
\begin{code}
  [] : List A
\end{code}
and every non-empty list can be expressed as
an element of |A| followed by a list of type |A|:
\begin{code}
  _∷_ : A → List A → List A  -- slash-::
\end{code}

Given any two lists, we can stick them `side by side',
or `concatenate' them, to produce another list:
\begin{code}
infix 10 _++_

_++_ : ∀{A} → List A → List A → List A
[] ++ ys = ys
(x ∷ xs) ++ ys = x ∷ (xs ++ ys)
\end{code}

As will be immediately clear, let us make a synonym,
\begin{code}
definition : ∀{A} {x : A} → x ≡ x
definition = ≡-refl
\end{code}

Now we prove associativity of concatenation by casing on the two possible forms
of the argument |xs|. We case on this argument since it is the left argument of |++|
which is defined inductively on its left-argument.
\begin{code}
++-assoc : ∀{A} {xs ys zs : List A} →  (xs ++ ys) ++ zs ≡ xs ++ (ys ++ zs)
++-assoc {xs = []} {ys} {zs} =
   ([] ++ ys) ++ zs
  ≡⟨ definition ⟩    -- i.e., first clause of definition of ++
    ys ++ zs
  ≡⟨ definition ⟩ 
   [] ++ (ys ++ zs)
  ∎
++-assoc {xs = x ∷ xs'} {ys} {zs} =
    ((x ∷ xs') ++ ys) ++ zs
  ≡⟨ definition {- of ++, second clause -} ⟩
    (x ∷ (xs' ++ ys)) ++ zs
  ≡⟨ definition {- of ++, first clause -} ⟩
    x ∷ ( (xs' ++ ys) ++ zs)
  ≡⟨ (let inductive-step : (xs' ++ ys) ++ zs ≡ xs' ++ (ys ++ zs)
          inductive-step = ++-assoc {xs = xs'}{ys = ys}{zs = zs}
      in ≡-cong (λ l → x ∷ l) inductive-step) ⟩
    x ∷ (xs' ++ (ys ++ zs))
  ≡⟨ definition {- of ++, first clause -} ⟩ 
    (x ∷ xs') ++ (ys ++ zs)
  ∎
\end{code}
Notice that we began with |xs| being |x ∷ xs'| then at the inductive step we used the
structurally smaller list |xs'|.

Notice how we have a `calculational', or equational, proof and such style is not even
primitively supported in the language! After all, you witnessed the definitions of the
combinators above!

Of-course, since all we did was mostly chase definitions, we can rewrite the lengthy
proofs more succinctly as
\begin{code}
++-assoc' : ∀{A} {xs ys zs : List A} →  (xs ++ ys) ++ zs ≡ xs ++ (ys ++ zs)
++-assoc' {xs = []} = ≡-refl
++-assoc' {xs = x ∷ xs'} = ≡-cong (λ l → x ∷ l) (++-assoc {xs = xs'})
\end{code}
Which you choose depends on your level of comfort ---both with the language and with
the theory.

The reader would do well to show that |[]| is a unit of |_++_| and so we have a monoid:
\begin{code}
freemon : ∀{A : Set} → Monoid
freemon {A} =
  record
    { 𝒮 = List A
    ; 0₊ = []
    ; _⊕_ = _++_
    ; ⊕-assoc = λ {x y z} → ++-assoc {A} {x} {y} {z}
    ; rightId = {! exercise!}
    ; leftId = {! exercise!}
    }
\end{code}



Since this article is in-fact a
\href{https://bitbucket.org/alhassy/blog/src/ac801720f0f6ca639cdcf2eb8506db6a7d30abb5/literate/blogliterately.lagda?at=master&fileviewer=file-view-default}{literate document},
the reader need only load it into their Emacs and begin having fun.

Enjoy!

%}}}

%}}}

% Local Variables:
% folded-file: t
% eval: (fold-set-marks "%{{{ " "%}}}")
% eval: (fold-whole-buffer)
% fold-internal-margins: 0
% end:

\end{document}
