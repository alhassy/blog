\documentclass{article}

%{{{ header
\usepackage[hmargin=20mm,top=12mm,bottom=20mm]{geometry}

% for latex support for symbols like 𝟘, 𝟚, 𝟛 etc
\usepackage{bbold}
% letters, numbers, and common puncutation can be made double struck
% http://ftp.math.purdue.edu/mirrors/ctan.org/fonts/bbold/bbold.pdf

% unicode stuff
% \usepackage[combine]{ucs}

% NOM=PathCat ; NAME="$NOM"Coloured ; sed "s/$NOM/$NAME/g" $NOM.lagda > $NAME.lagda ; agda --latex $NAME.lagda -i /media/musa/Data/agda-stdlib-0.9/src -i . > $NAME.tex ; cd latex ; pdflatex -shell-escape $NAME.tex ; cd .. 
%
%% stuff for agda latex backend
% \usepackage[utf8x]{inputenc}
% \usepackage{agda}
% \usepackage{/media/musa/Data/RATH-Agda/trunk/RathAgdaChars}
% \usepackage{mathrsfs}
% \usepackage{amsfonts}
% \usepackage{verbatim} %% seems to cause trouble when used in conjungation with comment-blocks...
% \newenvironment{spec}{\verbatim}{\endverbatim}
% \DeclareUnicodeCharacter{8718}{\ensuremath{BOX}} % U+220E box
  % END OF PROOF
% \DeclareUnicodeCharacter{8845}{\ensuremath{\cdots}} % 
  % Unicode character 8845 = U+228D:
  % MULTISET MULTIPLICATION
% \def\textSigma{\ensuremath{\Sigma}}
% \def\textbackslashMcG{GGG}
% \def\triv{1}
% \DeclareUnicodeCharacter{8759}{\ensuremath{++}} % U+2237 PROPORTION
% \DeclareUnicodeCharacter{7490}{\ensuremath{^W}} % U+1D42 MODIFIER LETTER CAPITAL W
% \DeclareUnicodeCharacter{7476}{\ensuremath{^H}} % U+1D34 MODIFIER LETTER CAPITAL H
% \DeclareUnicodeCharacter{7484}{\ensuremath{^O}} % U+1D3C MODIFIER LETTER CAPITAL O
% \DeclareUnicodeCharacter{7481}{\ensuremath{^M}} % U+1D39 MODIFIER LETTER CAPITAL M
% \DeclareUnicodeCharacter{120002}{\ensuremath{M}} % U+1D4C2 MATHEMATICAL SCRIPT SMALL M
% \DeclareUnicodeCharacter{8344}{\ensuremath{_m}} % U+2098: LATIN SUBSCRIPT SMALL LETTER M
% \renewenvironment{comment}{}{}

\usepackage{multicol}
\usepackage[round]{natbib} % replaces {harvard}
\usepackage{etex}

%% comment out when using agda latex backend
 \usepackage{comment}
 % apparently cannot have nested comment environements
% add to my script in the appropriate place
%   # throw away everything between \begin{comment}...\end{comment}
%   sed '/\\begin{comment}/,/\\end{comment}/d' -i $NAME.markdown


% for using colours on math symbols
\usepackage{minted}

\usepackage{hyperref}

%%%%%% for lhs2TeX
%include agda.fmt
%include /media/musa/Data/RATH-Agda/trunk/RathAgdaChars.sty
%format ++ = "++"
%%%% %format ⟶ = "{\color{red} ⟶}"

%%% hints in calculational proofs are coloured red
%%% hence can read proof by ignoring red or taking red into consideration.
%format ≡⟨ = "≡\begingroup\color{red}⟨"
%format ⟩  = "⟩\endgroup"
%%
%% the only time {-...-} comments are used in this literate document are
%% for english phrases for calculational hints and so we make them invisble
%% to avoid extra bracketing
\def\commentbegin{}
\def\commentend{}

%format 𝟙 = "\mathbb{1}"
%format ^ = "\;\widehat{}\;"

%format ⟶̇ = "\overset{.}{⟶}"
%format ≤̇ = "\overset{.}{≤}"

%format 𝒢⟶ = "𝒢 \hspace{ -0.8em } ⟶"
%format →  = "\hspace{0.5em} \to \hspace{0.5em}"
%%%%%%
\def\fcmp{\fatsemi}

\DeclareUnicodeCharacter{119990}{\ensuremath{a}} % U+1D4B6 curly a
% MATHEMATICAL SCRIPT SMALL A

\DeclareUnicodeCharacter{7491}{\ensuremath{a}}
% Unicode character 7491 = U+1D43:
% MODIFIER LETTER SMALL A

\DeclareUnicodeCharacter{7495}{\ensuremath{b}}
% Unicode character 7495 = U+1D47:
% MODIFIER LETTER SMALL B


\DeclareUnicodeCharacter{120009}{\ensuremath{t}} % U+1D4C9 curly t
% MATHEMATICAL SCRIPT SMALL T

\DeclareUnicodeCharacter{120007}{\ensuremath{r}} % U+1D4C7 curly r
% MATHEMATICAL SCRIPT SMALL R

\DeclareUnicodeCharacter{7506}{\ensuremath{^o}} % U+1D52 superscript o
% MODIFIER LETTER SMALL O

\DeclareUnicodeCharacter{7510}{\ensuremath{^p}} % U+1D56 superscript p
% MODIFIER LETTER SMALL P

\DeclareUnicodeCharacter{120005}{\ensuremath{p}} % U+1D4C5 curly p
% MATHEMATICAL SCRIPT SMALL P

\DeclareUnicodeCharacter{119997}{\ensuremath{h}} % U+1D4BD curly h
% MATHEMATICAL SCRIPT SMALL H

\DeclareUnicodeCharacter{8342}{\ensuremath{_k}} % U+2096 small k
% LATIN SUBSCRIPT SMALL LETTER K

\DeclareUnicodeCharacter{8339}{\ensuremath{_x}} % U+2093 small x
% LATIN SUBSCRIPT SMALL LETTER X

\DeclareUnicodeCharacter{7525}{\ensuremath{_v}}
% Unicode character 7525 = U+1D65:
% LATIN SUBSCRIPT SMALL LETTER V


\DeclareUnicodeCharacter{8345}{\ensuremath{_n}} % U+2099 small n
% LATIN SUBSCRIPT SMALL LETTER N

\DeclareUnicodeCharacter{8348}{\ensuremath{_t}} % U+209C small t
% LATIN SUBSCRIPT SMALL LETTER T


\DeclareUnicodeCharacter{120795}{\ensuremath{ \mathbb{3}}}
% Unicode character 120795 = U+1D7DB:
% MATHEMATICAL DOUBLE-STRUCK DIGIT THREE


\DeclareUnicodeCharacter{120794}{\ensuremath{ \mathbb{2} }}
% Unicode character 120794 = U+1D7DA:
% MATHEMATICAL DOUBLE-STRUCK DIGIT TWO

\DeclareUnicodeCharacter{120792}{\ensuremath{ \mathbb{0}}}
% Unicode character 120792 = U+1D7D8:
% MATHEMATICAL DOUBLE-STRUCK DIGIT ZERO


\DeclareUnicodeCharacter{120001}{\ensuremath{\mathcal{L}}} % U+1D4C1 small l
% MATHEMATICAL SCRIPT SMALL L

\DeclareUnicodeCharacter{119998}{\ensuremath{\mathcal{I}}} % U+1D4BE small i
% MATHEMATICAL SCRIPT SMALL I

\DeclareUnicodeCharacter{120008}{\ensuremath{\mathcal{S}}} % U+1D4C8 small s
% MATHEMATICAL SCRIPT SMALL S

\usepackage{relsize}
\DeclareUnicodeCharacter{9670}{\raisebox{2pt}{\ensuremath{\mathsmaller{\mathsmaller{\blacklozenge}}}}} % U+25C6
% BLACK DIAMOND


\begin{document}

 \centerline{\sc \large Notes on Dependently-typed programming with Agda}
%% \vspace{.5pc}
%% \centerline{\sc Musa Alhassy}
%% \centerline{\it (rough draft)}
%% \vspace{2pc}
%% These rough notes are what I am learning about X
%% \tableofcontents
%}}}

%% generate html using ::
%% agda --html FreeGraph.lagda -i /media/musa/Data/agda-stdlib-0.9/src/ -i ~/Desktop/

%% coloured literate html ::
%% NAME=FreeGraph ; sed 's/\\begin{code}/```agda/g' $NAME.lagda > $NAME.pandoc ; sed -i 's/\\end{code}/```/g' $NAME.pandoc ; pandoc -o $NAME.html $NAME.pandoc -Ss ; rm $NAME.pandoc

%%% READ ME
%%
%% generate PDF: NAME=PathCat ; lhs2TeX --agda $NAME.lagda > $NAME.tex && pdflatex -shell-escape $NAME.tex
%%
%% generate markdown for use with Hakyll: NAME=blogliterately; fromliterate $NAME
%%%

---
title: Graphs are to categories as lists are to monoids
date: March 27, 2016
tags: introductory
---

% tags: graphs, cats, adjunction

% ignore | in |...|
%
% NOM=PathCat ; NAME=Coloured2 ; sed 's/|//g' $NOM.lagda > $NAME.lagda ; sed 's/PathCat/Coloured2/g' -i $NAME.lagda; agda --latex $NAME.lagda -i /media/musa/Data/agda-stdlib-0.9/src -i . > $NAME.tex; cd latex ; pdflatex -shell-escape $NAME.tex ; cd .. 



% currently works
%
% replace ⟶̇ with NATARROW
% NOM=PathCat ; NAME=Coloured ; sed 's/⟶̇/NATARROW/g' $NOM.lagda > $NAME.lagda ; sed 's/PathCat/Coloured/g' -i $NAME.lagda; agda --latex $NAME.lagda -i /media/musa/Data/agda-stdlib-0.9/src -i . > $NAME.tex; cd latex ; pdflatex -shell-escape $NAME.tex ; cd .. 


% replace |...| with \verb|...|
%
% NOM=Coloured ; NAME=Coloured2 ; sed 's/|\(.*.\)|/\\verb|\1|/g' $NOM.lagda > $NAME.lagda ; sed 's/Coloured/Coloured2/g' -i $NAME.lagda; agda --latex $NAME.lagda -i /media/musa/Data/agda-stdlib-0.9/src -i . > $NAME.tex; cd latex ; pdflatex -shell-escape $NAME.tex ; cd .. 


%%
%% clean out directory from agdai and latex noise
%%
%% rm *.out ; rm *.aux ; rm *.log ; rm *.ptb ; rm *.pyg ; rm *.tex ; rm *.toc ; rm *.agdai
%%

The solutions to all {\color{red} |{! exercises !}|} are in the \href{https://bitbucket.org/alhassy/blog/overview}{literate source}
---the HTML is quite messy and no attempt at all has been made to clean it up;
the previous link also directs to a PDF version of this article.
Moreover, this article has just been put on pause. My interest has now shifted to the science of programming and so I am now writing articles on that. In retrospect, this article is quite long and more of a pamphlet than a blog post! I would do well to avoid this approach in future posts.


%{{{ to do's
{\color{red} there are roughly 40 To Do's ...}
\begin{comment}
\begin{enumerate}

\item

perhaps discuss the universal property of |∂|
::
in cat thry, new objects are defined via universal properties and for this case we have:
for any cat C, d gives us a best/universal cofunctor |F : C → dC|: every cofunctor |C → D|
factors uniquely (via a functor) through F.

this formalises the previous statement of reducing cofunctors to usual functors.

---

A sort-of hybrid of the previous two can be found in the exhilarating field of `allegories'.

Just as categories abstract functions, allegories abstract relations. They are just categories
with a notion of converse |-˘| satisfying the properties of |ah-yeah| and the morphisms are
further endowed with a meet semi-lattice structure for which converse and composition are monotonic.
The de-facto book on allegories is \url{INSERT LINK HERE}; lovingly known as `Cats and Alligators'.

(A main application of allegories is to the \href{http://www.csie.ntu.edu.tw/~b94087/aopa-intro.pdf}{algebra of programming})





\item
to read

adjoint functor theorem \url{http://wwwf.imperial.ac.uk/~buzzard/maths/research/notes/the_adjoint_functor_theorem.pdf}

underlying category \url{https://unapologetic.wordpress.com/2007/08/20/the-underlying-category/}

left adjoint \url{http://www.comicbooklibrary.org/articles/Left_adjoint}

Why forgetful functors usually have LEFT adjoint?
\url{http://mathoverflow.net/questions/6376/why-forgetful-functors-usually-have-left-adjoint}


Where are mathematics jobs advertised if not on mathjobs (e.g. in Europe and elsewhere)?
\url{http://mathoverflow.net/questions/952/where-are-mathematics-jobs-advertised-if-not-on-mathjobs-e-g-in-europe-and-els}


YONEDA

-------

\url{http://bartoszmilewski.com/2013/05/15/understanding-yoneda/}

\url{https://soffer801.wordpress.com/2012/02/22/applying-yonedas-lemma/}

\url{https://ncatlab.org/nlab/show/Yoneda+lemma}

\url{http://blog.sigfpe.com/2006/11/yoneda-lemma.html}

\url{https://www.schoolofhaskell.com/user/bartosz/understanding-yoneda#natural-transformations}

\url{http://bartoszmilewski.com/2015/09/01/the-yoneda-lemma/}

\url{https://soffer801.wordpress.com/2012/02/13/yonedas-lemma-part-1/}


NATURAL TRANSFORMATIONS

------------------------

\url{http://bartoszmilewski.com/2014/07/15/natural-transformations-and-ends/}

\url{https://ncatlab.org/nlab/show/category+of+presheaves}

\url{https://apocalisp.wordpress.com/2010/07/02/higher-rank-polymorphism-in-scala/}

\url{https://wiki.haskell.org/Polymorphism}

Constrained polymorphic functions as natural transformations
\url{https://mail.haskell.org/pipermail/beginners/2013-November/012917.html}

\url{https://lukepalmer.wordpress.com/2008/04/28/whats-a-natural-transformation/}

Natural transformations 2 (catsters)
\url{https://www.youtube.com/watch?v=XnrqHd39Cl0}

\url{https://soffer801.wordpress.com/2012/02/08/naturality-part-2/}

\url{https://soffer801.wordpress.com/2012/02/10/examples-of-natural-transformations-part-2/}

\url{https://drexel28.wordpress.com/2011/12/28/natural-transformations-pt-i/}

\url{https://wiki.haskell.org/Category_theory/Natural_transformation}

Why do natural transformations express the fact that a vector space is canonically embedded in its double-dual but not in its dual?
\url{http://math.stackexchange.com/questions/1617880/why-do-natural-transformations-express-the-fact-that-a-vector-space-is-canonical}

\url{http://bartoszmilewski.com/2015/04/07/natural-transformations/}

\url{http://bartoszmilewski.com/2014/09/29/how-to-get-enriched-over-magmas-and-monoids/}


Freedom and Forgetfulness
\url{http://blog.higher-order.com/}



\url{https://jsmyth.wordpress.com/2006/04/26/the-principle-of-the-identity-of-indiscernibles/}



ENDS

-----

THIS IS THE (CO)END, MY ONLY (CO)FRIEND
\url{http://arxiv.org/pdf/1501.02503.pdf}


SIMPLICAL SETS

---------------

A LEISURELY INTRODUCTION TO SIMPLICIAL SETS
\url{http://www.math.jhu.edu/~eriehl/ssets.pdf}


hompotopy co/limits
\url{http://www.math.jhu.edu/~eriehl/hocolimits.pdf}

The Nucleus of a Profunctor: Some Categorified Linear Algebra
\url{https://golem.ph.utexas.edu/category/2013/08/the_nucleus_of_a_profunctor_so.html}

\item
the approach taken here is to motivate cats from posets and the main
tools used there are indirect reasoning (yoneda) and galois connections (adjunctions).
As such, ``everything is an adjunction'' is a more apt slogan for us :-)

\item
when discussing enrichment

V-categories with |𝒱 = {true, false}| give preorders?

V-categories with |𝒱 = [0, ∞]| give metric-like things?

V-categories with |𝒱 = 𝒮e𝓉| give usual categories.

\item

The Universal Property of Categories
or
Enriched categories as a free cocompletion
\url{https://golem.ph.utexas.edu/category/2013/01/the_universal_property_of_cate.html}

\item
Project Scheduling and Copresheaves (an example of monoidal cats solving a cs problem)
\url{https://golem.ph.utexas.edu/category/2013/03/project_planning_parallel_proc.html}

\item
example of a cat.

Let Mat be the category whose objects are natural numbers and whose morphisms |m ⟶ n| are
|m × n| matrices (over an ambient field). This category is equivalent to the category of
finite-dimentional vector spaces (over the same field).

Perhaps mention mat in the examples of cats
and Perhaps mention the equivalence when mentioning vector spaces.


another example.

Count nouns, mass nouns and their
transformations: a unified category-theoretic
semantics
\url{https://marieetgonzalo.files.wordpress.com/1999/06/mnmac1.pdf}

adjunction between a category CN CN of count nouns and a category MN MN of mass nouns:
• \bullet The morphisms in these categories are relationships like “a man is a human,” resp. “water is liquid.”
• \bullet The left adjoint is pluralization: if “a dog” is a count noun, then “dogs” is a mass noun.
• \bullet The right adjoint is less familiar grammatically, but for example, if “water” is a mass noun, then “a body of water” is a count noun.

this is taken from here:
\url{https://golem.ph.utexas.edu/category/2013/03/spivak_on_category_theory.html}
where it seems that CN and MN are related to Spivak's Oolgs!


\item

see the links therein about weighted/indexed limits
\url{https://golem.ph.utexas.edu/category/2008/03/limits_and_pushforward.html}

weighted limits
\url{http://mathoverflow.net/a/37310/42716}


\item
when `motivating' functor categories, add

Transferring properties from a category D to the functor category [C,D]
\url{http://math.stackexchange.com/questions/270868/transferring-properties-from-a-category-mathcald-to-the-functor-category?rq=1}


\item

NatTrans Iso Extensionality

Natural transformations can be thought of as 2-dimensional morphisms since they are morphisms
between morphisms (functors) and as such there are so-called
\href{http://www.cs.ox.ac.uk/ralf.hinze/Kan.pdf}{string diagrams} that makes it easier to work
with them.

(NatTrans Iso Extensionality)
Let |η : F ⟶ G|, then |η ∈ F ≅ G ⇔ (∀ x • ηₓ ∈ F x ≅ G x)|

where |∀ i ∶ x ⟶ y • i ∈ (x ≅ y) ≡ (Σ j ∶ y ⟶ x • i ⨾ j ≡ id ∧ j ⨾ i ≡ id)|

ie |i ∈ x ≅ y ≡ Σ iso : x ≅ y • to iso ≡ i|

%{{{ comment
begin{comment}
a nat.trans. is an iso iff its components are isos.

Given |η : (∀ {X} → F X ⟶ G X)| with |∀ f → F₁ f ⨾ η {tgt f} ≡ η {src f} ⨾ G₁ f|.

|⇒|. Given nat.trans. |γ : ∀ {X} → G X → F X| with |γ ⨾ η = η ⨾ γ = Id|, we show
|∀ X • η {X} : F X ≅ G X| since it has an inverse |γ {X}| and indeed
|Id = (γ ⨾ η) {X} = γ {X} ⨾ η {X}| by assumption :-)

|⇐|. Given |∀ x • ∃ γ {x} • η {x} ⨾ γ {x} = γ {x} ⨾ η {x} = Id|
we let |γ = λ x → γ {x}| and show that it is a nat trans:
given |f|, |G₁ f ⨾ γ {tgt f} = γ {src f} ⨾ F₁ f|
::
\begin{spec}
  G₁ f ⨾ γ {tgt f}
≡⟨  η {x} ⨾ γ {x} = γ {x} ⨾ η {x} = Id ⟩
  γ {src f} ⨾ η {src f} ⨾ G₁ f ⨾ γ {tgt f}
≡⟨ η nat trans: F₁ f ⨾ η {tgt f} ≡ η {src f} ⨾ G₁ f ⟩
  γ {src f} ⨾ F₁ f ⨾ η {tgt f} ⨾ γ {tgt f}
≡⟨  η {x} ⨾ γ {x} = γ {x} ⨾ η {x} = Id ⟩
  γ {src f} ⨾ F₁ f
\end{spec}
Yay.
Moreover, this |γ| is an inverse for |η|:
|∀ X • (γ ⨾ η) {X} = γ {X} ⨾ η {X} = Id| :-)

end{comment}
%}}}

{\sc discuss what it means (intuitively?) for two functors to be (naturally) isomorphic}

\item

example of natural transformation between endofunctors of monoids.

Recall that a monoid can be construed as a one-object category whose morphisms are the monoid's
elements and composition is the monoid operation. Then an endofunctor is nothing more than a
monoid homomorphism from and to the same monoid. Recall that a natural transformation assigns a
morphism to each object in the category, but a monoid is a one-object category and so a natural
transformation on endofunctors is nothing more than a morphism in this category, i.e., a monoid
element. Thus a natural transformation of endofunctors |F, G| is nothing more than a monoid element
|m| with |∀ x • F x ⊕ m = m ⊕ G x|. In the case |F = G = Id|, natural transformations |Id ⟶ Id|
are just |𝒵 (M, ⊕, e) ≔ Σ m ∶ M • ∀ x : M • x ⊕ m = m ⊕ x|, also known as the
``\href{https://ncatlab.org/nlab/show/center}{center} of the
monoid'': the sub-monoid that is symmetric. Notice that it is never empty since |e| inhabits it
by definition of being a unit and so may be considered ``canonical''.
Moreover, the other elements of |𝒵| are natural but not canonical, and |e| is canonical for
endofunctors but not even a canadiate, let alone natural, for distinct functors.
This is a situation where the usage of ``natural'' and ``canonical'' differ,
but of course our setting is
particular categories ---those with one object--- and not arbitrary categories.
\\
see \url{https://qchu.wordpress.com/2012/02/06/centers-2-categories-and-the-eckmann-hilton-argument/}

If x and m commute, i.e., |x ⊕ m = m ⊕ x| then so do their images:
\begin{spec}
 f x ⊕ f m
= f (x ⊕ m)
= f (m ⊕ x)
= f m ⊕ f x

homomorphisms preserve commutativity so why isnt the center construction functorial? hmm :-(
\end{spec}

\item

in the section on functor conventions

consider discussing what prefixes mean.

also, what of prefixes "homo, iso, homeo"!

\item
in the section on opposite construction

discuss how the opposite of (finite) sets is the category of complete atomic boolean algebras (cab) ;)

\item
in section on examples of nat trans

also discuss |return, join, mzero, mplus| and other goodies from haskell ;)

\item
after the HOM construction

It is common that one of the Hom functor's arguments is fixed and the resulting
functors are known as the *(contravaraint and covaraint) yoneda functor(s)*.
What we really mean is that we can `curry' the |Hom| functor, since
it can be shown that |𝒳 ⟶ (𝒴 ^ 𝒵) ≅ (𝒳 × 𝒴 ⟶ 𝒵)| where |^ ≔ Func|, to obtain:
a functor |𝒞 ⟶ (𝒞 ᵒᵖ) ^ 𝒮e𝓉|.

%% I **vaguely** recall a notation is to write the arguments superscript of the fixed argument.
%% That is, |_ᵍ  ≔ (Id {A} ⟶ g)| and |ᶠ_ ≔ (f ⟶ Id {A})|.
%% This poses some difficulty in Agda, so we use |𝒴| and |𝒴˘| for the covaraint and
%% contravaraint versions of Yoneda.


This functor can be shown to be an `embedding' and so known as `the yoneda embedding',
and this informs us that a category is ``represetned'' by set-valued functors.
Thus the study of a category can be delegated to the study of functors.

SEE AUX.LAGDA regarding |yonedalemma|


DISCUSS WHY THE NAME YONEDA.


\item
add in the section on nat.trans. as functor categories.

%%
%% n-transfor(mation)s are functors C x G^n --> D for G^n is the n-category free on the cellular n-globe. Urs Schreiber
%%
%% "Transfor" is due to Sjoerd Crans and is a portmanteau of "transformation" and "functor." One can think of a k-transfor as like a degree-k map of chain complexes: it takes each 0-cell to a k-cell, each 1-cell to a (k+1)-cell, etc. -- this unifies functors with higher transfors. – Mike Shulman

\item
add when discussing limits

{\bf category of cones}
The constant map |K : ∀ {X Y} → X → Y → X,  K x y = x| can be made into a functor
|𝒦 : 𝒞 ⟶ 𝒞 ^ 𝒟| by sending an object |C| to ``a constant functor'':
the functor that on objects is
|K C : Obj 𝒞 → Obj 𝒟| and on morphisms is |K (Id {C}) : ∀ {x y} → x ⟶ y → K x ⟶ K y|,
and by sending morphisms |f : A ⟶ B| to natural transformations |K f : {C : Obj 𝒞} → A ⟶ B|.

also can then use this when discussing adjunctions later on

Observe that a natural transformation |η : 𝒦 C ⟶ F| boils down to a transformation
|η : ∀ {X} → C ⟶ F X| with triangle-equalities
|f : A ⟶ B ⇒  η {A} ⨾ F f = 𝒦 C f ⨾ η {B} = η {B}|.

Now we set
|Cones G ≔ Σ C ∶ Obj 𝒞 • NatTrans (𝒦 C) G|. The objects are `cones to |G| with apex |C|',
and cone morphisms are |𝒞|-maps satisfying certain triangle-equalities:
|(C , η) ⟶ (C' , η') ≔ Σ θ ∶ C ⟶ C' • ∀ x • ηₓ = θ ⨾ η'ₓ|.

(apparently cannot have nested comment environements)
begin{comment}
\begin{spec}
   (C, η) ⟶ (C', η') in Cones G
=  (C ⟶ C' in 𝒞) and (η ⟶ η' in (Func 𝒥 𝒞)^𝟚 )
Now,
  η ⟶ η' in (Func 𝒥 𝒞)^𝟚
={ arrows in arrow categories }
  Σ θ ∶ 𝒦 C ⟶ 𝒦 C' • Σ θ' : G ⟶ G • η ⨾ θ' = θ ⨾ η'
={ composition of nat-tran's }
  … • ∀ x • ηₓ ⨾ θ'ₓ = θₓ ⨾ ηₓ'
={ (𝒦 C ⟶ 𝒦 C') ≅ (C ⟶ C'), so |θ| is a single |𝒞|-map }
  … • ηₓ ⨾ θ'ₓ = θ ⨾ ηₓ' -- no dependece on x
={ }
  … • ηₓ ⨾ θ'ₓ = θ ⨾ ηₓ'
\end{spec}
natural choice for θ' = Id {G}. See section on co-slice category for how to continue.
end{comment}

A `limit' for |G| is a terminal object of |Cones G|.


Remember that functors take the underlying graph and deform it to fit on the target category's
underlying graph so that all edge connections are preserved, and the result is a `picture' of the
source in the target.
Then a limit can be construed as the closest/shortest pyramid/cone over the picture, when we
view our category's underylying graph in 3-dimensions.




\item
add in section on FUNC

show that |P 𝒞 ⇒ ∀ 𝒜 • P(Func 𝒜 𝒞)|, e.g. if |P 𝒞 ≔ 𝒞 has one object = 𝒞 monoid|
then the `function space' |Func 𝒜 𝒞| is also a monoid. I'm conjecturing this, but it may be false.
If so, then [to do] find a counter example.

\item
add in section on nat trans examples

discuss in general what intution there is for natural transformations |F × F ⟶ F|
when |F| is a set-valued functor and when the target of F has products.

\item
many categories may be built by taking obects to be sets with
extra structure and morphisms to be the set-functions that preserve this extra structure.
Then it is natural to consider the so-called forgetful-functors that given such an object yield
the underlying set and given a structure-preserving map it returns only the raw set-function.

\item
consider placing in naturality section

A natural transformation can be seen as a polymorphic operation that does not make use of specfic
facts about its arguments. [see sigfpe for more on this]

In formal languages, one says that an expression, or term, is of the form
|c , v , f(t₁, …, tₙ)|; that is, a constant, a varaible, or a function application of appropriate
arity to other terms.
Then one defines basic textual substitution |_[_ ≔ _] : Term → Variable → Term → Term|
by |c [x ≔ t] = c , v [x ≔ e] = If v ≈ x Then e Else v,
(f(t₁, …, tₙ))[x ≔ e] = f(t₁[x ≔ e], …, tₙ[x ≔ e])|.

Observe, if we make a substitution over an expression |a| then apply a function to the result
it's as if we applied the function then did the substitution.
|f( a [x ≔ e] ) = (f a) [x ≔ e]|

That is, with respect to textual substitution,
*all functions are natural transformations*.
Generally, this is obtained by using |𝒦|.

Compare this with: between discrete category, all functions are functors!


What does it mean to say |h : ∀ {X} → F X → G X| does not make use of specfic facts about its
arguments? Well if we replace the argument by another then we expect the results to be the same.
Given |f : A → B| then if we replace an argument

|h(F r x) = G r (h x)|


Given any expression |e|, replacing all occurences of |x| by |r x| then applying |h| is the same as
applying |h| to the expression then performing the subtitution.

h makes no use of specific facts about its arguments

==

evaluation after a substitution
is the same as substitution after evaluation.

\item
consider placing in naturality section

notice that |η : F ≅ G| determines one functor in terms of the other:
given |f : a → b| we obtain |G f| by going backwards along the equivalence from |G a| to |F a|
then perform |F f| then go forwards along the equivlence from |F b| to |F a|
---a lot like Agda |≡-subst|---:
|G f = η⁻¹ {a} ⨾ F f ⨾ η {b}|

discuss wheather |≡-subst| is natural transformation. why or why not.



\item
"The Yoneda lemma takes a category and gives you back a category of functors. That is, each object A in the original category is assigned to the functor Hom(A, -) and for each morphism |f : A → B|, you get Hom(f, -).
The Yoneda lemma acts like a completion, much like the algebraic closure of a field or compactification of a topological space. Even if your category is anemic, the Yoneda embedding will endow it will all kinds of useful properties. In particular, the resulting category will be have all limits and all colimits. It is also a topos (a "set-like" category)."

\item
is the subword order antisymmetric? I think so!
If so, consider mentioning the free po-monoid; otherwise the free preordered-monoid.

free order on a set is the set with strict equality; free monoid is lists;
so free pomonoid is just lists with subsequence order?


I'm a big fan of using the dependelty typing programming language Agda as a proof assistant for
doing mathematics since it allows me to express a specfication, a program implementation, and a
proof of correctness using the same formalism. All three, one place! No external documentation to
consult!

In whydp.lagda, add link
\href{http://plato.stanford.edu/entries/type-theory-intuitionistic/}{Intuitionistic Type Theory}.



\item
add to naturality section:


see: \url{http://math.stackexchange.com/questions/1703611/why-is-it-bad-to-pick-basis-for-a-vector-space}

If you find that to define a transformation you must make a choice (that associates to each
object some datum), then you may want to consider the category of obects paired with such choice-data
and morphisms to be the orginal morphisms between objects on the condition that they also now
respect the associated choice-datum.

In linear algebra, for example, if an arbitrary choice of `coordinates' makes a difference in a result, then such a result is not
nautral.
[

As Hermann Weyl wrote, "The introduction of a coordinate system to geometry is an act of violence".

\url{http://math.ucr.edu/home/baez/week247.html}

]


\item
\begin{spec}
• see p21 of backhouse for construction of evalaution functor

• a natrrans η : F ⟶ G

iff (claim)

is just a transformation η₀ : ∀ {x : Obj 𝒞} → F x ⟶ G x
that gives rise to a unique functor η : 𝒞 ⟶ F ↓ G ;
the naturality ensures the uniquness

iff (claim)

a functor η : 𝒞 → F ↓ G


\end{spec}


\item
The axioms for a functor are very similar to those of a monoid; phrasing is important. 

\item
after introducing opify, or when mentioning cat-equivalence, consider
showing that BACKEND ERROR %% |𝒞 ^ 𝒟 ≃ 𝒞 ᵒᵖ ^ 𝒟 ᵒᵖ|
;;
see \url{http://math.stackexchange.com/questions/1083670/functors-between-functor-categories}

\item
consider using agda-latex backend and in-place of lhs' formats, mimic them
say via \verb+\beginformats...\endformats+ which realises the commands between them as sed
comments. Moreov, creonsider making the commands of the form |l = r| which, using sed, gets
transformed into a sed-rewrite rule and is applied to the remainder of the document.

\item
hello world
\begin{spec}
How do we represent constained extrema categorically?
Let us calculate!

   ⊓ᵂ F = (⊓ x ∣ W x • F x)
⇔⟨ infimum characterisation ⟩ 
   ∀ c • c ≤ ⊓ᵂ F ⇔ ∀ x ∣ W x • c ≤ F x
⇔⟨ Z-notation ⟩
   ∀ c • c ≤ ⊓ᵂ F ⇔ ∀ x • W x ⇒ c ≤ F x
⇔⟨ ⟶ is just ≤ is poset categories ⟩
   ∀ c • (c ⟶ ⊓ᵂ F) ⇔ ∀ x • W x ⇒ (c ⟶ F x)
⇔⟨ pointwise ⇒ is just ⟶̇ in poset of truth values ⟩
   ∀ c • (c ⟶ ⊓ᵂ F) ⇔ W_ ⟶̇ (c ⟶ F_)
⇔⟨ ⇔ is just ≅ in poset categories ⟩
   ∀ c • (c ⟶ ⊓ᵂ F) ≅ W_ ⟶̇ (c ⟶ F_) , naturally in c

Thus, a weighted limit Limᵂ F is characterised by :: 
∀ c • (c ⟶ Limᵂ F) ≅ (W_ ⟶̇ (c ⟶ F_) naturally in c
::
for any functor F : 𝒞 ⟶ 𝒟 and weight W : 𝒞 ⟶ 𝒮e𝓉, (Use V if enriched category)

Just as ⊓ F = (⊓ x ∣ true • F x), limits are special weighted-limits.
Lim F ≅ Lim¹ F where 1 : 𝒞 ⟶ 𝒮e𝓉 is the terminal functor.

One-point rule (⊓ x ∣ x = y • F x) = F y has categorical analogue,
Limᴴᴼᴹ̂⁻ʸ F ≅ F y , for F : 𝒞 ⟶ 𝒮e𝓉 -in a discrete category x⟶y is just x=y.

Proof:
  x ⟶ Limᴴᴼᴹ̂⁻ʸ F
≅ Hom(_,y) ⟶̇ (x ⟶ F_)  lim char
≅ x ⟶ F y               yoneda

yoneda is just indirect equality, see below section on preorders


\end{spec}

\url{https://ncatlab.org/nlab/show/weighted+limit}

Ends.
\begin{spec}
The end of a functor F : 𝒞ᵒᵖ × 𝒞 ⟶ 𝒟 is denoted and defined by

∫ₓ F(x, x) ≔ Limᴴᴼᴹ F where Hom : 𝒞ᵒᵖ × 𝒞 ⟶ 𝒮e𝓉

In particular,
(F ⟶̇ G) ≅ ∫ₓ (F x ⟶ G x)

making the pointwise-intuition even greater.

Hom-dsitributivity over ends :: c ⟶ ∫ˣ F(x,x) ≅ ∫ₓ c ⟶ F(x,x)       (take coends to ends)
                                (∫ₓ F(x,x)) ⟶ c ≅ ∫ₓ (F(x,x) ⟶ c)   (take ends to ends)
::
this follows from the fact that (co)continous functors preserve ends, since ends are just limits.

\end{spec}

\item
recall from my csharp/monad notes about how functo F can be thought of as containder [x : x <- xs]..


with this view in mind, a naturaltransformation 1->F means that we have a wrap/singleton operation:
[x]

join operation is then union,

what is a general transformation F->G ;)


\item
in the section on functor cat examples,
in the slice exaample.

formalise the two functors (not the cats) and then realise the isomorphsim;
generalise results to comma categories |F ↓ G|.

in the same item is defined the notion of subcategory:
formalize this via a universal property ;)


---

in the section on: coslice of set are functor categories.

REPHRASE THat result AS A CATEGORY EQUIVALENCE THEOREM?


\item
kan extensions as ends
\begin{spec}
Given f : (P, ≤) ⟶ (Q, ≤) we ask if it has residuals
(1) ∀ g,h • f \ g ≤̇ h ⇔ g ≤̇ f ⨾ h
(2) ∀ g,h • h ⨾ f  ≤̇ g ⇔ h ≤̇ g / f

(( The left and right Kan exstenions of a functor F : 𝒞 ⟶ 𝒟 are defined to be the left and right
adjoints of the precomposition functor (F ⨾) : 𝒟 ^ 𝒜 ⟶ 𝒞 ^ 𝒜, and so necessairly satisfy the
above universal characterisations. We call one adjoint (/F) and the other (F\), then the chars
look like usual mutliplication rules, as in integer-division! Site the Art of String Driagrams,
or Dan teachs an old trick, whatever, some paper on kan extensions and string diagrams.
))

For (1),
   ∀ h • f \ g ≤̇ h ⇔ g ≤̇ f ⨾ h
⇔⟨ Recall GaloisConnections ::  ∀ y • L x ≤ y ≡ x ≤ U y ⇔  L x = ⊓ y ∣ x ≤ U y • y ⟩
  f ╲ g = ⊓ h ∣ g ≤̇ f ⨾ h • h
⇔⟨ ≤̇ is just ⟶̇ for posests ⟩
   f ╲ g = ⊓ h ∣ g ⟶ f ⨾ h • h
⇔⟨ weighted limits for posets ⟩
   f ╲ g = Limᵂ Id where W = (g ⟶ f ⨾ _)

Now,
tensor  _·_ : Set × 𝒞 ⟶ 𝒞 char :: (S · x ⟶ y) ≅ ( S ⟶ (x ⟶ y) )
it objectifies an S-family of x⟶y arrows.

Notice
  S · x ⟶ y
≅⟨ char ⟩
  S → (x ⟶ y)    -- function space in Set
≅⟨ In Set, A → B ≅ Π a ∶ A • B ⟩  
  Π s ∶ S • (x ⟶ y)
≅⟨ upper-bound/sum char ⟩  
  (Σ s ∈ S • x) ⟶ y

Hence S · x ≅ Σ s ∈ S • x , when the sums exists.

F ╲ G ⟶̇ H ≅ G ⟶̇ F ⨾ H

f ╲ g = λ b • ⊔ a ∣ f a ≤ b • g a

Showing g ≤ f ⨾ (f ╲ g)
  (f ⨾ (f ╲ g)) b
= (f ╲ g) (f b)
= ⊔ a ∣ f a ≤ f b • g a
≥ g b since ⊔X ≥ x for all x ∈ X

Showing g ≤ f ⨾ h implies f ╲ g ≤ h
  (f ╲ g) b ≤ h b
⇔ ∀ a ∣ f a ≤ b • g a ≤ h b
⇐ ∀ a ∣ f a ≤ b • h (f a) ≤ h b assumption
⇐ ∀ a ∣ f a ≤ b • f a ≤ b poset-functors are just monotonic functions
⇐ true    tradiing (∀ x ∣ R • P) ⇔ (∀ x • R ⇒ P) with ⇒-reflexitivity

hence char is satisfied :: F ╲ G ⟶̇ H ≅ G ⟶̇ F ⨾ H



∫ₓ F(x, x) ≔ Limᴴᴼᴹ F where Hom : 𝒞ᵒᵖ × 𝒞 ⟶ 𝒮e𝓉
\end{spec}

\item
after intro of isos ::

Talk about how this is a useful concept and how it makes two objects categorical indistuinghable.

Talk about how functors preserve isos, a form of leibniz, and also about yoneda, a form of
extensionality/indirect-equality(iso).

\item
discuss cateogry-equivalence and n-categories as a justification

\item
read "Natural isomorphisms in group theory"

\url{http://www.pnas.org/content/28/12/537.full.pdf}

\item
consider mentioning things along the lines of
\url{https://en.wikipedia.org/wiki/Type_theory#Relation_to_category_theory}

\item
consider mentioning that cat isos are always cat equiavlences, since the latter is weaker and so
admits more possibilities.

maybe mention an example of an equivalence being an isomorphism ::
for skeltal categories, isomorphism and equivalence coincide ::
\url{http://math.stackexchange.com/questions/1658973/how-to-show-that-skeletons-of-equivalent-categories-are-isomorphic}

\item
Recall from introductory logic class that ``expressions are functions of their variables'':
an expression |E| of type |T| that has varaibles |x₁ : S₁, …, xₖ : Sₖ| corresponds to the
function |λ (x₁, …, xₖ) : S₁ × ⋯ × Sₙ • E|, that is a syntactic item
|x₁ : S₁ , … xₖ : Sₖ ⊢ E : T| corresponds to a semantic item |S₁ × ⋯ Sₖ → T|.

Categorically, we can interpret well-typed terms |Γ ⊢ t : A| as morphisms |t : Γ ⟶ A|.
So one can say, loosely, `|type systems ≅ categories|'.

It is to be noted that we are assuming all terms come intrinsticly with a unique type and so this
idea does not work if the type system permits subtyping , mutli-typing, or is untyped, since
morphisms must have unique source and target.
%
One approach to fix this is by
\href{http://perso.crans.org/cohen/map2014/raw/Zeilberger.pdf}{thinking of}
\href{http://noamz.org/papers/funts.pdf}{functors as type refinement systems}.
%
% For a functor F we can define ``typing judgments'' |_∷_⟶_|, note the double-colon-arrow is what
% is being defined: |f ∷ S ⟶ T ⇔ f : F S ⟶ F T|.
% Let us define `the subtyping judgment' |S ≤ T ≔ id : W ⟶ W, where W = F S = F T|.
%
% Then a `derivation' of |f ∷ S ⟶ T| is a morphism |α : S ⟶ T| with |F α = f|.
%
% Note that we have the subsumption subtyping rule:
% |f ∷ S ⟶ T ≤ U ⇒ f ∷ S ⟶ U|
% since |f ∷ S ⟶ T ≤ U ⇔ f : F S ⟶ F T ∧ F T = F U ⇒ f : F S ⟶ F U ⇔ f ∷ S ⟶ U|.
% Likewise for the dual rule.

\item
a category is a `multi-sorted monoid', 'an algebra of functions', ...

\item

google: functors are generlized fibrations

\item
A functor from |𝒞| to |𝒟| can be thought of as ``a |𝒟|-representation of |𝒞|''
---the most obvious case is probably that the linear representation of groups, where
|𝒞| is a group and |𝒟| is the category of vector spaces and linear transformations and so
such a functor realizes the group elements and operation as invertible matrices (since group elements are the morphisms in the category as group and so must be mapped to linear transformations which are really just matrices and since functors preserve inverses the matrices must be invertible) and matrix
multiplication (since functors preserve composition and composition of linear transformations is tantamount to matrix multiplication), respectively.
Perhaps move this example down to the section on basis and mention how linear transformations
are matrices due to the notion of basis and then mention this case as another interesting example
of representation. Yeah probably do that!
{\sc perhaps consider discussing representation of, say, posets
as it requires less abstract algebra knowledge. Then perhaps tie that in with yoneda and
so justifying the term `representation'.}

Another example of representation is that of power sets: subsets correspond to predicates, i.e.,
in |𝒮e𝓉|, |(X ⟶ 𝟚) ≅ ℙ X| where |𝟚| is a 2-element set.

\item

Notes from reading \url{http://bartoszmilewski.com/2014/09/29/how-to-get-enriched-over-magmas-and-monoids/} ::

\begin{spec}

The defn of a cat can be recast as follows


Category' C:
  Ob : Set                                           -- objects
  Hom   : Ob x Ob → Obj 𝒮e𝓉                           -- hom-sets
  𝓂  : ∀ {a b c} → Hom (a , b) × Hom (a , c) → Hom (a,c) -- composition, multiplication
  𝒾  : ∀ {a} → {★} → Hom (a,a)                         -- unit "elements"
  assoc : …
  rightId : 𝓂 ∘ (Id × 𝒾) ≡ 𝓇  where 𝓇 : ∀ {A} → A × {★} ≅ A , i.e. (× {★}) ≅ Id (nat trans)
  leftId  : 𝓂 ∘ (𝒾 × Id) ≡ 𝓁  where 𝓁 : ∀ {A} → {★} × A ≅ A , i.e., ({★} × ) ≅ Id

Where all the arrows are functions, i.e., morphisms of 𝒮e𝓉.

Notice, for left identity,
  ∀ {a b} {f : Hom (a,b)} → 𝓂(𝒾 {a} ★ , f) ≡ f
⇔ … → 𝓂 ∘ (𝒾 {a} × Id { Hom(a,b) }) (★ , f) ≡ 𝓁 (★ , f)
⇔ 𝓂 ∘ (𝒾 × Id) ≡ 𝓁

We want to generlise and so replace (𝒮e𝓉, ×, {★}) with a category 𝒱 and binary endofunctor
⊕ : 𝒱 × 𝒱 ⟶ 𝒱 which has some unit 𝟙. Now elementhip was encoded by functions |{★} → Y|,
where |{★}| is the unit of |×|, and so we mimic this and encode elementship by |𝒱|-morphisms
|𝟙 ⟶ Y| ---notice that we have not claimed |𝟙| is terminal.

Then with these replacements, we obtain:

(𝒱 , ⊕, 𝟙)-EnrichedCategory C:
  Ob  : Set                                           -- type of objects
  Hom : Ob x Ob → Obj 𝒱                             -- mapping of "hom-objects"
  𝓂  : ∀ {a b c} → C (a , b) ⊕ C (a , c) ⟶ C (a,c) -- multiplication morphisms (in 𝒱)
  𝒾  : ∀ {a} → 𝟙 ⟶ C (a,a)                          -- unit "elements" (morphisms in 𝒱)
  assoc : …
  rightId : 𝓂 ∘ (Id × 𝒾) ≡ 𝓇  where 𝓇 : ∀ {A} → A ⊕ 𝟙 ≅ A , i.e. (⊕ 𝟙) ≅ Id (nat trans)
  leftId  : 𝓂 ∘ (𝒾 × Id) ≡ 𝓁  where 𝓁 : ∀ {A} → 𝟙 ⊕ A ≅ A , i.e., (𝟙 ⊕) ≅ Id

Observe we cannot speak of ``elements'' of |Hom(a,a)| since it is not a set and so we mimic
elements by requesting maps from the terminal object.

Notice, for example, we cannot even form the expression 𝟙 → Hom(a,a) since 𝟙 and Hom(a,a) are not
sets and as such there is no function-space between them! Consequently, we are forcefully led to
use the notion of morphism available: that of 𝒱 morphisms!

A triple (𝒱, ⊕, 𝟙) with the unit and associtiity laws is known as a "monoidal category".

[ex0]
any monoid, regarded as a category, is a monoidal category.

cats with finite products are monoidal.

[ex1]
Let 𝒟 be the category with object-set ℕ and morphisms |m ⟶ n| being order-presering
functions |1..m ⟶ 1..n|. Then (𝒟, +, 0) is a monoidal category.

[ex1']
The 𝒟 of [ex1] is a Poset-enriched category with object-set ℕ and hom-objects
|λ m n → 1..m ⟶ 1..n (in Poset)|. (or so I claim!)

[ex2]
For any cat 𝒞, Endo(𝒞) ≔ 𝒞 ^ 𝒞 is a monoidal category with functor composition and the
identity functor. ---recall `a monad is just a monoid in the category of endofunctors'.

A "monoid" "in" (𝒱, ⊕, 𝟙) is a triple (X : Obj 𝒱, m : X ⊕ X ⟶ X, e : 𝟙 ⟶ X)
with assotictivity m ∘ (m ⊕ Id) = m ∘ (Id ⊕ m) ∘ assoc
(
   ∀ x, y, z •  (x m y) m z = x m (y m z)
⇔ ⋯ • m( m (x,y) , z ) = m( x, m(y , z) )
⇔ ⋯ • m ∘ (m × Id) ( (x,y) , z) = m ∘ (Id × m) (x, (y, z))
⇔ m ∘ (m × Id) = m ∘ (Id × m) ∘ assoc
)
and unity m ∘ (Id ⊕ e) = 𝓇
(
    ∀ x • m(x , e) = x
⇔  ∀ x • m ∘ (Id × e) (x, ★) =𝓇(x, ★)
⇔  m ∘ (Id × e) = 𝓇
)
and m ∘ (e ⊕ Id) = 𝓁 .

We call this an "internal algebraic structure" since it's underlying carrier is not an object of
𝒮e𝓉.

wait a minute, a "a monoid in a monoidal category" is just "a one-object enriched-category"!
See p171 of catsformaths for examples.

[ex2']
A monoid in (𝒜𝒷, ⊕, ℤ) is a ring, and a monoid in k-𝓜𝓞𝓓 is just a k-algebra.

[ex3]
Every cat 𝒞 which has (co)products and a (co)terminal category is a monoidal category.

[ex4]
Categories enriched in (𝒮e𝓉, ×, {★}) are just usual categories.

[ex4]
Let 𝒱 be a symmetric monoidal closed category. Define |_* : 𝒱ᵒᵖ ⟶ 𝒱 : x ↦ x ^ 𝟙|
then we have a natural transformation |Id ⟶ **|.
(Isn't is the case that |X ≅ X*|, ie |Id ≅ _*|, but unnaturally so since we obtain this via
yoneda/indirect-inequality? See freyd intro to abelian cats book.)

[ex5]
a one-object category is a monoid,
a one-object Ab-category is a ring with unity where Ab is the category of abelian groups; i.e.
one-object groupoids whose composition is commutative.
Now a "left module" over a ring R is an Ab-functor |R ⟶ Ab|, for a right-module we use
|R ᵒᵖ ⟶ Ab|! So much algebra expressed so succiently :: abstract algebra made easy.
Consider citing dummit and foote!
(is a field a one-object Ab-groupoid ?)


An example enriched category is ``enriching a category over itself''.
If 𝒱 has exponentials ---the way to treat morphisms as objects; think of data-vs-methods---
then we can use those as "internal-hom-sets":

Suppose that we have a right adjoint to the `tensor product' |⊕|, that is
|(⊕ X) ⊣ (^ X)|, i.e., |∀ X, Y, Z • eval : (Z ⊕ X ⟶ Y) ≅ (Z ⟶ Y ^ X)| natural in |Z, Y|;
that is, |(Y ^ X, eval)| is the `best' pair |Σ Z ∶ Obj • Z ⊕ X ⟶ Y|.
That is to say, we can objectify the notion of hom-set.
(Notice that one can go the other way around: a category is `closed' if it has a notion of
internal hom-sets and then a left-adjoint yields a tensor-product!)
Then, we obtain |(X ⟶ Y) ≅ (𝟙 ⊕ X ⟶ Y) ≅ (𝟙 ⟶ Y ^ X)| which says: the external hom-set is
isomorphic to the elements of the internal hom-set.

Define 𝒱𝒱 to be the categeory enriched over 𝒱 as follows:
  Ob  = Obj 𝒱                                         -- type of objects
  Hom X Y = X ^ Y                             -- "internal hom sets"
  𝓂  = (use exponential characterisaton and assoc then apply ⊕)
  𝒾  = (use eponential char on identity of 𝒱)
  assoc = ???
  rightId = ??? ;;; 𝓂 ∘ (Id × 𝒾) ≡ 𝓇  where 𝓇 : ∀ {A} → A ⊕ 𝟙 ≅ A , i.e. (⊕ 𝟙) ≅ Id (nat trans)
  leftId  = ??? ;;; 𝓂 ∘ (𝒾 × Id) ≡ 𝓁  where 𝓁 : ∀ {A} → 𝟙 ⊕ A ≅ A , i.e., (𝟙 ⊕) ≅ Id
  
Anyhow, the notion of enriched-functor is nearly the same as before,

Functor F (between categories enriched over (𝒱, ⊕, 𝟙) ) :
  F₀ : Ob₁ → Ob₂                                -- object mapping
  F₁ : ∀ {a b} → Hom₁(a,b) ⟶ Hom₂(F₀ a, F₀ b)  -- arrow morphisms (in 𝒱)

Natural transformation from F to G:
  η : ∀ {x} → 𝟙 ⟶ Hom₂(F x , G X)   -- "component" morphsims
  prf : ∀ {a b : Ob} → 𝓂 ∘ (F ⊕ η {b}) ∘ 𝓇⁻¹ ≡ 𝓂 ∘ (η {a} ⊕ G) ∘ 𝓁⁻¹ -- naturality condition
  -- the composition is category |𝒱|

Recall that for the (𝒮e𝓉, ×, {★}) case,
∀ {a b} {f : a ⟶ b} →
    F f ⨾ η {b} ★ ≡ η {a} ★ ⨾ G f
⇔  ⋯ → 𝓂 (F f , η {b} ★) ≡ 𝓂 (η {a} ★ , G f)
⇔  ⋯ → 𝓂 ∘ (F × η {b}) (f , ★) ≡ 𝓂 ∘ (η {a} × G) (★ , f)
⇔  ⋯ → 𝓂 ∘ (F × η {b}) (𝓇⁻¹ f) ≡ 𝓂 ∘ (η {a} × G) (𝓁⁻¹ f)
⇔ ∀ {a b : Ob} → 𝓂 ∘ (F × η {b}) ∘ 𝓇⁻¹ ≡ 𝓂 ∘ (η {a} × G) ∘ 𝓁⁻¹

This' the so-called ``naturality hexagon'', since there are six |𝒱|-morphisms involved.

Yoneda let's us construe an arbitrary category in the familar setting of functions and elements
that is |𝒮e𝓉|, so we expect enriched-yoneda to give us a |𝒱|-perspective.

A "representable enriched functor" is a functor |Hom(X,_)| from an enriched category 𝒞 to 𝒱𝒱:
objects |Y| of enriched category 𝒞 are `represented' by objects |Hom(X,Y)| of the better understood and structure-rich category |𝒱|.

The enriched-yoneda says that for any enriched-functor |F| from |𝒞| to |𝒱𝒱|,
the enriched-nattrans |Hom(X,_) ⟶ F| are in bijection with elements |𝟙 ⟶ F K|
---these are both sets.
\end{spec}

See G. M. Kelly, Basic Concepts of Enriched Category Theory
\url{http://www.tac.mta.ca/tac/reprints/articles/10/tr10.pdf}

\item
objectify :: turn into data/object. ---cf the data-vs-method distinction in programming languages
also reailsed by the adjective "internal".


exponentals objectify hom-sets            [level 1]

???? objectify functors                   [level 2]
 
ends objectify natural transformations    [level 3]
(\url{http://bartoszmilewski.com/2014/07/15/natural-transformations-and-ends/})

\item
the morphism part of a functor says that we have the translation-law |(a ⟶ b) → (F a ⟶ F b)|,
that is the type |a ⟶ b| can be transformed into the type |F a ⟶ F b|.

Is this transformation functorial? That is, |(_⟶_) ⟶̇ (F_ ⟶ F_)|??

- consider placing this after the HOM definition below, and so it would be a good example/usage
of hom and pointwise.

- apparently this is excercise 2.5.7 in CatsForMaths ;; go read that section!

\item
consider citing hott on how discrete categories relaise functions as functors; which is really
a rephrasal of Leibniz, or substitution-of-equals-for-equals.

\item
read \url{https://www.google.ca/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=forgetful+functor+from+CAT+to+SET}

\item
mention that poset categories are skeletal and that the idea of isos being identities is one of the
big axioms of hott.

\item
"The development and prospects for category theory (1996)"

\url{http://link.springer.com/article/10.1007/BF00122247#page-1}

"I emphasize that the notions category and functor were not formulated or put in print until the idea of a natural transformation was also at hand."

\item
"A free object is the mother of all objects of that type, in the sense that every object
of that type is a quotient of the free object."

\url{http://www.cims.nyu.edu/~naor/homepage%20files/universal.pdf}

\item
mention how Freyd+Scedrov use a one-sorted approach to cats.

mention how had we used the two-sorted approach of graphs for cats, since cats are afterall built
upon graph structs, then our composition would be hideous to use as it would require a proof of
compatbaility every single time:
|fcmp : (f g : Arrow) → Tgt f ≡ Src g → Arrow|, and with this we need to prove/axiom
|Src (fcmp f g prf) = Src f ∧ Tgt (fcmp f g prf) = g|.

These problem are solved by introducing new datum:
|Mor x y ≔ Σ f : Arrow • Src f = x ∧ Tgt f = y|
then working iwth a new composition: |_⨾⨾_ : ∀{x y z} → Mor x y → Mor y z → Mor x z|
defined by |(f , prf1 , prf2) ⨾⨾ (g , prf3, prf4) ≔ (fcmp f g (≡-trans prf2 (≡-sym prf3)),…)|.
Now the proof-obligations have all been pushed to the background.

perhaps mention that objects can be embedeed into the arrow via identities, and as such are
sometimes seen to be secondary-class citezens when working with categories.
However, as the above little discussion shows, we need them for typing!
Otherwise, we work with raw structures that we refine by introducing typing!
Might as well request the typing to begin with, since it can be regained anyways...

\item
cite "BASIC CONCEPTS OF
ENRICHED CATEGORY THEORY"

\url{http://www.tac.mta.ca/tac/reprints/articles/10/tr10.pdf}

possibly mention that in the constructivst approach to cats, a cat has a setoid structure on
the hom-sets, and so constructive-cats are cats enriched over setoids..?

At least mention that Cat is a category enriched category ;)

\item
after HOM, perhaps mention that |x ↦ Hom(x,x)| is not functorial.

perhaps mention that hom gives objects in SET, and for a CCC |𝒞| there's a notion of hom that also
assigns an object is the category |𝒞|.

\item
read \url{https://en.wikipedia.org/wiki/Free_object}

\item
"Category Theory and Related Fields: History and Prospects"

\url{https://www.mfo.de/document/0908a/OWR_2009_08.pdf}

\item
mention naturaluty for Hom functor.

\item
mention that CAT is a subcategory of GRAPH, perhaps do so after introducing U.

\item
perhaps mention GADTs of Agda as fixed points of functors; cf church encoding of datatypes and Using Z.

\item
like other things in this list, this popped into my head: discuss polynomial functors.

\item
discuss free theorems.

see paper by reynolds and also
\url{http://bartoszmilewski.com/2014/09/22/parametricity-money-for-nothing-and-theorems-for-free/}

\item
read \url{https://www.google.ca/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=examples%20of%20functor%20categories}

\item formalise diagonal functor in agda. why? 'cuz it's fun!

\item
realise paths in G as graphmaps |(Σ n ∶ ℕ • ℙₙ) ⟶ G|

\item
take a look at
\url{http://mathoverflow.net/questions/139388/example-of-an-unnatural-isomorphism}

\item
a natural solution can be thought of as the best/universal solutinon, so attempt to phrase it
as such. Possibly consider a category of transformation, for given functors F and G, and then a
natural transformation is nothing more than a terminal/initial transformation?

\item
mention The Categorical Manifesto

\item
since a natural transformation can be thought
of as a functor, see way below, replace
the categories involved with functor categories
then check that such a functor/nattrans is
necessarily identity.

as such, we move to lax transformations
which have non-trivial morphisms between them..

read this \url{http://math.ucr.edu/home/baez/n_categories/index.html}

see
\url{https://ncatlab.org/nlab/show/transfor}

\item
steal/be-inspired from Annyoying Precision blog the following:

"A good stack of examples, as large as possible, is indispensable for a thorough understanding of any concept, and when I want to learn something new, I make it my first job to build one." – Paul Halmos

then, as a civilised thief, cite said blog.

\url{https://qchu.wordpress.com/2011/01/21/structures-on-hom-sets/}

\item
show that the operations |x ↦ Hom _ x| and |x ↦ Hom x _| are co/functors from a cat to a functor
cat, and so their behaviour on morphisms means they are sent to natural transformations.
These are known as the co/yoneda functors, yeah?

Is there a nattrans |Hom _ x → Hom y _| ?

\item
give example of how posets and monoids are graphs; consider using preorders instead and
use EWD's beautiful definition of a preorder.

introduce poset category before notion of functors and mention backhouse et al regarding using
posets as motivation for definitions in catthry, namely pointwise ordering gives
natural transformation.

\item
when discussing functor cats, see below,
we define operation on cats that yield new
cats. Can these operations be extended on
functors, and so make the constructions
functorial? Most likely YES, since
a construction |𝒞 ↦ 𝒞 ^ 𝒫| for some
particular |𝒫|, is nothing more than the
functor |(_⟶ 𝒫)|, which is a cofunctor,
and so proved below in the section on Hom.

Perhaps make mention of these ideas..

\item
We discussed how |𝟚 ⟶ 𝒞| gives the arrow
categry and generally |ℙₙ ⟶ 𝒞| gives the
n-path category.

What about the case |𝟛 = ℙ₃|.
Instead of three arrows in sequence, what if
instead we considered a triangle.

The categories |𝟘, 𝟙, 𝟚| where easy to form.
Now what about |𝟛|? Clearly it should be a
category of 3-arrows and 3-objects as the
previous construction. But the question is how
are they set. Do we place them in sequence, one
after the other from the first object to the
last object, or do we
wrap this sequence so that the the last arrow
concludes at the first obejct, or do we
set in sequnce all but the last arrow which
we set to be from the first to the last object
thus obtaining a sort-of semi-circle?
---there are of course many other possibilities, but these three seem, to me, `natural' enough for consideration.

The first option gives us the path of length
3, i.e. |ℙ₃|; the second option gives us the
poset category of |0..3-1|; and the last
option gives us the commuting triangle.
The first two are equivalent for |n < 2|,
that is for |𝟘, 𝟙, 𝟚|.
While last two being equivalent at |n=3|,
but what if considered other |n|?

The first two notion are already formalised,
but the last is not and it is ubquotious
---for example, commutative triangles and
commutative squares.

!!!!!!!!!!!!!!!!!

the remainder of this item is sketchy since
i'm super tired

long story short,
\url{https://ncatlab.org/nlab/show/factorization+category}

!!!!!!!!!!!!!!!!!

Given |n|, let
|𝒦₂ₙ| be the category of |2*n| objects |1..2n|
and arrows |(i,j)|
that can be pictured as a big rectangle
with the left-most upper corner labeld with
0 and the right-most lower corner lavbelled
with 2n, where the the top edge of the
rectangle consists of odd-labeled objects
|1,2,3,...,2k+1,...,2n-1| and a single
arrow between them
|(2i+1, 2j+1) : 2i+1 ⟶ 2j+1|,
liekwise the bottom edge consists of
even-labelled objects
|2,4,6,...,2k,...,2n| and a single arrow between consectuiv objects
|(2i, 2j) : 2i ⟶ 2j|.
finally there are two arrows connecting the
top row to the bottom row:
there is an arrow |(1,2) : 1 ⟶ 2| for the
left-side of the rectangle, and an arrow
|(2n-1, 2n) : 2n-1 ⟶ 2n| for the right
side of the rectagle.

(read the following by ignoring all instances
of |⟦…⟧| and so considering the case even n.)

Given |n = 2*k + ⟦n odd⟧|,
- the top row of the rectangle consists of
the |k| odd labels |2*i + 1| where |0 ≤ i ≤ k -⟦n even⟧|.
- the bottom row of the rectangle consists of
the |k - ⟦n odd⟧| many even labels |2*i|
where |i : 1..k-⟦n odd⟧|
- So the top edge may have one object more than
the bottom edge.
- We have a morphism between consequtive edges,
|2*i - 1 ⟶ 2*(i+1)-1| for |i < k|
and |2*i ⟶ 2*(i+1)| for |i < k - ⟦n odd⟧|
- finally for the left edge of the rectangle
we have a morphism |1 ⟶ 2|
- and for the right edge of the rectangle
we have a morphism |2*k - 1 ⟶ 2*(k - ⟦n odd⟧)|




(
if n is odd then 1..n has one more odd number
than even numbers, whereas if n is even then
the number of odd numbers coincides with that
of the even numbers.

Afterall, |1..n = evens 1..n + odds 1..n|
)

Now the commuting pentagon:
\begin{spec}
1 --> 3 --> 5
|           |
v           v
2 --------> 4
\end{spec}

Let |𝒫ₙ| be the poset category on |0..n-1|,
then |𝒫₁ = ℙ₀ = 𝟙 = Com₁|: a 1-element poset,
a 0-length path, a one-object category,
a 
Likewise |𝒫₂ = ℙ₁ = 𝟚|:
a two-element poset is just a path of length
1, that is a category with an arrow between
two distinct objects.

Now |𝒫₃| is the triangle category.

be the category with three obects
|0, 1, 2| and three arrows, notate them
|(i,j) : i ⟶ j|, and one law
|(0,2) = (0,1) ⨾ (0,2)|


\item
Let A denote the arrow category of our cat of
interest.

The arrow category construction gives us an
idea of morphisms-between-morphisms and so
we can make definitions:
0-cell is an object, 1-cell is a morphism,
2-cell is a morphism in A (ie a commuting
square), and generally an n-cell is a
morphism in |Aⁿ ≔ Arrow category of Aⁿ⁻¹|,
or an `n hypercube'.

(Notice that since CAT has exponentials,
we have |Aⁿ(𝒞) ≅ (𝟚ⁿ ⟶ 𝒞)|, where
|𝟚ⁿ ≔ Π i ∶ 1..n • 𝟚|
---what a minute, in CAT isn't it the case
that |𝒞ⁿ ≅ (ℙₙ ⟶ 𝒞)|, where
|ℙₙ| is the n-path category.

Perhaps mention some cool things about the
n-hypercube category |𝟚ⁿ|.

Does the limit exist? That is, does `|A∞|'
exist? (What is it.)

What happens if we replace |𝟚| with an
arbitrary category |𝒜|? Is the result still
interesting?
)

This is an |∞| category?
see references of
\url{https://ncatlab.org/nlab/show/infinity-category}

Also an |n|-category if we only consider
upto |n|-cells?

apparently this is an instance of an
n-fold category:
\url{https://ncatlab.org/nlab/show/n-fold+category}

\item
menion Ends, see
\url{http://bartoszmilewski.com/2014/07/15/natural-transformations-and-ends/}

\item
Just as |f ≤̇ g| gives us the intutive idea that the graph of |f| is everywhere bounded from above
from |g| ---that is, the graph of |f| is never above the graph of |g|---, the existence of a
natural transformation |F ⟶̇ G| gives us a similar inutuion that |F| is below/contained-in |G|
(in that any |F|-structure can be realised as a |G|-structre in some coherent fashion).

\item
We have a notion of morphisms-between-morphisms in category theory as the morphisms in an
arrow category |𝒞 ^ →|. If we take |𝒞 ≔ 𝒞𝒶𝓉|, the category of categories, then we obtain
a `natural' notion of morphisms-between-functors!

How close is the relation of this notion to that of natural transformations?
Is there some form of equivalence?

edit: delegated to mathoverflow.

\item
read ``Towards a Readable Formalisation of Category Theory'',
\url{http://users.cecs.anu.edu.au/~okeefe/work/fcat4cats04.pdf}

and
``Experiments in Formalizing Basic Category Theory in
Higher Order Logic and Set Theory''
at
\url{http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.22.8437&rep=rep1&type=pdf}

and
``catthry as an extension of per martin lof type theory''
at
\url{https://rd.host.cs.st-andrews.ac.uk/publications/CTMLTT.pdf}

\item
consider mentioning that residuation at the functor-level, also known as kan extension,
is anotehr example of adjunction; and converse?

\url{https://www.google.ca/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=adjunctions+as+kan+extensions}

\item
watch catsters on nattrans \url{https://www.youtube.com/watch?v=FZSUwqWjHCU}

\item
rewrite subsection on signatures then finish subsection on Free first-order logics

\item read
\url{https://en.wikipedia.org/wiki/Limerick_(poetry)}

\item cite
\url{http://www.cs.cornell.edu/gries/banquets/Limerick2011/Limericks.pdf}

\item add (and do!) excercise:
adjunction
|(𝒳 ⊗ 𝒴 ⟶ 𝒵) ≅ (𝒴 ⟶ 𝒵 ^ 𝒳)|, cf |uncurry₂| below
i.e., |(𝒳 ⊗) ⊣ (^ 𝒳)| ((and naturally so in |𝒳| !?))

\item cite
\url{http://www.math.mcgill.ca/triples/Barr-Wells-ctcs.pdf}

\item
instead of |publish|, consider using pandadoc to transform the resulting latex file into markdown

\end{enumerate}
\end{comment}
%}}}

Assuming familiarity with the Agda programming language, we motivate the need for
basic concepts of category theory with the aim of discussing adjunctions with
a running example of a detailed construction and proof of a free functor.

\tableofcontents

\part{Intro}

%{{{ Introduction
\section{Introduction}

Lists give free monoids |𝓁 A = (List A, ++, [])|
--- a monoid |𝒮 = (S, ⊕, 0₊)| is a triple consisting of a set with a binary operation on it that is
associative and has a unit; see a \href{http://alhassy.bitbucket.org/posts/blogliterately.html#monoids}{previous post} for a more formal definition.
That it is `free' means that to define a structure-preserving map between monoids
|(List A, ++, []) ⟶ (S, ⊕, 0₊)| it suffices to only provide a map between their
carriers |List A → S| ---freedom means that plain old maps between types freely,
at no cost or effort, give rise to maps that preserve monoid structure.
Moreover, the converse also holds and in-fact we have a bijection
\begin{spec}
  (𝓁 A ⟶ 𝒮) ≅ (A ⟶ 𝒰 𝒮)
\end{spec}
where we write |𝒰 (S, ⊕, 0₊) = S| for the operation that gives us the |𝒰|nderlying carrier
of a monoid.

Loosely put, one says we have an `adjunction', written |𝓁 ⊣ 𝒰|.

Observe that natural numbers |ℕ ≅ List Unit| are a monoid whose operation is commutative,
by using different kinds of elements |A| (and, importantly, still not imposing any equations),
we lose commutativity with |List A|.
Then by generalizing further to binary trees |BinTree A|, we lose associtivity and identity
are are only left with a set and an operation on it ---a structure called a `magma'.

This is the order that one usually learns about these inductively built structures.
One might be curious as to what the next step up is in this hierarchy of generalisations.
It is a non-inductive type called a `graph' and in this note we investigate them by
comparison to lists.
Just as we shifted structures in the hierarchy, we will
move to a setting called a `category' ---such are more structured than magmas
but less restrictive than monoids.

%}}}

%{{{ imports
\subsection*{imports}

Since the Agda prelude is so simple, the core language doesn't even come with booleans or numbers by default
---they must be imported from the standard library. This is a pleasant feature.
As a result, Agda code tends to begin with a host of imports.

\begin{code}

module PathCat where

open import Level using (Level) renaming (zero to ℓ₀ ; suc to ℓsuc ; _⊔_ to _⊍_)

-- numbers
open import Data.Fin
  using (Fin ; toℕ ; fromℕ ; fromℕ≤ ; reduce≥ ; inject≤)
  renaming (_<_ to _f<_ ; zero to fzero ; suc to fsuc)
open import Data.Nat hiding (fold)
open import Relation.Binary using (module DecTotalOrder)
open DecTotalOrder Data.Nat.decTotalOrder using () renaming (refl to ≤-refl)

-- Z-notation for sums
open import Data.Product using (Σ ; proj₁ ; proj₂ ; _×_ ; _,_)
Σ∶• : {a b : Level} (A : Set a) (B : A → Set b) → Set (a ⊍ b)
Σ∶• = Σ
infix -666 Σ∶•
syntax Σ∶• A (λ x → B) = Σ x ∶ A • B

-- equalities
open import Relation.Binary.PropositionalEquality using (_≗_ ; _≡_)
  renaming(sym to ≡-sym ; refl to ≡-refl ; trans to _⟨≡≡⟩_ ; cong to ≡-cong ; cong₂ to ≡-cong₂
   ; subst to ≡-subst ; subst₂ to ≡-subst₂ ; setoid to ≡-setoid)
\end{code}

Notice that we renamed transitivity to be an infix combinator.

Let us make equational-style proofs available for any type.
This is similar to what was discussed in a
\href{http://alhassy.bitbucket.org/posts/blogliterately.html#monoids}{previous post}
but much better.
\begin{code}  
module _ {i} {S : Set i} where
    open import Relation.Binary.EqReasoning (≡-setoid S) public

-- synonym for readability
definition : ∀{a} {A : Set a} {x : A} → x ≡ x
definition = ≡-refl
\end{code}

%}}}

%{{{ Graph definitions
\section{Graph definitions}

A `graph' is just a parallel-pair of maps,
\begin{code}
record Graph₀ : Set₁ where
  field
    V   : Set
    E   : Set
    src : E → V
    tgt : E → V
\end{code}
This of-course captures the usual notion of a set of nodes |V| and a set of directed and labelled
edges |E| where an edge |e| begins at |src e| and concludes at |tgt e|.

What is good about this definition is that it can be phrased in any category: |V| and |E| are
any two objects and |src, tgt| are a parallel pair of morphisms between them.
How wonderful! We can study notion of graphs in arbitrary categories!
---this idea will be made clearer when categories and functors are formally introduced.

However, the notion of structure-preserving map between graphs, or `graph-map' for short,
then becomes
\begin{code}
-- |\McG\-->|
record _𝒢⟶₀_ (G H : Graph₀) : Set₁ where
    open Graph₀
    field
      vertex : V(G) → V(H)
      edge   : E(G) → E(H)
      src-preservation : ∀ e → src(H) (edge e) ≡  vertex (src(G) e)
      tgt-preservation : ∀ e → tgt(H) (edge e) ≡  vertex (tgt(G) e)
\end{code}
This is a bit problematic in that we have two proof obligations and at a first glance it is not
at all clear their motivation besides ``structure-preserving''.

However, our main is in graphs in usual type theory, and as such we can use a definition that is
equivalent in this domain: a graph is a
type |V| of vertices and a `type' |v ⟶ v'| of edges for each pair of vertices |v , v'|.
\begin{code}
-- `small graphs' , since we are not using levels
record Graph : Set₁ where
  field
    V    : Set
    _⟶_ : V → V → Set
\end{code}

Now the notion of graph-map, and the meaning of structure-preserving, come to the forefront:
\begin{code}  
record GraphMap (G H : Graph) : Set₁ where    
    private
      open Graph using (V)
      _⟶g_ = Graph._⟶_ G
      _⟶h_ = Graph._⟶_ H
    field
      ver  : V(G) → V(H)                                   -- vertex morphism
      edge : {x y : V(G)} → (x ⟶g y) → (ver x ⟶h ver y) -- arrow preservation

open GraphMap
\end{code}
Note that |edge| essentially says that |mor| shifts, or translates, types
|x ⟶g y| into types |ver x ⟶h ver y|.

While equivalent, this two-piece definition is preferable over the four-piece one given
earlier since it means less proof-obligation and less constructions in general, but the same
expressiblity. Yay!

Before move on, let us give an example of a simple chain-graph.
For clarity, we present it in both variations.
\begin{code}
-- embedding, |j < n ⇒ j < suc n|
`_ : ∀{n} → Fin n → Fin (suc n)
` j = inject≤ j (≤-step ≤-refl) where open import Data.Nat.Properties using (≤-step)
-- this' an example of a `forgetful functor', keep reading!

[_]₀ : ℕ → Graph₀
[ n ]₀ = record { V = Fin (suc n)      -- |≈ {0, 1, ..., n - 1, n}|
    ; E = Fin n                        -- |≈ {0, 1, ..., n - 1}|
    ; src = λ j → ` j
    ; tgt = λ j → fsuc j
    }
\end{code}
That is, we have |(n+1)| vertices named |0, 1, …, n| and |n|-edges named |0, 1, …, n-1|
with one typing-axiom being |j : j ⟶ (j+1)|. Alternatively,

\begin{code}
[_] : ℕ → Graph
[ n ] = record {V = Fin (suc n) ; _⟶_ = λ x y → fsuc x ≡ ` y }
\end{code}

\subsection{Signatures}

A signature consists of sort symbols and function symbols each of which is associated source-sorts
and a target-sort.
A model or algebra of a language is an interpreation of the sort symbols as sets and function
symbols as functions between those sets
---later you may note that instead of sets and functions we may use the objects and morphisms of
a fixed category instead, and so get a model in that category.

Formally, one sorted signatures are defined:
\begin{code}
open import Data.Vec using (Vec) renaming (_∷_ to _,,_ ; [] to nil) -- , already in use for products :/
  
-- one sorted
record Signature : Set where
    field
     𝒩 : ℕ -- how many there are
     ar : Vec ℕ 𝒩 -- their arities: lookup i ar == arity of i-th function symbol

open Signature {{...}} -- |𝒩| now refers to the number of function symbols in a signature
\end{code}

For example, the signature of monoids consists of a single sort symbol |C| (which can be
interpretted as the carrier of the monoid) and two function symbols |m , u|
(which can be interpreted as the monoid multiplication and unit) with source-target
sort lists |((),C) , ((C,C), C)| ---some would notate this by |u :→ C , m : C × C → C|.
\begin{code}
MonSig : Signature
MonSig = record { 𝒩 = 2 ; ar = 0 ,, 2 ,, nil }
-- unit |u : X⁰ → X| and multiplication |m : X² → X|
\end{code}


Generalising on monoids by typing the multiplication we obtain
the signature of categories: it consists of three sort symbols |O, A, C| (which can be
interepreted as objects, arrows, and composable pairs of arrows) and four function symbols
|⨾ , src, tgt, id| with source-target sort lists |(C,A) , (A,O) , (A,O) , (O,A)|
---notice that only a language of symboll
has been declared without any properties besides those of typing. If we discard |C, ⨾, id| we
then obtain the signature of graphs. Without knowing what categories are, we have seen that their
signatures are similar to both the graph and monoid signatures and so expect their logicas to
also be similar.

A signature can be visualised in the plane by associting a dot for each sort symbol and an arrow
for each function symbols such that the arrow has a tail from each sort in the associated function
symbols source sorts list and the end of the arrow is the target sort of the sort symbol.
That is, a signature can be visualed as a hyper-graph.

A signature whose function symbols each have only one sort symbol for source-sorts is called a
`graph signature' since it corresponds to ---or can be visualised as--- a graph.

Then a model of a graph (signature) |𝒢| is an interpreation/realisation of the graph's vertices
as sets and the graph's edges as functions between said sets.

A model of |𝒢| is nothing more than a graph morphism
|𝒢 ⟶ 𝒮e𝓉𝒢𝓇𝒶𝓅𝒽|, where |𝒮e𝓉𝒢𝓇𝒶𝓅𝒽| is the graph with vertices sets and edges functions.

Notice that a graph is precicely a model of the graph |• ⇉ •| of two vertices and two edges from
the first to the second.
%}}}

\part{Category Theory}

% Category and functor defns
%% \section{A poor-man's definition of Category}

In this section we introduce the notion of a ``poor-man's category'' along with the notion of
structure preserving transformations and structure preserving transformations between such
transformations. The latter are known as \emph{natural transformations} and are considered one of
the most important pieces of the fundamentals of category theory; as such, we discuss them at
length. Afterwards, we relate this section back to our motivating discussion of graphs.

%{{{ cat defn
   \section{Strict Categories}
A category, like a monoid, is a a few types and operations for which some equations hold.
However, to discuss equations a notion of equality is needed and rather than enforce one
outright it is best to let it be given. This is a `set' in constructive mathematics:
a type with an |E|quivalence relation on it ---also called a setoid or an |E|-set.
However, then the structure must have a few added axioms: the operations must be congruences,
i.e., preserve the equivalence relation, and a structure-preserving map must also be a congruence.

For our purposes our we will use propositional equality and point-wise propositional equality,
and as such most of the proofs fall out of the fact that propositional equality is an equivalence.
However, this setoid structure becomes a bit of a noise and the issues of equivalences will be a
distraction from the prime focus. Instead, for our two cases where we use point-wise propositional,
we will postulate two forms of extensionality. Without question this is not a general approach
---then again, our aim is not to develope a library for category theory, which has already been
done so elegantly by Kahl who calls it the
\href{http://relmics.mcmaster.ca/RATH-Agda/RATH-Agda-2.0.0.pdf}{RATH-Agda} project.

\begin{code}
module _ where -- category definitions
    
 record Category {i j : Level} : Set (ℓsuc (i ⊍ j)) where
  infixr 10 _⨾_
  field
    Obj      : Set i
    _⟶_     : Obj → Obj → Set j
    _⨾_      : ∀{A B C : Obj} → A ⟶ B → B ⟶ C → A ⟶ C
    assoc    : ∀{A B C D} {f : A ⟶ B}{g : B ⟶ C} {h : C ⟶ D} → (f ⨾ g) ⨾ h ≡ f ⨾ (g ⨾ h)
    Id       : ∀{A : Obj} → A ⟶ A
    leftId   : ∀ {A B} {f : A ⟶ B} → Id ⨾ f ≡ f
    rightId  : ∀ {A B} {f : A ⟶ B} → f ⨾ Id ≡ f

 open Category using (Obj) public
 open Category {{...}} hiding (Obj) -- don't want this to be public, since --> also notates edges
\end{code}

However, similar to nearly everything else in this document, we can leave the setoid-approach as an excercise
for the reader, which of course has solutions being in the literate source.
%%
%% moved setoid-based theory to an appendix at the end, it seems I must enforce setoid structure at the outset
%% and I really do not think it is worth it for my intended purposes; moreover, it adds noise to the presentation without giving enough insight.

Moreover, lest you're not convinced that my usage of extensionality is at all acceptable,
then note that others have used it to simplify their presentations; e.g.,
\href{http://cs.ioc.ee/~tarmo/papers/jfr14.pdf}{Relative monads formalised}.
Such `appeal to authority' is for the lazy reader who dares not think for him or herself,
otherwise one ought to read up on the \href{https://ncatlab.org/nlab/show/principle+of+equivalence}{evils}
of using equality instead of equivalence relations so as to understand
\href{http://www.math.harvard.edu/~mazur/preprints/when_is_one.pdf}{when one thing is really another}.

The diligent reader may be interest to know that Maarten Fokkinga has written a very
\href{http://maartenfokkinga.github.io/utwente/mmf92b.pdf}{gentle introduction to category
theory using the calculational approach}; I highly recommend it!

In place of strict equality, one uses categorical isomorphism instead.
\begin{code}
 record Iso {i} {j} (𝒞 : Category {i} {j}) (A B : Obj 𝒞) : Set j where
   field
     to   : A ⟶ B
     from : B ⟶ A
     lid  : to ⨾ from ≡ Id
     rid  : from ⨾ to ≡ Id
     
 syntax Iso 𝒞 A B = A ≅ B within 𝒞
\end{code}

%}}}
% comment blocks must begin/end at column 0 !!
\begin{comment}
   %{{{ \subsection{n-categories}
\subsection{n-categories}
insert discussion on how CAT is a 2-category and so equiality is defered from objects to 1-arrows/functors
to obtain isomorphism and then the eqality for the equations involving functors is also
defered to 2-arrows/natTrans.

|𝒞 ≃ 𝒟 ≡ Σ F ∶ 𝒞 ⟶ 𝒟 • Σ G ∶ 𝒟 ⟶ 𝒞 • F ⨾ G ≅ G ⨾ F ≅ Id| and we speak of an equivelcne of
categories.

Nifty fact ::
|F : 𝒞 ⟶ 𝒟| is (part of) an equivalnece iff it is full, faithful, and ``essentially surectiive
on obejcts'': |∀ D : Obj 𝒟 • Σ C : Obj 𝒞 • F C ≅ D| ---note the iso.

also :: every category is equivalent to a skeletal subcategory

Examples.

|𝒫𝒶𝓇 ≃ 𝒫𝒮e𝓉| where |𝒫𝒶𝓇| is the supercategory of |𝒮e𝓉| whose morphisms are partial functions.

begin{comment}
For example,
let |𝒫𝒶𝓇| be the supercategory of |𝒮e𝓉| with morphisms being
`partial functions' |(A ⟶ B) ≡ (A → B + 𝟙)| where the extra element of |𝟙 = { * }| represents
`undefined'
---also known as the |Partial|, |Option|, or |Maybe| monads.
Then, |𝒫𝒶𝓇 ≃ 𝒫𝒮e𝓉| witnessed by |(A ⟶ B) → ( (A + 𝟙, *) ⟶ (B + 𝟙, *) )|
and conversely |( (A , a) ⟶ (B , b) ) → ( A - a ⟶ B - b)| where
|X - x ≡ Σ y ∶ X • ¬(x ≡ y)|.

\begin{spec}
_⟶_ : Set → Set → Set
A ⟶ B = A → B ⊎ ⊤

_⟶'_ : ∀ {a} (AA BB : Σ X ∶ Set a • X) → Set _
(A , a) ⟶' (B , b) = Σ f ∶ (A → B) • f a ≡ b

∣_∣ : ∀ {A B} → A ⟶ B → ((A ⊎ ⊤) , inj₂ tt) ⟶' ((B ⊎ ⊤) , inj₂ tt)
∣_∣ {A} {B} f = f' , refl
  where f' : A ⊎ ⊤ → B ⊎ ⊤
        f' (inj₂ tt) = inj₂ tt
        f' (inj₁ x) with f x
        ...| inj₁ x₁ = inj₁ x₁
        ...| inj₂ tt = inj₂ tt
\end{spec}
end{comment}

|𝒮e𝓉 ^ S ≃ 𝒮e𝓉 / S|, where |S| is construed as a discrete category on the lhs but a set on the
rhs.

[Stone Duality]
|FinBoolAlg ≃ FinSets ᵒᵖ| , witnessed by considering the collection of atoms of a boolean algebra
in one direction and the power set in the other.
Finiteness can be removed at the cost of completeness and atomicitiy,
|CompleteAtomicBoolAlg ≃ Set ᵒᵖ|.
%}}}
\end{comment}
   %{{{ A familiar |𝒮e𝓉|ting
\subsection*{A familiar |𝒮e𝓉|ting}
Let us give some elementary examples of the notion of a category to exhibit its ubiquity.

\begin{itemize}

\item
The collection of small (level 0) types and functions between them and usual function composition
with usual identity form a category and this is not at all difficult to see:
\begin{code}
 instance
  𝒮e𝓉 : ∀ {i} → Category {ℓsuc i} {i} -- this is a `big' category
  𝒮e𝓉 {i} = record {
      Obj = Set i
    ; _⟶_ = λ A B → (A → B)
    ; _⨾_ = λ f g → (λ x → g (f x))
    ; assoc = ≡-refl
    ; Id = λ x → x
    ; leftId = ≡-refl
    ; rightId = ≡-refl
    }
\end{code}
Sadly, this category is traditionally used to motivate constructions in arbitrary categories
and as such people usually thing of objects in an arbitrary category as nothing more than
sets with extra datum ---which is completely false.

\item
{\bf Sets are trivial categories}

Recall that a type, or set, is nothing more than a specified collection of values.

Every set is also a category: there is an object for each element, the only morphisms are (formal)
identities, and composition is constantly the identity.
Some define a set to be a category with only identity morphisms; also called a
`discrete category' when one wants to distance themself from set theory ;)
---less loosely, a discrete category over a type |S| has |Obj = S| and |(x ⟶ y) = (x ≡ y)|.

Discrete categories are quite an important space for \href{http://homotopytypetheory.org/}{hott}
people ... that's right, attractive people are interested in these things.

Observe that all arrows are invertible! ---due to the symmetry of equality.
Categories with this property are known as \emph{groupoids}.

\item
{\bf Categories are typed monoids}

Recall that a monoid |(M, ⊕, e)| is a type |M| with an associative operation |⊕ : M × M → M|
that has a unit |e|.

Every monoid is also a category: there is one object, call it |★|, the morphisms are the monoid
elements, and composition is the monoid operation. Some even define a monoid to be a one object
category. ---less loosely, for a monoid |(M, ⊕, e)| we take |Obj = {★} , _⟶_ = M|.

In fact, some would define a monoid to be a one-object category!

\item
{\bf Categories are coherently preordered sets}

\href{http://www.cs.utexas.edu/~EWD/ewd11xx/EWD1102.PDF}{Recall} that a preordered set, or preset,
is a type |P| with a relation |≤| on it that satisfies ``indirect inequality from above'':
\[|
  ∀ x , y • x ≤ y ⇔ (∀ z • y ≤ z ⇒ x ≤ z)
|\]
---equivalently, if it satisfies ``indirect equality from below'':
|∀ x , y • x ≤ y ⇔ (∀ z • z ≤ x ⇒ z ≤ y)|.
If we also have |∀ x , y • x ≤ y ∧ y ≤ x ⇒ x = y|, then we say |(P, ≤)| is a `poset' or an
`ordered set'.

Every (pre)ordered set is also a category: the objects are the elements, the morphisms are the order-relations, identities are the relfexitivity of |≤|, and composition is transitivity of |≤|.

Traditionally, classically, the relation |≤| is precicely a function |P × P ⟶ 𝔹 = {true, flase}|
and thus there is at-most one morphism between any two objects, and categories with this property
are called \emph{poset categories}.

In the constructive setting, the relation |≤| is typed |P × P → Set| and then
for a preset |(P, ≤)| we take |Obj = P, _⟶_ = a ≤ b| and insist
on `proof-irrelevance' |∀ {a b} (p q : a ≤ b) → p ≡ q| so that there is at most one morphism
between any two objects.
The restriction is not needed if we were using actual categories-with-setoids since then we would
\emph{define} morphism equality to be |((a, b, p) ≈ (a', b', q) ) = (a ≡ a' × b ≡ b')|.

Observe that in the case we have a poset, every isomorphism is an equality:
\[|
  ∀ x, y • x ≅ y ⇔ x ≡ y
|\]
Categories with this property are called \emph{skeletal}.
Again, hott people like this; so much so, that they want it, more-or-less, to be a
\href{http://arxiv.org/abs/1302.4731}{foundational axiom}!

Poset categories are a wonderful and natural motivator for many constructions and definitions in
category theory. This idea is so broad-reaching that it would not be an exaggeration to think of
\href{http://www.cs.nott.ac.uk/~psarb2/papers/abstract.html#CatTheory}{categories as coherently constructive lattices}!

\item
Equivalence relations are relations that are symmetric, reflexive, and transitive.
Alternatively, they are preorder categories where every morphism is invertible ---this is the
symmetry property. But categories whose morphisms are invertible are groupoids!

Hence, groupoids can be thought of as generalized equivalence relations.
Better yet, as "constructive" equivalence relations: there might be more than one morphism/construction
witnessing the equivalence of two items.

Some insist that a "true set" is a type endowed with an equivalence relation, that is a setoid.
However, since groupoids generalize equivalence relations, others might insist on a true set to be
a "groupoid". However, in the constructive setting of dependent-type theory, these notions
coincide!

\end{itemize}

It's been said that the aforementioned categories should be consulted whenever one learns a new
concept of category theory.
Indeed, these examples show that a category is a generalisation of a system of processes,
a system of compositionality, and an ordered system.

%}}}

%{{{ Functor defn
\section{Functors}
Now the notion of structure-preserving maps is just that of graphs but with attention to the algebraic
portions as well.
\begin{code}
 record Functor {i j k l} (𝒞 : Category {i} {j}) (𝒟 : Category {k} {l}) : Set (ℓsuc (i ⊍ j ⊍ k ⊍ l)) where
  field
    -- usual graph homomorphism structure
    obj   : Obj 𝒞 → Obj 𝒟                               -- object map
    mor   : ∀{x y : Obj 𝒞} → x ⟶ y → obj x ⟶ obj y    -- morphism preservation
    -- interaction with new algebraic structure
    id    : ∀{x   : Obj 𝒞} → mor (Id {A = x}) ≡ Id       -- identities preservation
    comp  : ∀{x y z} {f : x ⟶ y} {g : y ⟶ z} → mor (f ⨾ g) ≡ mor f ⨾ mor g  -- composition preservation

 open Functor using (obj ; mor) public
\end{code}
For a functor |F|, it is common practice to denote both |obj F| and |mor F| by |F| and this is usually
not an issue since we can use type inference to deduce which is meant. However, in the Agda formalization
we will continue to use the names |mor , obj|.

A functor can be thought of as endowing an object with some form of structure
---since categories are intrinsically structureless in category theory---
and so the morphism component of a functor can be thought of as preserving relations:
|f : a ⟶ b ⇒ F f : F a ⟶ F b| can be read as, ``if |a| is related to |b| (as witnessed by |f|)
then their structured images are also related (as witness by |F f|)''.

\subsubsection*{some synonyms}
While we're close to the definition, let's introduce some synonyms for readability
\begin{code}
 module _ {i j k l} {𝒞 : Category {i} {j}} {𝒟 : Category {k} {l}} {{F : Functor 𝒞 𝒟}} where
  functors-preserve-composition = Functor.comp F
  functors-preserve-identities  = Functor.id F
\end{code}
We make these as synonyms rather than names in the record since we do not want to use such lengthy
identifiers when realizing functor instances. The reason we do not make these synonyms in the
record but rather in a public dummy module is to make the functor in question found from the ambient
context (the |{{...}}|).

While we're making synonyms for readability, let's make another:
\begin{code}
 _even-under_ : ∀ {a b} {A : Set a} {B : Set b} {x y} → x ≡ y → (f : A → B) → f x ≡ f y 
 _even-under_ = λ eq f → ≡-cong f eq
\end{code}

An example usage is the proof |≡-cong (mor G) (id F) : mor G (mor F Id) ≡ mor G Id| can be
written more clearly as |functors-preserve-identities even-under (mor G)|, while longer it
is also self-documenting.

%{{{ {Functor Conventions}
\subsection*{Functor Conventions}
In informal mathematics a functor |F = (obj , mor, preservation proofs)|
is usually presented as ``|F = (F₀, F₁)| is a functor (exercise to reader)''.


"endo"morphism is a morphism with the
same source and target, an "auto"morphism
is an isomorphism with the same source and
target.

Say "co"functor as short for "co"ntravariant
functor. Notice that the composition of
cofunctors is a covaraint functor ---cf the multiplication of negative numbers is a positive functor.
%}}}

\subsection*{Examples}

A functor among monoids (as categories) is just a monoid homomorphism:
|(M, ⊕, e) ⟶ (N, ⊗, d) = Σ h ∶ M → N • ∀ x,y • h(x ⊕ y) = h x ⊗ h y ∧ h e = d|;
that is an identity and multiplication preserving function of the carriers.
Ny induction, |h| preserves all finite multiplications:
|h (⊕ i ∶ 1..n • xᵢ) = (⊗ i ∶ 1..n • h xᵢ)| where
|(★ i ∶ 1..n • yᵢ) ≔ e ★ y₁ ★ y₂ ⋯ ★ yₙ|.
More generally, functors preserve finite compositions: |F (⨾ i ∶ 1..n • fᵢ) = (⨾ i ∶ 1..n • F fᵢ)|
Cool beans :-)

A functor among poset categories is an order-preserving function.

A functor among discrete categories is just a function of the associated sets.

Two examples of functors from a poset (category) to a monoid (category).
\begin{itemize}
\item
|monus : (ℕ, ≤) ⟶ (ℕ,+, 0)| is a functor defined on morphisms by
|i ≤ j ⇒ monus(i,j) ≔ j - i| and then the functor laws become
|i - i = 0| and |(k - j) + (j - i) = k - i|.
\item
|div : (ℕ⁺, ≤) → (ℚ, ×, 1)| is defined on morphisms by
|i ≤ j → div(i,j) ≔ j / i| and the functor laws become
|i / i = 1| and |(k / j) × (j / i) = k / i|.
\end{itemize}

Hey, these two seem alarmingly similar! What gives!
Well, they're both functors from posets to monoids ;)
Also, they are instances of `residuated po-monoids'.
Non-commutative monoids may have not have a general inverse operation,
but instead might have left- and right- inverse operations known as residuals
---we'll mention this word again when discussing adjunctions and kan extensions.
Alternatively, they're are instances of
\href{http://link.springer.com.libaccess.lib.mcmaster.ca/article/10.1007/s10773-004-7710-7}{`(Kopka) Difference-posets'}.

\begin{comment}
They're also instances of a structure known
as a \emph{Kopka D-poset}, or Kopka difference-poset:
such a structure |(D, ≤, ∸, *, 0, 1)| consists of a poset |(D, ≤)| with least element |0|,
greates element |1|, abelian po-monoid |(D, ≤, *, 1)|, and a binary operation
|_∸_ : D × D → D| satisfying
\begin{itemize}
\item |a ≤ b ⇒ b ∸ a| is defined  %% contravariance.
\item |a ∸ 0 = a| %% since |0 ≤ a| and what??
\item |a ≤ b ≤ c ⇒ c - b ≤ c - a| %% contravariance.
\item |a ≤ b ≤ c ⇒ (c ∸ a) ∸ (c ∸ b) = b ∸ a| %% composition via functoricality?
\item |a ∸ (a * b) ≤ 1 ∸ b|, compare with $\frac{y}{y \times x} = \frac{1}{x}$.
\end{itemize}

The similarity is obtained as follows:
\begin{spec}
Assuming

(A0) a ∸ 0 = a, for all a
(A1) a ≤ b ≤ c ⇒ c ∸ b ≤ c ∸ a
(A2) a ≤ b ≤ c ⇒ (c ∸ a) ∸ (c ∸ b) = b ∸ a

(i)
   a ≤ b
⇒ 0 ≤ a ≤ b         0 is bottom
⇒ b ∸ a ≤ b ∸ 0     (A1)
⇒ b ∸ a ≤ b         (A0)

(ii) a ∸ a = 0
Proof.
  Suffices to show a ∸ a ≤ 0, since 0 is bottom element.

    a ∸ a
  = (a ∸ 0) ∸ (a ∸ 0)   (A0)
  = 0 ∸ 0               (A2) since 0 ≤ 0 ≤ a , since 0 bottom
  = 0                   (A0)
\end{spec}

%% google: ON D-POSETS OF FUZZY SETS by Roman Fric
\end{comment}






%}}}

%{{{ The four postulates of the apocalypse
\section{The four postulates of the apocalypse}

Categories have objects and morphisms between them, functors are morphisms between categories,
and then we can go up another level and consider morphisms between functors.
These `level 2 morphisms' are pretty cool, so let's touch on them briefly.

Using posets as our guide,
We extend the ordering to monotone functions |f , g| pointwise
|f ≤̇ g ≔ (∀ x • f x ≤ g x)| and with posets as our guide, we extend the notion of morphism between
functors to be a `witness' of these orderings |η : ∀ {X} → F X ⟶ G X|.
However, then for any morphism |f : A ⟶ B| we have two ways to get from |F A| to |G B| via
|F f ⨾ η {B}| and |η {A} ⨾ G f| and rather than choose one or the other, we request that they
are identical ---similar to the case of associtivity.
\begin{code}
 NatTrans : ∀ {i j i' j'}  {𝒞 : Category {i} {j}} {𝒟 : Category {i'} {j'}} (F G : Functor 𝒞 𝒟) → Set (j' ⊍ i ⊍ j)
 NatTrans {𝒞 = 𝒞} F G =
   Σ η ∶ (∀ {X : Obj 𝒞} → obj F X ⟶ obj G X) • (∀ {A B} {f : A ⟶ B} → mor F f ⨾ η {B} ≡ η {A} ⨾ mor G f)
\end{code}
The naturality condition is remembered by placing the target component |η {B}| \emph{after}
lifting |f| using the \emph{source} functor |F|;
likewise placing the source component \emph{before} applying the target functor.

Another way to remember it:
|η : F ⟶̇ G| starts at |F| and ends at |G|, so the naturaliry also starts with |F| and ends
with |G|: |F f ⨾ η {B} = η {A} ⨾ G f| :-)

It is at this junction that aforemenioed problem with our definition
of category comes to light: funnction equality is extensional and as such we cannot prove it.
Right now we have two function-like structures for which we will postulate a form of extensionality,
\begin{code}
 -- function extensionality
 postulate extensionality : ∀ {i j} {A : Set i} {B : A → Set j} {f g : (a : A) → B a}
                          → (∀ {a} → f a ≡ g a) → f ≡ g

 -- functor extensionality
 postulate funcext : ∀ {i j k l} {𝒞 : Category {i} {j} } {𝒟 : Category {k} {l} }
                     {F G : Functor 𝒞 𝒟} (oeq : ∀ {o} → obj F o ≡ obj G o)
                     → (∀ {X Y} {f : X ⟶ Y} → mor G f ≡ ≡-subst₂ _⟶_ oeq oeq (mor F f))
                     → F ≡ G

 -- graph map extensionality
 postulate graphmapext : {G H : Graph } {f g : GraphMap G H} (veq : ∀ {v} → ver f v ≡ ver g v)
                     → (∀ {x y} {e : Graph._⟶_ G x y} → edge g e ≡ ≡-subst₂ (Graph._⟶_ H) veq veq (edge f e))
                     → f ≡ g
                     
 -- natural transformation extensionality
 postulate nattransext : ∀ {i j i' j'} {𝒞 : Category {i} {j} } {𝒟 : Category {i'} {j'} } {F G : Functor 𝒞 𝒟} (η γ : NatTrans F G)
                       → (∀ {X} → proj₁ η {X} ≡ proj₁ γ {X}) -- this is not enough to regain |η,γ|
                       → η ≡ γ
\end{code}

Natural transformations are too cool to end discussing so briefly
and so we go on to discuss their usage is mathematics later on.

%}}}
   %{{{ 𝒞𝒶𝓉
   \subsection{A very big |𝒞𝒶𝓉|}

With the notions of categories, functors, and extensionality in-hand we can now discus the
notion of the category of small categories and the category
of small graphs. Afterwards we give another example of a functor, say how every category can be construed as a
graph.

First the category of \emph{smaller} categories,
\begin{code}
 instance
  𝒞𝒶𝓉 : ∀ {i j} → Category {ℓsuc (i ⊍ j)} {ℓsuc (i ⊍ j)}
  𝒞𝒶𝓉 {i} {j} = record {
      Obj = Category {i} {j}
    ; _⟶_ = Functor
    ; _⨾_ = λ F G → record {
          obj = obj F ⨾ obj G    -- this compositon lives in |𝒮e𝓉|
        ; mor = mor F ⨾ mor G
        ; id = λ {x} → begin
              (mor F ⨾ mor G) (Id {A = x})
            ≡⟨ definition {- of function composition -} ⟩
              mor G (mor F (Id {A = x}))
            ≡⟨ functors-preserve-identities even-under (mor G) ⟩
              mor G (Id {A = obj F x})
            ≡⟨ functors-preserve-identities {- and function composition -} ⟩
              Id {A = (obj F ⨾ obj G) x}
            ∎
        ; comp = λ {x y z f g} → begin
               (mor F ⨾ mor G) (f ⨾ g)
             ≡⟨ definition {- of function composition -} ⟩
               mor G (mor F (f ⨾ g))
             ≡⟨ functors-preserve-composition even-under (mor G) ⟩
               mor G (mor F f ⨾ mor F g)
             ≡⟨ functors-preserve-composition {- and function composition -}⟩
               (mor F ⨾ mor G) f ⨾ (mor F ⨾ mor G) g
             ∎
        }
    ; assoc = λ {a b c d f g h} → funcext ≡-refl ≡-refl
    ; Id = record { obj = Id ; mor = Id ; id = ≡-refl ; comp = ≡-refl }
    ; leftId = funcext ≡-refl ≡-refl
    ; rightId = funcext ≡-refl ≡-refl }
\end{code}

Some things to note,
\begin{itemize}

\item
We could have written |id = ≡-cong (mor G) (id F) ⟨≡≡⟩ id G|, but this is not terribly clear what is going on.
Especially since we introduced categories not too long ago, we choose to elaborate the detail.

Likewise, |comp = (≡-cong (mor G) (comp F)) ⟨≡≡⟩ (comp G)|.

\item
|assoc| is trivial since function composition is, by definition, associative.
Likewise |leftId, rightId| hold since functional identity is, by definition, unit of function composition.

\item
The definition of composition immediately gives us that |obj , mor| distributes over composition:
|obj (F ⨾ G) = obj F ⨾ obj G| and |mor (F ⨾ G) = mor F ⨾ mor G|.
\item
|𝒞𝒶𝓉| is a category of kind |(ℓsuc m, ℓsuc m)|, where |m = i ⊍ j|, and its objects
are categories of kind |(i , j)| and so it is not an object of itself. Thank-you Russel and friends!
( You may proceed to snicker at the paradoxical and size issues encountered by those who use set theory. )
---then again, I've never actually learned, nor even attempted to learn, any ``formal set theory'';
what I do know of set theory is usually couched in the language of type theory, I heart LADM!

\end{itemize}

%}}}
   %{{{ 𝒢𝓇𝒶𝓅𝒽
In a nearly identical way, just ignoring the algebraic datum, we can show that
|Graph|s with |GraphMap|s form a graph
\begin{spec}
  𝒢𝓇𝒶𝓅𝒽 : Category
  𝒢𝓇𝒶𝓅𝒽 = {! exercise !}
\end{spec}
\begin{comment}
\begin{code}
  𝒢𝓇𝒶𝓅𝒽 : Category
  𝒢𝓇𝒶𝓅𝒽 =
   record
    { Obj = Graph ; _⟶_ = GraphMap
    ; _⨾_ = λ f g → record { ver = ver f ⨾ ver g ; edge = edge f ⨾ edge g } -- using |𝒮et|
    ; assoc = ≡-refl  -- function composition is associtive, by definition
    ; Id = record { ver = Id ; edge = Id } ; leftId = ≡-refl ; rightId = ≡-refl
    -- functional identity is no-op, by definition
    }
    where open GraphMap
\end{code}
\end{comment}
%}}}
   %{{{ 𝒞𝒶𝓉s are 𝒢𝓇𝒶𝓅𝒽s
\subsection{|𝒞𝒶𝓉|s are |𝒢𝓇𝒶𝓅𝒽|s}
%% \subsection{Forgive and forget: the |𝒰|nderlying functor}
Let's formalize what we meant earlier when we said graphs are categories but ignoring the algebraic
data.

Given a category, we ignore the algebraic structure to obtain a graph,
\begin{code}
 𝒰₀ : Category → Graph
 𝒰₀ 𝒞 = record { V = Obj 𝒞 ; _⟶_ = Category._⟶_ 𝒞 }
\end{code}

Likewise, given a functor we `forget' the property that the map of morphisms needs to preserve all
finite compositions to obtain a graph map:
\begin{code}
 𝒰₁ : {𝒞 𝒟 : Category} → 𝒞 ⟶ 𝒟 → 𝒰₀ 𝒞 ⟶ 𝒰₀ 𝒟
 𝒰₁ F = record { ver = obj F ; edge = mor F }
\end{code}
This says that |𝒰₁| turns |ver, edge| into |obj , mor|,
|𝒰₁ ⨾ ver  ≡ obj| and |𝒰₁ ⨾ edge ≡ mor|, reassuring us that |𝒰₁| acts
as a bridge between the graph structures: |ver , edge| of graphs and
|obj , mor| of categories.

Putting this together, we obtain a functor.
\begin{code}
-- underlying/forgetful functor: every category is a graph
 𝒰 : Functor 𝒞𝒶𝓉 𝒢𝓇𝒶𝓅𝒽
 𝒰 = record { obj = 𝒰₀ ; mor = 𝒰₁ ; id = ≡-refl ; comp = ≡-refl }
\end{code}
We forget about the extra algebraic structure of a category and of a functor to
arrive at a graph and graph-map, clearly such `forgetfullness' preserves identities
and composition since it does not affect them at all!

Those familiar with category theory may exclaim that just as I have mentioned
the names `underlying functor' and `forgetful functor' I ought to mention
`stripping functor' as it is just as valid since it brings about connotations of
`stripping away' extra structure.
I'm assuming the latter is less popular due to its usage for
poor mathematical jokes and puns.

Before we move on, the curious might wonder if ``categories are graphs'' then what is the analgoue to
``$X$ are hypergraphs'', it is \href{http://arxiv.org/PS_cache/math/pdf/0305/0305049v1.pdf#page=178}{multicategories}.

The remainder of this part of these notes is to build-up the material needed to realize the notion of `forgetful'.
%}}}

%{{{ Natural Transformations
\section{Natural Transformations}

Recall, that a natural transformation |η : F ⟶ G| is a family
|∀ {X : Obj 𝒞} → F X ⟶ G X| that satisfies the naturality condition:
|∀ {A B} {f : A ⟶ B} → F f ⨾ η {B} ≡ η {A} ⨾ G f|. Let us look at this from a few different
angles; in particular,
\href{http://mathoverflow.net/questions/56938/what-does-the-adjective-natural-actually-mean/56956}{what does the adjective `natural' actually mean?} It's been discussed on many forums and we collect
a few of the key points here.

\begin{enumerate}

\item

{\bf naturality is identity of possible paths; or contraction of choices}

Given two functors |F , G|, for any object |x| we obtain two objects |F x, G x| and so a morphism
from |F| to |G| ought to map such |F x| to |G x|. That is, a morphsim of functors is a family
|η : ∀ {x : Obj} → F x ⟶ G x|. Now for any |f : a → b| there are two ways to form a morphism
|F a → G b|: |F f ⨾ η {b}| and |η {a} ⨾ G f|. Rather than make a choice each time we want such
a morphism, we eliminate the choice all together by insisting that they are identical.
This is the naturality condition.

This is similar to when we are given three morphisms |f : a → b , g : b → c , h : c → d|,
then there are two ways to form a morphism |a → d|: |(f ⨾ g) ⨾ h| and |f ⨾ (g ⨾ h)|.
Rather than make a choice each time we want such a morphism, we eliminate the choice all together
by insisting that they are identical. This is the associativity condition for categories.

Notice that if there's no morphism |F x ⟶ G x| for some |x|, they by definition there's no
natural transformation |F ⟶̇ G|.

\item

\[ |"the natural X"
  = "the X which requires no arbitrary choices"
  = "the canonical/standard X"|
\]  

That is,
\begin{spec}
  it is a natural construction/choice
=
  distinct people would arrive at the same construction;
  (no arbitrary choice or cleverness needed)
=  
  there is actually no choice, (ie only one possiility), and so
  two people are expected to arrive at the same `choice'
\end{spec}
%
Thus, if a construction every involves having to decide between distinct routes, then chances are
the result is not formally natural.

\href{http://math.stackexchange.com/questions/939404/do-natural-transformations-make-god-given-precise?rq=1}{Some would even say}: "natural" = "God-given".

\item
"natural" = "resonable or expected in the ambient context" ; sometimes this `inution' is developed
from working in a field for some time. Sometimes it just "feels" natural.

\item
"natural solution" = "has properties of all other solutions"

[To consider: is a natural solution then just an initial solution? That is, an intial
transformation?]

{\sc add this to todo's list}

\item
{\bf natural means polymorphic without type inspection}

%{{{ nattrans are polyfuncs

A natural transformation can be thought of as a polymorphic function
|∀ {X} → F X ⟶ G X| \emph{where} we restrict ourselves to avoid inspecting any |X|.

Recall that a `|mono|morphic' operation makes no use of type variables in its signature,
whereas a `|poly|morphic' operation uses type variables in its signature.

Inspecting type parameters or not leads to the distinction of ad hoc plymorphism vs. parametric
polymorphism ---the later is the kind of polymorphism employed in functional language like Haskell
and friends ans so such functions are natural transformations by default!
\href{http://ecee.colorado.edu/ecen5533/fall11/reading/free.pdf}{Theorems for free!}

For example,
\begin{spec}
size : ∀ {X} → List X → 𝒦 ℕ X   -- where |𝒦 x y ≔ Id {x}| for morphisms and |𝒦 x y ≔ x| for objects
size [x₁, …, xₙ] = n
\end{spec}
is a polymorphic function and so naturality follows and is easily shown.
So we have always have
\[|List f ⨾ size = size|\] %% since |𝒦 ℕ f = Id|, then extensionality
and so |size : List ⟶̇ 𝒦|.
\begin{comment}
for any |f : A ⟶ B| we have
\begin{spec}
  (List f ⨾ size) [x₁, …, xₙ]
=
  size (List f [x₁, …, xₙ])
=
  size [f x₁, …, f xₙ]
=
  n
=
  Id n
=                  
  (𝒦 ℕ f) n
=  
  (𝒦 ℕ f) (size [x₁ , …, xₙ])
=  
  (size ⨾ 𝒦 ℕ f) [x₁, …, xₙ]
\end{spec}
Hence, |size : List ⟶̇ 𝒦|.
\end{comment}

On the other hand, the polymorphic function
\begin{spec}
whyme : ∀ {X} → List X → 𝒦 Int X
whyme {X} [x₁,…,xₙ] = If X = ℕ then 1729 else n
\end{spec}
is not natural: the needed equation |F f ⨾ η {B} = η {A} ⨾ G f|
for any |f : A → B| breaks as witnessed by
|f = (λ x → 0) : ℝ → ℕ| and any list with length |n ≠ 1729|,
and this is easily shown.
\begin{comment}
\begin{spec}
  (List f ⨾ whyme) [x₁, …, xₙ]
=
  whyme (List f [x₁, …, xₙ])
=
  whyme [f x₁, …, f xₙ]
=
  if ℕ = ℕ then 1729 else n
=
  1729
≠
  n
=  
  if ℝ = ℕ then 1729 else n
=
  (𝒦 ℕ f) (whyme [x₁, …, xₙ])
=
  (whyme ⨾ 𝒦 Int f) [x₁, …, xₙ]
\end{spec}
\end{comment}

One might exclaim, ``hey! this only works 'cuz you're using Ramanujan's taxi-cab number!
1729 is the smallest number expressible as a sum of 2 cubes in 2 ways:
|1729 = 12³ + 1³ = 10³ + 9 ³|.'' I assure you that this is not the reason that naturality breaks,
and I commend you on your keen observation.

Notice that it is natural if we exclude the type inspected, |ℕ|.
That is, if we only consider |f : A → B| with |A ≠ ℕ ≠ B|.
In general, is it the case that a transformation can be made natural by excluding
the types that were inspected?

Before we move on, obverse that a solution in |h| to the absorptive-equation |F f ⨾ h = h|
is precisely a natural transformation from |F| to a diagonal functor.
\[|F f ⨾ h = h ⇔ Σ X : Obj • h ∈ F ⟶̇ 𝒦 X |\]
where |(x ∈ Σ y ∶ Y • P y) =  (Σ y ∶ Y • y ≡ x ∧ P y)|.

{\sc add to todo's: formalize |∈|; trickier than it looks ;) }

In particular, since |g ⨾ (λ _ → e) = (λ x → (λ _ → e) (g x) ) = (λ x → e)|
that is |g ⨾ K e = K e|, we have that
\[|∀ {F} {X} {e : X} → (K e) ∈ F ⟶̇ 𝒦 X|\]
Is the converse also true? If |h ∈ F ⟶̇ 𝒦 X| then |h = K e| for some |e|?


%}}}

% commented out
%{{{ monomorphic funcs are natural
\begin{comment}
\item

Notice that that monomorphic functions are always natural!

Given |m : X → Y| we can consture this as |m : ∀ {Z} → 𝒦 X Z → 𝒦 Y Z| and then we obtain
naturality: given |f : A → B|,
\begin{spec}
  m ⨾ 𝒦 X f
= m ⨾ Id
= m
= Id ⨾ m
= 𝒦 Y f ⨾ m
\end{spec}

this is probably less insightful, and probably a damaging observation...
\end{comment}
%}}}

\item
{\bf natural means no reference to types}

The idea that a natural transformation cannot make reference to the type variable at all can be
seen by yet another example.

\begin{spec}
  data 𝟙 : Set where ★ : 𝟙

  -- a choice function: for any type X, it yields an argument of that type
  postulate ε : (X : Set) → X

  nay : ∀ {X} → X → X
  nay {X} _ = ε X
\end{spec}

Now nautrality |Id f ⨾ nay {B} = nay {A} ⨾ Id f| breaks as witnessed by
|f = (λ _ → εℕ + 1) : 𝟙 → ℕ|.
\begin{comment}
\begin{spec}
  Id f ⨾ nay {ℕ}
=
  f ⨾ (λ _ → ε ℕ)
=
  λ _ → ε ℕ
≠
  λ _ → ε ℕ + 1
=
  λ _ → f (ε 𝟙)
=
  nay {𝟙} ⨾ Id f
\end{spec}
\end{comment}

From this we may hazard the following:
if we have natural transformations |ηᵢ : ∀ {X : Objᵢ} → F X ⟶ G X|
where the |Objᵢ| partition the objects available ---ie, |Obj = Σ i • Objᵢ|---
then the transformation |η {(i, X)} = ηᵢ| is generally unnatural since it clearly makes choices,
for each partition.

\item
{\bf natural means uniformaly and simultaneously defined}

A family of morphisms is `natural in x' precisely when it is defined
\emph{simultaneously} for all |x| ---there is no inspection of some particular x here and there,
no it is uniform! With this view, the naturality condition is thought of as a `simultaneity'
condition. \href{https://www.google.ca/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=general%20theory%20of%20natural%20equivalences}{[Rephrasing GToNE]}

The idea of naturality as uniformly-definable is pursued by
\href{http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.107.2336&rep=rep1&type=pdf}{Hodges and Shelah}.

\item

{\bf naturality is restructure-modify commutativity}

Recall that a functor can be thought of as endowing an object with structure.
Then a transformation can be thought of as a restructuring operation and naturality means
that it doesn't matter whether we restructure or modify first, as long as we do both.

\item
{\bf natural means obvious}

It may help to think of ``there's a natural transformation from F to G'' to mean
``there's an obvious/standard/canconical way to transform F structure into G structure''.

Likewise, ``F is naturally isomorphic to G'' may be read ``F is obviously isomorphic to G''.

Sometimes we can show ``F X is isomorphic to G X, if we make a choice dependent on X''
and so the isomorphism is not obvious, since a choice must be made.

\item
{\bf naturality is promotion}

I think Richard Bird refers to the naturality condition as a promotion law where the functors
involved are thought of as (list) constructions.

The nomenclature is used 
\href{http://www.cs.ox.ac.uk/files/3390/PRG69.pdf}{``to express the idea than operation on a compund structure can be `promoted' into its componenets'.}

Reading |F f ⨾ η {B} = η {A} ⨾ G f| from left to right:
mapping f over the result of handling a complicated strucure is the same as mapping f over the
complex dataum than handling the result.

Lists give many examples of natural transformations by considering
\href{https://link.springer.com/chapter/10.1007/3-540-51305-1_24}{a categorical approach to the theory of lists}.

\item
{\bf naturality as a rewrite rule}

The naturality condition can be seen as a rewrite rule that let's us replace a complicated or
inefficient side with a simplier or more efficient yet equivalent expression.
I think I first learned this view of equations at the insistence of
\href{https://www.google.ca/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=algebra%20of%20programming}{Richard Bird}.


\item

{\bf naturality is just model morphism}

Given two functors |F,G : 𝒞 ⟶ 𝒟| let us construe them as only graph homomorphisms.
Then each is a model of the graph |𝒰₀ 𝒞| ---each intereprets the nodes and edges of |𝒰₀ 𝒞| as
actual objects and morphisms of |𝒟|--- and a natrual transformation is then nothing
more than a morphism of models.

{\sc was the notion of model morphisms mentioned earlier when
models were introduced?}


\item
{\bf naturality yields pattern matching}

In the setting of types and functions, |η : F ⟶̇ G| means we have |η (F f x) = G f (η x)|
which when read left-to-right says that |η| is defined by pattern-matching on its argument
to obtain something of the form |F f x| then it is defined recursively by examining |x| and then
applying |G f| to the result ---of course there's some base case |f| definitions as well.

Alternatively, the input to |η| is of the form |F …| and its
output is of the form |G …|.

\end{enumerate}

%}}}
   %{{{ Example nat trans.
\subsection*{Example nat trans}

\begin{enumerate}

\item
{\bf pointwise monotonicity}

A functor among poset categories is an order-preserving function and a natural transformation
|f ⟶ g| is a proof that |f ≤ g| pointwise: |∀ x • f x ≤ g x| ---all the other pieces for a natural
transformation are automatic from the definition of poset category.

\item
{\bf conjugation}

A functor among monoids (as categories) is just a monoid homomorphism:
\[|(M, ⊕, e) ⟶ (N, ⊗, d) ≔ Σ h ∶ M → N • ∀ x,y • h(x ⊕ y) = h x ⊗ h y ∧ h e = d|\]
A natural transformation |(f, prf) ⟶ (g, prf')| is a point |n : N| with
|∀ x ∶ M • f x ⊗ n = n ⊗ g x|, a `conjugation' by |n| that takes |f| to |g|.
\begin{comment}
  η ∈ (f , prf) ⟶ (g , prf')
=                               { defn of natural transformation }
  η ∈ ∀ {x} → f x ⟶ g x in M
  with ∀ m • f m ⨾ η = η ⨾ g m
=                               { arrows in monoid categories }
  η ∈ N with ∀ x ∶ M • f x ⨾ η = η ⨾ g x
=                               { composition in monoid categories }
  η ∈ N with ∀ x ∶ M • f x ⊗ η = η ⊗ g x
\end{comment}  
  
\item
{\bf fold}

Recall from the introduction |𝒰(S, ⊕, e) = S| was the underlying functor from monoids to sets.
Let |𝒰 × 𝒰| be the functor that for objects |M ↦ 𝒰 M × 𝒰 M| and for morphisms
|h ↦ λ (x,y) → (h x, h y)|. Then the monoid multiplication (of each monoid) is a natural
transformation |𝒰 × 𝒰 ⟶ 𝒰|, where naturality says that for any monoid homomorphism |h|, the
application of |𝒰 h| to the (monoid) multiplication of two elements is the same as the
(monoid) multiplication of the |𝒰 h|-images of the two elements, and this is evident from the
homomorphism condition.

Extending to finite products, |𝒱 ≔ Σ n ∶ ℕ • ∏ i ∶ 1..n • 𝒰|, the natural transformation
|𝒱 ⟶̇ 𝒰| is usually called fold, reduce, or cata and
|𝒱| is known as the
free monoid functor with notations |A* = List A = 𝒱 A|.

Loosely put,
\begin{spec}

𝒱₀ : Monoid → Set
𝒱₀ M = Σ n ∶ ℕ • ∏ i : 1..n • 𝒰 M -- finite sequences of elements from M

𝒱₁ : ∀ {M N : Monoid} → (M ⟶ N) → 𝒱₀ M → 𝒱₀ N
𝒱₁ (h , prf) = λ (n , x₁, …, xₙ) → (n , h x₁ , … , h xₙ)

fold : ∀ {M : Monoid} → 𝒱₀ M → 𝒰₀ M
fold {(M, ⊕, e)} = λ (n , x₁, …, xₙ) → x₁ ⊕ ⋯ ⊕ xₙ
\end{spec}

Now for any monoid homomorphism |h|, applying induction, yields
\begin{spec}
h₀(x₁ ⊕ ⋯ ⊕ xₙ) = h₀ x₁ ⊕ ⋯ ⊕ h₀ xₙ where h₀ = 𝒰 (h₀, prf) = 𝒰 h
\end{spec}
which is just naturality
\begin{spec}
𝒰 h ∘ fold {M} = fold {N} ∘ 𝒱 h
\end{spec}

\item
{\bf every operation in any multisorted algebraic structure gives a natural
transformation. 
}

This is mentioned in the Barr-wells-ctcs category theory text, citing
[Linton,1969b], [Linton, 1969a].

For example, |src, tgt| ---from the graph signature--- give natural transformations
|V ⟶ E| from the vertex functor to the edge functor |…|

\item
{\bf representability}

Recall that |V(G)| is essentially |ℙ₀ ⟶ G| where
|ℙₙ| is the graph of |n| edges on |n+1| vertices |0..n| with typing |i : i-1 ⟶ i|,
which I like to call `the path graph of length n'; and in particular |ℙ₀| is the the graph of
just one dot, called 0, and no edges. ---earlier I used the notation [n], but I'm using P since
I like the view point of paths.

What does it mean that `|V(G)| is essentially |ℙ₀ ⟶ G|'?
It means that the vertices functor
---|𝒱 : Graph ⟶ Set| takes objects |G ↦ V(G)| and morphisms |h ↦ ver h|---
can be represented as the hom functor |(ℙ₀ ⟶_)|, that is to say
|𝒱 ≅ (ℙ₀ ⟶ _) within Func Graph Set| ---notice that we arrived at this expression by
quote-unqoute eta-reducing `|V(G)| is essentially |ℙ₀ ⟶ G|' ;)

More generally, we have the functor |ℙₙ ⟶ _| which yields all paths of length n for a given
graph.

Observe that we also have an edges functor.
\\
Recall the `untyped edges', or arrows, |A(G) ≔ Σ x ∶ V(G) • Σ y ∶ V(G) • (x ⟶ y)|,
then (arrows) |𝒜 : Graph ⟶ Set| takes objects |G ↦ A(G)| and morphisms
|h ↦ λ (x,y,e) → (ver h x, ver h y, edge h e)|.

\end{enumerate}
%}}}

%{{{ Func
\section{Functor Categories}

With a notion of morphisms between functors, one is led inexorably to ask
whether functors as objects and natural transformations as morphisms constitute
a category?
They do!
However, we leave their definition to the reader ---as usual, if the reader is ever so desperate
for solutions, they can be found as comments in the unruliness that is the source file.
\begin{spec}
  Func : ∀ {i j i' j'} (𝒞 : Category {i} {j}) (𝒟 : Category {i'} {j'}) → Category
  Func 𝒞 𝒟 = {! exercise !}
\end{spec}

A hint: the identity natural transformation is the obvious way to get from |F X| to |F X|,
for any |X| given |F| ---well the only way to do so, without assuming anything else about the
functor |F|, is simply |Id {F X}|. This is the `natural' choice, any other choice would be
`unnatural' as it would require some `cleverness'. Another hint: the obvious way to define
|η ⨾ γ| to get |F X ⟶ H X| from |F X ⟶ G X ⟶ H X| is composition of morphisms in the category!
That is, pointwise composition. Nothing `clever', just using the obvious candidates!

\begin{comment}
\begin{code}
 instance
  Func : ∀ {i j i' j'} (𝒞 : Category {i} {j}) (𝒟 : Category {i'} {j'}) → Category {ℓsuc (i ⊍ j ⊍ i' ⊍ j')} {j' ⊍ i ⊍ j}
  Func {i} {j} {i'} {j'} 𝒞 𝒟 = record {
      Obj = Functor 𝒞 𝒟
    ; _⟶_ = NatTrans
    ; _⨾_ = λ {A B C} η γ → comp {A} {B} {C} η γ
    ; assoc = λ {F G H K η γ ω} → nattransext {i} {j} {i'} {j'} {𝒞} {𝒟} {F} {K} (comp {F} {H} {K} (comp {F} {G} {H} η γ) ω) (comp {F} {G} {K} η (comp {G} {H} {K} γ ω)) assoc
    ; Id = λ {F} → iden F
    ; leftId = λ {F G η} → nattransext {i} {j} {i'} {j'} {𝒞} {𝒟} {F} {G} (comp {F} {F} {G} (iden F) η) η leftId
    ; rightId = λ {F G η} → nattransext {i} {j} {i'} {j'} {𝒞} {𝒟} {F} {G} (comp {F} {G} {G} η (iden G)) η rightId
    }
    where
      iden : (A : Functor 𝒞 𝒟) → NatTrans A A
      iden A = Id , (rightId ⟨≡≡⟩ ≡-sym leftId)

      comp : {A B C : Functor 𝒞 𝒟} → NatTrans A B → NatTrans B C → NatTrans A C
      comp = λ {F G H} ηpf γpf → let (η , nat) = ηpf ; (γ , nat') = γpf in
           (λ {X} → η {X} ⨾ γ {X}) , (λ {A B f} → begin
           mor F f ⨾ η {B} ⨾ γ {B}
          ≡⟨ ≡-sym assoc ⟨≡≡⟩ (≡-cong₂ _⨾_ nat ≡-refl ⟨≡≡⟩ assoc) ⟩
            η {A} ⨾ mor G f ⨾ γ {B}
          ≡⟨ ≡-cong₂ _⨾_ ≡-refl nat' ⟨≡≡⟩ ≡-sym assoc ⟩
            (η {A} ⨾ γ {A}) ⨾ mor H f
          ∎)
\end{code}
\end{comment}
This is a good exercise as it will show you that there is an identity functor and that composition of functors
is again a functor. Consequently, functors are in abundance: given any two, we can form new ones by composition.

It is a common construction that when a type |Y| is endowed with some structure, then we can endow
the function space |X → Y|, where |X| is any type, with the same structure and we do so
`pointwise'. This idea is formalized by functor categories.
Alternatively, one can say we have `categorified' the idea; where
\emph{categorification} is the process of replacing types and functions with categories and
functors and possibly adding some coherence laws.

There are people who adhere to something called `set theory' which is essentialy type theory but
ignoring types, loosely put they work only with the datatype
\begin{spec}
data SET : Set where
  Elem : ∀ {A : Set} → A → SET
\end{spec}
Such heathens delegate types-of-types into `classes' of `small' and `big' sets and it's not
uniform enough for me.
Anyhow, such people would say that functor categories ``cannot be constructed (as sets)'' unless
one of the categories involved is ``small''. Such shenanigans is ignored due to the hierarchy of
types we are using :-)

We must admit that at times the usage of a single type, a `uni-typed theory' if you will can be
used when one wants to relise types in an extrinsic fashion rather than think of data as
intrinsically typed. Everything has its place ... nonetheless, I prefer (multi)typed settings!

%}}}
   %{{{ examples of functor categories

\subsection*{examples of functor categories}

\begin{enumerate}

\item
Let |𝟙 ≔ [ • ]| be the discrete category of one object (and only the identity arrow on it).
Then |𝒞 ≅ Func 𝟙 𝒞|.

\item
For example,
let |𝟚₀ ≔ [• •]| be the discrete category of two objects.
Then square category can be defined |𝒞 ⊗ 𝒞 ∶≅ Func 𝟚₀ 𝒞|.
The subscript 0 is commonly used for matters associated with objects and
the name |𝟚₀| is suggestive of the category of 2 objects only.

More generally, if |𝒩| is the discrete category of |n| objects, then
the |n|-fold product category is defined
|(∏ i ∶ 1..n • 𝒞) ∶≅ Func 𝒩 𝒞|.

\item
We can add an arrow to |𝟚₀| to obtain another category...

Let |𝟚 ≔ • ⟶ •| be the category of two objects, call them 0 and 1, with one arrow between them.
Then a functor |𝟚 ⟶ 𝒞| is precisely a morphism of |𝒞| and a natural transformation
|f ⟶ g| boils down to just a pair of morphisms |(h,k)| with |h ⨾ g = f ⨾ k|.
Hence, `the arrow category of |𝒞|' is
$\mathcal{C}^{\mathbb{2}} := \mathcal{C}^{\to}
$|≔ Func 𝟚 𝒞|:
the category with objects being |𝒞|-morphisms and morphisms being `commutative squares'.

Notice that the notation $\mathcal{C}^{\mathbb{2}}$ is an abbreviation for |Func 𝟚 𝒞| and
as such it is common to write |𝒳 ^ 𝒴 ≔ Func 𝒳 𝒴|.

\item
It is a common heuristic that when one suspects the \emph{possibility} of a category |𝒞|, then one
can make probes to discover its structure. The objects are just functors |𝟙 ⟶ 𝒞| and the
morphisms are just functors |𝟚 ⟶ 𝒞|.

\item

The ``category of presheaves of |𝒞|'' is the category |PSh 𝒞 ≔ Func (𝒞 ᵒᵖ) 𝒮e𝓉|.

This is a pretty awesome category since it allows nearly all constructions in |𝒮e𝓉| to be
realised! Such as subsets, truth values, and even powersets! All these extra goodies make it
a ``topos'' aka ``power allegory'' ---the first is a category that has all finite limits and
a notion of powerset while the second, besides the power part, looks like a totally different beast;
the exhilaration!

\item

The `slice category' of |𝒞| `over' |B : Obj 𝒞| is the category
|𝒞 / B ≔ Σ F ∶ Func 𝟚 𝒞 • (F 1 = B)|. That is, the category of objects being |𝒞|-morphisms with
target |B| and morphisms |f ⟶ g| being |(h,k)| with |h ⨾ g = f ⨾ k| but a natural choice for
|k : B ⟶ B| is |Id| and so we can use morphism type |(f ⟶' g) ≔ Σ h : src f ⟶ src g • h ⨾ g = f|.
This is seen by the observation |(h, k) ∈ f ⟶ g ⇔ h ∈ (f ⨾ k) ⟶' g|; of course a formal
justification is obtained by showing |_⟶_ ≅ _⟶'_ within Func (𝒞 ᵒᵖ ⊗ 𝒞) 𝒮e𝓉|...which I have
not done and so may be spouting gibberish!

\begin{comment}
The isomorphism is witnessed as follows.

To :: |(h,k) : f ⟶ g ⇒ h : (f ⨾ k) ⟶' g|,

from :: |h : f ⟶' g ⇒ (h, Id) : f ⟶ g|.

Rid ::
\begin{spec}
    (h , k) : f ⟶ g
⇒  h : f ⨾ k ⟶' g
⇒ (h, Id) : f ⨾ k ⟶ g
≡ (h , k) : f ⟶ g
\end{spec}
where the equivalence is just
|(h,k) ∈ f ⟶ g ⇔ (h , Id) ∈ (f ⨾ k) ⟶ g|.

Lid ::
\begin{spec}
   h : f ⟶' g
⇒ (h, Id) : f ⟶ g
⇒ h : f ⨾ Id ⟶' g
≡ h : f ⟶' g
\end{spec}

Of course none of this is formal(ly in Agda) and so should be taken with great precaution!
---since it may be all wrong!
\end{comment}

Just as the type |Σ x ∶ X • P x| can be included in the type |X|, by forgetting the second
component, so too the category |Σ F ∶ 𝟚 ⟶ 𝒞 • F 1 ≈ B| can be included into the category
|𝒞| and we say it is a `subcategory' of |𝒞|.

The notation |Σ o ∶ Obj 𝒞 • P o| defines the subcategory of |𝒞| obtained by deleting
all objects not satisfying |P| and deleting all morphisms incident to such objects; i.e.,
the category |𝒟| with
\[|Obj 𝒟 ≡ Σ o ∶ Obj 𝒞 • P o|    \text{ and }   |(o , pf) ⟶𝒟 (o' , pf') ≡ o ⟶𝒞 o'| \]

This is the largest/best/universal subcategory of |𝒞| whose objects satisfy |P|.
\\
Formalize this via a universal property ;)

\item
{\bf Coslices of |𝒮e𝓉| are functor categories}

%% [fibres]
|Func S 𝒮e𝓉 ≅ 𝒮e𝓉 / S|, where S in the left is construed as a discrete category and in the right
is construed as an object of |𝒮e𝓉|.

This is because a functor from a discrete category need only be a function of objects since
there are no non-identity morphisms. Then a functor |f : S ⟶ Set| yields an |S|-targeted
function |(Σ s ∶ S • f s) → S : (λ (s , X) → s)|. Conversely a function |g : X → S| yields
a functor |S ⟶ Set : (λ s → (Σ x ∶ X • g x ≡ s))|.

\begin{comment}
[to]
Indeed a functor |f : S ⟶ Set| is determined by giving a set |f s| for each element |s ∈ S|
---since there are no non-identity morphisms. But this is preceiscely a function |g| with target
|S|! Define |g : (Σ s • f s) → S| that given an element (s,x) returns s.

[from]
Conversley,
given a function |g : X → S| we have a functor |S ⟶ SET| determined by sending each element s
to its preimage under g: |g⁻¹{s} ≔ Σ x • g x ≈ s|, an object of SET.

Rid:
\begin{spec}
given f : S ⟶ Set

  from (to f)
= λ s → Σ x ∈ src(to f) • (to f) x ≈ s
= λ s → Σ s' ∈ S • Σ x' ∈ f s' • (to f) (s', x') ≈ s
= λ s → Σ s' ∈ S • Σ x' ∈ f s' • s' ≈ s
≡⟨ one point rule ⟩
  λ s → Σ x' ∈ f s • ⊤   where ⊤ short for ⊤rue
= λ s → f s              top is unit for products
= f
\end{spec}
some of those equlaities are clearly not equalities!
---maybe simplified if we get rid of those |∈| and rephrase to use the word `type' rather than
`set'!

then the other inverse law...
\end{comment}

Because of this example, |𝒞 / B| can be thought of as `|𝒞|-objects indexed by |B|'.

\item
{\bf Natural transformations as functor categories}

In a similar spirit, we can identify natural transformations as functors!
\[|Func 𝒞 (𝒟 ^ 𝟚) ≅ (Σ F , G ∶ 𝒞 ⟶ 𝒟 • NatTrans F G)|\]

A functor |N : 𝒞 ⟶ 𝒟 ^ 𝟚| gives, for each object |C : Obj 𝒞| an object in |𝒟 ^ 𝟚| which
is precisely an arrow in |𝒟|, type it as |N C : FC ⟶ GC| i.e., |FC = N C 0 , GC = N C 1|,
and for each arrow |f : A ⟶ B in 𝒞| we obtain an arrow |N f : N A ⟶ N B| in |𝒟 ^ 𝟚|
which is precisely a commutative square in |𝒟|,
that is a pair of |𝒟|-arrows |(Ff , Gf) = Nf| with |N A ⨾ Gf = Ff ⨾ N B|.

Extended the notation |Fx| of the previous paragraph to a functor |𝒞 ⟶ 𝒟| by |F x ≔ Fx|; likewise for |G|.
The object and morphism mappings are clear, but what about functoriality? We prove it for both |F, G| together.
\begin{multicols}{2}
\begin{spec}
Identity:

  (F Id , G Id)
≡⟨ definition of F and G ⟩
  N Id
≡⟨ N is a functor ⟩
  Id in 𝒟 ^ 𝟚
≡⟨ identity in arrow categories ⟩
  (Id , Id)
\end{spec}
\columnbreak
\begin{spec}
Composition:

  ( F (f ⨾ g) , G (f ⨾ g) )
≡⟨ definition of F and G ⟩
  N (f ⨾ g)
≡⟨ N is a functor ⟩
  N f ⨾ N g
≡⟨ definition of F and G ⟩
  (Ff, Gf) ⨾ (Fg , Gg)
≡⟨ composition in arrow categories ⟩
  (Ff ⨾ Fg , Gf ⨾ Gg)
≡⟨ definition of F and G as notation ⟩
  (F f ⨾ F g , G f ⨾ G g)
\end{spec}
\end{multicols}

Sweet!

Conversely, given a natural transformation |η : F ⟶ G|
we define a functor |N : 𝒞 ⟶ 𝒟 ^ 𝟚| by sending objects |C| to |η {C} : F C ⟶ G C|, which is an
object is |𝒟 ^ 𝟚|, and sending morphisms |f : A ⟶ B| to pairs |(G f , F f)|, which is a morphism
in |𝒟 ^ 𝟚| due to naturality of |η|: |η {A} ⨾ G f = F f ⨾ η {B}|. It remains to show that |N|
preserves identities and composition.

Now it remains to show that these two processes are inverses and the isomorphism claim is complete. Woah!

Similarly, we can show |Func (𝟚 ⊗ 𝒞) 𝒟 ≅ (Σ F₀ , F₁ ∶ 𝒞 ⟶ 𝒟 • NatTrans F₁ F₂)| by showing
the ``the universal property of exponentiation'' |𝒳 ⟶ (𝒵 ^ 𝒴) ≅ (𝒳 ⊗ 𝒴 ⟶ 𝒵|, or more
directly: to/from direction obtained by setting |H i = Fᵢ| on objects and likewise for morphisms
but with |H(Id, 1) = η| where |1 : 0 ⟶ 1| is the non-identity arrow of |𝟚|.

(Spoilers! Alternatively: |Arr (Func 𝒞 𝒟) ≅ 𝟚 ⟶ 𝒞 ^ 𝒟 ≅ 𝒞 × 𝟚 ⟶ 𝒟| since |𝒞𝒶𝓉| has exponentials,
and so the objects are isomorphic; i.e., natural transformations correspond to functors |𝒞×𝟚⟶𝒟|)

Why are we mentioning this alternative statement? Trivia knowledge of-course!
On a less relevant note, if you're familiar with the theory of stretching-without-tearing,
formally known as topology and it's pretty awesome, then you might've heard of paths and
deformations of paths are known as homotopies and are just continuous functions
|X × I ⟶ Y| for topological spaces |X| and |Y| and |I = [0,1]| is the unit interval.
Letting |𝒥 = 𝟚| be the `categorical interval' we have that functors |𝒞 × 𝒥 ⟶ 𝒟|
are, by the trivia-relevant result, the same as natural transformations.
That is, natural transformations extend the notion of homotopies, or path-deformations.

\item
On \href{http://mathoverflow.net/a/75686/42716}{mathoverflow}, the above is recast succinctly as:
a natural transformation from |F| to |G| is a functor, targeting an arrow category, whose `source'
is |F| and whose `target' is |G|.
\[
  |
    (F ⟶̇ G : 𝒞 ⟶ 𝒟) ≅ Σ η ∶ 𝒞 ⟶ Arr 𝒟 • Src ∘ η = F ∧ Tgt ∘ η = G
  |
\]
Where, the projection functors
\begin{spec}
Src, Tgt                         : Arr 𝒟 ⟶ 𝒟
Src (A₁ , A₂ , f)                = A₁
Src (f  , g  , h₁ , h₂ , proofs) = h₁
\end{spec}
with |Tgt| returning the other indexed items.

\end{enumerate}
%}}}
   %{{{ Graphs as functors
\subsection*{Graphs as functors}

We give an example of a functor by building on our graphs from before.
After showing that graphs correspond to certain functors, we then
mention that the notion of graph-map is nothing more than the the associated
natural transformations!

\begin{code}
 module graphs-as-functors where
\end{code}

Let us construct our formal graph category, which contains the ingredients for
a graph and a category and nothing more than the equations needed of a category.
The main ingredients of a two-sorted graph are two sort-symbols |E, V|, along with
two function-symbols |s, t| from |E| to |V| ---this is also called `the signature
of graphs'. To make this into a category, we need function-symbols |id| and a composition
for which it is a unit.
\begin{code}
  -- formal objects
  data 𝒢₀ : Set where E V : 𝒢₀

  -- formal arrows
  data 𝒢₁ : 𝒢₀ → 𝒢₀ → Set where
     s t : 𝒢₁ E V              
     id  : ∀ {o} → 𝒢₁ o o 

  -- (forward) composition
  fcmp : ∀ {a b c} → 𝒢₁ a b → 𝒢₁ b c → 𝒢₁ a c
  fcmp f id = f
  fcmp id f = f
\end{code}

   %{{{ 𝒢 : Category
Putting it all together,
\begin{code}
  instance
   𝒢 : Category
   𝒢 = record
        { Obj = 𝒢₀
        ; _⟶_ = 𝒢₁
        ; _⨾_ = fcmp
        ; assoc = λ {a b c d f g h} → fcmp-assoc f g h
        ; Id = id
        ; leftId = left-id
        ; rightId = right-id
        }
    where
       -- exercises: prove associativity, left and right unit laws
\end{code}
\begin{comment}
\begin{code}
       -- proofs are just C-c C-a after some casing
       fcmp-assoc : ∀ {a b c d} (f : 𝒢₁ a b) (g : 𝒢₁ b c) (h : 𝒢₁ c d) → fcmp (fcmp f g) h ≡ fcmp f (fcmp g h)
       fcmp-assoc s id id = ≡-refl
       fcmp-assoc t id id = ≡-refl
       fcmp-assoc id s id = ≡-refl
       fcmp-assoc id t id = ≡-refl
       fcmp-assoc id id s = ≡-refl
       fcmp-assoc id id t = ≡-refl
       fcmp-assoc id id id = ≡-refl
       right-id : ∀ {a b} {f : 𝒢₁ a b} → fcmp f id ≡ f
       right-id {f = s} = ≡-refl
       right-id {f = t} = ≡-refl
       right-id {f = id} = ≡-refl
       left-id : ∀ {a b} {f : 𝒢₁ a b} → fcmp id f ≡ f
       left-id {f = s} = ≡-refl
       left-id {f = t} = ≡-refl
       left-id {f = id} = ≡-refl
\end{code}
\end{comment}
%}}}

   %{{{ toFunc : Graph → Functor 𝒢 𝒮e𝓉
Now we can show that every graph |G| gives rise to a functor: a semantics of |𝒢| in |𝒮e𝓉|.
\begin{code}
  toFunc : Graph → Functor 𝒢 𝒮e𝓉
  toFunc G = record { obj = ⟦_⟧₀ ; mor = ⟦_⟧₁ ; id = ≡-refl ; comp = λ {x y z f g} → fcmp-⨾ {x} {y} {z} {f} {g} }
    where
      ⟦_⟧₀ : Obj 𝒢 → Obj 𝒮e𝓉
      ⟦ 𝒢₀.V ⟧₀ = Graph.V G
      ⟦ 𝒢₀.E ⟧₀ = Σ x ∶ Graph.V G • Σ y ∶ Graph.V G • Graph._⟶_ G x y
          
      ⟦_⟧₁ : ∀ {x y : Obj 𝒢} → x ⟶ y → ⟦ x ⟧₀ ⟶ ⟦ y ⟧₀
      ⟦ s ⟧₁ (src , tgt , edg) = src
      ⟦ t ⟧₁ (src , tgt , edg) = tgt
      ⟦ id ⟧₁ x = x

      -- exercise: |fcmp| is realised as functional composition
      fcmp-⨾ : ∀{x y z} {f : 𝒢₁ x y} {g : 𝒢₁ y z} → ⟦ fcmp f g ⟧₁ ≡ ⟦ f ⟧₁ ⨾ ⟦ g ⟧₁
\end{code}
\begin{comment}
\begin{code}
      fcmp-⨾ {f = s} {id} = ≡-refl
      fcmp-⨾ {f = t} {id} = ≡-refl
      fcmp-⨾ {f = id} {s} = ≡-refl
      fcmp-⨾ {f = id} {t} = ≡-refl
      fcmp-⨾ {f = id} {id} = ≡-refl
\end{code}
\end{comment}
%}}}

Conversely, every such functor gives a graph:
\begin{code}
  fromFunc : Functor 𝒢 𝒮e𝓉 → Graph
  fromFunc F = record {
      V    = obj F 𝒢₀.V
    ; _⟶_ = λ x y → Σ e ∶ obj F 𝒢₀.E • src e ≡ x × tgt e ≡ y -- the type of edges whose source is x and target is y
    }
    where tgt src : obj F 𝒢₀.E → obj F 𝒢₀.V
          src = mor F 𝒢₁.s
          tgt = mor F 𝒢₁.t
\end{code}

It is to be noted that we can define ``graphs over |𝒞|'' to be the category |Func 𝒢 𝒞|.
Some consequences are as follows: notion of graph in any category, the notion of graph-map
is the specialisation of natural transformation (!), and most importantly: all the power of functor categories
is avaiable for the study of graphs.

In some circles, you may hear people saying an `algebra over the signature of graphs' is an interpretation
domain (|𝒞|) and an operation (|Functor 𝒢 𝒞|) interpreting the symbols.

We no longer make use of this two-sorted approach to graphs.
%}}}

%{{{ A few categorical constructions :: _⊗_, _ᵒᵖ, Hom, pointwise, Fst, Snd
\section{A few categorical constructions}

We briefly take a pause to look at the theory of category theory.
In particular, we show a pair of constructions to get new categories from old ones,
interpret these constructions from the view of previously mentioned categories, and
discuss how to define the morphism type |_⟶_| on morphisms themselves, thereby
yielding a functor.

%{{{ Opposite
\subsection{Opposite}
The `dual' or `opposite' category |𝒞 ᵒᵖ| is the category constructed from |𝒞| by
reversing arrows: |(A ⟶ B) ≔ (B ⟶ A)| and then necessairly |(f ⨾ g) ≔ g ⨾ f|.
A contravariant functor, or cofunctor, is a functor F from an opposite category and so
there is a reversal of compostions: |F(f ⨾ g) = F g ⨾ F f|.
\begin{spec}
 _ᵒᵖ : ∀ {i j} → Category {i} {j} → Category
 𝒞 ᵒᵖ = {! exercise !}
\end{spec}
\begin{comment}
\begin{code} 
 _ᵒᵖ : ∀ {i j} → Category {i} {j} → Category
 𝒞 ᵒᵖ = record {
      Obj = Obj 𝒞
    ; _⟶_ = λ A B → (B ⟶ A)
    ; _⨾_ = λ f g → (g ⨾ f)
    ; assoc = ≡-sym assoc
    ; Id = Id
    ; leftId = rightId
    ; rightId = leftId
    }
\end{code}
\end{comment}

Notice that |𝒞 ᵒᵖ ᵒᵖ = 𝒞| and |𝒞 ᵒᵖ ≅ 𝒞|
--one may have an intuitive idea of what this isomorphsim means,
but formally it is only meaningful in the context of an ambient category; keep reading ;)

-- we must admit that for categories, the notion of isomorphism is considered less useful
than that of equivalence which weakens the condition of the to-from functors being
inverses to just being naturaly isomorphic to identites. See evil.

Some interpretations:
\begin{itemize}
\item
  |𝒮e𝓉 ᵒᵖ| is usual sets and functions but with `backwards composition':
\begin{code}
 infix 10 _∘_
 _∘_ : ∀ {i j } {{r : Category {i} {j}}} {A B C : Obj r} → B ⟶ C →  A ⟶ B → A ⟶ C
 f ∘ g = g ⨾ f
 \end{code}
 Indeed, we have  |g ⨾ f within 𝒞 = f ∘ g within 𝒞 ᵒᵖ|; which is how these composition operators
 are usually related in informal mathematics (without mention of the ambient categories of-course).

On a more serious note, the opposite of 𝒮e𝓉 is clearly |𝓉e𝒮| haha
---technically for the purposes of this pun we identify the words `opposite' and `reverse'.

\item
  For a discrete category, its opposite is itself.
\item
  For a monoid (viewed as a category), its opposite is itself if the monoid operation is commutative, otherwise
  it is the `dual monoid'.
\item
  For a poset (viewed as a category), its opposite is the `dual poset': |(P, ⊑)ᵒᵖ = (P, ⊒)|.

  In particular, the `least upper bound', or `supremum' in |(P, ⊑)| of two elements
  |x,y| is an element |s| with the `universal property': |∀ z • x ⊑ z ∧ y ⊑ z ≡ s ⊑ z|.
  However, switching |⊑| with |⊒| gives us the notion of `infimum', `greatest upper bound'!
  So any theorems about supremums automatically hold for infimums since the infifum is nothing
  more than the supremum in the dual category of the poset.

  It is not difficult to see that this idea of `2 for the price of 1' for theorems holds for all
  categories.

\item
  What about the category of functors and natural transformations?
\end{itemize}

Speaking of functors, we can change the type of a functor by |ᵒᵖ|-ing its source and target,
while leaving it alone,
\begin{code}
 -- this only changes type
 opify : ∀ {i j i' j'} {𝒞 : Category {i} {j}} {𝒟 : Category {i'} {j'}} → Functor 𝒞 𝒟 → Functor (𝒞 ᵒᵖ) (𝒟 ᵒᵖ)
 opify F = record { obj = obj F ; mor = mor F ; id = Functor.id F ; comp = Functor.comp F }
\end{code}

\begin{quote}
``Category Theory is the |opi|um of the people!'' --- Karl Marx might say it had cats existed in his time
\end{quote}

This two definitions seem to indicate that we have some form of opposite-functor |…| ;)
---keep reading!

|opify| seems to show that |Functor 𝒞 𝒟 ≡ Functor (𝒞 ᵒᵖ) (𝒟 ᵒᵖ)|, or alternatively a
functor can have `two different types' ---this is akin to using the integers as reals
without writing out the inclusion formally, leaving it implicit; however, in the Agda mechanization
everything must be made explicit ---the type system doesn't let you get away with such things.
Professor Maarten Fokkinga has informed me that
the formalization allowing multiple-types is called a
\href{http://maartenfokkinga.github.io/utwente/mmf92b.pdf}{pre-category}.
%}}}
  %{{{ ∂ and ah-yeah
\subsubsection*{ah-yeah: ∂ and dagger categories}

With |𝒞𝒶𝓉| in-hand, we can formalise the opposite, or |∂|ual, functor:
\begin{code}
 ∂ : ∀ {i j} → 𝒞𝒶𝓉 {i} {j} ⟶ 𝒞𝒶𝓉
 ∂ = record { obj = _ᵒᵖ ; mor = opify ; id = ≡-refl ; comp = ≡-refl }
\end{code}

Conjecture: assuming categories are equipped with a contravariant invultionary functor that is identity on objects, we can show that the identity functor is naturally isomorphic to the opposite functor.

\begin{code}
 ah-yeah : ∀ {i j}
     -- identity on objects cofunctor
     → (_˘  : {{𝒞 : Obj (𝒞𝒶𝓉 {i} {j})}} → ∀ {x y : Obj 𝒞} → x ⟶ y → y ⟶ x)
     → (Id˘ : ∀ {𝒞} {x : Obj 𝒞} → (Category.Id 𝒞 {x}) ˘ ≡ Category.Id 𝒞 {x})
     → (⨾-˘ : ∀ {𝒞} {x y z : Obj 𝒞} {f : x ⟶ y} {g : y ⟶ z} → (f ⨾ g)˘ ≡ g ˘ ⨾ f ˘)
     -- which is involutionary
     → (˘˘ : ∀ {𝒞} {x y : Obj 𝒞} {f : x ⟶ y} → (f ˘) ˘ ≡ f)
     -- which is respected by other functors
     → (respect : ∀ {𝒞 𝒟} {F : 𝒞 ⟶ 𝒟} {x y : Obj 𝒞} {f : x ⟶ y} → mor F (f ˘) ≡ (mor F f)˘)
     -- then
     → ∂ ≅ Id within Func (𝒞𝒶𝓉) 𝒞𝒶𝓉
\end{code}
\begin{spec}
 ah-yeah = {! exercise !}
\end{spec}
\begin{comment}
\begin{code}
 ah-yeah {i} {j} _˘ Id˘ ⨾-˘ ˘˘ respect = record {
     to = II
   ; from = JJ
   ; lid = nattransext {𝒞 = 𝒞𝒶𝓉} {𝒞𝒶𝓉} {F = ∂} {G = ∂} (Category._⨾_ 𝒩𝓉 {∂} {Id} {∂} II JJ) (Category.Id 𝒩𝓉 {∂}) (funcext ≡-refl (≡-sym ˘˘))
   ; rid = nattransext {𝒞 = 𝒞𝒶𝓉} {𝒞𝒶𝓉} {F = Id} {G = Id} (Category._⨾_ 𝒩𝓉 {Id} {∂} {Id} JJ II) (Category.Id 𝒩𝓉 {Id}) (funcext ≡-refl (≡-sym ˘˘)) }
           -- we use NatTrans extensionality on category |𝒞𝒶𝓉| on functor |Id| with natural transformations
           -- the LHS starting at Id going to |∂| then to |Id| and the RHS being the natural transformation on the identity functor.
   where
     𝒩𝓉 = Func 𝒞𝒶𝓉 𝒞𝒶𝓉 -- the category of |𝒩|atural transormations as morphisms
     
     I : ∀ {𝒞} → obj ∂ 𝒞 ⟶ 𝒞
     I = λ {𝒞} → record { obj = Id ; mor = _˘ ; id = Id˘ ; comp = ⨾-˘ }
     
     Inat : ∀ {𝒞 𝒟} {F : 𝒞 ⟶ 𝒟} → mor ∂ F ⨾ I ≡ I ⨾ F
     Inat = λ {𝒞 𝒟 F} → funcext {F = mor ∂ F ⨾ I} {G = I ⨾ F}≡-refl (λ {x y f} → respect {𝒞} {𝒟} {F} {y} {x} {f})

     II : NatTrans ∂ Id
     II = I , (λ {𝒞} {𝒟} {F} → Inat {𝒞} {𝒟} {F})

     J : ∀ {𝒞} → 𝒞 ⟶ obj ∂ 𝒞
     J = record { obj = Id ; mor = _˘ ; id = Id˘ ; comp = ⨾-˘ }

     Jnat : ∀ {𝒞 𝒟} {F : 𝒞 ⟶ 𝒟} → J ⨾ mor ∂ F ≡ F ⨾ J
     Jnat = λ {𝒞 𝒟 F} → funcext {F = J ⨾ mor ∂ F} {G = F ⨾ J} ≡-refl (λ {x y f} → ≡-sym (respect {𝒞} {𝒟} {F} {x} {y} {f}))

     JJ : NatTrans Id ∂
     JJ = J , (λ {𝒞} {𝒟} {F} → ≡-sym (Jnat {𝒞} {𝒟} {F}))
\end{code}
\end{comment}

Some things to note.
\begin{itemize}
\item
Categories whose morphisms are all isomorphisms are called `groupoids' ---groups are just one-object groupoids.
Consequently, restricted to groupoids the opposite functor is naturally isomorphic to the identity functor!

In fact, the group case was the motivator for me to conjecture the theorem, which took a while to prove since I hadn't
a clue what I needed to assume. Here we'd use |a ˘ ≔ a ⁻¹|.

\item
Consider the category |Rel| whose objects are sets and whose morphisms are `typed-relations' |(S, R, T)|, where |R| is a relation
from set |S| to set |T|, and
composition is just relational composition
---the notion of `untyped', or multi-typed, morphisms is formalized as pre-categories;
see \href{http://maartenfokkinga.github.io/utwente/mmf92b.pdf}{Fokkinga}.
Then we can define an endofunctor by taking |-˘| to be relational converse: |x (R ˘) y ⇔ y R x|.
Consequently, restricted to the category |Rel| we have that the opposite functor is naturally isomorphic to the identity functor.

\begin{comment}
Indeed, in the proof above, all quantification's become over a unit type: only possibility is
|Rel|.

Then, |I : Rel ᵒᵖ ⟶ Rel| and |J : Rel ⟶ Rel ᵒᵖ|, and the lid-rid proofs amount to saying that
the two are inverses.
\end{comment}
\end{itemize}
%
The above items are instance of a more general concept, of course.

A category with an involutionary contravariant endofunctor that is the identity on objects
is known as \emph{a dagger category, an involutive/star category, or a category with converse}
---and the functor is denoted as a superscript suffix by |†, *, ˘|, respectively.
The dagger notation probably comes from
the hilbert space setting while the converse notation comes from the relation algebra setting.
As far as I know, the first two names are more widely known.
A dagger category bridges the gap between arbitrary categories and groupoids.

Just as matrices with matrix multiplication do not form a monoid but rather a category, we have
that not all matrices are invertible but they all admit transposition and so we have a dagger
category. In the same vein, relations admit converse and so give rise to a category with converse.

Besides relations and groupoids, other examples include
\begin{itemize}
\item
discrete categories with the dagger being the identity functor
\item
every monoid with an anti-involution is trivially a dagger category; e.g.,
lists with involution being reverse.

\item
commutative monoids are anti-involutive monoids with antiinvolution being identity
\end{itemize}

Spoilers!! Just as the category of categories is carestian closed, so too is the category of dagger
categories and dagger preserving functors (cf |respect| above).

%% a pseudoheap is a set H with an binary-operation assining function  ⟨_⟩ : H → (H × H → H)
%% such that
%% mutual-assoc:  (a ⟨b⟩ c) ⟨d⟩ e = a ⟨b⟩ (c ⟨d⟩ e)
%% commutative: x ⟨a⟩ y = y ⟨a⟩ x
%%
%% Every involutive monoid is a pseudoheap:
%% x ⟨y⟩ z ≔ x · y ˘ · z




%}}}

%{{{ Products
\subsection{Products}

For any two categories |𝒞| and |𝒟| we can construct their `product' category
|𝒞 ⊗ 𝒟| whose objects and morphisms are pairs with components from |𝒞| and |𝒟|:
|Obj (𝒞 ⊗ 𝒟) = Obj 𝒞 × Obj 𝒟| and |(A , X) ⟶ (B , Y) = (A ⟶ B) × (X ⟶ Y)|.
\begin{spec}
 -- we cannot overload symbols in Agda and so using |⊗| in-place of more common |×|.
 _⊗_ : ∀ {i j i' j'} → Category {i} {j} → Category {i'} {j'} → Category
 𝒞 ⊗ 𝒟 = {! exercise !}
\end{spec}
\begin{comment}
\begin{code}
 infix 5 _⊗_
 _⊗_ : ∀ {i j i' j'} → Category {i} {j} → Category {i'} {j'} → Category
 𝒞 ⊗ 𝒟 = record {
      Obj = Obj 𝒞 × Obj 𝒟
    ; _⟶_ = λ AX BY → let (A , X) = AX ; (B , Y) = BY in (A ⟶ B) × (X ⟶ Y)
    ; _⨾_ = λ fk gl → let (f , k) = fk ; (g , l) = gl in (f ⨾ g , k ⨾ l)
    ; assoc = helper assoc assoc 
    ; Id = Id , Id
    ; leftId = helper leftId leftId
    ; rightId = helper rightId rightId
    }
    where helper : ∀ {i j} {A : Set i} {B : Set j} {a a' : A} {b b' : B} → a ≡ a' → b ≡ b' → (a , b) ≡ (a' , b')
          helper ≡-refl ≡-refl = ≡-refl
\end{code}
\end{comment}

Observe that in weaker languages a category is specified by its objects, morphisms, and composition
---the proof obligations are delegated to comments, if they are realized at all.
In such settings, one would need to prove that this construction actually reduces a full-fledged
category. Even worse, this proof may be a distance away in some documentation.
With dependent types, our proof obligation is nothing more than another component of the program,
a piece of the category type.

In a similar fashion we can show that the sum of two categories is again a category and in general
we have the same for quantified variants: |𝒬 𝒞 ∶ Family • 𝒞| where |𝒬 ∈ { ∏ , Σ }|.
For the empty family, the empty sum yields the category |𝟘| with no objects and
the empty sum yields the category |𝟙| of one object.
One can then show the usual `laws of arithmetic' ---i.e., an commutative monoid---
hold in this setting: letting |★ ∈ {+,×}|, we have
associtivity |A ★ (B ★ C) ≅ (A ★ B) ★ C|, symmetry |A ★ B ≅ B ★ A|,
unit |𝟙 × A ≅ 𝟘 + A ≅ A|, and zero |𝟘 × A ≅ 𝟘|.
These notions can be defined for any category though the objects may or may not exist
--- in |𝒮e𝓉| and |𝒢𝓇𝒶𝓅𝒽|, for example, they do exist ;) ---and these associated arithmetical
laws also hold.

Question: what of the distributively law,
|A × (B + C) ≅ (A × B) + (A × C)|, does it hold in the mentioned cases?
Let |𝒫𝒮e𝓉| be the category of sets with a distinguished point, i.e.,  |Σ S : Obj 𝒮e𝓉 • S|, and
functions that preserve the `point', one can then show ---if he or she so desires, and is not
lazy--- that this category has notions of product and sum but distributively fails.

Some interpretations:
\begin{itemize}
\item
  For discrete categories, this is the usual Cartesian product.
\item
  For monoid (or poset) categories, this says that the product of two monoids (or posets) is again
  a monoid (respectively poset. This follows since the product does not affect the number of
  objects and so the product is again a one-object category, i.e., a monoid (poset respectively).
\end{itemize}

As expected, we have projections,
\begin{code}
 Fst : ∀ {i j i' j'} {𝒞 : Category {i} {j}} {𝒟 : Category {i'} {j'}} → Functor (𝒞 ⊗ 𝒟) 𝒞
 Fst = record { obj = proj₁ ; mor = proj₁ ; id = ≡-refl ; comp = ≡-refl }

 Snd : ∀ {i j i' j'} {𝒞 : Category {i} {j}} {𝒟 : Category {i'} {j'}} → Functor (𝒞 ⊗ 𝒟) 𝒟
 Snd = record { obj = proj₂ ; mor = proj₂ ; id = ≡-refl ; comp = ≡-refl }
\end{code}

%}}}
  %{{{ currying
\subsubsection*{Currying}

For types we have |(𝒳 × 𝒴 ⟶ 𝒵) ≅ (𝒳 ⟶ 𝒵 ^ 𝒴) ≅ (𝒴 ⟶ 𝒵 ^ 𝒳)|, and categories are
essentially types endowed with nifty structure,
and so we expect it to hold in that context as well.
\begin{spec}
  -- Everyone usually proves currying in the first argument,
  -- let's rebel and do so for the second argument
 curry₂ : ∀ {ix jx iy jy iz jz}
          {𝒳 : Category {ix} {jx}} {𝒴 : Category {iy} {jy}} {𝒵 : Category {iz} {jz}}
        → Functor (𝒳 ⊗ 𝒴) 𝒵 → Functor 𝒴 (Func 𝒳 𝒵)
 curry₂ = {! exercise !}
\end{spec}
\begin{comment}
\begin{code}
 curry₂ : ∀ {ix jx iy jy iz jz} {𝒳 : Category {ix} {jx}} {𝒴 : Category {iy} {jy}} {𝒵 : Category {iz} {jz}}
         → Functor (𝒳 ⊗ 𝒴) 𝒵 → Functor 𝒴 (Func 𝒳 𝒵)
 curry₂ {𝒳 = 𝒳} {𝒴} {𝒵} F = record {
    obj = funcify
  ; mor = natify
  ; id = λ {x} → nattransext {F = funcify x} {funcify x} (natify (Id {A = x})) (Category.Id (Func 𝒳 𝒵) {A = funcify x}) (Functor.id F)
  ; comp = λ {x y z f g} → nattransext {F = funcify x} {funcify z} (natify (f ⨾ g)) ( Category._⨾_ (Func 𝒳 𝒵) {A = funcify x} {B = funcify y} {C = funcify z} (natify f) (natify g) ) (begin
             mor F (Id , f ⨾ g)
           ≡⟨ ≡-cong (λ e → mor F (e , f ⨾ g)) (≡-sym rightId) ⟩
             mor F (Id ⨾ Id , f ⨾ g)
           ≡⟨ functors-preserve-composition ⟩
             mor F (Id , f) ⨾ mor F (Id , g)
           ∎) }
  where funcify : (y : Obj 𝒴) → Functor 𝒳 𝒵
        funcify = λ Y → record {
            obj = λ X → obj F (X , Y)
          ; mor = λ f → mor F (f , Id {A = Y})
          ; id = Functor.id F
          ; comp = λ {x y z f g} → begin
                mor F (f ⨾ g , Id)
              ≡⟨ ≡-cong (λ x → mor F (f ⨾ g , x)) (≡-sym rightId) ⟩
                mor F (f ⨾ g , Id ⨾ Id)
              ≡⟨ Functor.comp F ⟩
                mor F (f , Id) ⨾ mor F (g , Id)
              ∎ }
        
        natify : {x y : Obj 𝒴} → x ⟶ y → NatTrans (funcify x) (funcify y)
        natify = λ {x y} f → (λ {z} → mor F (Id {A = z} , f)) , (λ {a b g} → begin
                mor F (g , Id) ⨾ mor F (Id , f)
              ≡⟨ ≡-sym functors-preserve-composition ⟩
                mor F (g ⨾ Id , Id ⨾ f)
              ≡⟨ ≡-cong₂ (λ x y → mor F (x , y)) rightId leftId ⟩
                mor F (g , f)
              ≡⟨ ≡-sym (≡-cong₂ (λ x y → mor F (x , y)) leftId rightId) ⟩
                mor F (Id ⨾ g , f ⨾ Id)
              ≡⟨ functors-preserve-composition ⟩
                mor F (Id , f) ⨾ mor F (g , Id)
              ∎)
\end{code}
\end{comment}
%}}}

%{{{ pointwise
\subsection{Pointwise extensions and the hom functor}
Just as addition can be extended to number-valued functions pointwise |f + g ≔ λ x → f x + g x|,
we can do the same thing with functors.
\begin{spec}
 -- for bifunctor |_⊕_| and functors |F, G| we have a functor |x ↦ F x ⊕ G x|
 pointwise : ∀ {ic jc id jd ix jx iy jy}
   {𝒞 : Category {ic} {jc}} {𝒟 : Category {id} {jd}} {𝒳 : Category {ix} {jx}} {𝒴 : Category {iy} {jy}}
   → Functor (𝒳 ⊗ 𝒴) 𝒟 → Functor 𝒞 𝒳 → Functor 𝒞 𝒴
   → Functor 𝒞 𝒟
 pointwise = {! exercise !}
\end{spec}
\begin{comment}
\begin{code}
 pointwise : ∀ {ic jc id jd ix jx iy jy} {𝒞 : Category {ic} {jc}} {𝒟 : Category {id} {jd}}
   {𝒳 : Category {ix} {jx}} {𝒴 : Category {iy} {jy}}
   (_⊕_ : Functor (𝒳 ⊗ 𝒴) 𝒟) (F : Functor 𝒞 𝒳) (G : Functor 𝒞 𝒴) → Functor 𝒞 𝒟
 pointwise Bi F G = record {
     obj = λ C → obj Bi (obj F C , obj G C)
   ; mor = λ {x y} x→y → mor Bi (mor F x→y , mor G x→y)
   ; id = λ {x} → begin
          mor Bi (mor F Id , mor G Id)
        ≡⟨ ≡-cong₂ (λ f g → mor Bi (f , g)) (Functor.id F) (Functor.id G) ⟩
          mor Bi (Id , Id)
        ≡⟨ functors-preserve-identities ⟩
          Id
        ∎
   ; comp = λ {x y z x⟶y y⟶z} → begin
       mor Bi (mor F (x⟶y ⨾ y⟶z) , mor G (x⟶y ⨾ y⟶z))
     ≡⟨ ≡-cong₂ (λ f g → mor Bi (f , g)) (Functor.comp F) (Functor.comp G) ⟩
       mor Bi (mor F x⟶y ⨾ mor F y⟶z , mor G x⟶y ⨾ mor G y⟶z)
     ≡⟨ functors-preserve-composition ⟩
      (mor Bi (mor F x⟶y , mor G x⟶y)) ⨾ (mor Bi (mor F y⟶z , mor G y⟶z))
     ∎ }
\end{code}
\end{comment}

Since |p ≡ (proj₁ p , proj₂ p)|, we have that the pointwise extension along the projections
is the orginal operation.
\begin{code} 
 exempli-gratia : ∀ {𝒞 𝒟 𝒳 𝒴 : Category {ℓ₀} {ℓ₀}} (⊕ : Functor (𝒳 ⊗ 𝒴) 𝒟)
                → let _⟨⊕⟩_ = pointwise ⊕
                   in
                      Fst ⟨⊕⟩ Snd ≡ ⊕
 exempli-gratia Bi = funcext (≡-cong (obj Bi) ≡-refl) (≡-cong (mor Bi) ≡-refl)
\end{code}

An example bifunctor is obtained by extending the --> to morphisms:
given |f : A ⟶ B , g : C ⟶ D| we define |(f ⟶ g) : (B ⟶ C) → (A ⟶ C)| by
|λ h → f ⨾ h ⨾ g| as this is the only way to define it so as to meet the type requirements.
\begin{code}
 Hom : ∀ {i j} {𝒞 : Category {i} {j} } → Functor (𝒞 ᵒᵖ ⊗ 𝒞) 𝒮e𝓉
   -- hence contravariant in `first arg' and covaraint in `second arg'
 Hom {i} {j} {𝒞} = record {
     obj = λ AB → let (A , B) = AB in A ⟶ B
   ; mor = λ fg → let (f , g) = fg in λ h → f ⨾ h ⨾ g
   ; id = λ {AB} → let (A , B) = AB in extensionality (λ {h} → begin
        Id ⨾ h ⨾ Id
      ≡⟨ leftId ⟩
        h ⨾ Id
      ≡⟨ rightId ⟩
        h
      ∎)
   ; comp = λ {x y z fg fg'} → let (f , g) = fg ; (f' , g') = fg' in extensionality (λ {h} → begin
            (f' ⨾ f) ⨾ h ⨾ (g ⨾ g')
          ≡⟨ assoc ⟩
            f' ⨾ (f ⨾ (h ⨾ (g ⨾ g')))
          ≡⟨ ⨾-cong₂ (≡-sym assoc) ⟩
            f' ⨾ ((f ⨾ h) ⨾ (g ⨾ g'))
          ≡⟨ ⨾-cong₂ (≡-sym assoc) ⟩
            f' ⨾ ((f ⨾ h) ⨾ g) ⨾ g'
          ≡⟨ ⨾-cong₂ (≡-cong₂ _⨾_ assoc ≡-refl) ⟩
            f' ⨾ (f ⨾ h ⨾ g) ⨾ g'
          ∎)}
            where
              ⨾-cong₂ : ∀ {A B C : Obj 𝒞} {f : A ⟶ B} {g g' : B ⟶ C} → g ≡ g' → f ⨾ g ≡ f ⨾ g'
              ⨾-cong₂ = ≡-cong₂ _⨾_ ≡-refl
\end{code}
The naming probably comes from the algebra/monoid case where the functors are
monoid |hom|omorphisms. Some prefer to use the name |Mor|, short for |mor|phisms,
and that's cool too. While Haskell programmers might call this the |Reader| functor.

Usual notation for this functor is |Hom|, but I like Fokkinga's much better.
He uses |(_⟶_)| and writes |(f ⟶ g) = λ h • f ⨾ h ⨾ g|
---the first argument of Hom is the first argument of the composition and the last
argument to Hom is the last argument of the resulting composition :-)

%}}}

%}}}

%{{{ Simplicity |𝒰|nderlies complexity
\section{Simplicity |𝒰|nderlies complexity}

\begin{quote}
``One way is to make it so |𝒮|imple that there are obviously no deficiencies, and the other way is to
make it so |𝒞|omplicated that there are no obvious deficiencies. The first method is far more
difficult. It demands the same skill, devotion, insight, and even inspiration as the discovery of
the simple physical laws which |𝒰|nderlie the complex phenomena of nature.''
---\href{https://en.wikiquote.org/wiki/C._A._R._Hoare}{C.A.R. Hoare}
\end{quote}

%% The complex philosophy behinds games such as Chess and
%% \href{http://playgo.to/iwtg/en/}{Go} arise from some simple board game rules.

In this section we discuss what it means to be a `forgetful functor'?

The modifier `forgetful' is meaningful when there's a notion of extra structure.
Indeed any functor |F : 𝒞 ⟶ 𝒮| can be thought of as forgetful by construing the objects of
|𝒞| as objects of |𝒮| with extra structure.
Mostly: \emph{you know it (to be forgetful) when you see it.}

%{{{ From injections to faithful functors}
\subsection{Being forgetful: from injections to faithful functors}

A common example from set theory is the `inclusion' of a subset |A| of |B|, the injection
|ι : A ↪ B : a ↦ a| ---it is essentially a form of `type casting': |a ∈ A| and |ι a = a ∈ B|.
Such injections `forget' the property that the argument is actually a member of a specified
subset. Indeed, construing sets as categories then functions becomes functors and inclusions
are then forgetful functors!

Since a functor consists of two maps and some properties, we can speak about properties of the
functor and about properties of either of its maps.
The common definitions are a functor |F| is |faithful| if its operation
on morphisms is |injective, and it is |full| if morphisms starting and ending at |F| are a result
of applying |F|; i.e., |F₁| is surjective \emph{on} the image of |F₀|:
|∀ x,y ∶ Obj • ∀ g ∶ F₀ x ⟶ F₀ y • ∃ f ∶ x ⟶ y • F₁ f = g|.

Now we can generalize the previous example.
Every faithful functor |F : 𝒞 ⟶ 𝒟| can be construed as forgetful:
the |𝒞|-maps can be embedded into the |𝒟|-maps, since |F| is faithful, and so can be thought of
as a special sub-collection of the |𝒟|-maps; then |F| `forgets' the property of being in this
special sub-collection.


Are faithful functors in abundance? Well any functor forgetting only axioms
(and/or structure) is faithful:

  suppose |𝒞| consists of |𝒟| objects satisfying some axioms and |𝒟| maps preserving this structure:
  that is, pairs of an object/morphism with a proof that it satisfies the axioms/preserves-structure.
  Then |F : 𝒞 ⟶ 𝒟| forgets only axioms means |F (f, proof) = f|.
  Now given, |F (f , prf) = F (g , prf) ⇔ f ≡ g ⇔ (f , prf) = (g , prf)| -- equality does not (extensionally) depend on proof components.
  Hence, faithful :-)

  (Likewise for forgetting extra structure).

Of course we're not saying all forgetful functors are necessarily faithful.
A simple counterexample is the absolute value function:
given a real number |x| it's absolute value |∣x∣| is obtained by totally ignoring its sign
---of course |x| and |∣x∣| are equidistant from the 0 and equidistant-from-0 is an equivalence
relation and so the the two are isomorphic in some sense.
%
\begin{comment}
E is an equivalence, where x E y ≡ ∣ x - y ∣ = 0

Refl: x E x ⇐ ∣ x - x ∣ = 0 ⇐ ⊤
Sym:  x E y ⇒ ∣x - y∣ = 0 ⇒ ∣y - x∣ = 0 ⇒ y E x
Trans: x E y E z ⇒ ∣x - y∣ = ∣y - z∣ = 0 ⇒ ∣x - z∣ = ∣x - y + y - z∣ ≤ ∣x - y∣ + ∣y - z∣ = 0 + 0 = 0
⇒ x E z

A simpler definition of E is x E y ≡ ∣x∣ = ∣y∣
and that is the kernel of the absolute function and so an equivalence.
\end{comment}
%
Motivated by this, given a set |S| it's size is denoted |∣ S ∣| which totally forgets about the
elements of the set ---of course it can be shown that two sets are isomorphic precisely if they are
equinumerous.
%
I assume it is with these as motivators, some people write |∣·∣| for a forgetful functor.

%}}}

\noindent\rule[0.5ex]{\linewidth}{1pt}

%{{{ Of basis vectors
\subsection{Of basis vectors}
If you've ever studied abstract algebra ---the math with vector spaces--- then you may recall that
a collection of vectors |B| is called a `basis' if every vector can be written as a linear
combination of these vectors: for any vector |v|, there are scalars |c₁, …, cₙ| and vectors
|b₁, …, bₙ| in |B| with |v = c₁·b₁ + ⋯ + cₙ·bₙ|. That is, a basis is a collection of `building
blocks' for the vector space. Then any function |f| between basis sets immediately lifts to a
linear transformation (think vector space morphism) |F| as follows: given a vector |v|, since we
have a basis, we can express it as |c₁·b₁ + ⋯ + cₙ·bₙ|, now define
|F v ≔ c₁·(f b₁) + ⋯ + cₙ·(f bₙ)|. Sweet! To define a complicated linear transformation of vector
spaces, it more than suffices to define a plain old simple function of basis sets.
Moreover, by definition, such |F| maps basis vectors to basis vectors: |f = ι ⨾ F| where
|ι : B ↪ V| is the inclusion that realises basis vectors as just usual vectors in the vector
space.  Slogan: vector space maps are determined by where they send their basis, and basis-vectors
are preserved.

In the case of |(List A, ++, [])| we may consider |A| to be a `basis' of the monoid ---indeed,
every list can be written as a linear combination of elements of |A|, given list
|[x₁, …, xₙ] : List A| we have |[x₁, …, xₙ] = x₁ + ⋯ + xₙ| where |x + y ≔ [x] ++ [y]|.
As similar reasoning as above, or if you have familarity with |foldr|/|reduce|, we have a slogan:
monoid homomorphisms from lists are determined by where they send their basis and basis-vectors
are preserved.

Now the general case, suppose |F ⊣ U| is a free-forgetful adjunction with |U : 𝒞 ⟶ 𝒮|; that is,
given a simple-object |S| there's simple-map |ι : S ⟶ U(F S)| ---a way to realise `basis
vectors'--- such that for any complicated-object |C| and simple-maps |φ : S ⟶ U C|, there is a
unique complicated-map |Φ : F S ⟶ C| that preserves the basis vectors: |φ = ι ⨾ U Φ|.
By analogy to the previous two cases, we may
consdier |U X| to be a `basis', and make the slogan: complicated-maps from free objects are
determined by where they send their basis and `basis vectors' are preserved.

[

In more categorical lingo, one says |ι| is the `insertion of generators'.

Question: does the way we took |ι| in the previous graph show that it is a natural
transformation |ι  : Id ⟶ F ⨾ U| ?
---the naturality just says that a `homomorphism' |F f| on the free object is completely detemined
by what |f| does to the generators.

]
%}}}

%{{{ Of adjunctions
\subsection{Of adjunctions}

An adjunction |L ⊣ U|, where the |L|ower adjoint is from |𝒮| to |𝒞| and the |U|pper adjoint is in
the opposite direction, lends itself to an elemntary interpretation if we consider
|𝒞| to be some universe of |𝒞|omplicated items of study, while |𝒮| to be a universe of |𝒮|imple
items of study. Then adjointness implies that given a simple-object |S| and a complicated-object
|C|, a simple-map |X ⟶ U C| corresponds to a complicated-map |L S ⟶ C|. To work with
complicated-maps it is more than enough to work with simple-maps!

|now we say F : 𝒞 ⟶ 𝒟 is adjoint to G : 𝒟 ⟶ 𝒞, written F ⊣ G, iff|
| (F ∘ X ⟶₁ Y) ≅ (X ⟶₁ G ∘ Y)  | (in FUNC)
\begin{code}
 _⊣₀_ : ∀ {i j} {𝒞 𝒟 : Category {i} {j}} → Functor 𝒞 𝒟 → Functor 𝒟 𝒞 → Set (i ⊍ j)
 _⊣₀_ {𝒞 = 𝒞} {𝒟} F G =
                           (F ′ ∘ X  ⟶ₙₐₜ Y) ≅ (X ⟶ₙₐₜ G ∘ Y) within Func (𝒞 ᵒᵖ ⊗ 𝒟) 𝒮e𝓉
   where
     X = Fst ; Y = Snd ; _′ = opify -- only changes types
          
     infix 5 _⟶ₙₐₜ_
     _⟶ₙₐₜ_ : ∀ {i j} {𝒜 : Category {i} {j}} →
            Functor (𝒞 ᵒᵖ ⊗ 𝒟) (𝒜 ᵒᵖ) → Functor (𝒞 ᵒᵖ ⊗ 𝒟) 𝒜 → Functor (𝒞 ᵒᵖ ⊗ 𝒟) 𝒮e𝓉
     _⟶ₙₐₜ_ = λ {i j 𝒜} → pointwise (Hom {i} {j} {𝒜})
\end{code}
Note that if we use the built-in rewrite mechanism by adding
|{𝒞 𝒟 : Category {ℓ₀} {ℓ₀}} → Functor (𝒞 ᵒᵖ) (𝒟 ᵒᵖ) ≡ Functor 𝒞 𝒟| as a rewrite rule, then
might can get away without using |opify|.

We want to say for any objects |X, Y|, the collection of morphisms |(F A ⟶ B)| is isomorphic
to the collection |(A ⟶ G B)| and naturally so in |A| and |B|.

Unfolding it, we have
\begin{code}
 record _⊣_ {i j i' j'} {𝒞 : Category {i} {j}} {𝒟 : Category {i'} {j'}} (F : Functor 𝒞 𝒟) (G : Functor 𝒟 𝒞)
   : Set (j' ⊍ i' ⊍ j ⊍ i) where
   field
     ⌊_⌋ : ∀ {X Y} → obj F X ⟶ Y → X ⟶ obj G Y -- left-adjunct , |L ≈ ⌊|
     ⌈_⌉ : ∀ {X Y} → X ⟶ obj G Y → obj F X ⟶ Y -- right-adjunct, |r ≈ ⌈|
     lid : ∀ {X Y} {d : obj F X ⟶ Y} → ⌈ ⌊ d ⌋ ⌉ ≡ d
     rid : ∀ {X Y} {c : X ⟶ obj G Y} → ⌊ ⌈ c ⌉ ⌋ ≡ c
     lfusion : ∀ {A B C D} {f : A ⟶ B} {ψ : obj F B ⟶ C} {g : C ⟶ D}
             → ⌊ mor F f ⨾ ψ ⨾ g ⌋ ≡ f ⨾ ⌊ ψ ⌋ ⨾ mor G g
     rfusion : ∀ {A B C D} {f : A ⟶ B} {ψ : B ⟶ obj G C} {g : C ⟶ D}
             → ⌈ f ⨾ ψ ⨾ mor G g ⌉ ≡ mor F f ⨾ ⌈ ψ ⌉ ⨾ g
\end{code}
This is easier for verifying an adjunction, while the former is easier for remembering and understanding what an adjunction actually it.

\begin{spec}
  Hom : {𝒞 : Category {ℓ₀} {ℓ₀} } → Functor (𝒞 ᵒᵖ ⊗ 𝒞) 𝒮e𝓉
  Y : ∀ {𝒞 𝒟} → Functor (𝒞 ⊗ 𝒟) 𝒟
  X : ∀ {𝒞 𝒟} → Functor (𝒞 ⊗ 𝒟) 𝒞
   pointwise : ∀ {𝒞 𝒳 𝒴 : Category {ℓ₀} {ℓ₀}} {i j} {𝒟 : Category {i} {j}}
   (_⊕_ : Functor (𝒳 ⊗ 𝒴) 𝒟) (F : Functor 𝒞 𝒳) (G : Functor 𝒞 𝒴) → Functor 𝒞 𝒟

  Hom {𝒜} : 𝒜 ᵒᵖ ⊗ 𝒜 ⟶ 𝒮e𝓉
  F : 𝒞 ᵒᵖ ⟶ 𝒟
  X : 𝒞 ᵒᵖ × 𝒟 ⟶ 𝒞 ᵒᵖ
  X ⨾ F : 𝒞 ᵒᵖ × 𝒟 ⟶ 𝒟
  Y : 𝒞 ᵒᵖ × 𝒟 ⟶ 𝒟
\end{spec}


As the slogan goes `adjunctions are everywhere'.
They can be said to capture the notions of optimization and efficiency, but also that of simplicity.

For example, the supremum of a function is defined to be an upper bound of its image set and the least such bound.
Formally, this definition carries a few quantifiers and so a bit length.
More elegantly, we can say the supremum operation is left-adjoint to the constant function: |sup ⊣ K|
which means |∀ z • sup f ≤ z ⇔ f ≤ K z| where |K x y = x| and the |≤| on the right is point-wise.
This formulation of supremum is not only shorter to write but easier to use in calculational proofs.

For the efficiency bit, recall that it is efficient to specify a |𝒮|imple-map, then use the adjuction, to obtain
a |𝒞|omplicated-map. Recall in the last paragraph how we define the super complicated notion of supremum of a function
in terms of the most elementary constant function!

Adjunctions over poset categories are called `Galois connections' and a good wealth of
material on them can be found in nearly any writing by \href{http://www.cs.nott.ac.uk/~psarb2/papers/papers.html}{Backhouse et al},
while a very accessible introduction is by \href{http://www.cs.nott.ac.uk/~psarb2/MPC/galois.ps.gz}{Aarts},
and there is also an Agda mechanisation by \href{http://relmics.mcmaster.ca/RATH-Agda/AContext-2.1.pdf}{Kahl et al}.

Regarding forgetful functors:
generally, but not always, forgetful functors are faithful and have left adjoints
---because the notion of `forget' ought to have a corresponding notion of `free'.
An exception to this is the category of fields, which has a forgetful functor to the
category of sets with no left adjoint. [Source: wikipedia]

%}}}

\subsection{adjunction examples}
\begin{enumerate}

\item
Another awesome thing about adjunctions |L ⊣ U| is that they give us `representable functors',
aka `the best kind of functors', when terminal objects exist.
An object |𝟙| is `terminal' if
for any object |A| there is a unique morphism |! {A} : A ⟶ 𝟙|. In |𝒮e𝓉| we have
|(A ⟶ 𝟙) ≅ 𝟙| and |(𝟙 ⟶ A) ≅ A|. Specializing the adjunction, where |U : 𝒞 ⟶ 𝒮e𝓉|, to
a given set |A| and |𝟙| we obtain |(L 𝟙 ⟶ A) ≅ (𝟙 ⟶ U A) ≅ U A| and so one says
`|U| is represented by |L 𝟙|'. In particular, if |𝒞| is built on |𝒮e𝓉| by adding some structure
and we are interested in the utilising the elements of an object |A| then it suffices to utilise
the maps |L 𝟙 ⟶ A|.

\item
In the case of a free-forgetful adjunction, this says that a forgetful functor is represented by the
free object with generator |𝟙|.

\item
For example, for monoids we have |𝟙 ≔ ({*}, ⊕, *), x ⊕ y ≔ ⋆| is the one-point monoid.
Then every monoi-homomorpgism from |𝟙| just picks out an element of the carrier of a monoid and so
|(𝟙 ⟶ M) ≅ 𝒰 M| where |𝒰| is the forgetful functors for monoids mentioned in the introduction.

\end{enumerate}

%{{{ Concluding remarks
\subsection{Concluding remarks}
A final note about free objects ---arising from an adjoint to a forgetful functor.

``The free object is generic'': the only truths provable for the free
object are precisely those that hold for every complicated-object.

(Begin squinting eyes)

This follows from the
definition of adjunction which says we can construct a unique morphism between complicated-objects
from a simple-map and by naturality we may transport any proof for the free object to any
complicated object.

(Feel `free' to stop squinting your eyes)



%}}}

For futher reading consider reading the adjoint article at
\href{http://www.comicbooklibrary.org/articles/Left_adjoint}{the combic book library}
and for more on the adjective forgetful see
\href{https://ncatlab.org/nlab/show/forgetful+functor}{ncatlab}
or \href{http://mathworld.wolfram.com/ForgetfulFunctor.html}{mathworld}
A nice list of common free objects can be found
\href{https://en.wikipedia.org/wiki/Free_object#List_of_free_objects}{here}.

|⟦|
Challenge; true or false: for forgetful |U : 𝒞 ⟶ 𝒮e𝓉| a free functor exists when |𝒞| is a
monad over |𝒮e𝓉|?
|⟧|

You might be asking, `musa, when am I ever going to encounter this in daily life?
In a popular setting?' This concept is everywhere, even inclusions as mentioned earlier are an
instance. For the second question, enjoy
\href{https://www.youtube.com/watch?v=BipvGD-LCjU}{this lovely group} --they use the words `forgetful functors' ;)

The remainder of this document can be seen as one fully-worked out example of constructing a
free functor for the forgetful |𝒰| defined above.

%}}}

\begin{comment}
%{{{ ex. Free first-order logics
\subsection{Free first-order logics}
Free first-order logics.
\begin{code}
module RSD where

  data 𝟙 : Set where ⋆ : 𝟙

  open import Data.Vec renaming (_∷_ to _,,_) -- , already in use for products :/

  data Term (𝒮 : Signature) (Carrier : Set) (Var : Set) : Set where
    var : Var → Term 𝒮 Carrier Var
    con : Carrier → Term 𝒮 Carrier Var
    app : (i : Fin 𝒩) → Vec (Term 𝒮 Carrier Var) (lookup i ar) → Term 𝒮 Carrier Var
    -- |app i [t₁, …, tₖ]| read as: apply i-th function-symbol |fᵢ| to |k = arity (fᵢ)| terms |t₁, …, tₖ|

  infix 10 _≈_
  _≈_ : {A B : Set} → A → B → A × B
  _≈_ = _,_
  
  record Logic (𝒮 : Signature) (Carrier : Set) (Var : Set) : Set where
    field
      #Eqns : ℕ
      eqns : Vec ((Term 𝒮 Carrier Var) × (Term 𝒮 Carrier Var)) #Eqns

  -- use integers as varaibles
  MyVars = ℕ
  x y z : MyVars
  x = 0
  y = 1
  z = 2
  -- alternative is to parameterise module by a universe of variables.

  MonoidThry : {X : Set} → Logic MonSig X MyVars
  MonoidThry {X} = record { #Eqns = 3 ;
    eqns = ε · var x ≈ var x
      ,, var x · ε ≈ var x
      ,, (var x · var y) · var z ≈ var x · (var y · var z)
      ,, [] }
    where
      -- the function symbols
      u = fromℕ≤ {0} {2} (s≤s z≤n)
      m = fromℕ≤ {1} {2} (s≤s (s≤s z≤n))

      -- conventional monoid notation
      ε : Term MonSig X MyVars
      ε = app u []
      _·_ : (l r : Term MonSig X MyVars) → Term MonSig X MyVars
      _·_ = λ l r → app m (l ,, r ,, [])
\end{code}
%}}}
\end{comment}


\part{solving the initial goal}

%{{{ Path definition
\section{Path definition}
We can now define a `path' of length |n| of a graph |G| to be a graph-map
|[ n ] ⟶ G|.
\begin{code}
Path₀ : ℕ → Graph₀ → Set (ℓsuc ℓ₀)
Path₀ n G = [ n ]₀ 𝒢⟶₀ G
\end{code}
Unfolding the definition of graph-morphisms, this just says that a path of length |n|
BACKEND-ERROR %% consists of a sequence |⟨v_0, v_1, v_2,  …, v_n⟩| of vertices of |G| and a sequence of edges of
BACKEND-ERROR %% |G| |⟨e_0, e_1, …, e_n-1⟩| with typing |e_i : v_i ⟶ v_(i+1)|.

The definition is pretty slick! However, as the name suggests, perhaps we can concatenate paths
and it's not at all clear how to do this for the vertex- and edge- morphisms of the graph-maps
involved, whereas it's immediately clear how to do this with sequences: we just concatenate the
sequences and ensure the result is coherent.

Since the vertices can be obtained from the edges via |src| and |tgt|, we can dispense with them
and use the definition as outlined above by
\begin{code}
open import Data.Vec using (Vec ; lookup)

record Path₁ (n : ℕ) (G : Graph₀) : Set (ℓsuc ℓ₀) where
  open Graph₀
  field
    edges     : Vec (E G) (suc n)
    coherency : {i : Fin n} → tgt G (lookup (` i) edges) ≡ src G (lookup (fsuc i) edges)
\end{code}
That is, edges BACKEND-ERROR %% |[e_0, …, e_n]| with coherency |tgt e_i ≡ src e_i+1|.

Great, we've cut the definition of |Path₀| in half but that fact that we get a raw list of edges
and then need coherency to ensure that it is a well-formed path is still not terribly lovely.
After all, we're in Agda, we're among kings, we should be able to form the list in such a way that
the end result is a path. Let's do that!

Enough of this repetition, let us fix a graph |G|,
\begin{code}
module Path-definition-2 (G : Graph₀) where
  open Graph₀ G

  mutual
    data Path₂ : Set where
      _!   : V → Path₂
      cons : (v : V) (e : E) (ps : Path₂) (s : v ≡ src e) (t : tgt e ≡ head₂ ps) → Path₂

    head₂ : Path₂ → V
    head₂ (v !) = v
    head₂ (cons v e p s t) = v
\end{code}

Defining paths for the parallel-pair approach to graphs leaves us with the need to carry
proofs around, and this is a tad too clunky in this case.

\begin{code}
module Path-definition-3 (G : Graph) where

  open Graph G

  -- handy dandy syntax
  infixr 5 cons
  syntax cons v ps e = v ⟶[ e ]⟶ ps -- v goes, by e, onto path ps

  -- we want well-formed paths
  mutual
    data Path₃ : Set where
      _!   : (v : V) → Path₃
      cons : (v : V) (ps : Path₃) (e : v ⟶ head₃ ps) → Path₃

    head₃ : Path₃ → V
    head₃ (v !) = v
    head₃ (v ⟶[ e ]⟶ ps) = v

  -- motivation for the syntax declaration above
  example : (v₁ v₂ v₃ : V) (e₁ : v₁ ⟶ v₂) (e₂ : v₂ ⟶ v₃) → Path₃
  example v₁ v₂ v₃ e₁ e₂ = v₁ ⟶[ e₁ ]⟶ v₂ ⟶[ e₂ ]⟶ v₃ !

  end₃ : Path₃ → V
  end₃ (v !) = v
  end₃ (v ⟶[ e ]⟶ ps) = end₃ ps

  -- typed paths
  record _⇝_ (x y : V) : Set where -- |\ squigarrowright|
    field
      path   : Path₃
      start  : head₃ path ≡ x
      finish : end₃ path  ≡ y
\end{code}
This seems great, but there's always room for improvement:

- since the |cons| constructors third argument depends on its first, we must
  use a syntax declaration to get the desired look. Such atheistic is not only
  pleasing but reminiscent of diagrammatic paths;
  moreover, it's guaranteed to be an actual path and not just an
  alternating lists of vertices and edges.
  Using the clunky |Path₂|, we'd write
  \begin{spec}
  v₁ ⟶[ v₁≈se₁ , e₁ , te₁≈v₂ ]⟶ v₂ ⟶[ v₂≈se₂ , e₂ , te₂≈v₃ ]⟶ v₃ !
  where
  syntax cons v e ps s t = v ⟶[ s , e , t ]⟶ ps
  \end{spec}
  yuck!

  Finally, the syntax-declaration does not make the emacs agda-mode auto-case using
  the syntax, and so I have to write it out by hand, each time I want to use the syntax.

- Again since |cons| third argument depends on the second argument, we need a mutual
  definition to extract the item of the dependence. Perhaps if we embed this item at
  the type level we may avoid the need of an auxiliary mutually-defined function.

- By defining what the start and finish of a path, we can assign types to it.
  However, this approach is reminiscent of the parallel-pair approach to graphs,
  as in |Graph₀|, which we argued is less preferable to the typed-approach to graphs.
  Perhaps defining paths with types by default, we can reap the benefits and simplicity
  of the typed-approach to graphs.
\begin{code}
module TypedPaths (G : Graph) where

  open Graph G hiding(V)
  open Graph   using (V)

  data _⇝_ : V G → V G → Set where
    _!         : ∀ x → x ⇝ x
    _⟶[_]⟶_  : ∀ x {y ω} (e : x ⟶ y) (ps : y ⇝ ω) → x ⇝ ω
\end{code}

---

Show that graphmaps preserve paths: |(f : G ⟶ H)  → x ⇝ y → fᵥ x ⇝ fᵥ y|;
this is nothing more than type-preservation for f to be a functor |𝒫G ⟶ 𝒫H| ;)

\begin{spec}
a connected b ≡ (a ⇝ b) ⊎ (b ⇝ a) -- path between a and b; not `from a to b'.
\end{spec}
this is an equivalence relation whose equivelnce classes are called the connected components.
Call this collection |𝒦G|. For any category |𝒞|, define |𝒦 𝒞 ≔ 𝒦 (𝒰₀ 𝒞)| which is a
subcategory of |𝒞|. Phrase the connected components subcategory using a universal property,
thereby avoiding the need for quotient types.

Since graphmaps preserve paths, every graph map can be extended to connected components,
|𝒦f : 𝒦G ⟶ 𝒦H : (connected componenet of x) ↦ (connected comopnent of fᵥ x)|.

Hence, we have a functor |𝒦 : Graph ⟶ Set|.

Then there is a natural transformation |𝒱 ⟶ 𝒦|, where V is the vertices functor.

Proof.

Such a transformation means we can realise vertices as connected components and this suggests
taking |βG : 𝒱G → 𝒦G| which takes a vertix to the connected-component |β|lock that contains it.
Then given graph map |f : G ⟶ H|,
\begin{spec}
  𝒱 f ⨾ βG
≡ λ a → the block containing fᵥ a
≡ λ a → 𝒦f (the block containg a)
≡ βH ⨾ 𝒦f
\end{spec}
yeah!


Moreover, if we let |𝒟 : Set → Cat| be the free category functor that associates each set with
the discrete category over it, then we have |𝒦| is the assoicated forgetful functor.

Proof.

given a set map |f : S ⟶ 𝒦 C|, this assigns a connected component for each s of S.
Let |R c| be a choice of Representative for an equivalence block, then
f can be made iinto a functor by sending each s, now construed as an object, to the |C|-object
|R (f s)|.

given a functor |F : 𝒟 S ⟶ C|, define a set-map that sends each s to the connected component
that contains it, ie |β|.

now verify the needed laws.

I saw this someplace on stack exchange for the adjoint to the free category functor.
Anyhow, should consider the intution(?) behind this construction since it's not immediately clear
why the connected components, that or cuz is nearly 6 in the morning and i am needs of sleep.

src :: \url{http://math.stackexchange.com/questions/1640298/coforgetful-functors}

---

One might think that since we can write
\begin{code}
  src : {x y : V G} (e : x ⟶ y) → V G
  src {x} {y} e = x
\end{code}
we can again ignore vertices and it suffices to just keep a coherent list of edges.
Then what is an empty path at a vertex? This' enough to keep vertices around
---moreover, the ensuing terms look like diagrammatic paths! Cool!

One more thing, a handy-dandy combinator for forming certain equality proofs of paths.
\begin{code}
  ⟶-≡ : ∀{x y ω} {e : x ⟶ y} {ps qs : y ⇝ ω} → ps ≡ qs → (x ⟶[ e ]⟶ ps) ≡ (x ⟶[ e ]⟶ qs)
  ⟶-≡ {x} {y} {ω} {e} {ps} {qs} eq = ≡-cong (λ r → x ⟶[ e ]⟶ r) eq
\end{code}
Less usefully, we leave as excercises
\begin{spec}
  edges : ∀ {x ω} (p : x ⇝ ω) → List (Σ s ∶ V G • Σ t ∶ V G • s ⟶ t)
  edges = {! exercise !}

  path-eq : ∀ {x y} {p q : x ⇝ y} → edges p ≡ edges q → p ≡ q
  path-eq = {! exercise !}
\end{spec}
Given time, |path-eq| could be rewritten so as to be more easily applicable.
For now, two path equality proofs occur in the document and both are realised by
quick-and-easy induction.
\begin{comment}
Solutions.
\begin{code}
  open import Data.List using (List ; [] ; _∷_)
  edges : ∀ {x ω} (p : x ⇝ ω) → List (Σ s ∶ V G • Σ t ∶ V G • s ⟶ t)
  edges {x} (.x !) = []
  edges {x} (.x ⟶[ e ]⟶ ps) = (x , _ , e) ∷ edges ps

  path-eq : ∀ {x y} {p q : x ⇝ y} → edges p ≡ edges q → p ≡ q
  path-eq {x} {p = .x !} {q = .x !} pf = ≡-refl
  path-eq {x} {p = .x !} {q = .x ⟶[ e ]⟶ q} ()
  path-eq {x} {p = .x ⟶[ e ]⟶ p} {q = .x !} ()
  path-eq {x} {p = .x ⟶[ e ]⟶ p} {q = .x ⟶[ e₁ ]⟶ q} pf with edges p | pf
  path-eq {x} {p = .x ⟶[ e ]⟶ p} {q = .x ⟶[ .e ]⟶ q} pf | .(edges q) | ≡-refl = ⟶-≡ (path-eq (uncons pf))
    where uncons : ∀{A : Set} {x y : A} {xs ys : List A} → x ∷ xs ≡ y ∷ ys → xs ≡ ys
          uncons {A} {x} {.x} {xs} {.xs} ≡-refl = ≡-refl        
\end{code}
\end{comment}
%}}}
%{{{ category of paths over a graph
\subsection{category of paths over a graph}
   %{{{ concatenation ++
Now we turn back to the problem of concatenating two paths.
\begin{code}
  infixr 5 _++_
  _++_ : ∀{x y z} → x ⇝ y → y ⇝ z → x ⇝ z
  x ! ++ q = q                                     -- left unit
  (x ⟶[ e ]⟶ p) ++ q = x ⟶[ e ]⟶ (p ++ q)        -- mutual-associativity
\end{code}
Notice that the the base case indicate that |!| forms a left-unit for |++|,
while the inductive case says that path-formation associates with path concatenation.

If we had not typed our paths, as in |Path₂|, we would need to carry around a
proof that paths are compatible for concatenation:
\begin{spec}
  concatenate : (p q : Path) (coh : end p ≡ head q) → Path
  syntax concatenate p q compatibility = p ++[ compatibility ] q
\end{spec}
Even worse, to show |p ++[ c ] q ≡ p ++[ c' ] q| we need to invoke proof-irrelevance of
identity proofs to obtain |c ≡ c'|, each time we want such an equality! Moving the proof
obligation to the type level removes this need.

As can be seen, being type-less is a terrible ordeal.

%}}}
   %{{{ units of ++[]
Just as the first clause of CONCAT AND BANG %|_++_| indicates |_!| is a left unit,
\begin{code}
  leftId : ∀ {x y} {p : x ⇝ y} → x ! ++ p ≡ p
  leftId = ≡-refl
\end{code}
Is it also a right identity?
\begin{code}
  rightId : ∀ {x y} {p : x ⇝ y} → p ++ y ! ≡ p
  rightId {x} {.x} {.x !} = ≡-refl
  rightId {x} {y } {.x ⟶[ e ]⟶ ps} = ≡-cong (λ q → x ⟶[ e ]⟶ q) rightId
\end{code}

%}}}
   %{{{ ++-assoc
Is this operation associative?
\begin{code}
  assoc : ∀{x y z ω} {p : x ⇝ y} {q : y ⇝ z} {r : z ⇝ ω} → (p ++ q) ++ r ≡ p ++ (q ++ r)
  assoc {x} {p = .x !} = ≡-refl
  assoc {x} {p = .x ⟶[ e ]⟶ ps} {q} {r} = ≡-cong (λ s → x ⟶[ e ]⟶ s) (assoc {p = ps})
\end{code}
%}}}

Hence, we've shown that the paths over a graph constitute a category! Let's call it |𝒫 G|.

%}}}
%{{{ |𝒫|-functor
\subsection{The |𝒫|ath to freedom}
In the last section, we showed that the paths over a graph make a category,
\begin{code}
𝒫₀ : Graph → Category
𝒫₀ G = let open TypedPaths G in
    record
      { Obj = Graph.V G
      ; _⟶_ = _⇝_
      ; _⨾_ = _++_
      ; assoc = λ {x y z ω p q r} → assoc {p = p}
      ; Id = λ {x} → x !
      ; leftId = leftId
      ; rightId = rightId
      }
\end{code}

Can we make |𝒫| into a functor by definining on morphisms?
That is, to lift graph-maps to category-maps, i.e., functors.

\begin{code}
𝒫₁ : ∀{G H} → GraphMap G H → Functor (𝒫₀ G) (𝒫₀ H)
𝒫₁ {G} {H} f = record { obj = ver f ; mor = mo ; id = ≡-refl ; comp = λ {x} {y} {z} {p} → comp {p = p} }
    where
      open TypedPaths {{...}} public

      mo : {x y : Graph.V G} →  x ⇝ y → (ver f x) ⇝ (ver f y)
      mo (x !) = ver f x !
      mo (x ⟶[ e ]⟶ p) = ver f x ⟶[ edge f e ]⟶ mo p

      comp : {x y z : Graph.V G} {p : x ⇝ y} {q : y ⇝ z} → mo (p ++ q) ≡ mo p ++ mo q
      comp {x} {p = .x !} = ≡-refl -- since ! is left unit of ++
      comp {x} {p = .x ⟶[ e ]⟶ ps} = ⟶-≡ (comp {p = ps})
\end{code}
Sweet!

With these two together, we have that |𝒫| is a functor.

\begin{code}
𝒫 : Functor 𝒢𝓇𝒶𝓅𝒽 𝒞𝒶𝓉
𝒫 = record { obj = 𝒫₀ ; mor = 𝒫₁ ; id = λ {G} → funcext ≡-refl (idm {G}) ; comp = funcext ≡-refl compmor }
    where
      open TypedPaths {{...}} public
         
      idm : ∀ {G} {x y} {p : x ⇝ y} → mor (Category.Id 𝒞𝒶𝓉 {𝒫₀ G}) p ≡ mor (𝒫₁ (Category.Id 𝒢𝓇𝒶𝓅𝒽)) p
      idm {G} {x} {p = .x !} = ≡-refl
      idm {G} {x} {p = .x ⟶[ e ]⟶ ps} = ⟶-≡ (idm {p = ps})

      open Category {{...}}
      compmor : ∀ {G H K} {f : G ⟶ H} {g : H ⟶ K} {x y} {p : x ⇝ y} → mor (𝒫₁ f ⨾ 𝒫₁ g) p ≡ mor(𝒫₁ (f ⨾ g)) p
      compmor {x = x} {p = .x !} = ≡-refl
      compmor {x = x} {p = .x ⟶[ e ]⟶ ps} = ⟶-≡ (compmor {p = ps})

\end{code}
%}}}

%{{{ freedom
\section{ Free at last }

|⟦|
``Free at last, free at last, thank God almighty we are free at last.'' -- Martin Luther King Jr.
%% Martin Luther King Jr., I Have a Dream: Writings and Speeches That Changed the World
|⟧|

Recall why lists give the `free monoid': we can embed a type |A| into |List A| by the map BACKEND-ERROR %% |[_]|,
and we can lift any map |f : A ⟶ B| to a monoid map
BACKEND-ERROR %% |foldr (λ a b → f a ⊕ b) e : (List A , _++_ , []) ⟶ (B, _⊕_ , e)|
---i.e., |[a₁, …, aₖ] ↦ f a₁ ⊕ ⋯ ⊕ f aₖ|---
such that |∀ a • f a ≡ foldr f e [ a ] | and this lifted map is unique.

Likewise, let us show that |𝒫G| is the `free category' over the graph |G|.
This amounts to saying that there is a way, say graph-map |ι|, to embed |G| into |𝒫G|,
and a way to lift any graph-map |f : G 𝒢⟶ 𝒰₀ 𝒞| to a functor |lift f : Functor 𝒫G 𝒞|
such that |f ≡ ι ⨾ 𝒰₁ (lift f)| and uniquely so.

Let's begin!
\begin{code}
module freedom (G : Obj 𝒢𝓇𝒶𝓅𝒽) {𝒞 : Category {ℓ₀} {ℓ₀} } where

  open TypedPaths G using (_! ; _⟶[_]⟶_ ;  _⇝_ ; _++_)
  open Category {{...}}
\end{code}

   %{{{ Defining the needed operations
\subsection{Defining the needed operations}
The only obvious, and most natural, way to embed a graph into its `graph of paths' is to send
vertices to vertices and edges to paths of length 1.
\begin{code}  
  ι : G ⟶ 𝒰₀ (𝒫₀ G)
  ι = record { ver = Id ; edge = λ {x} {y} e → x ⟶[ e ]⟶ y ! }
\end{code}

Given a graph map |f|, following the list-analagoue of |[a₁, …, aₖ] ↦ f a₁ ⊕ ⋯ ⊕ f aₖ|
we attempt to lift the map onto paths by taking edges |e₁, …, eₖ| to a morphism |e₁' ⨾ ⋯ ⨾ eₖ'|
where |eᵢ' = edge f eᵢ|.
Of course, we then need to verify that this construction is indeed
functorial.
\begin{code}
  lift : G ⟶ 𝒰₀ 𝒞 → 𝒫₀ G ⟶ 𝒞
  lift f = record {
      obj = λ v → ver f v -- this is the only way to obtain an object of |𝒞|; hope it works!
     ; mor = toMap ; id = ≡-refl ; comp = λ {x y z p q} → proof {x} {y} {z} {p} {q}
     }
     where
          toMap : ∀ {x y} → x ⇝ y → ver f x ⟶ ver f y
          toMap (y !) = Id
          toMap (x ⟶[ e ]⟶ p) = edge f e ⨾ toMap p

          proof : ∀{x y z} {p : x ⇝ y} {q : y ⇝ z} → toMap (p ++ q) ≡ toMap p ⨾ toMap q
          proof {p = ._ !} = ≡-sym leftId
          proof {p = ._ ⟶[ e ]⟶ ps} = ≡-cong (λ m → edge f e ⨾ m) (proof {p = ps}) ⟨≡≡⟩ ≡-sym assoc
\end{code}
Now we have the embedding and the lifting, it remains to show that the aforementioned property
holds and so does uniqueness.

{\sc show that we have a natural transformation |Id ⟶ 𝒰₀ ∘ 𝒫₀| }

%}}}

   %{{{ Equality
\begin{comment}
Before postulating extensionality, I used the following notions.

to talk of equations, we need appropriate equalitites

\begin{code}
  _≈g_ : ∀{G H : Graph} (f g : G ⟶ H) → Set
  _≈g_ {G} {H} f g = Σ veq ∶ (∀ {v} → ver f v ≡ ver g v) •
    (∀ {x y e} → edge g {x} {y} e ≡ ≡-subst₂ (λ a b → Graph._⟶_ H a b) veq veq (edge f {x} {y} e))

  _≋_ : ∀{𝒞 𝒟 : Category} (f g : 𝒞 ⟶ 𝒟) → Set
  F ≋ G = 𝒰₁ F ≈g 𝒰₁ G   -- proofs (x , y) now replaced with: funcext x y
\end{code}
Spelled-out:
\begin{spec}
_≋_ {G} {H} f g = Σ veq ∶ (∀ {v} → obj f v ≡ obj g v) •
  (∀ {x y e} → mor g {x} {y} e ≡ ≡-subst₂ (λ a b → Category._⟶_ H a b) veq veq (mor f e))
\end{spec}

\end{comment}

%}}}

   %{{{ Realizing the proof-obligations
\subsection{Realizing the proof-obligations}
\begin{code}
  property : ∀{f : G ⟶ 𝒰₀ 𝒞} → f ≡ (ι ⨾ 𝒰₁ (lift f))
  property {f} = graphmapext
    -- proving |∀ {v} → ver f v ≡ ver (ι ⨾ 𝒰₁ (lift f)) v| by starting at the complicated side and simplifying
    (λ {v} → ≡-sym (begin
              ver (ι ⨾ 𝒰₁ (lift f)) v
            ≡⟨ definition {- of |ver| on composition -}⟩
              (ver ι ⨾ ver (𝒰₁ (lift f))) v
            ≡⟨ definition {- of |𝒰₁| says |ver (𝒰₁ F) = obj F| -}⟩ 
              (ver ι ⨾ obj (lift f)) v
            ≡⟨ definition {- of |lift| says |obj (lift f) = ver f| -}⟩
              (ver ι ⨾ ver f) v
            ≡⟨ definition {- of |ι| on vertices is idenity -}⟩
              ver f v
            ∎))
    
    -- proving |edge (ι ⨾g 𝒰₁ (lift f)) e ≡ edge f e|
    (λ {x} {y} {e} → begin
               edge (ι ⨾ 𝒰₁ (lift f)) e
             ≡⟨ definition {- of |edge| on composition -} ⟩
               (edge ι ⨾ edge (𝒰₁ (lift f))) e
             ≡⟨ definition {- of |𝒰| says |edge (𝒰₁ F) = mor F| -} ⟩
               (edge ι ⨾ mor (lift f)) e
             ≡⟨ definition {- chasing gives |mor (lift f) (edge ι e) = edge f e ⨾ Id| -} ⟩
               edge f e ⨾ Id
             ≡⟨ rightId ⟩
               edge f e
             ∎)
\end{code}
Observe that we simply chased definitions and as such |(≡-refl , rightId)| suffices as a proof,
but it's not terribly clear why, for human consumption and so we choose to elaborate with the
detail.

Finally, it remains to show uniqueness,
\begin{code}
  uniqueness : ∀{f : G ⟶ 𝒰₀ 𝒞} {F : 𝒫₀ G ⟶ 𝒞} → f ≡ (ι ⨾ 𝒰₁ F) → lift f ≡ F
  uniqueness {.(ι ⨾ 𝒰₁ F)} {F} ≡-refl = funcext ≡-refl (≡-sym pf)
    where
      pf : ∀{x y} {p : x ⇝ y} → mor (lift (ι ⨾ 𝒰₁ F)) p ≡ mor F p
      pf {x} {.x} {p = .x !} = ≡-sym (Functor.id F)
      pf {x} {y} {p = .x ⟶[ e ]⟶ ps} = begin
         mor (lift (ι ⨾ 𝒰₁ F)) (x ⟶[ e ]⟶ ps)
       ≡⟨ definition {- of |mor| on |lift|, the inductive clause -} ⟩
         edge (ι ⨾ 𝒰₁ F) e ⨾ mor (lift (ι ⨾ 𝒰₁ F)) ps
       ≡⟨ ≡-cong₂ _⨾_ ≡-refl (pf {p = ps}) ⟩ -- inductive step
         edge (ι ⨾ 𝒰₁ F) e ⨾ mor F ps
       ≡⟨ definition {- of |edge| says it preserves composition -} ⟩
         (edge ι ⨾ edge (𝒰₁ F)) e ⨾ mor F ps
       ≡⟨ ≡-refl ⟩ {- definition of |𝒰| gives |edge (𝒰₁ F) = mor F|-}
         (edge ι ⨾ mor F) e ⨾ mor F ps
       ≡⟨ definition {- of functional composition (|𝒮et|) -} ⟩
          mor F (edge ι e) ⨾ mor F ps
       ≡⟨ ≡-sym (Functor.comp F) {- i.e., functors preserve composition -} ⟩
          mor F (edge ι e ++ ps)
       ≡⟨ definition {- of embedding and concatenation -} ⟩
         mor F (x ⟶[ e ]⟶ ps)
       ∎
\end{code}

\begin{comment}
Below is the uniqueness proof before making postulates.

\begin{code}
-- Since equality of functors makes use of |subst|s all over the place, we will need
-- a lemma about how |subst| factors/distributes over an operator composition.
  subst-dist : ∀ {S : Set} {v v' : S → Category.Obj 𝒞} (veq : ∀ {z} → v z ≡ v' z)
         → ∀ x t y {ec : v x ⟶ v t} {psc : v t ⟶ v y}
         →  
           ≡-subst₂ _⟶_ veq veq ec ⨾ ≡-subst₂ _⟶_ veq veq psc
         ≡ ≡-subst₂ _⟶_ veq veq (ec ⨾ psc)
  subst-dist veq x t y rewrite veq {x} | veq {t} | veq {y} = ≡-refl

  uniquenessOld : ∀{f : G ⟶ 𝒰₀ 𝒞} {F : 𝒫₀ G ⟶ 𝒞} → f ≈g (ι ⨾ 𝒰₁ F) → lift f ≡ F
  uniquenessOld {f} {F} (veq , eeq) = funcext veq pf
    where
    
      𝒮 : ∀{x y} → ver f x ⟶ ver f y → obj F x ⟶ obj F y
      𝒮 m = ≡-subst₂ _⟶_ veq veq m
      
      pf : ∀{x y} {p : x ⇝ y} → mor F p ≡ 𝒮( mor (lift f) p )
      pf {x} {.x} {.x !} rewrite (veq {x})= Functor.id F
      pf {x} {y}  {.x ⟶[ e ]⟶ ps} rewrite (eeq {e = e}) =  begin
          mor F (x ⟶[ e ]⟶ ps)
       ≡⟨ definition {- of embedding and concatenation -} ⟩
          mor F (edge ι e ++ ps)
       ≡⟨ Functor.comp F {- i.e., functors preserve composition -} ⟩
          mor F (edge ι e) ⨾ mor F ps
       ≡⟨ ≡-cong₂ _⨾_ eeq (pf {p = ps}) ⟩ -- inductive step
          𝒮(edge f e) ⨾ 𝒮(mor (lift f) ps)
       ≡⟨ subst-dist veq x _ y ⟩
          𝒮( edge f e ⨾ mor (lift f) ps )
       ≡⟨ definition {- of |mor| on |lift|, the inductive clause -} ⟩
          𝒮( mor (lift f) (x ⟶[ e ]⟶ ps) )
       ∎
\end{code}
\end{comment}
%}}}

   %{{{ Another freedom proof
\subsection{Another freedom proof}

However, saying each graph-map gives rise to exactly one unique functor is tantamount to
saying the type |GraphMap G (𝒰₀ 𝒞)| is isomorphic to |Functor (𝒫₀ G) 𝒞|, that is
|(𝒫₀ G ⟶ 𝒞) ≅ (G ⟶ 𝒰₀ 𝒞)| ---observe that this says we can `move' |𝒫₀| from the left to
the right of an arrow at the cost of it (and the arrow) changing.

A few healthy excercises,
\begin{spec}
  lift˘ : Functor 𝒫G 𝒞 → GraphMap G (𝒰₀ 𝒞)
  lift˘ F = ι ⨾g 𝒰₁ F -- i.e., |obj F , (mor F ∘ edge ι)|

  rid : ∀{f : GraphMap G (𝒰₀ 𝒞)} → ∀{x y} {e : x ⟶g y} → lift˘ (lift f) ≡ f
  rid = {! exercise !}

  lid : ∀{F : Functor 𝒫G 𝒞} → lift (lift˘ F) ≡ F
  lid = {! exercise !}
\end{spec}
One can of course obtain these proofs from the other ones without recourse to definitions,
however for comprehension one would do well to prove them from first principles.
The worked out solutions are available in the literate source file of this document.

%{{{ worked out solutions
\begin{comment}
\begin{code}
  lift˘ : Functor (𝒫₀ G) 𝒞 → GraphMap G (𝒰₀ 𝒞)
  lift˘ F = ι ⨾ 𝒰₁ F -- |≡ obj F # (mor F ∘ edge ι)|
  
  rid₀ : ∀ {f : GraphMap G (𝒰₀ 𝒞)} → ver (lift˘ (lift f)) ≡ ver f
  rid₀ {f} = begin
      ver (lift˘ (lift f))
    ≡⟨⟩ -- ver of lift˘ ; defn of lift˘
      obj (lift f)
    ≡⟨⟩ -- defn of lift.obj
      ver f
    ∎
-- note that |≡-refl| would have suffcied, but we show all the steps for clarity, for human consumption!

  open Graph G renaming (_⟶_ to _⟶g_)
  rid₁ : ∀{f : GraphMap G (𝒰₀ 𝒞)} → ∀{x y} {e : x ⟶g y} → edge (lift˘ (lift f)) e ≡ edge f e
  rid₁ {f} {x} {y} {e} = begin
      edge (lift˘ (lift f)) e
    ≡⟨⟩ -- |lift˘.edge| definition
      mor (lift f) (edge ι e)
    ≡⟨⟩ -- |lift.mor| on an edge; i.e., the inductive case of |toMap|
      edge f e ⨾ Id
    ≡⟨ rightId ⟩
      edge f e
    ∎
    -- without {{..}} I'd have to write |Category.Id 𝒞| and |Category._⨾_ 𝒞 …| and |Category.rightId 𝒞| above

  rid : ∀{f : GraphMap G (𝒰₀ 𝒞)} → lift˘ (lift f) ≡ f
  rid {f} = graphmapext ≡-refl (≡-sym (rid₁ {f}))
  
  lid₀ : ∀{F : Functor (𝒫₀ G) 𝒞} → obj (lift (lift˘ F)) ≡ obj F
  lid₀ {F} =  begin
      obj (lift (lift˘ F))
    ≡⟨⟩ -- obj of lift
      ver (lift˘ F)
    ≡⟨⟩ -- ver of lift˘
       obj F
    ∎

  lid₁ : ∀{F : Functor (𝒫₀ G) 𝒞} → ∀ {x y} {p : x ⇝ y} → mor (lift (lift˘ F)) p ≡ mor F p
  lid₁ {F} {x} {.x} {p = (.x) !} = begin
      mor (lift (lift˘ F)) (x !)
    ≡⟨⟩ -- mor of lift on !
      Id 
    ≡⟨ ≡-sym (Functor.id F) ⟩ -- ! is identity path in |𝒫G| and functtor preserve identites
       mor F (x !)
    ∎
    
  lid₁ {F} {x} {y} {p = .x ⟶[ e ]⟶ ps} = begin
      mor (lift (lift˘ F)) (x ⟶[ e ]⟶ ps)
    ≡⟨⟩ -- mor on lift on inductive case
      edge (lift˘ F) e ⨾ mor (lift (lift˘ F)) ps
    ≡⟨  ≡-cong (λ w → edge (lift˘ F) e ⨾ w) (lid₁ {F}) ⟩
      edge (lift˘ F) e ⨾ mor F ps
    ≡⟨⟩ -- edge on lift˘
      mor F (edge ι e) ⨾ mor F ps
    ≡⟨ ≡-sym (Functor.comp F) ⟩ -- factor out Functor.mor
      mor F (edge ι e ++ ps)
    ≡⟨⟩ -- defn of ++
      mor F (x ⟶[ e ]⟶ ps)
    ∎

  lid : ∀ {F : Functor (𝒫₀ G) 𝒞} → lift (lift˘ F) ≡ F
  lid  {F} = funcext ≡-refl (≡-sym (lid₁ {F}))

  -- old version
  lift-≈ : ∀{f f' : GraphMap G (𝒰₀ 𝒞)} → f ≈g f' → lift f ≋ lift f'
  lift-≈  {f} {f'} (veq , eeq) = veq , (λ {x} {y} {p} → pf {x} {y} {p})
    where
      pf : {x y : V} {p : x ⇝ y} → mor (lift f') p ≡ ≡-subst₂ _⟶_ veq veq (mor (lift f) p)
      pf {x} {.x} {p = .x !} rewrite (veq {x}) = ≡-refl
      pf {x} {y} {p = .x ⟶[ e ]⟶ ps} = begin 
           mor (lift f') (x ⟶[ e ]⟶ ps)
         ≡⟨⟩
           edge f' e ⨾ mor (lift f') ps
         ≡⟨ ≡-cong₂ _⨾_ eeq (pf {p = ps}) ⟩
           ≡-subst₂ _⟶_ veq veq (edge f e) ⨾ ≡-subst₂ _⟶_ veq veq (mor (lift f) ps) 
         ≡⟨ subst-dist veq x _ y ⟩
            ≡-subst₂ _⟶_ veq veq (mor (lift f) (x ⟶[ e ]⟶ ps))
         ∎
\end{code}
\end{comment}
%}}}

We can then given an alternate proof of uniqueness |∀{f h}→ f ≡ (ι ⨾ 𝒰₁ h) → lift f ≡ h|.
\begin{code}
  uniqueness' : ∀{f h}→ f ≡ (ι ⨾ 𝒰₁ h) → lift f ≡ h
  uniqueness' {f} {h} f≈ι⨾𝒰₁h = begin
      lift f
    ≡⟨ ≡-cong lift f≈ι⨾𝒰₁h ⟩
      lift (ι ⨾ 𝒰₁ h)
    ≡⟨ definition {- of |lift˘| -} ⟩
      lift (lift˘ h)
    ≡⟨ lid ⟩
      h
    ∎
\end{code}
%}}}

   %{{{ |𝒫 ⊣ 𝒰| proof
\subsection{|𝒫 ⊣ 𝒰|}
   %{{{ reasoning for naturality proof obligation
\begin{comment}
\begin{verbatim}
Following is probably full of type errors, since it's all by hand!

Anyhow, let's recall the notion of natural isomorphism, then apply it to adjunctions then to our
particular adjunction for paths.

|F ≅ G| means |∀ A : Obj 𝒞 • ∃ Φ : F A ≅ G A • ∀ X Y : Obj 𝒞 • ∀ f : X ⟶ Y • F f ⨾ Φ {Y} ≈ Φ {X} ⨾ G f|
Given f w e have two ways to get from F X to G Y:
first lift f to |F f : F X → F Y| then shift to |F f ⨾ Φ {Y} : F X → G Y|,
alternatively we shift then lift to obtain |Φ {X} ⨾ G f : F X → G Y|
and ideally we'd like to avoid having to make a choise and so request
these two approaches to be the same.


For the case |F ⊣ G|.

FF := F _ ⟶ _ : 𝒞ᵒᵖ×𝒟 → 𝒟' : λ (A , B) → F A ⟶ B ; where 𝒟' is the cats of arrows of |𝒟|.
                            : λ (m : Mor 𝒞, n : Mor 𝒟) → λ k : F (Tgt m) ⟶𝒟 Src n → F m ⨾ k ⨾ n 


Given m : Xₘ ⟶𝒞 Yₘ and n : Xₙ ⟶𝒟 Yₙ, we have |(m , n) : (Yₘ , Xₙ) ⟶ (Xₘ , Yₙ)| is an arrow of |𝒞ᵒᵖ × 𝒟|.
we have FF (m , n) : (F Yₘ ⟶𝒟 Xₙ) → (F Xₘ ⟶𝒟 Yₙ)
        Φ {A} {B} : (F A ⟶𝒟 B) ≅ (A ⟶𝒞 G B)
then
FF (m , n) ⨾ Φ {Yₘ} {Xₙ} : (F Yₘ ⟶𝒟 Xₙ) → (Xₘ ⟶𝒟 G Yₙ) : k ↦ Φ {Yₘ} {Xₙ} (F m ⨾ k ⨾ n)

Likewise,
GG (m, n) : (Yₘ ⟶𝒞 G Xₙ) → (Xₘ ⟶𝒟 G Yₙ) : k ↦ m ⨾ k ⨾ G n
and thus
Φ {Xₘ} {Yₙ} ⨾ GG (m, n) : (F Yₘ ⟶𝒟 Xₙ) → (Xₘ ⟶𝒟 G Yₙ) : k ↦ m ⨾ Φ {Yₘ} {Xₙ} k ⨾ G n

Hence we need,
|∀ k : F Yₘ ⟶𝒟 Xₙ • Φ {Yₘ} {Xₙ} (F m ⨾ k ⨾ n) ≈ m ⨾ Φ {Yₘ} {Xₙ} k ⨾ G n|

For our case |𝒫 ⊣ 𝒰|.
Naturality becomes, using |𝒢| for |𝒢𝓇𝒶𝓅𝒽| and |𝒞| for |𝒞𝒶𝓉|,
|∀ G H : Obj 𝒢𝓇𝒶𝓅𝒽 • ∀ 𝒞 𝒟 : Obj 𝒞𝒶𝓉 • ∀ g : GraphMap G H • ∀ F : Functor 𝒞 𝒟 • 
∀ k : Functor (𝒫 H) 𝒞 • lift˘ {G} {𝒞} (𝒫 g ⨾ k ⨾ F) ≈ g ⨾ lift˘ {H} {𝒞} k ⨾ 𝒰 F|

That is, for every graph map g and functor F, for appropriate functor k we have
|lift˘ (𝒫 g ⨾ k ⨾ F) ≈ g ⨾ lift˘ k ⨾ 𝒰 F| in the category of graphs.
\end{verbatim}
\end{comment}
%}}}

That is, |𝒫 ⊣ 𝒰| and say |𝒫| is a `free-functor' since it is left-adjoint to a forgetful-functor.

We have
\begin{spec}
lift  : G ⟶ 𝒰₀ 𝒞 → 𝒫G ⟶ 𝒞
lift˘ : 𝒫G ⟶ 𝒞 → G ⟶ 𝒰₀ 𝒞
\end{spec}
and we proved them to be inverses hence
|(𝒫G ⟶ 𝒞) ≅ (G ⟶ 𝒰₀ 𝒞)|.

To realise |𝒫 ⊣ 𝒰| it remains to exhibit `naturality':
for every graph map g and functor F, for appropriate functor k we have
|lift˘ (𝒫 g ⨾ k ⨾ F) ≈ g ⨾ lift˘ k ⨾ 𝒰 F| in the category of graphs.
\begin{code}
module _ {G H : Graph} {𝒞 𝒟 : Category {ℓ₀} {ℓ₀}} (g : GraphMap G H) (F : Functor 𝒞 𝒟) where

  private
    lift˘ = λ {A} {C} B → freedom.lift˘ A {C} B
    lift = λ {A} {C} B → freedom.lift A {C} B
  open Category {{...}}

  naturality˘ : ∀ {K : Functor (𝒫₀ H) 𝒞} → lift˘ (𝒫₁ g ⨾ K ⨾ F) ≡ (g ⨾ lift˘ K ⨾ 𝒰₁ F)
  naturality˘ = graphmapext ≡-refl ≡-refl
\end{code}
That was easier than assumed! haha ---much harder to formalize but so easy to prove lolz
It says we can `shunt' |lift˘| into certain compositions at the cost
of replacing functor instances.

Just we needed to prove two inverse laws for |lift| and |lift˘|, we need two naturality proofs.
\begin{code}
  naturality : ∀ {k : GraphMap H (𝒰₀ 𝒞)} → lift (g ⨾ k ⨾ 𝒰₁ F) ≡ (𝒫₁ g ⨾ lift k ⨾ F)
  naturality {k} = funcext ≡-refl (λ {x y p} → proof {x} {y} {p})
    where
      open TypedPaths {{...}}
      proof : ∀ {x y} {p : x ⇝ y} → mor (𝒫₁ g ⨾ lift {H} {𝒞} k ⨾ F) p ≡ mor (lift {G} {𝒟} (g ⨾ k ⨾ 𝒰₁ F)) p
      proof {x} {p = .x !} = functors-preserve-identities
      proof {x} {p = .x ⟶[ e ]⟶ ps} = begin
            mor (𝒫₁ g ⨾ lift {H} {𝒞} k ⨾ F) (x ⟶[ e ]⟶ ps)
         ≡⟨ definition ⟩ 
            (mor (𝒫₁ g) ⨾ mor (lift {H} {𝒞} k) ⨾ mor F) (x ⟶[ e ]⟶ ps)
         ≡⟨ definition {- of function composition and |𝒫₁ ⨾ mor| -} ⟩
            mor F (mor (lift {H} {𝒞} k) (mor (𝒫₁ g) (x ⟶[ e ]⟶ ps)))
         ≡⟨ definition ⟩
            mor F (mor (lift {H} {𝒞} k) (ver g x ⟶[ edge g e ]⟶ mor (𝒫₁ g) ps))
         ≡⟨ definition {-of |lift ⨾ mor| on inductive case for paths -} ⟩
            mor F (edge k (edge g e) ⨾ mor (lift {H} {𝒞} k) (mor (𝒫₁ g) ps))
         ≡⟨ functors-preserve-composition ⟩
            mor F (edge k (edge g e)) ⨾ mor F (mor (lift {H} {𝒞} k) (mor (𝒫₁ g) ps))
         ≡⟨ definition {- of function composition -} ⟩
            (edge g ⨾ edge k ⨾ mor F) e ⨾ (mor (𝒫₁ g) ⨾ mor (lift {H} {𝒞} k) ⨾ mor F) ps
         ≡⟨ {- |𝒰₁ ⨾ edge = mor| and edge and mor are functorial by -} definition ⟩
            edge (g ⨾ k ⨾ 𝒰₁ F) e ⨾ mor (𝒫₁ g ⨾ lift {H} {𝒞} k ⨾ F) ps
         ≡⟨ {-inductive hypothesis: -} ≡-cong₂ _⨾_ ≡-refl (proof {p = ps}) ⟩
            edge (g ⨾ k ⨾ 𝒰₁ F) e ⨾ mor (lift {G} {𝒟} (g ⨾ k ⨾ 𝒰₁ F)) ps
         ≡⟨ definition {-of |lift ⨾ mor| on inductive case for paths -}⟩
            mor (lift {G} {𝒟} (g ⨾ k ⨾ 𝒰₁ F)) (x ⟶[ e ]⟶ ps)
         ∎
\end{code}

\href{http://maartenfokkinga.github.io/utwente/mmf92b.pdf}{Fokkinga [Theorem A.4]},
among others, would call
these laws `fusion' instead since they inform us how to compose, or `fuse', a morphism with a
|lift˘|ed morphism: taking |F| to be the identity and remembering that functors preserve
identities, we have that |g ⨾ lift˘ K ≡ lift˘( 𝒫₁ g ⨾ K)| --we can push a morphism into a |lift˘|
at the cost of introducing a |𝒫₁|; dually for |lift|ed morphisms.

Formally,
\begin{code}
𝒫⊣𝒰 : 𝒫 ⊣ 𝒰
𝒫⊣𝒰 = record{
    ⌊_⌋ = lift˘
  ; ⌈_⌉ = lift
  ; lid = lid
  ; rid = λ {G 𝒞 c} → rid {G} {𝒞} {c}
  ; lfusion = λ {G H 𝒞 𝒟 f F K} → naturality˘ {G} {H} {𝒞} {𝒟} f K {F}
  ; rfusion = λ {G H 𝒞 𝒟 f k F} → naturality {G} {H} {𝒞} {𝒟} f F {k} }
  where
    module _ {G : Graph} {𝒞 : Category} where open freedom G {𝒞} public
\end{code}

%}}}

%}}}

%{{{ fold over paths
\section{fold over paths}

Observe that for the freedom proof we recalled
that ists determine a form of quantification, `folding':
given an operation |⊕|, we may form |[x₁, …, xₖ] ↦ x₁ ⊕ ⋯ ⊕ xₖ|.
Then used that to define our operation |lift|, whose core was essentially,
\begin{code}
module folding (G : Graph) where
  open TypedPaths G
  open Graph G
  
  fold : {X : Set} (v : V → X) (f : ∀ x {y} (e : x ⟶ y) → X → X) → (∀ {a b} → a ⇝ b → X)
  fold v f (b !) = v b
  fold v f (x ⟶[ e ]⟶ ps) = f x e (fold v f ps)  
\end{code}          

For example, what is the length of a path?
\begin{code}
  length : ∀{x y} → x ⇝ y → ℕ
  length = fold (λ _ → 0)          -- single walks are length 0
                (λ _ _ n → 1 + n)  -- edges are one more than the length of the remaining walk
\end{code}
Let's verify that this is actually what we intend by the length of a path.
\begin{code}
  length-! : ∀{x} → length (x !) ≡ 0
  length-! = ≡-refl   -- true by definition of |length|: the first argument to the |fold|

  length-ind : ∀ {x y ω} {e : x ⟶ y} {ps : y ⇝ ω} → length (x ⟶[ e ]⟶ ps) ≡ 1 + length ps
  length-ind = ≡-refl -- true by definition of |length|: the second-argument to the |fold|
\end{code}

Generalzing on |length|, suppose we have a `cost function' |c| that assigns a cost of traversing
an edge. Then we can ask what is the total cost of a path:
\begin{code}
  path-cost : (c : ∀{x y}(e : x ⟶ y) → ℕ) → ∀{x y}(ps : x ⇝ y) → ℕ
  path-cost c = fold (λ _ → 0)           -- no cost on an empty path
                     (λ x e n → c e + n) -- cost of current edge plus cost of remainder of path
\end{code}
Now, we have BACKEND-ERROR %% |length = path-cost (λ _ → 1)|: length is just assigning a cost of 1 to each edge.

Under suitable conditions, list fold distributes over list concatenation, can we find an analogue
for paths?
\begin{code}
  fold-++ :  ∀{X : Set} {v : V → X} {g : ∀ x {y} (e : x ⟶ y) → X}
          → (_⊕_ : X → X → X)
          → ∀{x y z : V} {p : x ⇝ y} {q : y ⇝ z}
          → (unitl : ∀{x y} → y ≡ v x ⊕ y)                    -- image of |v| is left unit of |⊕|
          → (assoc : ∀ {x y z} → x ⊕ (y ⊕ z) ≡ (x ⊕ y) ⊕ z )  -- |⊕| is associative 
          → let f : ∀ x {y} (e : x ⟶ y) → X → X
                f = λ x e ps → g x e ⊕ ps
             in
               fold v f (p ++ q) ≡ fold v f p ⊕ fold v f q
  fold-++ {g = g} _⊕_ {x = x} {p = .x !} unitl assoc =  unitl
  fold-++ {g = g} _⊕_ {x = x} {p = .x ⟶[ e ]⟶ ps} unitl assoc =
    ≡-cong (λ exp → g x e ⊕ exp) (fold-++ _⊕_ {p = ps} unitl assoc) ⟨≡≡⟩ assoc
\end{code}

Compare this with the proof-obligation of |lift|.

%}}}

%{{{ Lists are special kinds of paths
\subsection{Lists are special kinds of paths}
We called our path concatenation BACKEND-ERROR %% |_++_|, why the same symbol as that for list concatenation?

How do we interpret a list over |A| as graph? Well the vertices can be any element of |A|
and an edge |x ⟶ y| merely indicates that ``the item after |x| in the list is the element |y|'',
so we want it to be always true; or always inhabited without distinction of the inhabitant:
so use a unit type.
\begin{code}
module lists (A : Set) where

  open import Data.Unit

  listGraph : Graph
  listGraph = record { V = A ; _⟶_ = λ a a' → ⊤ }
\end{code}
I haven't a clue if this works, you read my reasoning above.

The only thing we can do is test our hypothesis by looking at the typed paths over this graph.
In particular, we attempt to show every non-empty list of |A|'s corresponds to a path.
Since a typed path needs a priori the start and end vertes, let us construe
|List A ≅ Σ n ∶ ℕ • Fin n → A| --later note that |Path G ≅ Σ n ∶ ℕ • [n] 𝒢⟶ G|--.
\begin{code}
  open TypedPaths listGraph
  open folding listGraph

  -- every non-empty list of 'A's corresonds to a path!
  toPath : ∀{n} (list : Fin (suc n) → A) →  list fzero  ⇝ list ( fromℕ n )
  toPath {zero} list = list fzero !
  toPath {suc n} list = list fzero ⟶[ tt ]⟶ toPath {n} (λ i → list(fsuc i))
    -- note that |list : Fin (suc (suc n)) → A| while |suc ⨾ list : Fin (suc n) → A|
    
  -- if | list ≈ [x , y , z] | then
  -- |fsuc ⨾ list ≈ [y , z ] |
  -- |fsuc ⨾ fsuc ⨾ list = [z] |
\end{code}
Hm! Look at that, first guess and it worked! Sweet.

Now let's realize the list fold as an instance of path fold,
\begin{code}
  -- list type former
  List = λ (X : Set) → Σ n ∶ ℕ • (Fin n → X)

  -- usual list folding
  foldr : ∀{B : Set} (f : A → B → B) (e : B) → List A → B
  foldr f e (zero , l) = e
  foldr f e (suc n , l) = fold (λ a → f a e) (λ a _ rem → f a rem) (toPath l)

  -- example
  listLength : List A → ℕ -- result should clearly be |proj₁| of the list, anyhow:
  listLength = foldr (λ a rem → 1 + rem) -- non-empty list has length 1 more than the remainder
                     0                    -- empty list has length 0

  -- let's prepare for a more useful example

  -- empty list
  [] : ∀{X : Set} → List X
  [] = 0 , λ ()

  -- cons for lists
  _∷_ : ∀{X : Set} → X → List X → List X
  _∷_ {X} x (n , l) = 1 + n , cons x l
    where
      -- |cons a l ≡ λ i : Fin (1 + n) → if i ≈ 0 then a else l i|
      cons : ∀{n} → X → (Fin n → X) → (Fin (suc n) → X)
      cons x l fzero = x
      cons x l (fsuc i) = l i
  
  map : ∀ {B} (f : A → B) → List A → List B
  map f =  foldr (λ a rem → f a ∷ rem) []  -- looks like the usual map don't it ;)

  -- list concatenation
  _++ℓ_ : List A → List A → List A
  l ++ℓ r = foldr _∷_ r l -- fold over |l| by cons its elements infront of |r|

  -- Exercise: write path concatenation as a path-fold.
\end{code}
%}}}

This note took longer to write than I had initally assumed; perhaps I should have taken into
account Hofstadter's Law, which says:
"It always takes longer than you expect, even when you take into account Hofstadter's Law.".

[insert url to GEB]

%{{{ setoid based approach
\begin{comment}
I wrote this rushedly; very rough solutions.

\begin{spec}
module _ where -- category definitions
 record Category' {i j k : Level} : Set (ℓsuc (i ⊍ j ⊍ k)) where
  infixr 10 _⨾_
  infix 5 _≈_
  field
    -- graph structure
    Obj'  : Set i
    _⟶_ : Obj' → Obj' → Set j

    -- setoid structure
    _≈_  : ∀ {A B} (f g : A ⟶ B) → Set k
    ≈-refl  : ∀ {A B} {f : A ⟶ B} → f ≈ f
    ≈-sym   : ∀ {A B} {f g : A ⟶ B} → f ≈ g → g ≈ f
    ≈-trans : ∀ {A B} {f g h : A ⟶ B} → f ≈ g → g ≈ h → f ≈ h

    -- typed-monoid-like structure
    _⨾_     : ∀{A B C : Obj'} → A ⟶ B → B ⟶ C → A ⟶ C
    ⨾-cong  : ∀ {A B C} {f f' : A ⟶ B} {g g' : B ⟶ C} → f ≈ f' → g ≈ g' → f ⨾ g ≈ f' ⨾ g'  
    assoc   : ∀{A B C D} {f : A ⟶ B}{g : B ⟶ C} {h : C ⟶ D} → (f ⨾ g) ⨾ h ≈ f ⨾ (g ⨾ h)
    Id      : ∀{A : Obj'} → A ⟶ A
    leftId  : ∀ {A B} {f : A ⟶ B} → Id ⨾ f ≈ f
    rightId : ∀ {A B} {f : A ⟶ B} → f ⨾ Id ≈ f
 open Category' {{...}} renaming (_⟶_ to _⟶'_ ; _⨾_ to _⨾'_ ; Id to Id' ; leftId to leftId' ; rightId to rightId' ; assoc to assoc' ; _≈_ to _≈'_) public

 record Functor' {i j k i' j' k'} (𝒞 : Category' {i} {j} {k}) (𝒟 : Category' {i'} {j'} {k'}) : Set (ℓsuc (i ⊍ j ⊍ k ⊍ i' ⊍ j' ⊍ k')) where
  field
    -- graph-map structure
    obj'  : Category'.Obj' 𝒞 → Category'.Obj' 𝒟                               -- object map
    mor'  : ∀{x y : Category'.Obj' 𝒞} → x ⟶' y → obj' x ⟶' obj' y    -- morphism preservation

    -- interaction with setoid structure
    cong : ∀ {x y : Category'.Obj' 𝒞} {f g : x ⟶' y} → f ≈' g → mor' f ≈' mor' g

    -- preservation of finite compositions
    id   : ∀{x : Category'.Obj' 𝒞} → mor' (Id' {A = x}) ≈' Id'       -- identities preservation
    comp : ∀{x y z : Category'.Obj' 𝒞} {f : x ⟶' y} {g : y ⟶' z} → mor' (f ⨾' g) ≈' mor' f ⨾' mor' g  -- composition preservation

 subst-sym : ∀ {a} {A : Set a} (P : A → A → Set) {x x' y y' : A} (xeq : x ≡ x') (yeq : y ≡ y') {p : P x y} {q : P x' y'} → q ≡ ≡-subst₂ P xeq yeq p → p ≡ ≡-subst₂ P (≡-sym xeq) (≡-sym yeq) q
 subst-sym P ≡-refl ≡-refl {p} {.p} ≡-refl = ≡-refl

 subst-trans : ∀ {a} {A : Set a} (P : A → A → Set) {x x' x'' y y' y'' : A} (xeq : x ≡ x') (yeq : y ≡ y') (xeq' : x' ≡ x'') (yeq' : y' ≡ y'') {p : P x y} {q : P x' y'} {r : P x'' y''} → r ≡ ≡-subst₂ P xeq' yeq' q → q ≡ ≡-subst₂ P xeq yeq p → r ≡ ≡-subst₂ P (xeq ⟨≡≡⟩ xeq') (yeq ⟨≡≡⟩ yeq') p
 subst-trans P ≡-refl ≡-refl ≡-refl ≡-refl {p} {.p} {.p} ≡-refl ≡-refl = ≡-refl

       -- this' like subst-dist, with |≡-cong| of two equations using subst
 subst-compose : ∀ {a} {A : Set a} (P : A → A → Set) {x x' y y' z z' : A} (xeq : x ≡ x') (yeq : y ≡ y') (zeq : z ≡ z') (p : P x y) (p' : P x' y') (q : P y z) (q' : P y' z') → (_◇_  : ∀{m n k} → P m n → P n k → P m k) → p' ≡ ≡-subst₂ P xeq yeq p → q' ≡ ≡-subst₂ P yeq zeq q → (p' ◇ q') ≡ ≡-subst₂ P xeq zeq (p ◇ q)
 subst-compose P ≡-refl ≡-refl ≡-refl p .p q .q _◇_ ≡-refl ≡-refl = ≡-refl
       -- taking cases |p' = subst ... p| and |q' = subst ... q| gives subst-dist ?

 subst-cong : ∀ {a} {A : Set a} (P : A → A → Set) {x x' y y' : A} (xeq : x ≡ x') (yeq : y ≡ y') {p : P x y} {p' : P x' y'} → p' ≡ ≡-subst₂ P xeq yeq p →
                    ∀ {b} {B : Set b} (Q : B → B → Set) {f₀ : A → B} (f : ∀ {m n} → P m n → Q (f₀ m) (f₀ n)) {f'₀ : A → B} {f' : ∀ {m n} → P m n → Q (f'₀ m) (f'₀ n)}
                    → (eq₀ : ∀ {x} → f₀ x ≡ f'₀ x) (eq : ∀ {m n} {r : P m n} → f' r ≡ ≡-subst₂ Q eq₀ eq₀ (f r)) 
                    → f' p' ≡ ≡-subst₂ Q (≡-cong f₀ xeq ⟨≡≡⟩ eq₀) (≡-cong f₀ yeq ⟨≡≡⟩ eq₀) (f p)
 subst-cong P ≡-refl ≡-refl {p} {.p} ≡-refl Q f eq₀ eq rewrite eq {r = p} = ≡-refl

 open import Function using (_∘_)
 instance
  𝒞𝒶𝓉' : Category' {ℓsuc ℓ₀} {ℓsuc ℓ₀} {_}
  𝒞𝒶𝓉' =
   record
     { Obj' = Category' {ℓ₀} {ℓ₀} {_}
     ; _⟶_ = Functor'
     ; _≈_ = λ {𝒞} {𝒟} F G → Σ oeq ∶ (∀ {o} → Functor'.obj' F o ≡ Functor'.obj' G o) • ((∀ {X Y} {f : X ⟶' Y} → Functor'.mor' G f ≡ ≡-subst₂ _⟶'_ oeq oeq (Functor'.mor' F f)))
     ; ≈-refl = ≡-refl , ≡-refl
     ; ≈-sym = λ pf → let (oeq , meq) = pf in ≡-sym oeq , subst-sym _⟶'_ oeq oeq meq
     ; ≈-trans = λ pf1 pf2 → let (oeq₁ , meq₁) = pf1 ; (oeq₂ , meq₂) = pf2 in oeq₁ ⟨≡≡⟩ oeq₂ , subst-trans _⟶'_ oeq₁ oeq₁ oeq₂ oeq₂ meq₂ meq₁
     ; _⨾_ = λ {A} {B} {C} F G → record { obj' = Functor'.obj' G ∘ Functor'.obj' F ; mor' = Functor'.mor' G ∘ Functor'.mor' F ; cong = Functor'.cong G ∘ Functor'.cong F ; id = λ {x} → Category'.≈-trans C (Functor'.cong G (Functor'.id F)) (Functor'.id G) ; comp = Category'.≈-trans C (Functor'.cong G (Functor'.comp F)) (Functor'.comp G) }
     ; ⨾-cong = λ {C D A} {F} {F'} {G} {G'} feq geq → let (oeq₁ , meq₁) = feq ; (oeq₂ , meq₂) = geq in ≡-cong (Functor'.obj' G) oeq₁ ⟨≡≡⟩ oeq₂ , subst-cong (Category'._⟶_ D) oeq₁ oeq₁ meq₁ (Category'._⟶_ A) (Functor'.mor' G) oeq₂ meq₂
     ; assoc = ≡-refl , ≡-refl
     ; Id = record { obj' = λ x → x ; mor' = λ x → x ; cong = λ {x} {y} {f} {g} z → z ; id = ≈-refl ; comp = ≈-refl }
     ; leftId = ≡-refl , ≡-refl
     ; rightId = ≡-refl , ≡-refl
     }
     where

 instance
  𝒮et' : Category'
  𝒮et' =
    record
      { Obj' = Set
      ; _⟶_ = λ A B → (A → B)
      ; _≈_ = λ f g → ∀ {x} → f x ≡ g x
      ; ≈-refl = ≡-refl
      ; ≈-sym = λ eq → ≡-sym eq
      ; ≈-trans = λ f≈g g≈h → f≈g ⟨≡≡⟩ g≈h
      ; _⨾_ = λ f g → g ∘ f
      ; ⨾-cong = λ {A B C} {f f'} {g g'} f≈f' g≈g' → ≡-cong g f≈f' ⟨≡≡⟩ g≈g'
      ; assoc = ≡-refl
      ; Id = λ x → x
      ; leftId = ≡-refl
      ; rightId = ≡-refl
      }

  -- make this as an excercise, since it is essentially CAT but without extra proof obligations for functors
  𝒢𝓇𝒶𝓅𝒽' : Category'
  𝒢𝓇𝒶𝓅𝒽' =
    record
      { Obj' = Graph
      ; _⟶_ = GraphMap
      ; _≈_ = λ {G} {H} f g → Σ veq ∶ (∀ {v} → ver f v ≡ ver g v) •
    (∀ {x y e} → edge g {x} {y} e ≡ ≡-subst₂ (λ a b → Graph._⟶_ H a b) veq veq (edge f {x} {y} e))
      ; ≈-refl = ≡-refl , ≡-refl
      ; ≈-sym = λ f≈g → let (veq , eeq) = f≈g in ≡-sym veq , subst-sym (Graph._⟶_ _) veq veq eeq
      ; ≈-trans = λ f≈g g≈h → let (veq₁ , eeq₁) = f≈g ; (veq₂ , eeq₂) = g≈h in veq₁ ⟨≡≡⟩ veq₂ , subst-trans (Graph._⟶_ _) veq₁ veq₁ veq₂ veq₂ eeq₂ eeq₁
      ; _⨾_ = λ f g → record { ver = ver f ⨾' ver g ; edge = edge f ⨾' edge g } -- using |𝒮et|
      ; ⨾-cong = λ {G} {H} {K} {f} {f'} {g} {g'} f≈f' g≈g' → let (veq₁ , eeq₁) = f≈f' ; (veq₂ , eeq₂) = g≈g' in ≡-cong (ver g) veq₁ ⟨≡≡⟩ veq₂ , subst-cong (Graph._⟶_ _) veq₁ veq₁ eeq₁ (Graph._⟶_ _) (edge g) veq₂ eeq₂
      ; assoc = ≡-refl , ≡-refl
      ; Id = record { ver = Category'.Id 𝒮et' ; edge = Category'.Id 𝒮et' }
      ; leftId = ≡-refl , ≡-refl
      ; rightId = ≡-refl , ≡-refl
      }

  𝒰' : Functor' 𝒞𝒶𝓉' 𝒢𝓇𝒶𝓅𝒽'
  𝒰' =  record {
     obj' = λ 𝒞 → record { V = Category'.Obj' 𝒞 ; _⟶_ = Category'._⟶_ 𝒞 }
   ; mor' = λ F  → record { ver = Functor'.obj' F ; edge = Functor'.mor' F }
   ; cong = λ f≈g → f≈g ; id = ≡-refl , ≡-refl ; comp = λ {x} {y} {z} {f} {g} → ≡-refl , ≡-refl }


𝒫'₀ : Graph → Category'
𝒫'₀ G = let open TypedPaths G in 
  record
    { Obj' = Graph.V G
    ; _⟶_ = _⇝_
    ; _≈_ = _≡_
    ; ≈-refl = ≡-refl
    ; ≈-sym = ≡-sym
    ; ≈-trans = _⟨≡≡⟩_
    ; _⨾_ = _++_
    ; ⨾-cong = λ p≈p' q≈q' → ≡-cong₂ _++_ p≈p' q≈q'
    ; assoc = λ {x y z ω p q r} → assoc {p = p}
    ; Id = λ {x} → x !
    ; leftId = leftId
    ; rightId = rightId
    }

𝒫₁' : ∀{G H} → GraphMap G H → Functor' (𝒫'₀ G) (𝒫'₀ H)
𝒫₁' {G} {H} f = record { obj' = ver f ; mor' = fmap ; cong = ≡-cong fmap ; id = ≡-refl ; comp = λ {x} {y} {z} {p} → comp {p = p} }
    where
      open TypedPaths {{...}} public

      fmap : {x y : Graph.V G} →  x ⇝ y → (ver f x) ⇝ (ver f y)
      fmap (x !) = ver f x !
      fmap (x ⟶[ e ]⟶ p) = ver f x ⟶[ edge f e ]⟶ fmap p

      comp : {x y z : Graph.V G} {p : x ⇝ y} {q : y ⇝ z} → fmap (p ++ q) ≡ fmap p ++ fmap q
      comp {x} {p = .x !} = ≡-refl -- since ! is left unit of ++
      comp {x} {p = .x ⟶[ e ]⟶ ps} = ⟶-≡ (comp {p = ps})

𝒫' : Functor' 𝒢𝓇𝒶𝓅𝒽' 𝒞𝒶𝓉'
𝒫' = record { obj' = 𝒫'₀ ; mor' = 𝒫₁' ; cong = λ f≈g → proj₁ f≈g ,  gg f≈g ; id = ≡-refl , idm ; comp = ≡-refl , compmor }
    where
      open TypedPaths {{...}} public
         
      idm : ∀ {G} {x y} {p : x ⇝ y} → Functor'.mor' (Category'.Id 𝒞𝒶𝓉' {𝒫'₀ G}) p ≡ Functor'.mor' (𝒫₁' (Category'.Id 𝒢𝓇𝒶𝓅𝒽')) p
      idm {G} {x} {p = .x !} = ≡-refl
      idm {G} {x} {p = .x ⟶[ e ]⟶ ps} = ⟶-≡ (idm {p = ps})    

      -- general version of gelping, keep it around, possibly as an excercise
      helping : ∀ {a b} {A : Set a} (B : A → A → Set b) (P : A → A → Set) (cons : (s : A) (i : A) → B s i → (t : A) → P i t → P s t) →
                  {x x' i i' t t' : A} {e : B x i} {e' : B x' i'} {ps : P i' t'}
                  (eqi : i' ≡ i) (eqt : t' ≡ t) (eqx : x' ≡ x) (eqe : e' ≡ ≡-subst₂ B (≡-sym eqx) (≡-sym eqi) e)
                → cons x i e t (≡-subst₂ P eqi eqt ps) ≡ ≡-subst₂ P eqx eqt (cons x' i' e' t' ps) 
      helping B P cons ≡-refl ≡-refl ≡-refl ≡-refl = ≡-refl

      gelping :  {G : Graph} {x x' i i' t t' : Graph.V G} {e : Graph._⟶_ G x i} {e' : Graph._⟶_ G x' i'} {ps : i' ⇝ t'}
                  (eqi : i' ≡ i) (eqt : t' ≡ t) (eqx : x' ≡ x) (eqe : e' ≡ ≡-subst₂ (Graph._⟶_ G) (≡-sym eqx) (≡-sym eqi) e)
                → x ⟶[ e ]⟶ (≡-subst₂ _⇝_ eqi eqt ps) ≡ ≡-subst₂ _⇝_ eqx eqt (x' ⟶[ e' ]⟶ ps) -- read right-to-left this says we can shunt a subst over the inductive path constructor
      gelping ≡-refl ≡-refl ≡-refl ≡-refl = ≡-refl

      -- or in the case of graph maps
      gelpingg : {G H : Graph} {f g : GraphMap G H} {x i t : Graph.V G} {e : Graph._⟶_ G x i} {ps : ver f i ⇝ ver f t}
                 (eq : Category'._≈_ 𝒢𝓇𝒶𝓅𝒽' f g)
                → let veq = proj₁ eq in
                  ver g x ⟶[ edge g e ]⟶ (≡-subst₂ _⇝_ veq veq ps) ≡ ≡-subst₂ _⇝_ veq veq (ver f x ⟶[ edge f e ]⟶ ps)
      gelpingg (veq , eeq) = gelping veq veq veq (subst-sym (Graph._⟶_ _) veq veq eeq)

      gg : ∀ {x y} {f g : GraphMap x y} (f≈g : Category'._≈_ 𝒢𝓇𝒶𝓅𝒽' f g) {X Y : Category'.Obj' (𝒫'₀ x)} {p : (𝒫'₀ x Category'.⟶ X) Y}
         → Functor'.mor' (𝒫₁' g) p ≡ ≡-subst₂ (Category'._⟶_ (𝒫'₀ y)) (proj₁ f≈g) (proj₁ f≈g) (Functor'.mor' (𝒫₁' f) p)
      gg eq {X = X} {p = .X !} rewrite (proj₁ eq) {X} = ≡-refl
      gg eq {X = X} {p = .X ⟶[ e ]⟶ ps} rewrite gg eq {p = ps} = gelpingg eq

      open Category {{...}}
      compmor : ∀ {G H K} {f : G ⟶ H} {g : H ⟶ K} {x y} {p : x ⇝ y} → Functor'.mor' (𝒫₁' f ⨾' 𝒫₁' g) p ≡ Functor'.mor'(𝒫₁' (f ⨾' g)) p
      compmor {x = x} {p = .x !} = ≡-refl
      compmor {x = x} {p = .x ⟶[ e ]⟶ ps} = ⟶-≡ (compmor {p = ps})

module freedom' (G : Obj 𝒢𝓇𝒶𝓅𝒽) {𝒞' : Category' {ℓ₀} {ℓ₀} {ℓ₀} } where

  open TypedPaths G using (_! ; _⟶[_]⟶_ ;  _⇝_ ; _++_)
  open Category {{...}}

  ι' : G ⟶ Functor'.obj' 𝒰' (𝒫'₀ G)
  ι' = record { ver = Id ; edge = λ {x} {y} e → x ⟶[ e ]⟶ y ! }

  lift' : G ⟶ (Functor'.obj' 𝒰') 𝒞' → 𝒫'₀ G ⟶' 𝒞'
  lift' f = record { obj' = λ v → ver f v ; mor' = toMap ; cong = cong ; id = ≈-refl ; comp = λ {x y z p q} → proof {x} {y} {z} {p} {q} }
     where
          toMap : ∀ {x y} → x ⇝ y → ver f x ⟶' ver f y
          toMap (y !) = Id'
          toMap (x ⟶[ e ]⟶ p) = edge f e ⨾' toMap p
          cong : ∀ {x y} {p q : x ⇝ y} → p ≡ q → Category'._≈_ 𝒞' (toMap p) (toMap q)
          cong ≡-refl = ≈-refl

          proof : ∀{x y z} {p : x ⇝ y} {q : y ⇝ z} → Category'._≈_ 𝒞' (toMap (p ++ q)) (toMap p ⨾' toMap q)
          proof {p = ._ !} = ≈-sym leftId' -- ≡-sym (Category'.leftId {!!})
          proof {p = ._ ⟶[ e ]⟶ ps} = ≈-trans (⨾-cong ≈-refl (proof {p = ps})) (≈-sym (Category'.assoc 𝒞'))
{-
  property' : ∀{f : G ⟶ (Functor'.obj' 𝒰') 𝒞'} → Category'._≈_ 𝒢𝓇𝒶𝓅𝒽' f (ι' ⨾' (Functor'.mor' 𝒰') (lift' f))
  property' {f} = ≡-refl , {!now need to add setoid structure to graphs!}
-}
\end{spec}
\end{comment}
%}}}
%{{{ old stuff

\begin{comment}
However, we must admit that a slight downside of typed approach, the two-piece definition, is now
we will need to use the following `shifting' combinators: they shift, or slide, the edge-types.
\begin{spec}
-- |\>>| , casting
_⟫_ : ∀{x y y'} → x ⟶ y → y ≡ y' → x ⟶ y'
e ⟫ ≡-refl = e

-- casting leaves the edge the same, only type information changes
≅-⟫ : ∀{x y y'} {e : x ⟶ y} (y≈y' : y ≡ y') → e ≅ e ⟫ y≈y'
≅-⟫ ≡-refl = ≅-refl
\end{spec}
Such is the cost of using a typed-approach.

Even worse, if we use homogeneous equality then we'd have the ghastly operator
\begin{spec}
≡-⟫ : ∀{x y y'} {e : x ⟶ y} (y≈y' : y ≡ y') → e ⟫ y≈y' ≡ (≡-subst (λ ω → x ⟶ ω) y≈y' e)
\end{spec}

\end{comment}
%}}}

old imports
\begin{spec}
open import Data.Empty using (⊥-elim)
open import Relation.Nullary
open import Data.Nat.Properties using (≰⇒> ; <-trans ; m≤m+n ; _+-mono_ ; ≤-step ; ≤⇒pred≤)
open import Relation.Binary.PropositionalEquality.Core using () renaming (subst to ≡-subst)
open import Data.Nat.Properties.Simple using (+-comm ; +-right-identity ; +-suc)
open import Data.Fin.Properties using (bounded ; toℕ-fromℕ≤ ; inject≤-lemma)
\end{spec}

% Local Variables:
% folded-file: t
% eval: (fold-set-marks "%{{{ " "%}}}")
% eval: (fold-whole-buffer)
% fold-internal-margins: 0
% end:

Can we turn any relation into a category? Well we know that preorder relations yield categories,
so let's consider transforming arbitrary relations to preorders.

Suppose we have a relation |R : X → X → Set| and we want it to be a preorder such as
|≤ : ℕ → ℕ → Set|. Then we need it to be reflexive and transitivie; that is,
|∀ x ∶ X • R x x|, |∀ x y ∶ X • R x y → R y x|, and |∀ x y z ∶ X • R x y → R y z → R x z| are
provable, respectively.

(As it stands, this relation is precicely a graph!
If we want a relation in the traditional sense of ordered pairs, then we want a simple-graph.
\begin{spec}
simple : ∀ {x y} (p q : R x y) → p ≡ q    -- at most one edge between any two pair
\end{spec}
)


[[

above when defined poset category, or rather after hom is defined,

mention how intervals a..b are realised in the cat, say via hom??

]]

Then, |≤R| is a total order.
\begin{spec}
data _≤R : X → X → Set where
  embed : ∀ {x y} → R x y → x ≤R y                      -- existing edges
  refl  : ∀ {x} → x ≤R x                                 -- empty path
  trans : ∀ {x y z} → x ≤R y → y ≤R z → x ≤R z         -- path concatenation
\end{spec}
Observe that |embed| says that the order |≤R| contains |R|. 

(|≤R| is also known as the "reachiability poset of R" ??)

Usual definition is |≤R ≔ R* = λ x y → Σ n ∶ ℕ • Rⁿ x y| where
|R⁰ x y = (x ≡ y)| and |Rⁿ⁺¹ x y ≡ Σ i • R x y ∧ Rⁿ i y|; the reflexive transitive closure of
|R|. While this is more compact, the Agda version is easier to work with and it is equivalent
since |embed| corresponds to |n=1|, |refl| corresponds to |n = 0|, and |trans| corresponds to
the `multiplication' operation since |Rⁿ⁺¹ x y ⇔ Σ a,b ∶ ℕ • a + b ≡ n ∧ Σ i • Rᵃ x i ∧ Rᵇ i y|
---or so I claim!

For example, if |R = { (1,2) , (3,4) }| then
\begin{spec}
≤R =
{
  (1,2) , (3,2),               -- embed
  (1,1), (2,2), (3,3),         -- refl
  -- trans gives no new pairs
}
\end{spec}
An example algorithm for finding the transitive closure is Warshall's algorithm.

Notice that if |R| reflexive or transitive, then we do not have uniqunenss of proofs for
|≤R|. In particular, suppose |R| is reflexive and such proofs are constructed by |r_|.
Then a proof of |x ≤R x| can be obtained in two ways: |refl {x}| or |embed (r x)|.

Now the resulting category can be thought of as the free-category on |R|; what's the associated
adjunctin to this claim o mine? That is, functors from this free cat correspond to relational
homomorphisms?? Consider consulting Schmidt and Strohnelin.

Is this is the least preorder relation on R?
\begin{spec}
suppose ⊑ is a reflexive relation that contains R, then

given p : x ≤R y  --ignoring transitivity
there are two cases.

Case p = embed q. Then q yields a proof of x ⊑ y since ⊑ contians R and q is an R proof.
Case p = refl {x}. Then x ⊑ x holds since ⊑ is relfexive.

Hence, ≤R (ignoring transitivity) is the least reflexive relation contianing R.

Suppose ⊑ is also transitive.

Then the only remaining case is

Case p = trans q r, where q : x ≤R y, r : y ≤R z, Then by induction we have proofs
  x ⊑ y ⊑ z, but ⊑ is transitive and so we have a proof of x ⊑ z.

Thus, ≤R is the least preorder containing R!! Woah! Awesome!

\end{spec}


Every preorder can be obtained as the closure of its Hasse/covers relation:
|∀ R preorder • R ≅ ≤[ R ] ≅ ≤[ Covers R ]| (in the category of relations and relation homomorphisms),
where |Covers R x y ≡ x ≠ y ∧ x R y ∧ ¬ Σ z • z ≠ x R z R y ≠ z|. ?? Is this true, or do I just
think it to be true...

In particular, taking |R = ℙₙ|, which is a hasse relation, yields the free preorder on R
which is essentially the free category on the poset |≤[ R ]|.


----

Now R can be thought of as a directed graph.
If we take |R = { (i, i+1) ∣ i ∈ 0..n-1} |
then |≤R| is the free graph on |ℙₙ|, right??

moreover it is a total order: we can show
\begin{spec}
total : ∀ {x y} → x ≤R y ⊎ y ≤R x
antisym : ∀ {x y} → x ≤R y → y ≤R x → x ≡ y
\end{spec}

Also such categories of paths are known as simplicies??

\url{https://ncatlab.org/nlab/show/simplex+category}

\url{http://mathoverflow.net/questions/159989/internal-logic-of-the-topos-of-simplicial-sets}

\end{document}

% Local Variables:
% folded-file: t
% eval: (fold-set-marks "%{{{ " "%}}}")
% eval: (fold-whole-buffer)
% fold-internal-margins: 0
% end:
