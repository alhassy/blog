# README :: how to set up
# cd ~
# gedit .bashrc
# place the following function at the bottom, save and close
# source .bashrc
# go to blog/literate
# fromLiterate myfile.lagda ; cd .. ; ./site rebuild

# ADD TO SCRIPT: alter the \section{S} rewrite rule to produce: <a name="S with spaces replaced by dashes"/> S ##
# so that headers come equipped with usable anchors immediately.
# example of where I had to do this manually was in SoP3 for Eucilidean Algorithm section.

# COULDN'T WE USE PANDA FOR TRANSLATION FROM LATEX TO MARKDOWN??
# (probably, but this was fun as well.)

function fromliterate(){
  NAME="$1"    # name of file we're working on

  # translate all LaTeX back-tics to markdown single-quotes
  sed "s/\`/'/g" $NAME.lagda > $NAME.markdown                    # rewrite to new file

  # replace literate code block delimiters with those of markdown
  sed -i 's/\\begin{code}/```agda/g' $NAME.markdown   
  sed -i 's/\\end{code}/```/g' $NAME.markdown      

  # replace literate spec block delimiters with those of markdown
  sed -i 's/\\begin{spec}/```agda/g' $NAME.markdown   
  sed -i 's/\\end{spec}/```/g' $NAME.markdown      

  # generate coloured Standalone html file
  #
  # pandoc -o $NAME.html $NAME.markdown -Ss

  # remove all LaTeX comments: anything including and after a percent sign
  sed 's/%.*//' -i $NAME.markdown    # the backslash escapes symbols

  # replace latex \section{...} with markdown \n#...
  sed 's/\\section{\(.*.\)}/\n#\1/' -i $NAME.markdown
  # src: read sed tutorial here http://www.grymoire.com/Unix/Sed.html#uh-3

  # replace latex \subsection{...} with markdown \n##...
  sed 's/\\subsection{\(.*.\)}/\n##\1/' -i $NAME.markdown

  # replace latex \href{link}{name} with markdown-style [name](link)
  sed 's/\\href{\(.*.\)}{\(.*.\)}/[\2](\1)/' -i $NAME.markdown

  # remove any remaining lines beginning with, or containing, LaTeX commands
  sed '/\\.*/ d' -i $NAME.markdown   # escaping the backslash gives us a backslash-symbol
  #
  # also remove any leading blank lines at the top, before the title-date,
  # otherwise hakyll (./site) complains
  sed '/./,$!d' -i $NAME.markdown

  # translate all literate-agda |...| code markers to markdown-backquotes
  sed -i 's/|/`/g' $NAME.markdown

  # move file to blog/posts
  mv $NAME.markdown ../posts
  }
