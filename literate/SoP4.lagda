---
title: SoP4: Arbitrary Type Formers
date: April 21, 2016
tags: SoP
---

% cd literate ; fromliterate SoP4 ; cd .. ; ./site rebuild ; ./site watch

This post discusses how to add arbitrary type former symbols to the grammar introduced in the
\href{http://alhassy.bitbucket.org/posts/SoP1.html#examples}{previous SoP article}, thereby extending the expressitivity
of the language.

\begin{code}
{-# OPTIONS --no-positivity-check #-}
-- {-# OPTIONS --no-positivity-check --show-implicit #-}


module SoP4 where

open import Data.Vec renaming (map to vmap) hiding (tail)
open import Data.Bool hiding (_∧_ ; _∨_ ; _≟_) renaming (Bool to 𝔹)
open import MyPrelude hiding  (_⨾_ ; swap ; [_] ; _$_) renaming (¬_ to NOT ; _>_ to _ℕ>_)
open import Data.String -- my very first time using Data.String; `primTrustMe' :] ...!
\end{code}

Just as in the \href{http://alhassy.bitbucket.org/posts/SoP3.html}{previous post},
we need to use a dangerous option. It's needed for our new improved(?) notion of type.

!! TO DO: make func more level polymorphic by using Lift and redefine $ do to the lift/lower
packing/unpacking so that we can just do stuff like 'f $ vec' with the plumming done by $  !!
\begin{comment}
\begin{code}
-- exercise: Func n s t ≡ s → s → ⋯ → s → t , with n-many s arguments
Func : ∀ {ℓ} (a : ℕ) (s : Set ℓ) (t : Set ℓ) → Set ℓ
Func zero s t = t
Func (suc a) s t = s → Func a s t

_$_ : ∀ {ℓ n} {s t : Set ℓ} → Func n s t → Vec s n → t
f $ [] = f
f $ (x ∷ xs) = f x $ xs
-- a sort-of leap-rule: x leaps over $ at the cost of turining it into a ∷

-- If S → s and t → T then Func n s t → Func n S T, note the contravaraince
func-alt : ∀ {n ℓ ℓ'}{s t : Set ℓ} {s' t' : Set ℓ'} → (s' → s) → (t → t') → Func n s t → Func n s' t'
func-alt {zero}  restrict extend t₀ = extend t₀
func-alt {suc n} restrict extend r x = func-alt {n} restrict extend (r (restrict x))
\end{code}
\end{comment}

%{{{ Definition
\section{Definition}

A *type former* is a way to form new types from existing ones.
A *type* is just a type former paired with the type parameters it needs.
So the two are mutually defined:
\begin{code}
-- a 'type symbol' is a named item that is formed by taking a number of other such named items,
-- it's meaning is understood by an operation that takes the aforementioned parameters and produces an concrete Agda Set.

record Type {ℓ : Level} : Set (ℓsuc ℓ) where
  constructor MkType
  field
    name       : String
    arity      : ℕ
    parameters : Vec Type arity
    semantics  : Func arity Type (Set ℓ)

open Type
\end{code}
We use |name|ed type formers since we use that field to decide equality; keep reading.
Also, notice that |Type {ℓ}| is a bit analogous to Agda's |Set ℓ| in that we can lift between
`type-universes':
\begin{code}
Liftₜ : ∀ {ℓ m} → Type {m} → Type {ℓ ⊔ m}
Liftₜ {ℓ} (MkType name zero [] semantics) = MkType name zero [] (Lift {ℓ = ℓ} semantics)
Liftₜ {ℓ} (MkType name (suc arity) (p ∷ ps) semantics) = Liftₜ {ℓ} (MkType name arity ps (semantics p))
\end{code}

Question: does we have |semantics (Liftₜ t) ≡ func-alt lift lift (semantics t)|? Or something similar?

%}}}

%{{{ Semantics
\section{Semantics}
Just as the type former definition was similar to the
\href{http://alhassy.bitbucket.org/posts/SoP2.html}{|InterpretedFunctionSymbol|} definition, it is no
surprise that their semantics are also similarly defined:
\begin{code}
⟦_⟧ₜ : ∀ {ℓ} → Type {ℓ} → Set ℓ
⟦ MkType name arity paramters semantics ⟧ₜ = semantics $ paramters
\end{code}
% Perhaps if we blur the distinction between value and type, as in dependent type theory,
% we can simplify affairs. For now, let us proceed in this naive approach
% ---since it's easy and intuitive, if anything else.
%}}}

%{{{ Examples
\section{Examples}
Let's regain our old types,
\begin{code}
𝒩 : Type {ℓzero}
𝒩 = MkType "𝒩" 0 [] ℕ
\end{code}
Read: 𝒩 is a type named "𝒩" that takes 0 arguments being [].

Observe that compared with the
\href{http://alhassy.bitbucket.org/posts/SoP1.html}{initial version},
we no-longer need worry about |Lift|.
However, this comes at the cost of using a dangerous |option| and, as you'll see,
a postulate for equality.
Is this a fair trade? Well, we also get a whole new plane of expressitivity, so
it just might be worth it.

Likewise for propositions,
\begin{code}
𝒫 : Type
𝒫 = MkType "𝒫" 0 [] Set
\end{code}

However, we can now introduce a more complex type into our toy language!
\begin{code}
-- Imperative languages have arrays (not lists) as primitives ;)
Array : ∀ {ℓ} → Type {ℓ} → Type {ℓ} 
Array t = MkType "Array" 1 [ t ] (List ∘ ⟦_⟧ₜ)
  where
    open import Data.List using (List)
\end{code}
Read this as, "|Array t| is a (parameterised) type named |Array| that takes |1| parameter which is |t|
and is reifed as |List ⟦ t ⟧ₜ|".
%}}}

%{{{ Equality
\section{Equality}

Considering two types to be equal if they are syntactically indistinguishable (ie irrespective of their semantics, more-or-less)
\begin{code}
{- insert blub about trust me and eg how it can be used to mimic Haskell canonical typeclases
-- is theere an option or some compiler method to enforce this?
UniqueSemantics : {!!} -- ∀ {ℓ} {t : TT ℓ} (p q : HasSemantics t) → p ≡ q
UniqueSemantics = {!!} -- _ _ = 

 insert blub about why this is bad due to lack of computation
postulate
  type-eq-is-syntactic-ish
    : ∀ {n a ℓ p} {𝒮 𝒮' : Func a Type (Set ℓ)} → MkType n a p 𝒮 ≡ MkType n a p 𝒮'
-}

type-eq-is-syntactic-ish
  : ∀ {n a ℓ p} {𝒮 𝒮' : Func a Type (Set ℓ)} → MkType n a p 𝒮 ≡ MkType n a p 𝒮'
type-eq-is-syntactic-ish = trustMe where open import Relation.Binary.PropositionalEquality.TrustMe
-- making this a postulate will break things, trust me! or try it yourself.

claim₀ : ∀ {ℓ n} {𝒮 𝒮' : Func 0 Type (Set ℓ)} → 𝒮 ≡ 𝒮' → MkType n 0 [] 𝒮 ≡ MkType n 0 [] 𝒮'
claim₀ p = type-eq-is-syntactic-ish

claim₀˘ : ∀ {ℓ n} {𝒮 𝒮' : Func 0 Type (Set ℓ)} → MkType n 0 [] 𝒮 ≡ MkType n 0 [] 𝒮' → 𝒮 ≡ 𝒮'
claim₀˘ ≡-refl = ≡-refl

-- claim₁ : ∀ {ℓ n p} {𝒮 𝒮' : Func (suc zero) Type (Set ℓ)} → MkType n 0 [] (𝒮 p) ≡ MkType n 0 [] (𝒮' p) → MkType n 1 [ p ] 𝒮 ≡ MkType n 1 [ p ] 𝒮'

-- claimmm : ∀ {ℓ n a p ps} {𝒮 𝒮' : Func (suc a) Type (Set _)} → MkType n a (𝒮 p) ps ≡ MkType n a (𝒮' p) ps → MkType n (suc a) 𝒮 (p ∷ ps) ≡ MkType n (suc a) 𝒮' (p ∷ ps)

\end{code}
we can show that type equality is decidable:
\begin{spec}
_≟ₜ_ : ∀ {ℓ} → Decidable {A = Type {ℓ}} _≡_
\end{spec}

This was a bit of fun coding, so I'll leave it to you to do yourself as well.
The approach I took used the idea that pair equality is extensional:
\begin{spec}
∀ p q p' q' → (p , q) ≡ (p' , q') → p ≡ p' × q ≡ q'
\end{spec}
In particular,
\begin{spec}
∀ a₁ a₂ a₃ a₄ a₁' a₂' a₃' a₄' → MkType a₁ a₂ a₃ a₄ ≡ MkType a₁ a₂ a₃ a₄ → aᵢ ≡ aᵢ'
\end{spec}
excluding the semantics component, ie |i ≠ 3|.
Moreover, notice that identical types have identical 'tails' --see the semantics definition
above.

If you come up with a proof that does not rely on such ingredients, let me know!

\begin{comment}
Solution.

\begin{code}
contrapositive : ∀ {a b} {A : Set a} {B : Set b} → (A → B) → (NOT B → NOT A)
contrapositive a→b = λ ¬b → λ a → ¬b (a→b a)

Type-eq₁ : ∀ {ℓ} {t t' : Type {ℓ}} → t ≡ t' → name t ≡ name t'
Type-eq₁ ≡-refl = ≡-refl

Type-eq₂ : ∀ {ℓ} {t t' : Type {ℓ}} → t ≡ t' → arity t ≡ arity t'
Type-eq₂ ≡-refl = ≡-refl

Type-eq₃ : ∀ {n n' a ℓ} {t t' : Func a (Type {ℓ}) (Set ℓ) } {ps ps'} → MkType n a ps t ≡ MkType n' a ps' t' → ps ≡ ps'
Type-eq₃ ≡-refl = ≡-refl
-- note that there is no a', otherwise we'd need to be substitutive, as above.
-- alternatively
-- using this in a with-clause results in unifcation trouble.
Type-eq₃' : ∀ {ℓ} {t t' : Type {ℓ}} → (eq : t ≡ t') → ≡-subst (Vec Type) (Type-eq₂ eq) (parameters t) ≡ parameters t'
Type-eq₃' ≡-refl = ≡-refl

Type-eq₃₁ : ∀ {n n' a ℓ} {t t' : Func (suc a) (Type {ℓ}) (Set ℓ) } {p ps p' ps'}
        → MkType n (suc a) (p ∷ ps) t ≡ MkType n' (suc a) (p' ∷ ps') t' → p ≡ p'
Type-eq₃₁ ≡-refl = ≡-refl 
-- name reasoning: subscript three for third item of MkType, subscript 1 or 2 for first or second item of _∷_
Type-eq₃₂ : ∀ {n n' a ℓ} {t t' : Func (suc a) (Type {ℓ}) (Set ℓ) } {p ps p' ps'}
        → MkType n (suc a) (p ∷ ps) t ≡ MkType n' (suc a) (p' ∷ ps') t' → ps ≡ ps'
Type-eq₃₂ ≡-refl = ≡-refl

-- identical types have identical tails
Type-eq₄ : ∀ {n n' a ℓ} {t t' : Func (suc a) (Type {ℓ}) (Set ℓ) } {p ps p' ps'}
        → MkType n (suc a) (p ∷ ps) t ≡ MkType n' (suc a) (p' ∷ ps') t'
        → MkType n a ps (t p) ≡ MkType n' a ps' (t' p')
Type-eq₄ ≡-refl = ≡-refl

_≟ₜ_ : ∀ {ℓ} → Decidable {A = Type {ℓ}} _≡_
MkType name arity parameters semantics ≟ₜ MkType name₁ arity₁ parameters₁ semantics₁ with name ≟ name₁
MkType name arity parameters semantics ≟ₜ MkType name₁ arity₁ parameters₁ semantics₁ | no ¬prf = no (contrapositive Type-eq₁ ¬prf) -- differently named types are necessairly different types
MkType name arity parameters semantics ≟ₜ MkType .name arity₁ parameters₁ semantics₁ | yes ≡-refl with arity ≟ℕ arity₁
MkType name arity parameters semantics ≟ₜ MkType .name arity₁ parameters₁ semantics₁ | yes ≡-refl | no ¬prf = no (contrapositive Type-eq₂ ¬prf) -- differently arited types are necessairly different types
MkType name .0 [] semantics ≟ₜ MkType .name .0 [] semantics₁ | yes ≡-refl | yes ≡-refl = yes type-eq-is-syntactic-ish
MkType name _ (x ∷ parameters) semantics ≟ₜ MkType .name _ (x₁ ∷ parameters₁) semantics₁ | yes ≡-refl | yes ≡-refl with x ≟ₜ x₁
MkType name _ (x ∷ parameters) semantics ≟ₜ MkType .name _ (x₁ ∷ parameters₁) semantics₁ | yes ≡-refl | yes ≡-refl | no ¬prf = no (contrapositive Type-eq₃₁ ¬prf) -- differentpy paramertises types are necessairly different types
MkType name _ (p ∷ ps) 𝒮 ≟ₜ MkType .name _ (.p ∷ ps') 𝒮' | yes ≡-refl | yes ≡-refl | yes ≡-refl with MkType name _ ps (𝒮 p) ≟ₜ MkType name _ ps' (𝒮' p)
MkType name _ (p ∷ ps) 𝒮 ≟ₜ MkType .name _ (.p ∷ ps') 𝒮' | yes ≡-refl | yes ≡-refl | yes ≡-refl | no ¬p = no (contrapositive Type-eq₄ ¬p) -- differently tailed/semtantics types are necessairly differnet types
MkType name _ (p ∷ ps) 𝒮 ≟ₜ MkType .name _ (.p ∷ ps') 𝒮' | yes ≡-refl | yes ≡-refl | yes ≡-refl | yes p₁ with Type-eq₃ p₁
MkType name _ (p ∷ ps) 𝒮 ≟ₜ MkType .name _ (.p ∷ .ps) 𝒮' | yes ≡-refl | yes ≡-refl | yes ≡-refl | yes p₁ | ≡-refl = yes type-eq-is-syntactic-ish
-- note
-- MkType name ._ tf (p ∷ ps) ≟ₜ MkType .name ._ reify (.p ∷ ps') | yes ≡-refl | yes ≡-refl | yes ≡-refl with lem _≟ₜ_ ps ps'
-- causes termination checking to fail :/
-- where
-- lem : ∀ {ℓ} {A : Set ℓ} → Decidable {A = A} _≡_ → ∀ {n} → Decidable {A = Vec A n} _≡_
\end{code}
\end{comment}

%}}}

%{{{ Strings for variables
\section{Strings for variables}

While we're at it, let's extend our notion of identifiers to be arbitrary
strings instead of the three |x , y , z| of the initial post.

\begin{code}
-- typed strings
data Variable {ℓ} : Type {ℓ} → Set (ℓsuc ℓ) where
  MkVar : String → ∀ {t} → Variable t
\end{code}

The common identifiers, |╲Mcx ╲Mcy ╲Mcz|,
\begin{code}
𝓍 𝓎 𝓏 : ∀ {ℓ} {t : Type {ℓ}} → Variable t
𝓍 = MkVar "x" ; 𝓎 = MkVar "y" ; 𝓏 = MkVar "z"
\end{code}

Possibly useful for readability, eg |"x" ∶ 𝒩|.
\begin{code}
_∶_ : ∀ {ℓ} → String → (t : Type {ℓ}) → Variable t
name ∶ type = MkVar name
\end{code}

(Unjustified) decidable equality,
\begin{code}
_≟ᵥ_ : ∀ {a b t s} → Variable {a} t → Variable {b} s → 𝔹
MkVar x ≟ᵥ MkVar y with x ≟ y
MkVar x ≟ᵥ MkVar y | yes p = true
MkVar x ≟ᵥ MkVar y | no ¬p = false
\end{code}

%}}}

-- below is old stuff

-- make everuthing work then try new stuff out by a simple program to sum elements of an array

%{{{ old stuff


•
adding ∀{ℓ}→… since Type uses a level.

•
⟦_⟧ₑ
{-# TERMINATING #-} -- need to add this as I know it will terminate; I did not need this when I was using Agda 2.4, but today I switched to Agda 2.5
-- all recursive calls clearly, humanly at least, are smaller and so it'll terminate. If I don't use the pragma, then I'd need to do some cleverness
-- that is not terribly insightful, afaik.

•
We can still manually embed operations into expressions without too much trouble, however |Lift|
rears its head since interpreted function symbols need the source and target to live in the same
universe, a property it inheritys from (the current implementaiton of) |Func|. 

To regain the heterogenous |unreify| combinator, the ones that take naturals and return
propositions for example, takes a bit of work.
For example, the \href{http://alhassy.bitbucket.org/posts/SoP2.html}{old}
|lift₂| and its associated |unreify| are revamped as
\begin{spec}
liftₑ : ∀ {ℓ m t} → Expr {m} t → Expr (Liftₜ {ℓ ⊔ m} t)

unreify₂ℓℓ : ∀ {ℓs ℓ t} {s : Type {ℓs}}
           → (⟦ s ⟧ₜ → ⟦ s ⟧ₜ → ⟦ t ⟧ₜ) → (a b : Expr (Liftₜ {ℓ ⊔ ℓs} s)) → Expr {ℓ ⊔ ℓs} t

unreify₂ℓ : ∀ {ℓs ℓ t s} → (⟦ s ⟧ₜ → ⟦ s ⟧ₜ → ⟦ t ⟧ₜ) → (a b : Expr {ℓs} s) → Expr {ℓ ⊔ ℓs} t
\end{spec}
Now one uses this last combinator without refeering to lifts or expression embeddings. Neato!


\begin{code}

State : ∀ {ℓ} → Set (ℓsuc ℓ)
State {ℓ} = {t : Type {ℓ}} → Variable t → ⟦ t ⟧ₜ

_[_↦_] : ∀ {ℓ} → State {ℓ} → ∀ {t} → Variable t → ⟦ t ⟧ₜ → State
_[_↦_] σ {t} v e {s} v∶s with t ≟ₜ s
_[_↦_] σ v e v∶s | yes ≡-refl = if v ≟ᵥ v∶s then e else σ v∶s
_[_↦_] σ v e v∶s | no ¬p = σ v∶s



-- Interpreted function symbols; source and target are in the same type-universe
record IFS {ℓ : Level} (arity : ℕ) : Set (ℓsuc ℓ) where
  field
    src tgt : Type {ℓ}
    reify   : Func arity ⟦ src ⟧ₜ ⟦ tgt ⟧ₜ



--an expression is a variable, a constant symbol, or a function symbol applied to other terms.
data Expr {ℓ : Level} : Type {ℓ} → Set (ℓsuc ℓ) where
  Var : ∀ {t} (v : Variable t) → Expr t
  𝒞  : ∀ {t} → ⟦ t ⟧ₜ → Expr t
  app : {arity : ℕ} (fsym : IFS {ℓ} arity)
      → Vec (Expr {ℓ} (IFS.src fsym)) arity → Expr (IFS.tgt fsym)

infix 5 ⟦_⟧ₑ
{-# TERMINATING #-} -- need to add this as I know it will terminate; I did not need this when I was using Agda 2.4, but today I switched to Agda 2.5
-- all recursive calls clearly, humanly at least, are smaller and so it'll terminate. If I don't use the pragma, then I'd need to do some cleverness
-- that is not terribly insightful, afaik.
⟦_⟧ₑ : ∀ {ℓ t} → Expr {ℓ} t → State {ℓ} → ⟦ t ⟧ₜ
⟦ Var v ⟧ₑ σ = σ v
⟦ 𝒞 C ⟧ₑ σ = C
⟦ app {zero } fsym [] ⟧ₑ σ = IFS.reify fsym
⟦ app {suc n} fsym (e ∷ args) ⟧ₑ σ = 
  let
    open IFS fsym
    fsym-tail : IFS n
    fsym-tail = record { src = src ; tgt = tgt ; reify = reify (⟦ e ⟧ₑ σ) }
    -- cannot use syntax: record fsym {reify = ...} since fsym is an IFS (suc n) and
    -- we want an IFS n.
  in
  ⟦ app fsym-tail args ⟧ₑ σ

-- The substitution over a variable occurs if the variables match, substitution over
-- constants does nothing, and substitution over function symbol application is to just substitute
-- over the argument terms.
_[_/_] : ∀ {ℓ s t} (E : Expr {ℓ} t) (e : Expr {ℓ} s) (v : Variable s) → Expr t
_[_/_] {ℓ} {s} {t} (Var v) v₁ e' with s ≟ₜ t
Var v [ e / v₁ ] | yes ≡-refl = if v ≟ᵥ v₁ then e else Var v
Var v [ e / v₁ ] | no ¬p = Var v
𝒞 c  [ e / v  ] = 𝒞 c
app fsym args [ e / v ] = app fsym (helper args)
  where helper : ∀ {n} → Vec (Expr (IFS.src fsym)) n → Vec (Expr (IFS.src fsym)) n
        helper {zero } [] = []
        helper {suc n} (E ∷ ls) = E [ e / v ] ∷ helper ls

-- alternative notation for constant formation
as₀ : ∀ {ℓ} (t : Type {ℓ}) → ⟦ t ⟧ₜ → Expr t
as₀ t e = 𝒞 e
syntax as₀ t e = e as t

[/]₂-test₁ : (Var 𝓍)[ 42 as 𝒩 / 𝓍 ] ≡ 42 as 𝒩
[/]₂-test₁ = ≡-refl
-- mention/test that this fails if we use postulate of syntactic-ish instead of trustMe

[/]-test : ∀ {f : ℕ → ℕ → ℕ}
  → let _⊕_ = λ a b → app (record { src = 𝒩 ; tgt = 𝒩 ; reify = f }) (a ∷ b ∷ [])
     in
          (Var 𝓍 ⊕ 𝒞 1)[ 123 as 𝒩 / 𝓍 ]
        ≡ ( Var 𝓍 [ 123 as 𝒩 / 𝓍 ]) ⊕ (𝒞 1 [ 123 as 𝒩 / 𝓍 ])
        ×
          (Var 𝓍 ⊕ 𝒞 1)[ 123 as 𝒩 / 𝓍 ] ≡ (𝒞 123 ⊕ 𝒞 1)
[/]-test {f} = ≡-refl , ≡-refl

\end{code}
_[_,_/₂_,_] : ∀ {ℓ s₁ s₂ t} → Expr {ℓ} t → Expr {ℓ} s₁ → Expr {ℓ} s₂ → Variable s₁ → Variable s₂ → Expr {ℓ} t
_[_,_/₂_,_] {ℓ} {s₁} {s₂} {t} (Var v) e₁ e₂ v₁ v₂ with s₂ ≟ₜ t
Var v [ e₁ , e₂ /₂ v₁ , v₂ ] | yes ≡-refl = if v ≟ᵥ v₂ then e₂ else (Var v) [ e₁ / v₁ ]
_[_,_/₂_,_] {ℓ} {s₁} {s₂} {t} (Var v) e₁ e₂ v₁ v₂ | no ¬p with s₁ ≟ₜ t
Var v [ e₁ , e₂ /₂ v₁ , v₂ ] | no ¬p | yes ≡-refl = if v ≟ᵥ v₁ then e₁ else Var v
Var v [ e₁ , e₂ /₂ v₁ , v₂ ] | no ¬p₁ | no ¬p = Var v
𝒞 c [ e₁ , e₂ /₂ v₁ , v₂ ] = 𝒞 c
(app fsym args) [ e₁ , e₂ /₂ v₁ , v₂ ] = app fsym (mapper args)
  where open IFS fsym
        mapper : ∀ {n} → Vec (Expr src) n → Vec (Expr src) n
        mapper {zero} [] = []
        mapper {suc n} (E ∷ ES) = (E [ e₁ , e₂ /₂ v₁ , v₂ ]) ∷ mapper ES


-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
-- README
--
-- use unreify₂ to turn an ⊕ : A → A → A into one for Expr A → Expr A → Expr A
-- use unreify₂ℓ to turn an ⊕ : A → A → B  into on for Expr A → Expr A → Expr B, *where* B lives in a higher universe than A
--
-- Similarly for subscript 1.

-- Perhaps the name |unreify| is not the best..something to consider!

-- two argument case; homogeneous universe
unreify₂ : ∀ {ℓ s t} → (⟦ s ⟧ₜ → ⟦ s ⟧ₜ → ⟦ t ⟧ₜ) → (a b : Expr {ℓ} s) → Expr {ℓ} t
unreify₂ {ℓ} {s} {t} r a b = app symb (a ∷ b ∷ [])
  where
    symb : IFS 2
    symb = record { src = s ; tgt = t ; reify = r }

-- one argument case; homogenous universe
unreify₁ : ∀ {ℓ s t} → (⟦ s ⟧ₜ → ⟦ t ⟧ₜ) → (Expr {ℓ} s → Expr {ℓ} t)
unreify₁ {ℓ} {s} {t} r a = app symb (a ∷ [])
  where
    symb : IFS 1
    symb = record { src = s ; tgt = t ; reify = r }

infixr 9 ¬_
¬_ : (p : Expr 𝒫) → Expr 𝒫
¬_ = unreify₁ NOT

infixr 8 _∨_
_∨_ : (p q : Expr 𝒫) → Expr 𝒫
_∨_ = unreify₂ _⊎_

liftₑ : ∀ {ℓ m t} → Expr {m} t → Expr (Liftₜ {ℓ ⊔ m} t)
liftₑ {ℓ} {m} {t} (Var {t = .t} (MkVar x {t = .t})) = Var (MkVar x)
liftₑ {ℓ} {m} {MkType name zero reify []} (𝒞 c) = 𝒞 (lift c)
liftₑ {ℓ} {m} {MkType name (suc arity) reify (p ∷ ps)} (𝒞 c) = liftₑ {ℓ} {t = MkType name arity (reify p) ps} (𝒞 c)
liftₑ {ℓ} {m} {.(IFS.tgt fsym)} (app {arity = arity} fsym x) = app {ℓ ⊔ m} {arity} ff (mapper x)
  where
    open IFS fsym

    -- just casing and auto :-)
    restrict : ∀ a b (t : Type {b}) → ⟦ Liftₜ {a} t ⟧ₜ → ⟦ t ⟧ₜ
    restrict a b (MkType name zero reify []) (lift lower) = lower
    restrict a b (MkType name (suc arity₁) reify (p ∷ ps)) t₀ = restrict a b (MkType name arity₁ (reify p) ps) t₀

    -- again casing and auto :D
    extend : ∀ a b (t : Type {b}) → ⟦ t ⟧ₜ → ⟦ Liftₜ {a} t ⟧ₜ
    extend a b (MkType name zero reify []) t₀ = lift t₀
    extend a b (MkType name (suc arity₁) reify (p ∷ ps)) t₀ = extend a b (MkType name arity₁ (reify p) ps) t₀

    r : Func arity ⟦ Liftₜ src ⟧ₜ ⟦ Liftₜ tgt ⟧ₜ
    r = func-alt {arity} (restrict (ℓ ⊔ m) m src) (extend (ℓ ⊔ m) m tgt) reify

    ff : IFS {ℓ ⊔ m} arity
    ff = record { src = Liftₜ src ; tgt = Liftₜ tgt ; reify = r }

    mapper : ∀ {n} → Vec (Expr (IFS.src fsym)) n → Vec (Expr (IFS.src ff)) n
    mapper {zero} [] = []
    mapper {suc n} (e ∷ es) = (liftₑ {ℓ} e) ∷ (mapper es)

-- Notice that we cannot write an arbitrary argument version of the following, as in
-- ∀ {n ℓs ℓ t} {s : Type {ℓs}} → Func n ⟦ s ⟧ₜ ⟦ t ⟧ₜ → Func n (Expr (Liftₜ {ℓ ⊔ ℓs} s)) (Expr {ℓ ⊔ ℓs} t)
-- since Func, for now, needs the source and target to live in the same universe.
-- So until we alter Func, say by adding a Lift someplace, we're stuck with what we have.
--
-- two argument case; hetrogenous universe
unreify₂ℓℓ : ∀ {ℓs ℓ t} {s : Type {ℓs}} → (⟦ s ⟧ₜ → ⟦ s ⟧ₜ → ⟦ t ⟧ₜ) → (a b : Expr (Liftₜ {ℓ ⊔ ℓs} s)) → Expr {ℓ ⊔ ℓs} t
unreify₂ℓℓ {ℓs} {ℓ} {t} {s} r a b = app {ℓ ⊔ ℓs} {2} sym (a ∷ b ∷ [])
  where

    r' : ∀ {ℓ' ℓs'} {s' : Type {ℓs'}} {t' : Type {ℓ'}}
      → (⟦ s' ⟧ₜ → ⟦ s' ⟧ₜ → ⟦ t' ⟧ₜ)
      → ⟦ Liftₜ {ℓ'} s' ⟧ₜ → ⟦ Liftₜ {ℓ'} s' ⟧ₜ → ⟦ t' ⟧ₜ
    r' {ℓ'} {ℓs'} {MkType name zero reify []} {t'} r₀ (lift p) (lift q) = r₀ p q
    r' {ℓ'} {ℓs'} {MkType name (suc arity) reify (e ∷ es)} {t'} r₀ p q = r' {ℓ'} {ℓs'} {MkType name arity (reify e) es} {t'}
                                                                           r₀ p q
    sym : IFS 2
    sym = record { src = Liftₜ s ; tgt = t ; reify = r' {ℓs ⊔ ℓ} {ℓs} {s} {t} r }

unreify₂ℓ : ∀ {ℓs ℓ t s} → (⟦ s ⟧ₜ → ⟦ s ⟧ₜ → ⟦ t ⟧ₜ) → (a b : Expr {ℓs} s) → Expr {ℓ ⊔ ℓs} t
unreify₂ℓ {ℓs} {ℓ} {t} {s} r a b = unreify₂ℓℓ {ℓs} {ℓ} {t} {s} r (liftₑ {ℓ ⊔ ℓs} a) (liftₑ {ℓ ⊔ ℓs} b)

{- orginally wrote this, then went back and genralise to get the new unreify functions above
infix 10 _>_
_>_ : (m n : Expr 𝒩) → Expr 𝒫
m > n = app {ℓsuc ℓzero} {2} symb (liftₑ m  ∷ liftₑ n ∷ [])
  where
        r : Lift ℕ → Lift ℕ → Set
        r (lift p) (lift q) = q <ℕ p
        symb : IFS 2
        symb = record { src = Liftₜ 𝒩 ; tgt = 𝒫 ; reify = r } -- 
-}
infix 10 _>_
_>_ : (m n : Expr 𝒩) → Expr 𝒫
_>_ = unreify₂ℓ (λ x y → y <ℕ y)

infix 10 _<_
_<_ : (m n : Expr 𝒩) → Expr 𝒫
m < n = n > m

infix 10 _≠_
_≠_ : (m n : Expr 𝒩) → Expr 𝒫
_≠_ = unreify₂ℓ (λ x y → x ≡ y → ⊥)

-- Excellent! Now the user need only use these combinators and worry not about the underlying
-- presentation of interpreted function symbols.

infix 10 _≈_
_≈_ : (m n : Expr 𝒩) → Expr 𝒫
_≈_ = unreify₂ℓ _≡_

infixr 11 _-_
_-_ : (m n : Expr 𝒩) → Expr 𝒩
_-_ = unreify₂ _∸_

infix 10 _≤_
_≤_ : (m n : Expr 𝒩) → Expr 𝒫
_≤_ = unreify₂ℓ _≤ℕ_

infixr 9 _∧_
_∧_ : (p q : Expr 𝒫) → Expr 𝒫
_∧_ = unreify₂ _×_

infixr 11 _+_
_+_ : (m n : Expr 𝒩) → Expr 𝒩
_+_ = unreify₂ _+ℕ_

-- Even more so, we can form simple notions of quantification.
-- For example, simple summation:
--
-- sum over `body' with index `dummy' ranging over all natural numbers that are at most `bound'
infix 10 Σ_≤_•_
Σ_≤_•_ : Variable 𝒩 → ℕ → Expr 𝒩 → Expr 𝒩
Σ_≤_•_ dummy bound body =
         app symb (vmap (λ i → body [ 𝒞 i / dummy ]) (downFrom bound))
  where

    -- downFrom n ≡ [n , n-1, n-2, …, 1, 0]
    downFrom : (n : ℕ) → Vec ℕ (suc n)
    downFrom zero = zero ∷ []
    downFrom (suc n) = suc n ∷ downFrom n

    -- ⟨ i , n ⟩ takes n-many arguments and adds them up then adds i on top of that
    --
    -- ⟨ i , n ⟩ a₁ a₂ ⋯ aₙ ≡ i + a₁ + a₂ + ⋯ + aₙ ;; a fold in differnt form
    ⟨_,_⟩ : ℕ → (n : ℕ) → Func n ⟦ 𝒩 ⟧ₜ ⟦ 𝒩 ⟧ₜ
    ⟨ i , zero  ⟩   = i
    ⟨ i , suc n ⟩ j = ⟨ i +ℕ j , n ⟩

    example : ⟨ 0 , 4 ⟩ 100 22 31 7 ≡ (100 +ℕ 22 +ℕ 31 +ℕ 7)
    example = ≡-refl

    symb : IFS (suc bound)
    symb = record { src = 𝒩 ; tgt = 𝒩 ; reify = ⟨ 0 , suc bound ⟩ }    
--
--
-- To Do: consider making unreifyₙ, this is the homogenous version, as it is essentially the above
-- and so useful for quantifiers.

module testing (default : ∀ {ℓ} {t : Type {ℓ}} → ⟦ t ⟧ₜ) where

  --  default state
  σ₀ : ∀ {ℓ} → State {ℓ}
  σ₀ {ℓ} {t} v = default {t = t}

  downFrom : (n : ℕ) → Vec ℕ (suc n)
  downFrom zero = zero ∷ []
  downFrom (suc n) = suc n ∷ downFrom n

  -- ⟨ i , n ⟩ takes n-many arguments and adds them up then adds i on top of that
  ⟨_,_⟩ : ℕ → (n : ℕ) → Func n ⟦ 𝒩 ⟧ₜ ⟦ 𝒩 ⟧ₜ
  ⟨ i , zero  ⟩   = i
  ⟨ i , suc n ⟩ j = ⟨ i +ℕ j , n ⟩

  example : ⟨ 0 , 4 ⟩ 100 22 31 7 ≡ (100 +ℕ 22 +ℕ 31 +ℕ 7)
  example = ≡-refl

  symb : ∀ {bound} → IFS (suc bound)
  symb {bound} = record { src = 𝒩 ; tgt = 𝒩 ; reify = ⟨ 0 , suc bound ⟩ }    

-- Then, say, the sum of the successor of naturals below 3 is 10:
-- ie 10 ≡ 10
  sum-test :   ⟦ Σ 𝓍 ≤ 0 • Var 𝓍 + 𝒞 1 ⟧ₑ σ₀
           ≡ ⟦ app symb (vmap (λ i → 𝒞 i + 𝒞 1) (downFrom 0)) ⟧ₑ σ₀
  sum-test = {!!}



\end{spec}

Notice that |Σ x ≤ 3 • Var x + 𝒩 2| is really |Σ x ≤ 3 • (Var x + 𝒩 2)| and not
|(Σ x ≤ 3 • Var x) + 𝒩 2|; which is consistent with the convention that quantifiers have scope as
large/rightwards as possible.
We've achieved this by the fixity declarations: summation is 10, which is higher priority than
the 11 of addition.

Let's consider a more complicated example; say a summation that involves variables.
\begin{spec}
σ₁ : State
σ₁ {𝒩} x = lift 1
σ₁ {𝒩} y = lift 2
σ₁ {𝒩} z = lift 3
σ₁ {𝒫} v = ⊥

sum-test2 :  ⟦ Σ x ≤ 3 • (Var x + Var y) - Var z ⟧ₑ σ₁
           ≡ ⟦ (𝒩 2 - 𝒩 3) + (𝒩 3 - 𝒩 3) + (𝒩 4 - 𝒩 3) + (𝒩 5 - 𝒩 3)  ⟧ₑ σ₁
sum-test2 = ≡-refl
\end{spec}
Notice that |x| in the sum is not treated as a global variable and so state |σ₁| is not called-upon for it.

Before we move on, let us remark that placing a bound of the form |x ∶ m … n| is tricky and I've failed to realize it
since the interpretation portion of the symbol means I need a vector of length |suc (n ∸ m)| which
is difficult to write. I've tried mimicking the Haskell definition and anyhow it's not of immediate
import. Still, it was hard and I just decided to use the case |m ≡ 0| only.

This can be remedied by adding quantifiers explicitly to the expression syntax, thereby taking in
a state for the interpretation. In particular,
\begin{spec}
⟦ SUM dummy bound range body ⟧ σ =
  reify (vmap (λ i → if ⟦ range [ dummy / 𝒩 i ] ⟧ σ then body [ dummy / 𝒩 i ] else 𝒩 0) (downFrom bound))
\end{spec}
Just something to think about.
%}}}

%{{{ unreify
\section{Unreify}

While regaining some of our function symbols, one might've noticed a general pattern and it is that
which we abstract here. That is, we will write a function that takes a real item and turns it into
an an interpreted function symbol application.

begin{spec}


Let's wrap-up by looking at an intersting theorem.
\begin{spec}
-- constant expression constructor
𝒞 : ∀ {t} → ⟦ t ⟧ₜ → Expr t
𝒞 {𝒩} e = 𝒩 (lower e)
𝒞 {𝒫} e = 𝒫 e

⟦𝒞⟧ : ∀ {t} {c : ⟦ t ⟧ₜ} {σ : State} → ⟦ 𝒞 c ⟧ₑ σ ≡ c
⟦𝒞⟧ {𝒩} = ≡-refl
⟦𝒞⟧ {𝒫} = ≡-refl

inverses : ∀ {s t} (f : ⟦ s ⟧ₜ → ⟦ t ⟧ₜ) (σ : State)
         →  let reify : ∀ {s' t'} (g : Expr s' → Expr t') → (⟦ s' ⟧ₜ → ⟦ t' ⟧ₜ) 
                reify g = λ a → ⟦ g (𝒞 a) ⟧ₑ σ in
          reify (unreify₁ f) ≗ f   -- pointwise equality
inverses f σ = λ a → ≡-cong f ⟦𝒞⟧
\end{spec}
Notice that this reify differs from |InterpretedFunctionSymbol.reify| !
What of |unreify₁ (reify f) ≗ f| ? exercise.

% notice that reify plays the role of eval if we construe this as an exponential object!

%}}}

\begin{comment}
%{{{ old stuff that refers to the new expression datatype
\begin{spec}
infix 2 _⇒_
_⇒_ : Expr 𝒫 → Expr 𝒫 → Set₁
P ⇒ Q = ∀ (σ : State) → ⟦ P ⟧ₑ σ → ⟦ Q ⟧ₑ σ

True : Expr 𝒫
True = 𝒫 ⊤

everything-implies-truth : ∀ {R} → R ⇒ True
everything-implies-truth = λ _ _ → tt

False : Expr 𝒫
False = 𝒫 ⊥

false-is-strongest : ∀ {Q} → False ⇒ Q
false-is-strongest σ ()

⇒-refl : ∀ {P} → P ⇒ P
⇒-refl σ pf = pf

-- C-c C-a
⇒-trans : ∀ {P Q R} → P ⇒ Q → Q ⇒ R → P ⇒ R
⇒-trans = λ {P} {Q} {R} z₁ z₂ st z₃ → z₂ st (z₁ st z₃)

∧-sym : ∀ {P Q} → P ∧ Q ⇒ Q ∧ P
∧-sym st (p , q) = q , p

infix 7 _≔_ _,_≔₂_,_
infixr 5 _⨾_
data Program : Set₁ where
  skip  : Program
  abort : Program
  _⨾_   : Program → Program → Program
  _≔_      : ∀ {t} → Variable t → Expr t → Program
  _,_≔₂_,_ : ∀ {s t} → Variable t → Variable s → Expr t → Expr s → Program

infixr 5 _⨾⟨_⟩_
data ⟦_⟧_⟦_⟧ : Expr 𝒫 → Program → Expr 𝒫 → Set₁ where
  _⨾⟨_⟩_      : ∀ {Q S₁ P₁ P₂ S₂ R} → ⟦ Q ⟧ S₁ ⟦ P₁ ⟧ → P₁ ⇒ P₂ → ⟦ P₂ ⟧ S₂ ⟦ R ⟧ → ⟦ Q ⟧ S₁ ⨾ S₂ ⟦ R ⟧
  skip-rule  : ∀ {Q R} → Q ⇒ R → ⟦ Q ⟧ skip ⟦ R ⟧
  absurd     : ∀ {Q R} → Q ⇒ False → ⟦ Q ⟧ abort ⟦ R ⟧
  assignment : {t : Type} (v : Variable t) (e : Expr t) (Q R : Expr 𝒫)
             → Q ⇒ R [ e / v ] → ⟦ Q ⟧ v ≔ e ⟦ R ⟧ 
  simuAssign : {t₁ t₂ : Type} (v₁ : Variable t₁) (v₂ : Variable t₂) (e₁ : Expr t₁) (e₂ : Expr t₂)
               (Q R : Expr 𝒫) → Q ⇒ R [ e₁ , e₂ /₂ v₁ , v₂ ] → ⟦ Q ⟧ v₁ , v₂ ≔₂ e₁ , e₂ ⟦ R ⟧               

-- for easy copy/paste: ⟦ ? ⟧ ? ≔ ? since ? ⟦ ? ⟧
syntax assignment v e Q R Q⇒Rₑˣ = ⟦ Q ⟧ v ≔ e since Q⇒Rₑˣ ⟦ R ⟧
syntax simuAssign v₁ v₂ e₁ e₂ Q R Q⇒Rₑˣ = ⟦ Q ⟧ v₁ , v₂ ≔₂ e₁ , e₂ since Q⇒Rₑˣ ⟦ R ⟧
syntax skip-rule {Q} {R} Q⇒R = ⟦ Q ⟧⟨ Q⇒R ⟩⟦ R ⟧ -- ⟦ Q ⟧ skip-since Q⇒R ⟦ R ⟧

-- excercise in C-c C-c and C-c C-a   ^_^
strengthen : ∀ Q {P R S} → Q ⇒ P → ⟦ P ⟧ S ⟦ R ⟧ → ⟦ Q ⟧ S ⟦ R ⟧
strengthen Q {S = skip} Q⇒P (skip-rule P⇒R) = skip-rule (λ st z₁ → P⇒R st (Q⇒P st z₁))
strengthen Q {S = abort} Q⇒P (absurd P⇒False) = absurd (λ st z₁ → P⇒False st (Q⇒P st z₁))
strengthen Q {S = S₁ ⨾ S₂} Q⇒P (⟦P⟧S₁⟦P₁⟧ ⨾⟨ pf ⟩ ⟦P₂⟧S₂⟦R⟧) = strengthen Q Q⇒P ⟦P⟧S₁⟦P₁⟧ ⨾⟨ pf ⟩ ⟦P₂⟧S₂⟦R⟧
strengthen Q {S = .v ≔ .e} Q⇒P (assignment v e P R P⇒Rᵛₑ) = assignment v e Q R (λ st z₁ → P⇒Rᵛₑ st (Q⇒P st z₁))
strengthen Q {S = .v₁ , .v₂ ≔₂ .e₁ , .e₂} Q⇒P (simuAssign v₁ v₂ e₁ e₂ P R P⇒R-sub) = simuAssign v₁ v₂ e₁ e₂ Q R (λ st z₁ → P⇒R-sub st (Q⇒P st z₁))

syntax strengthen Q Q⇒P ⟦P⟧S⟦R⟧ = ⟦ Q ⟧⇒⟨ Q⇒P ⟩ ⟦P⟧S⟦R⟧
\end{spec}
%}}}
\end{comment}

%{{{ Examples
\section{Examples}

Let's produce a big example that uses all program constructs, except abort, and also makes use of the new symbols.
\begin{spec}
-- ∧ monotonicity: a nifty way to introduce implications involving ∧
∧-⇒ : ∀ {P Q P' Q'} → P ⇒ P' → Q ⇒ Q' → P ∧ Q ⇒ P' ∧ Q' 
∧-⇒ p q σ (a , b) = p σ a , q σ b

-- the refl case is so ubiquitous, let's name it.
_⨾⟨⟩_ : ∀ {Q S₁ P S₂ R} → ⟦ Q ⟧ S₁ ⟦ P ⟧ → ⟦ P ⟧ S₂ ⟦ R ⟧ → ⟦ Q ⟧ S₁ ⨾ S₂ ⟦ R ⟧
S ⨾⟨⟩ T = S ⨾⟨ (λ σ z₁ → z₁) ⟩ T -- C-c C-a

test : ⟦ True ⟧
           x ≔ 𝒩 3
         ⨾ skip
         ⨾ y ≔ 𝒩 4
         ⨾ x ≔ (Σ z ≤ 4 • Var z + Var z) -- sum of all even's that're at-most 8 = 0+2+4+6+8 = 20 
         ⨾ x , y ≔₂ Var x + Var y , Var x - Var y
       ⟦ Var x ≈ 𝒩 24 ∧ Var y ≈ 𝒩 16 ⟧
test =
        ⟦ True ⟧
            x ≔ 𝒩 3                                     since (λ σ _ → ≡-refl)
        ⟦ Var x ≈ 𝒩 3                                 ⟧
          ⨾⟨ (λ σ z₁ → z₁)  ⟩ -- ⇒-refl ⟩
        ⟦ Var x ≈ 𝒩 3                                 ⟧⟨ (λ σ z₁ → z₁) -- ⇒-refl 
        ⟩⟦ Var x ≈ 𝒩 3                                ⟧
          ⨾⟨ (λ σ _ → ≡-refl , ≡-refl) ⟩
        ⟦ 𝒩 20 + 𝒩 4 ≈ 𝒩 24 ∧ 𝒩 20 - 𝒩 4 ≈ 𝒩 16   ⟧
          y ≔ 𝒩 4                                       since (λ σ _ → ≡-refl , ≡-refl)
        ⟦ 𝒩 20 + Var y ≈ 𝒩 24 ∧ 𝒩 20 - Var y ≈ 𝒩 16 ⟧
          ⨾⟨ (λ σ z₁ → z₁) ⟩ -- ⇒-refl ⟩
        ⟦ (Σ z ≤ 4 • Var z + Var z) + Var y ≈ 𝒩 24 ∧ (Σ z ≤ 4 • Var z + Var z) - Var y ≈ 𝒩 16 ⟧
          x ≔ (Σ z ≤ 4 • Var z + Var z)                  since (λ σ z₁ → z₁) -- ⇒-refl
        ⟦ Var x + Var y ≈ 𝒩 24 ∧ Var x - Var y ≈ 𝒩 16 ⟧
          ⨾⟨⟩
        ⟦ Var x + Var y ≈ 𝒩 24 ∧ Var x - Var y ≈ 𝒩 16 ⟧
          x , y ≔₂ Var x + Var y , Var x - Var y since (λ σ z₁ → z₁) -- ⇒-refl
        ⟦ Var x ≈ 𝒩 24 ∧ Var y ≈ 𝒩 16 ⟧
\end{spec}
Notice that the type is a claim we are making about the correctness of a program
and the body is a proof of that claim.

Of course one should check that our earlier programs still work with the function symbols in-place.
Exercise ;)
\begin{comment}
\begin{spec}
prog₂ : ⟦ True ⟧ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₂ = ⟦ True         ⟧
           x ≔ 𝒩 5      since (λ _ _ → ≡-refl) 
        ⟦ Var x ≈ 𝒩 5 ⟧

swap2 : ∀ {X Y : ℕ} → ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧ x , y ≔₂ Var {𝒩} y , Var {𝒩} x ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
swap2 {X} {Y} = ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧
                  x , y ≔₂ Var y , Var x         since ∧-sym {Var x ≈ 𝒩 X} {Var y ≈ 𝒩 Y}
                ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧

prog₃ : ⟦ True ⟧ skip ⨾ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₃ =  ⟦ True ⟧⟨    ⇒-refl {True}
         ⟩⟦ True ⟧
        ⨾⟨ (λ st x₁ → x₁) ⟩
        ⟦ True ⟧
        x ≔ 𝒩 5 since (λ _ _ → ≡-refl)
        ⟦ Var x ≈ 𝒩 5 ⟧

prog₄ : ⟦ True ⟧ x ≔ 𝒩 5 ⨾ x ≔ 𝒩 3 ⟦ Var x ≈ 𝒩 3  ⟧ 
prog₄ = ⟦ True ⟧
          x ≔ 𝒩 5       since (λ _ _ → ≡-refl)
        ⟦ 𝒩 3 ≈ 𝒩 3 ⟧
        ⨾⟨ ⇒-refl {𝒩 3 ≈ 𝒩 3} ⟩
        ⟦ 𝒩 3 ≈ 𝒩 3 ⟧
          x ≔ 𝒩 3       since ⇒-refl {𝒩 3 ≈ 𝒩 3}
        ⟦ Var x ≈ 𝒩 3 ⟧

constantEx : ∀ {e : Expr 𝒩} {c : ℕ} → ⟦ Var y ≈ 𝒩 c ⟧ x ≔ e ⟦ Var y ≈ 𝒩 c ⟧
constantEx {e} {c} = ⟦ Var y ≈ 𝒩 c ⟧
                       x ≔ e       since (λ st pf → pf)
                     ⟦ Var y ≈ 𝒩 c ⟧

leq : ⟦ 𝒩 7 ≤ Var x ⟧ x ≔ Var x + 𝒩 3 ⟦ 𝒩 10 ≤ Var x  ⟧
leq = ⟦ 𝒩 7 ≤ Var x ⟧
         x ≔ Var x + 𝒩 3       since lemma1
      ⟦ 𝒩 10 ≤ Var x ⟧
      where
        lemma1 : 𝒩 7 ≤ Var x ⇒ 𝒩 10 ≤ Var x + 𝒩 3
        lemma1 σ 7≤σx = 7≤σx +-mono ≤ℕ-refl {3}
\end{spec}
\end{comment}
%% Awesome! Looks like a paper-and-pencil annotation (with proofs)!
%}}}

\section{Closing}

It seems that we've managed to get a lot done --up to chapter 9 of SoP excluding arrays--.
Hope you've enjoyed the article!


\begin{comment}
%% perhaps a future article about arrays and domain conditions?

%% add division symbol and take care about domain!

%% add array type former?

 mentioned above, in a future post, I intend to replace the explicit constructors by what interpreted
  function symbols, but it seems that I need them to be augmented with a `domain' operation.
  For example, the symbol |÷| can be interpreted as integral division with domain condition being the
  second argument is non-zero.

(
while we're at it, perhaps implement variables as ints under the hood:

data Variable : Type → Set where
  MkVar : ∀ {t} → ℕ → Variable t

_≟ᵥ_ : ∀ {t s} → Variable t → Variable s → 𝔹
(MkVar m) ≟ᵥ (MkVar n) with m ≟ℕ n
MkVar m ≟ᵥ MkVar .m | yes refl = true
MkVar m ≟ᵥ MkVar n | no ¬p = false
(MkVar m) ≟ᵥ _ = false

)
\end{comment}



% Local Variables:
% folded-file: t
% eval: (fold-set-marks "%{{{ " "%}}}")
% eval: (fold-whole-buffer)
% fold-internal-margins: 0
% end:



