---
title: SoP1: Assignment
date: March 31, 2016
tags: SoP
---

This post discusses the syntax and semantics of a simple imperative language with simultaneous assignment.

% cd literate ; fromliterate SoP1 ; cd .. ; ./site rebuild ; ./site watch

\begin{code}
module SoP1 where

open import Data.Bool using(if_then_else_ ; true ; false) renaming (Bool to 𝔹 )
open import MyPrelude hiding  (_⨾_ ; swap) renaming (_×_ to _AND_)
\end{code}
The last import just brings into context the usual notions of propositional equality,
sums, products, function operations,
and a small theory of natural number arithmetic.

%{{{ DataTypes
\section{DataTypes}

We want to describe sets of states of program variables and to write assertions about them.
For simplicity, in our very elementary language (for now!), we only consider |𝒩|atural numbers and
|𝒫|ropositions.
\begin{code}
data Type : Set where
  𝒩  : Type
  𝒫  : Type
\end{code}

Which are then interpreted as elements of type |Set₁|:
\begin{code}
-- subscript t, for "t"ypes
⟦_⟧ₜ : Type → Set₁
⟦ 𝒩 ⟧ₜ = Lift ℕ
⟦ 𝒫 ⟧ₜ = Set
\end{code}
Notice that we interpret propositions constructively, since we're in Agda after-all.
Had we interpreted them as Booleans, reasoning would be a bit tricky since Booleans are not
proof-carrying data. Alternatively: a proposition describes the set of states in which it is true
and in Agda these notions coincide via Curry-Howard.

The |Lift| data type behaves, roughly, as follows: |∀ (i j : Level) → i ≤ j → Set i → Set j|.
That is, it lets us embed a small type into a larger type.
The `equivalence' |A ≅ Lift A| is witnessed by the constructor |lift| and inversely by the eliminator
|lower|.
% record Lift {a ℓ} (A : Set a) : Set (a ⊔ ℓ) where field lower : A

Finally, without any surprise, we can decide when the types coincide or not:
\begin{code}
_≟ₜ_ : Decidable {A = Type} _≡_
𝒩 ≟ₜ 𝒩 = yes ≡-refl
𝒩 ≟ₜ 𝒫 = no (λ ())
𝒫 ≟ₜ 𝒩 = no (λ ())
𝒫 ≟ₜ 𝒫 = yes ≡-refl
\end{code}

It feels like we ought to be able to mechanically derive equality as is done in
Haskell; and indeed there is
\href{http://stackoverflow.com/questions/36209339/haskell-deriving-mechanism-for-agda}{discussion}
on how to achieve this.

%}}}

%{{{ Variables

\section{Variables}

To make assertions of program variables, we need to have variables in the first place.

We use an explicitly dedicated datatype for formal variables.
\begin{code}
data Variable : Type → Set where
  x y z : ∀ {t} → Variable t

_≟ᵥ_ : ∀ {t s} → Variable t → Variable s → 𝔹
x ≟ᵥ x = true
x ≟ᵥ _ = false
y ≟ᵥ y = true
y ≟ᵥ _ = false
z ≟ᵥ z = true
z ≟ᵥ _ = false
\end{code}

A more generic approach would be to parameterise the module by a type for variables that is
endowed with decidable equality ---a setoid.

Note that we cannot consider using de Bruijn indices, as in \href{http://mazzo.li/posts/Lambda.html}{Agda by Example: λ-calculus},
since our variables are not bound to any quantifier. --perhaps I will switch to begin-end
blocks introducing/quantifying over variables.
%}}}

%{{{ Expressions
\section{Expressions}

With types and variables in-hand, we can discuss how to form terms and how to assign meaning to
them.

\subsection{Syntax}

A *term* is defined to be a constant symbol |c|, a variable |v|, or a function/relation symbol |f| applied
to terms:
\begin{spec}
term ∷= constant c ∣ variable v ∣ application f(t₁, …, tₙ)
\end{spec}

For now, we take our function and relation symbols to be addition, ordering, equality, and
conjunction.
\begin{code}
data Expr : Type → Set₁ where
  -- varaibles
  Var : ∀ {t} (v : Variable t) → Expr t

  -- constant constructors
  𝒩 : ℕ → Expr 𝒩
  𝒫 : Set → Expr 𝒫

  -- function/relation symbol application
  _+_ : (m n : Expr 𝒩) → Expr 𝒩
  _≈_ _≤_ : (e f : Expr 𝒩) → Expr 𝒫
  _∧_ : (p q : Expr 𝒫) → Expr 𝒫

infix 11 _+_
infix 10 _≈_ _≤_
infix 8 _∧_
\end{code}
Notice that we make a natural constant by using |𝒩| and the type of the resulting expression is of type |𝒩|,
with this overloading, we can loosely say |𝒩| is the `constructor' for the `type' |𝒩|.
\begin{comment}
If this were the C programming
language, we could, for example, write
\begin{spec}
𝒩 n = 𝒩(5);
\end{spec}
\end{comment}
Likewise for |𝒫|.

Another candidate for constant constructor is,
\begin{spec}
𝒞 : ∀ {t} → ⟦ t ⟧ₜ → Expr t

syntax 𝒞 {t} c = c ∶ t -- ghost colon
\end{spec}
However, I prefer the pair | 𝒩, 𝒫 | as they clearly denote the type of variable I'm working with;
moreover, I cannot make them synonyms for |_ ∶ 𝒩 , _ ∶ 𝒫 | since |𝒩 , 𝒫 | are already defined
and Agda only permits constructor overloading.

Also, I thought about using |𝒱| instead of |Var| but the latter seems clearer and the former clashes with the aforementioned convention of the curly-letters denoting the type of the associated
constant.

\begin{comment}
One final item to note is that |Expr| is `large': |Expr t : Set₁| for any |t|.
Why is this the case? Recall that in Agda we have two main rules
\begin{spec}
-- type "of" type
Set i : Set (i + 1)

-- dependent funtion type
If
   A : Set i
   B : A → Set j
Then
   (a : A) → B a : Set (max i j)

Datatypes belong to universe Set i precisely when all the arguments of all constructors live in
universes below level i.

A record of |n+1| fields can be thought of as a dependent function on |Fin n|, which would live in
Set i if all dependent return types live in universes below i.

Alternativel, dependent intersection provides a different way to look at record:
http://www.cs.cornell.edu/home/kreitz/teaching/cs671/kopylov02a.pdf

discuss church endcoding?
\end{spec}

Now we a constructor of |Expr| is |𝒫 : Set → Exp 𝒫| and the |𝒫 : Set (max 0|
\end{comment}

\subsection{ Semantics }

An expression like |Var x + 𝒩 5| can appear in a program;
when encountered, it is `evaluated' in the current machine `state' to produce another natural
number. What is this state? It is an association of identifiers to values.

\begin{code}
State : Set₁
State = ∀ {t : Type} → Variable t → ⟦ t ⟧ₜ
\end{code}

It is common to denote the interpretation of an expression |e| in state |σ| by |⟦ e ⟧ σ| where
the `semantics-brackets' are defined by
\begin{spec}
⟦ c ⟧ σ =  c  ; ⟦ v ⟧ σ = σ v ; ⟦ f(t₁, …, tₙ) ⟧ σ = ⟦ f ⟧( ⟦ t₁ ⟧ σ , …, ⟦ tₙ ⟧ σ)
\end{spec}
Formally,
\begin{code}
-- subscript e, for "e"xpressions
⟦_⟧ₑ : ∀ {t} → Expr t → State → ⟦ t ⟧ₜ
⟦_⟧ₑ (Var v)  σ = σ v
⟦_⟧ₑ (𝒩 q)   σ = lift q
⟦_⟧ₑ (𝒫 p)   σ = p
⟦_⟧ₑ (e + e₁) σ = lift $ lower (⟦ e ⟧ₑ σ) +ℕ     lower (⟦ e₁ ⟧ₑ σ)
⟦_⟧ₑ (e ≈ e₁) σ = lift (lower (⟦ e ⟧ₑ σ)) ≡ lift (lower (⟦ e₁ ⟧ₑ σ)) -- propositional equality
⟦_⟧ₑ (e ≤ e₁) σ =       lower (⟦ e ⟧ₑ σ)  ≤ℕ     lower (⟦ e₁ ⟧ₑ σ) 
⟦_⟧ₑ (e ∧ e₁) σ =              ⟦ e ⟧ₑ σ   AND            ⟦ e₁ ⟧ₑ σ    -- Cartesian product
\end{code}
%
%  -- isn't lift ∘ lower = id ?? no, cuz of level 'refresh' ! See link above.
%

The addition case works as follows: |⟦ e + e₁ ⟧ σ| is obtained by recursively obtaining the
semantics of the arguments, then using |lower| on the results to obtain the underlying naturals
(of the lifted number type), then adding them, then packing up the whole thing into the lifted
number type again. Similarly for the other cases.

Sometimes we want to update the state for a variable, say when it is reassigned, and we accomplish
this by `function patching':
\begin{spec}
f [z ↦ y] = λ x → If x ≟ z Then y Else f x
\end{spec}
Formally,
\begin{comment}
\begin{code}
_[_↦_] : State → ∀ {t} → Variable t → ⟦ t ⟧ₜ → State
_[_↦_] σ {t} v e {s} v∶s with t ≟ₜ s
_[_↦_] σ v e v∶s | yes ≡-refl = if v ≟ᵥ v∶s then e else σ v∶s
_[_↦_] σ v e v∶s | no ¬p = σ v∶s
\end{code}
\end{comment}
% why two copies of function patching? One works, the other is for the looks.
\begin{spec}
_[_↦_] : State → ∀ {t} → Variable t → ⟦ t ⟧ₜ → State
_[_↦_] σ {t} v e {s} v∶s with t ≟ₜ s
_[_↦_] σ v e v∶s ∣ yes ≡-refl = if v ≟ᵥ v∶s then e else σ v∶s
_[_↦_] σ v e v∶s ∣ no ¬p = σ v∶s
\end{spec}

\subsection{Reasoning about Boolean expressions}

Say proposition |Q| is *weaker* than |P| if |P ⇒ Q|; alternatively, |P| is *stronger* than |Q|.
`A stronger proposition makes more restrictions on the combinations of values its identifiers can
be associated with, a weaker proposition makes fewer.' That is, |Q| is `less restrictive' than
|P|.
\begin{code}
infix 2 _⇒_
_⇒_ : Expr 𝒫 → Expr 𝒫 → Set₁
P ⇒ Q = ∀ (σ : State) → ⟦ P ⟧ₑ σ → ⟦ Q ⟧ₑ σ
\end{code}

The weakest proposition is |true|, or any tautology, because it represents the set of all states
---ie it is always inhabited.
\begin{code}
True : Expr 𝒫
True = 𝒫 ⊤

everything-implies-truth : ∀ {R} → R ⇒ True
everything-implies-truth = λ _ _ → tt
\end{code}

The strongest proposition is |false|, because it represents the set of no states
---ie it is never inhabited.
\begin{code}
False : Expr 𝒫
False = 𝒫 ⊥

false-is-strongest : ∀ {Q} → False ⇒ Q
false-is-strongest σ ()
\end{code}

Before we move on, let us remark that |_⇒_| admits familiar properties, such as
\begin{code}
⇒-refl : ∀ {P} → P ⇒ P
⇒-refl σ pf = pf

-- C-c C-a
⇒-trans : ∀ {P Q R} → P ⇒ Q → Q ⇒ R → P ⇒ R
⇒-trans = λ {P} {Q} {R} z₁ z₂ st z₃ → z₂ st (z₁ st z₃)

∧-sym : ∀ {P Q} → P ∧ Q ⇒ Q ∧ P
∧-sym st (p , q) = q , p
\end{code}

Using |⇒-refl| without mentioning the implicit parameter yields yellow since Agda for some reason
cannot infer them! However, if we comment out the definition of |⟦_⟧ₑ| and postulate it instead,
then no yellow :/ Send help!
%}}}

%{{{ Textual Substitution
\section{Textual Substitution}

Any expression |E| can be considered as a function |E(v₁, …, vₙ)| of it's free-variables, say
|v₁, …, vₙ|, then we can form a new expression |E(e₁, …, eₙ)| for any expressions |eᵢ|.
However, this is cumbersome since the order of variables must be specified and also troublesome
when I only want to substitute a few of the free variables. This is remedied by naming the variables
to be replaced (irrespective of whether they are even in the given expression) and indicating
its replacement as well ---a sort of `named arguments' approach.

This `textual substitution' of variables |v| by expressions |e| is defined,
informally, by three rules:
\begin{spec}
c [ e / v ]           = e
u [ e / v ]           = IF variable u is one of the vᵢ THEN eᵢ ELSE u
f(t₁, …, tₙ) [ e / v ] = f (t₁ [ e / v ], …, tₙ [ e / v ])
\end{spec}

The new term to be introduced occurs *above* the
fraction symbol,
|expression [ new_term / old_variable]|,
and we read |E [t / x] | as `E with t replacing x'.
As such, some would also write |E [x ╲ t]|; other notations include
|E[x ≔ t]| and |Eˣₜ|.
%
% We reserve the `becomes' notation for program assignment, to be defined later on.
% and as a subscript in the other notation.

Warning! Simultaneous is not iteration! In general, |E [ u , v / x , y] ≠ (E [ u / x])[ v / y]|.
For example,
\begin{spec}
(x + y)[ x + y , z / x , y] = (x + y) + z ≠ (x + z) + z = (x + y)[x + y / x][z / y]
\end{spec}

Anyhow, we implement the case |n = 1| and leave the case |n = 2| as an exercise for the reader.
At the moment, I do not foresee the need for the general case; nor do I see a way to make
it (quickly) syntactically appealing in Agda!

\begin{comment}
\begin{code}
_[_/_] : ∀ {s t} (E : Expr t) (e : Expr s) (v : Variable s) → Expr t
_[_/_] {s} {t} (Var v) u e with s ≟ₜ t
Var v [ e / u ] | yes ≡-refl = if v ≟ᵥ u then e else Var v
Var v [ e / u ] | no ¬p = Var v
𝒩 n [ e / v ] = 𝒩 n
𝒫 p [ e / v ] = 𝒫 p
(E + F) [ e / v ] = E  [ e / v ]  +  F [ e / v ]
(E ≈ F) [ e / v ] = E  [ e / v ]  ≈  F [ e / v ]
(E ≤ F) [ e / v ] = (E [ e / v ]) ≤ (F [ e / v ])
(E ∧ F) [ e / v ] = (E [ e / v ]) ∧ (F [ e / v ])
\end{code}
\end{comment}
\begin{spec}
_[_/_] : ∀ {s t} (E : Expr t) (e : Expr s) (v : Variable s) → Expr t
_[_/_] {s} {t} (Var v) u e with s ≟ₜ t
Var v [ e / u ] ∣ yes ≡-refl = if v ≟ᵥ u then e else Var v
Var v [ e / u ] ∣ no ¬p = Var v
𝒩 n [ e / v ] = 𝒩 n
𝒫 p [ e / v ] = 𝒫 p
(E + F) [ e / v ] = E  [ e / v ]  +  F [ e / v ]
(E ≈ F) [ e / v ] = E  [ e / v ]  ≈  F [ e / v ]
(E ≤ F) [ e / v ] = (E [ e / v ]) ≤ (F [ e / v ])
(E ∧ F) [ e / v ] = (E [ e / v ]) ∧ (F [ e / v ])
\end{spec}
Observe,

  - For the variable case, we substitute precisely when the types match; otherwise
    we cannot even define it.

  - If the types do match, then we pattern-match on the proof to obtain |≡-refl| so
    that Agda unifies them, thereby making the given conditional well-typed.

  - If the types do not match, irrespective of whether the variables are identical or not,
    the only thing we can do, while meeting the type requirements, is to leave them alone.
    This subtlety leads to a bit of poor items such as |Var {𝒩} x [ e / x (of type 𝒫) ] = Var {𝒩} x|, or formally:
\begin{code}
subtle : ∀ {e : Expr 𝒫} → (Var {𝒩} x)[ e / x ] ≡ Var {𝒩} x
subtle = ≡-refl
\end{code}
Note that because of the substitution, the second instance of |x| has to have the same type as |e|, which
is |𝒫|. Since the two occurrences of |x| on the left have different types, the substitution does nothing.

  - The last few cases are identical; we intend to remedy this identical coding in the next post by using
    `interpreted function symbols'.

The following is an exercise for the reader, where we want them to implement the function so that
|E [e, d / v,v] = E [d / v]|.
\begin{spec}
_[_,_/₂_,_] : ∀ {s₁ s₂ t} (E : Expr t) (e₁ : Expr s₁) (e₂ : Expr s₂) (v₁ : Variable s₁) (v₂ : Variable s₂)  → Expr t
E [ e₁ , e₂ /₂ v₁ , v₂ ] = {! exercise !}
\end{spec}
\begin{comment}
\begin{code}
-- "hide" this and leave it as an excercise to reader.
-- note that we pattern match on s2 first in order to get the convention |(x, x ≔ e,d) = x≔d|
_[_,_/₂_,_] : ∀ {s₁ s₂ t} (E : Expr t) (e₁ : Expr s₁) (e₂ : Expr s₂) (v₁ : Variable s₁) (v₂ : Variable s₂)  → Expr t
_[_,_/₂_,_] {s₁} {s₂} {t} (Var v) e₁ e₂ u v₁ with s₂ ≟ₜ t
Var v [ e₁ , e₂ /₂ v₁ , v₂ ] | yes ≡-refl = if v ≟ᵥ v₂ then e₂ else (Var v) [ e₁ / v₁ ] -- very important call at the end!
_[_,_/₂_,_] {s₁} {s₂} {t} (Var v) e₁ e₂ v₁ v₂ | no ¬p with s₁ ≟ₜ t
Var v [ e₁ , e₂ /₂ v₁ , v₂ ] | no ¬p | yes ≡-refl = if v ≟ᵥ v₁ then e₁ else Var v -- already tried |₂| and just tried |₁|, no more sunscripts to try.
_[_,_/₂_,_] {s₁} {s₂} {t} (Var v) e₁ e₂ v₁ v₂ | no ¬p₁ | no ¬p = Var v
𝒩 n [ e₁ , e₂ /₂ v₁ , v₂ ] = 𝒩 n
𝒫 b  [ e₁ , e₂ /₂ v₁ , v₂ ] = 𝒫 b
(E + F)  [ e₁ , e₂ /₂ v₁ , v₂ ] = E [ e₁ , e₂ /₂ v₁ , v₂ ] + F [ e₁ , e₂ /₂ v₁ , v₂ ]
(E ≈ F)  [ e₁ , e₂ /₂ v₁ , v₂ ] = E [ e₁ , e₂ /₂ v₁ , v₂ ] ≈ F [ e₁ , e₂ /₂ v₁ , v₂ ]
(E ≤ F)  [ e₁ , e₂ /₂ v₁ , v₂ ] = E [ e₁ , e₂ /₂ v₁ , v₂ ] ≤ F [ e₁ , e₂ /₂ v₁ , v₂ ]
(p ∧ q)  [ e₁ , e₂ /₂ v₁ , v₂ ] = p [ e₁ , e₂ /₂ v₁ , v₂ ] ∧ q [ e₁ , e₂ /₂ v₁ , v₂ ] 
\end{code}
\end{comment}

To close we give some lemmas dealing with textual substitution.
Their proofs would be by induction on expressions and using the definitions of substitution;
all-in-all quite a bore and so not proven here. From Chapter 4 of SoP:

   -
|σ [ v ↦ σ v ] = σ|: if we update at position |v| by doing the same thing we currently
do there, then that's the same thing as no update at all.
\begin{code}
postulate patching-id : ∀ {t} {σ : State} {v : Variable t} → ∀ {u : Variable t} → (σ [ v ↦ σ v ]) u ≡ σ u
\end{code}

   -
Substituting an expression |e| for |v| in |E| and then evaluating in σ yields the same result as
substituting the *value* of |e| in σ for |v| and the evaluating.
% (SoP, 4.6.1).
%
\begin{code}
-- constant helper
𝒞 : ∀ {t} → ⟦ t ⟧ₜ → Expr t
𝒞 {𝒩} c = 𝒩 (lower c)
𝒞 {𝒫} c = 𝒫 c

postulate eval-over-sub : ∀ {t s} {v : Variable t} {σ : State} {e : Expr t} {E : Expr s}
                        → ⟦ E [ e / v ]  ⟧ₑ σ ≡ ⟦ E [ 𝒞 (⟦ e ⟧ₑ σ) / v ] ⟧ₑ σ
\end{code}

   -
Evaluating |E[x / e]| in state σ is the same as evaluating |E| in σ with |x| updated to evaluation
of |e|.
% (SoP, 4.6.2)
\begin{code}
postulate sub-to-state : ∀ {t} {v : Variable t} {e : Expr t} {s} {E : Expr s} {σ : State}
               → ⟦ E [ e / v ] ⟧ₑ σ ≡ ⟦ E ⟧ₑ (σ [ v ↦ ⟦ e ⟧ₑ σ ])
\end{code}
That is, evaluating a substituted expression is the same as evaluating the original expression in an
updated state. Notice that this law allows us to make substitutions part of the syntax and the
law is then how to evaluate such syntax! Is that a better approach?!

   -
   % (SoP, 4.4.7)
\begin{code}
postulate iterated-substitution : ∀ {t} {v : Variable t} {e d : Expr t} {s} {E : Expr s}
                                → (E [ e / v ]) [ d / v ] ≡ E [ e [ d / v ] / v ]
\end{code}
   
   - Substitution of a non-occurring variable: |(E[x / u])[y / v] = E [x / u[y / v] ]|, if |y| is not free in |E|. (SoP, 4.4.8)

   - 
Provided |x = (x₁, …, xₙ)| are distinct identifiers and |u = (u₁, …, uₙ)| are fresh, distinct
identifiers, we have |E[x / u][u / x] = E|.
% (SoP, 4.6.3)

For the curious, here is a very uninsightful proof of one of the above lemmas:
\begin{spec}
patching-id-pf : ∀ {t} {σ : State} (v : Variable t) → ∀ (u : Variable t) → (σ [ v ↦ σ v ]) u ≡ σ u
patching-id-pf {t} {σ} v u with t ≟ₜ t
patching-id-pf v u ∣ no ¬p = ≡-refl
patching-id-pf x x ∣ yes ≡-refl = ≡-refl
patching-id-pf x y ∣ yes ≡-refl = ≡-refl
patching-id-pf x z ∣ yes ≡-refl = ≡-refl
patching-id-pf y x ∣ yes ≡-refl = ≡-refl
patching-id-pf y y ∣ yes ≡-refl = ≡-refl
patching-id-pf y z ∣ yes ≡-refl = ≡-refl
patching-id-pf z x ∣ yes ≡-refl = ≡-refl
patching-id-pf z y ∣ yes ≡-refl = ≡-refl
patching-id-pf z z ∣ yes ≡-refl = ≡-refl
\end{spec}
% above does not work since I'm using FAKE pipe \| rather than real pipe |
% I do so for the resulting output...one of my rewrites replaces pipes with ticks...
\begin{comment}
below is working code!
\begin{code}
patching-id-pf : ∀ {t} {σ : State} (v : Variable t) → ∀ (u : Variable t) → (σ [ v ↦ σ v ]) u ≡ σ u
patching-id-pf {t} {σ} v u with t ≟ₜ t
patching-id-pf v u | no ¬p = ≡-refl
patching-id-pf x x | yes ≡-refl = ≡-refl
patching-id-pf x y | yes ≡-refl = ≡-refl
patching-id-pf x z | yes ≡-refl = ≡-refl
patching-id-pf y x | yes ≡-refl = ≡-refl
patching-id-pf y y | yes ≡-refl = ≡-refl
patching-id-pf y z | yes ≡-refl = ≡-refl
patching-id-pf z x | yes ≡-refl = ≡-refl
patching-id-pf z y | yes ≡-refl = ≡-refl
patching-id-pf z z | yes ≡-refl = ≡-refl
\end{code}
\end{comment}
Admittedly, it could have been simplified had we implemented equality as a |Dec|idable rather than
just a Boolean.

To be fair, here's a another proof of another result: substitution is monotonic. Informally,
\begin{spec}  
  ⟦ P [ v / e ] ⟧ₑ σ
≡
  ⟦ P ⟧ₑ (σ [ v ↦ ⟦ e ⟧ₑ σ ])   -- since sub-to-state
→ 
  ⟦ Q ⟧ₑ (σ [ v ↦ ⟦ e ⟧ₑ σ ])   -- since P ⇒ Q
≡                              
  ⟦ Q [ v / e ] ⟧ₑ σ            -- since sub-to-state
\end{spec}
Rather than create/import the needed syntacic sugar, let's just `wing-it':
\begin{code}
subst-over-⇒ : ∀ {P Q t} {v : Variable t} {e : Expr t} → P ⇒ Q → P [ e / v ] ⇒ Q [ e / v ]
subst-over-⇒ {P} {Q} {t} {v} {e} P⇒Q σ
  rewrite sub-to-state {t} {v} {e} {𝒫} {P} {σ} | sub-to-state {t} {v} {e} {𝒫} {Q} {σ} = P⇒Q _
\end{code}
%}}}

%{{{ Programming Language
\section{Programming Language}

The syntax of our miniature language is as follows,
\begin{code}
data Program : Set₁ where
  skip  : Program
  abort : Program
  _⨾_   : Program → Program → Program
  _≔_      : ∀ {t} → Variable t → Expr t → Program
  _,_≔₂_,_ : ∀ {s t} → Variable t → Variable s → Expr t → Expr s → Program
\end{code}
The subscript |2| in simultaneous assignment is needed, otherwise there'll be parsing conflicts
with the notation for elementary assignment.

\begin{comment}
construction site
\begin{code}
-- x1 , x2, x3, ..., xn ≔ v1, v2, ..., vn
-- x1, n-1 vars ≔ v1 , n-1 vals
-- arbitrary parallel assignment
open import Data.Fin using (Fin) renaming (suc to fsuc)
HVec0 : ∀ {ℓ} (n : ℕ) (f : Fin n → Set ℓ) → Set ℓ
HVec0 {ℓ} n f = (i : Fin n) → f i

tail : ∀ {n} {ℓ} → (Fin (suc n) → Set ℓ) → (Fin n → Set ℓ)
tail {n} {ℓ} f = λ i → f (fsuc i)

-- nonempty heterogenous sequences
open import Data.Vec using (Vec ; []; _∷_) renaming (map to vmap)
data HVec {ℓ} : (n : ℕ) → Vec (Set ℓ) n → Set (ℓsuc ℓ) where
  ε   : HVec 0 []
  _,_ : ∀ {n types t} → t → HVec n types → HVec (suc n) (t ∷ types)

data Assign : Set₂ where
  Mk : {n : ℕ} (types : Vec Type (suc n)) → HVec (suc n) (vmap Variable types)
     → HVec (suc n) (vmap Expr types) → Assign
  -- mention for posterity
  _≔_ : ∀ {t} → Variable t → ⟦ t ⟧ₜ → Assign
  _,,_,,_ : ∀ {t} → Variable t → Assign → ⟦ t ⟧ₜ → Assign -- _,_ makes parsing difficutl due to _×_

eg : Assign
eg = Mk (_ ∷ []) (x , ε) (𝒩 1 , ε) -- (x , (y , (z , ε))) (𝒩 1 , (𝒩 3 , (𝒩 4 , ε)))
\end{code}
\end{comment}

As in usual programming languages, the precedence for the program constructors cannot be greater than that of expressions;
otherwise, we'll need many parentheses.
\begin{code}
infix 7 _≔_
infixr 5 _⨾_
\end{code}

Now we quickly turn to the program semantics ---we do not give `operational semantics' but instead use `Hoare Triples'.
The traditional notation for Hoare Triples is |{Q} S {R}|, with assertions enclosed in braces.
However, since braces are one of the few reserved symbols in Agda, we instead employ the semantics-brackets
yet again. This is not too much of a leap since the conditions are seen to *define* the program and so assign
it meaning ---the ideology mentioned in SoP is that one begins with the pre- and post-conditions then derives the program.
\begin{code}
infixr 5 _⨾⟨_⟩_

data ⟦_⟧_⟦_⟧ : Expr 𝒫 → Program → Expr 𝒫 → Set₁ where
  _⨾⟨_⟩_      : ∀ {Q S₁ P₁ P₂ S₂ R} → ⟦ Q ⟧ S₁ ⟦ P₁ ⟧ → P₁ ⇒ P₂ → ⟦ P₂ ⟧ S₂ ⟦ R ⟧ → ⟦ Q ⟧ S₁ ⨾ S₂ ⟦ R ⟧
  skip-rule  : ∀ {Q R} → Q ⇒ R → ⟦ Q ⟧ skip ⟦ R ⟧
  absurd     : ∀ {Q R} → Q ⇒ False → ⟦ Q ⟧ abort ⟦ R ⟧
  assignment : {t : Type} (v : Variable t) (e : Expr t) (Q R : Expr 𝒫)
             → Q ⇒ R [ e  / v ] → ⟦ Q ⟧ v ≔ e ⟦ R ⟧ 
  simuAssign : {t₁ t₂ : Type} (v₁ : Variable t₁) (v₂ : Variable t₂) (e₁ : Expr t₁) (e₂ : Expr t₂)
               (Q R : Expr 𝒫) → Q ⇒ R [ e₁ , e₂ /₂ v₁ , v₂ ] → ⟦ Q ⟧ v₁ , v₂ ≔₂ e₁ , e₂ ⟦ R ⟧ 
\end{code}

Those who know me know that I simply adore types and so might be wondering why I considered
raw untyped programs, |Program|, then correspondingly
adorned each constructor with a type by considering
programs typed by their specification.
I do this so that

  - given an arbitrary program, we can show it satisfies a specification

  - typed-programs can have more than one solution and so we might want to indicate
  which solution we are interested in.

Anyhow, some remarks are in order:

  - sequential composition is executed by doing the first part then the second part:
    if we want to establish |R| from |Q| by executing |S₁ ⨾ S₂| then necessarily we
    establish an intermediate |P| from |Q| by executing |S₁| and from that
    we reach |R| via |S₂|. Does composition order matter? Nope, we have
    |(S₁ ⨾ S₂) ⨾ S₃ = S₁ ⨾ (S₂ ⨾ S₃)| ---just as |(m × n) × k = m × (n × k)|.

       ``
         Be aware of the role of the semicolon; it is used to *combine* adjacent,
         independent commands into a single command, much the same way it is used
         in English to combine independent clauses. (For an example of its use
         in English, see the previous sentence.)
       ''

       In particular, the semicolon is not a statement terminator;
       which is great since we haven't even defined a notion of statement!

  - execution of |skip| does nothing; some languages denote it by whitespace of by |;|.
    It is an explicit way to say nothing is done ---which is just its definition:
    given |Q| we wish to establish |R| by doing nothing, and this is only possible if
    |Q ⇒ R|. Interestingly, just as |1 × n = n × 1 = n| we have |skip ⨾ S = S ⨾ skip = S|.

  - |abort| should never be executed, since it can only be executed in a state satisfying
    |False| and no state satisfies |False|! If it is ever executed, then the program (and its proof)
    is in error and abortion is called for. Which fits in nicely with the law
    |abort ⨾ S = S ⨾ abort = abort| ---similar to |0 × n = n × 0 = 0|.

  - After execution of |x ≔ e| we have that |x| will contain the value |e| and so
  |R| can be established precisely if |R|, with the value of |x| replaced by the value of |e|
  can be established before the execution. That is, if we can show that the condition with updated
  variables can be established, then making the update establishes the condition.

  - Assignment ``|x| becomes |e|'' should really have proof obligation
  |Q   ⇒  ( domain e  cand  Rˣₑ )|
  where |domain| is a predicate that describes the set of all states in which |e| is well-defined
  and so may be evaluated, and |cand| is just lazy-conjunction: |x cand y = if x then y else false|.
  
  As mentioned above, in a future post, I intend to replace the explicit constructors by what interpreted
  function symbols, but it seems that I need them to be augmented with a `domain' operation.
  For example, the symbol |÷| can be interpreted as integral division with domain condition being the
  second argument is non-zero.
  For now, all of our expressions are well-defined. In the future, we intend to use
  `interpreted function symbols with domain'.

  In practice, the |domain| condition is omitted altogether since assignments should be always
  written in contexts in which the expressions can be properly evaluated.
  If we are deriving our program and know not the context, then the full definition is quiet
  useful.

We now introduce some combinators to make our annotated programs look more like those presented in
the literature.
\begin{code}
-- for easy copy/paste: ⟦ ? ⟧ ? ≔ ? since ? ⟦ ? ⟧
syntax assignment v e Q R Q⇒Rₑˣ = ⟦ Q ⟧ v ≔ e since Q⇒Rₑˣ ⟦ R ⟧
syntax simuAssign v₁ v₂ e₁ e₂ Q R Q⇒Rₑˣ = ⟦ Q ⟧ v₁ , v₂ ≔₂ e₁ , e₂ since Q⇒Rₑˣ ⟦ R ⟧
syntax skip-rule {Q} {R} Q⇒R = ⟦ Q ⟧⟨ Q⇒R ⟩⟦ R ⟧ -- ⟦ Q ⟧ skip-since Q⇒R ⟦ R ⟧
\end{code}

For example, ---Example 1, page 118 of SoP---
\begin{code}
prog₀ : ⟦ 𝒩 5 ≈ 𝒩 5 ⟧ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₀ = assignment _ _ _ _ (λ _ _ → ≡-refl)
\end{code}
Now with out handy-dandy syntax:
\begin{code}
prog₁ : ⟦ 𝒩 5 ≈ 𝒩 5 ⟧ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧ 
prog₁ = ⟦ 𝒩 5 ≈ 𝒩 5 ⟧ x ≔ 𝒩 5 since (λ st → ⇒-refl {𝒩 5 ≈ 𝒩 5} st) ⟦ Var x ≈ 𝒩 5 ⟧
\end{code}
Before we move on, we know from propostional logic that all theorems can be replaced with |true|.
In particular, reflexitvity of equality does not give us much (in our toy language) and so
we can replace it with true which was defined earlier as the unit type.
\begin{code}
prog₂ : ⟦ True ⟧ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₂ = ⟦ True         ⟧
           x ≔ 𝒩 5      since (λ _ _ → ≡-refl) 
        ⟦ Var x ≈ 𝒩 5 ⟧
\end{code}
%}}}

\begin{comment}
%{{{ not occurs

use

{-# OPTIONS --show-implicit #-}

as it helps with level issues!!

It is to be noted that

    execution of |x ≔ e| always establishes |x ≈ e|, provided |x| does not occur in |e|
\begin{spec}
_not-occurs-in_ : ∀ {t} → Variable t → Expr t → Set₁
_not-occurs-in_ {t} v E = ∀ {e : Expr t} → E [ v / e ] ≡ E
-- v not-occurs-in E = ∀ {e : Expr _} → _≡_ {ℓsuc ℓzero} {{!!}} {!!} E -- E [ v / e ] ≡ E
--∀ {e} {σ : State} → ⟦ E [ v / e ] ⟧ₑ σ ≡ ⟦ E ⟧ₑ σ      -- substitution is identity-interpreted
--
-- type of v is not allowed to vary as this gives unneeded expressability.
-- typed varaibles seem more trouble than they're worth..

not-occurs-interpreted : ∀ {v : Variable 𝒩} {E} → v not-occurs-in E
                       → ∀ {e} {σ : State} → ⟦ E [ v / e ] ≈ E ⟧ₑ σ
not-occurs-interpreted {v} {Var {t = .𝒩} v₁} pf {e} {σ} = {!!}
not-occurs-interpreted {v} {𝒩 p} pf {e} {σ} = {!!}
not-occurs-interpreted {v} {E + E₁} pf {e} {σ} = {!!}

-- not-occurs-lemma : ∀ {l r} → x not-occurs-in (l + r) → x not-occurs-in l
-- not-occurs-lemma pf {e} = {!!}

yoyo : ∀ (E e : Expr 𝒩) v → v not-occurs-in E → True ⇒ E [ v / e ] ≈ E
yoyo E e v pf σ tt = {!!}

claim :  ∀ (e : Expr 𝒩) → x not-occurs-in e → ⟦ True ⟧ x ≔ e ⟦ Var x ≈ e ⟧
claim (Var x) pf = assignment x (Var x) (𝒫 ⊤) (Var x ≈ Var x) (λ st x₁ → ≡-refl)
claim (Var y) pf = assignment x (Var y) (𝒫 ⊤) (Var x ≈ Var y) (λ _ _ → ≡-refl)
claim (Var z) pf = assignment x (Var z) (𝒫 ⊤) (Var x ≈ Var z) (λ _ _ → ≡-refl)
claim (𝒩 n)  pf = assignment x (𝒩 n) (𝒫 ⊤) (Var x ≈ 𝒩 n) (λ st _ → ≡-refl)
claim (l + r) pf = assignment x (l + r) (𝒫 ⊤) (Var x ≈ l + r) helper
  where
        help : ∀ {st : State} →
                ⟦(l + r) ≈ ((l + r)[ x / (l + r) ])⟧ₑ st
        help = λ {St} → {!!} -- sym (pf {l + r} {St})

        helper : True ⇒ (l + r) ≈ ((l + r)[ x / (l + r) ])
        helper st tt = {!help!}
\end{spec}
%}}}
\end{comment}

%{{{ Examples
\section{Examples}

We now port over some simple programs from Gries' text and show that readability is still maintained!
We begin with the most complicated one, that is expressible in the language as described so far.

\begin{code}
swap : ∀ {X Y : ℕ} → ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧
                             z ≔ Var {𝒩} x
                           ⨾ x ≔ Var {𝒩} y
                           ⨾ y ≔ Var {𝒩} z
                      ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
swap {X} {Y} =
                   ⟦ Var x ≈ 𝒩 X  ∧ Var y ≈ 𝒩 Y ⟧
                     z ≔ Var x                     since ∧-sym {Var x ≈ 𝒩 X} {Var y ≈ 𝒩 Y}
                   ⟦ Var y ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                   ⨾⟨ (λ st z₁ → z₁) ⟩
                   ⟦ Var y ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                     x ≔ Var y                     since (λ st z₁ → z₁)
                   ⟦ Var x ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                   ⨾⟨ (λ st z₁ → z₁) ⟩
                   ⟦ Var x ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                     y ≔ Var z                     since (λ st z₁ → z₁)
                   ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
\end{code}
% to get rid of yellow, replace ⇒-refl with 
Some things to note:

  - We needed to mention the type 𝒩 since the variables are in-fact always independent; eg, the first |x|
    is not at all related to any second occurrence of |x|. This is like the numerals: one 4 does not affect
    another 4.

  - The proof was really just |C-c C-a/r|, auto and refinements! What I did manually was replace
    the |assignment| constructor with the preferred syntax.

  - finally, equality syntax, as defined so far, is only well-defined for 𝒩 and so we could not
    prove |swap| for all types.

Swapping becomes a one-liner using simultaneous assignment.
\begin{code}
swap2 : ∀ {X Y : ℕ} → ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧ x , y ≔₂ Var {𝒩} y , Var {𝒩} x ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
swap2 {X} {Y} = ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧
                  x , y ≔₂ Var y , Var x         since ∧-sym {Var x ≈ 𝒩 X} {Var y ≈ 𝒩 Y}
                ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
\end{code}
One popular language that offers such a command is Python.

Another example of sequence ---many C-c C-r!
\begin{code}
prog₃ : ⟦ True ⟧ skip ⨾ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₃ =  ⟦ True ⟧⟨    ⇒-refl {True}
         ⟩⟦ True ⟧
        ⨾⟨ (λ st x₁ → x₁) ⟩
        ⟦ True ⟧
        x ≔ 𝒩 5 since (λ _ _ → ≡-refl)
        ⟦ Var x ≈ 𝒩 5 ⟧
\end{code}

\begin{comment}
Skip is idempotent --just like |1² = 1 × 1 = 1|--; page 115.
\begin{code}
skip² : ∀ {R} → ⟦ R ⟧ skip ⨾ skip ⟦ R ⟧
skip² {R} = ⟦ R ⟧⟨ ⇒-refl {R}
            ⟩⟦ R ⟧
            ⨾⟨ ⇒-refl {R} ⟩
            ⟦ R ⟧⟨ ⇒-refl {R}
            ⟩⟦ R ⟧
\end{code}
\end{comment}

Multiple assignments to the same variable mean only the last assignment is realized.
\begin{code}
prog₄ : ⟦ True ⟧ x ≔ 𝒩 5 ⨾ x ≔ 𝒩 3 ⟦ Var x ≈ 𝒩 3  ⟧ 
prog₄ = ⟦ True ⟧
          x ≔ 𝒩 5       since (λ _ _ → ≡-refl)
        ⟦ 𝒩 3 ≈ 𝒩 3 ⟧
        ⨾⟨ ⇒-refl {𝒩 3 ≈ 𝒩 3} ⟩
        ⟦ 𝒩 3 ≈ 𝒩 3 ⟧
          x ≔ 𝒩 3       since ⇒-refl {𝒩 3 ≈ 𝒩 3}
        ⟦ Var x ≈ 𝒩 3 ⟧
\end{code}
Awesome! Looks like a paper-and-pencil annotation (with proofs)!


If the post condition does not involve the variable assigned, then we can ignore it: |x not free in P yields ⟦ P ⟧ x ≔ e ⟦ P ⟧|.
% One says, P is invariant (wrt assignments to variables it does not mention).
Here is a particular case, obtained by C-c C-a then using syntactic sugar --page 119--,
\begin{code}
constantEx : ∀ {e : Expr 𝒩} {c : ℕ} → ⟦ Var y ≈ 𝒩 c ⟧ x ≔ e ⟦ Var y ≈ 𝒩 c ⟧
constantEx {e} {c} = ⟦ Var y ≈ 𝒩 c ⟧
                       x ≔ e       since (λ st pf → pf)
                     ⟦ Var y ≈ 𝒩 c ⟧
\end{code}

Sometimes we'll need lemmas in our annotations.
For example, If I want |x| to be at least 10 after increasing it by 3,
then I necessairly need |x| to be at least 7 before the increment.
[Page 118, but no negative numbers.]
\begin{code}
leq : ⟦ 𝒩 7 ≤ Var x ⟧ x ≔ Var x + 𝒩 3 ⟦ 𝒩 10 ≤ Var x  ⟧
leq = ⟦ 𝒩 7 ≤ Var x ⟧
         x ≔ Var x + 𝒩 3       since lemma1
      ⟦ 𝒩 10 ≤ Var x ⟧
      where
        lemma1 : 𝒩 7 ≤ Var x ⇒ 𝒩 10 ≤ Var x + 𝒩 3
        lemma1 σ 7≤σx = 7≤σx +-mono ≤ℕ-refl {3}
\end{code}
where we have used the fact that addition preserves order,
|_+-mono_ : ∀ {x y u v : ℕ} → x ≤ℕ y → u ≤ℕ v → x +ℕ u ≤ y +ℕ v|.
%}}}

%{{{ Program Equality
\section{Program Equality}

Earlier, when introducing the programming language semantics, we mentioned
certain equalities; such as, |skip| is identity of composition:
|skip ⨾ S = S ⨾ skip = S|. We never defined program equality! Let's fix that.

Two programs are considered equal precisely when they satisfy the same specifications,
\begin{code}
infix 4 _≈ₚ_
_≈ₚ_ : Program → Program → Set₁
S ≈ₚ T = ∀ {Q R} → ⟦ Q ⟧ S ⟦ R ⟧ ⇔ ⟦ Q ⟧ T ⟦ R ⟧
\end{code}
Where |x ⇔ y| just means a pair of functions |x → y| and |y → x|.

\begin{comment}
  -- when there is ambiguity, my convention is to be use the last instance: |x , x ≔ e , f  EQUALS x ≔ f|, and I've implemented things so it occurs this way. In particular, we'll prove this.

\begin{code}
postulate todolem : ∀ {t s} (E : Expr s) (e f : Expr t) → E [ e , f /₂ x , x ] ≡ E [ f / x ]

todo : ∀ {t} (e f : Expr t) Q R → ⟦ Q ⟧ x , x ≔₂ e , f ⟦ R ⟧ → ⟦ Q ⟧ x ≔ f ⟦ R ⟧
todo e f Q R (simuAssign .x .x .e .f .Q .R pf) rewrite todolem R e f = assignment x f Q R pf
\end{code}
\end{comment}

Let us prove the aforementioned unity law. We begin with an expected result, then the needed pieces.
\begin{spec}
strengthen : ∀ Q {P R S} → Q ⇒ P → ⟦ P ⟧ S ⟦ R ⟧ → ⟦ Q ⟧ S ⟦ R ⟧
strengthen = {! exercise in C-c C-c and C-c C-a   ^_^ !}
\end{spec}
\begin{comment}
\begin{code}
-- excercise in C-c C-c and C-c C-a   ^_^
strengthen : ∀ Q {P R S} → Q ⇒ P → ⟦ P ⟧ S ⟦ R ⟧ → ⟦ Q ⟧ S ⟦ R ⟧
strengthen Q {S = skip} Q⇒P (skip-rule P⇒R) = skip-rule (λ st z₁ → P⇒R st (Q⇒P st z₁))
strengthen Q {S = abort} Q⇒P (absurd P⇒False) = absurd (λ st z₁ → P⇒False st (Q⇒P st z₁))
strengthen Q {S = S₁ ⨾ S₂} Q⇒P (⟦P⟧S₁⟦P₁⟧ ⨾⟨ pf ⟩ ⟦P₂⟧S₂⟦R⟧) = strengthen Q Q⇒P ⟦P⟧S₁⟦P₁⟧ ⨾⟨ pf ⟩ ⟦P₂⟧S₂⟦R⟧
strengthen Q {S = .v ≔ .e} Q⇒P (assignment v e P R P⇒Rᵛₑ) = assignment v e Q R (λ st z₁ → P⇒Rᵛₑ st (Q⇒P st z₁))
strengthen Q {S = .v₁ , .v₂ ≔₂ .e₁ , .e₂} Q⇒P (simuAssign v₁ v₂ e₁ e₂ P R P⇒R-sub) = simuAssign v₁ v₂ e₁ e₂ Q R (λ st z₁ → P⇒R-sub st (Q⇒P st z₁))
\end{code}
\end{comment}
What about?
|weaken : ∀ {Q P} R {S} → P ⇒ R → ⟦ Q ⟧ S ⟦ P ⟧ → ⟦ Q ⟧ S ⟦ R ⟧| ?
\begin{comment}
weaken {Q} {P} R {S₁ ⨾ S₂} P⇒R (⟦Q⟧S₁⟦P₁⟧ ⨾⟨ P₁⇒P₂ ⟩ ⟦P₂⟧S₂⟦P⟧) = ⟦Q⟧S₁⟦P₁⟧ ⨾⟨ P₁⇒P₂ ⟩ (weaken R P⇒R ⟦P₂⟧S₂⟦P⟧)
weaken R P⇒R (skip-rule Q⇒P) = skip-rule (λ st z₁ → P⇒R st (Q⇒P st z₁))
weaken R P⇒R (absurd Q⇒False) = absurd Q⇒False
weaken R P⇒R (assignment v e Q P Q⇒Pᵛₑ) = assignment v e Q R (⇒-trans {Q} {P [ v / e ]} {R [ v / e ]} Q⇒Pᵛₑ (subst-over-⇒ {P} {R} P⇒R))
weaken R P⇒R (simuAssign v₁ v₂ e₁ e₂ Q P Q⇒P-sub) = {!another subst over!}
\end{comment}

Let's add some syntacic sugar ---good ol' sweet stuff ^_^
\begin{code}
syntax strengthen Q Q⇒P ⟦P⟧S⟦R⟧ = ⟦ Q ⟧⇒⟨ Q⇒P ⟩ ⟦P⟧S⟦R⟧
\end{code}



\begin{code}
tada : ∀ {S Q R} →  ⟦ Q ⟧ skip ⨾ S ⟦ R ⟧ → ⟦ Q ⟧ S ⟦ R ⟧
tada {S} {Q} {R} (skip-rule Q⇒P₁ ⨾⟨ P₁⇒P₂ ⟩ ⟦P₂⟧S⟦R⟧) =
  ⟦ Q ⟧⇒⟨ (λ σ z₁ → P₁⇒P₂ σ (Q⇒P₁ σ z₁))⟩ ⟦P₂⟧S⟦R⟧


tada˘ : ∀ {S} {Q R}  → ⟦ Q ⟧ S ⟦ R ⟧ →  ⟦ Q ⟧ skip ⨾ S ⟦ R ⟧
tada˘ {S} {Q} {R} ⟦Q⟧S⟦R⟧ =
  ⟦ Q ⟧⟨ ⇒-refl {Q}
  ⟩⟦ Q ⟧
  ⨾⟨ ⇒-refl {Q} ⟩
  ⟦Q⟧S⟦R⟧

skip-left-unit : ∀ {S} → skip ⨾ S ≈ₚ S
skip-left-unit = tada , tada˘
\end{code}

These abstract-proofs are not as clear as the examples, but that's to be expected since
the syntax was made with concrete code annotation in mind.

%}}}

\section{Closing}

It seems that we've managed to get a lot done --up to chapter 9 of SoP excluding arrays,
quantification, and a few other function and relation symbols. We intend to remedy the situation
in future posts! Hope you've enjoyed the article!

% Local Variables:
% folded-file: t
% eval: (fold-set-marks "%{{{ " "%}}}")
% eval: (fold-whole-buffer)
% fold-internal-margins: 0
% end:
