---
title: SoP2: Interpreted Function Symbols
date: April 2, 2016
tags: SoP
---

% cd literate ; fromliterate SoP2 ; cd .. ; ./site rebuild ; ./site watch

This post discusses how to add arbitrary function symbols to the grammar introduced in the
\href{http://alhassy.bitbucket.org/posts/SoP1.html#examples}{previous SoP article}; thereby making the presentation of SoP1 more uniform!
%
The code for this article is available
\href{https://bitbucket.org/alhassy/blog/src/a0c404801efe189d9e06066985d3e7d9e6131b34/literate/SoP2.lagda?at=master&fileviewer=file-view-default}{here.}

\begin{code}
{-# OPTIONS --no-positivity-check #-}
-- used for new type Type

module SoP4 where

open import Data.Vec renaming (map to vmap)
open import Data.Bool hiding (_∧_ ; _∨_ ; _≟_) renaming (Bool to 𝔹)
open import MyPrelude hiding  (_⨾_ ; swap ; [_]) renaming (¬_ to NOT ; _>_ to _ℕ>_) 
\end{code}
We build on the types and variables of the previous session, but alter the expressions.
(The program language grammar is also unchanged, but refers to the new expressions instead!)

\begin{code}
open import Data.String -- my very first time using Data.String; `primTrustMe' :] ...!

-- Func a s t ≡ s → s → ⋯ → s → t , with a-many s arguments
Func : ∀ {ℓ} (a : ℕ) (s t : Set ℓ) → Set ℓ
Func zero s t = t
Func (suc a) s t = s → Func a s t

-- here we need no positivity checking..
mutual
  -- a bit analgous to Agda's 'Set ℓ'
  data Type {ℓ : Level} : Set (ℓsuc ℓ) where
    MkType : {arity : ℕ} (tf : TypeFormer {ℓ} arity) → Vec Type arity → Type
    
  record TypeFormer {ℓ : Level} (arity : ℕ) : Set (ℓsuc ℓ) where
       field
         name   : String -- used for equality purposes
         reify  : Func arity Type (Set ℓ)

open TypeFormer {{...}}

⟦_⟧ₜ : ∀ {ℓ} → Type {ℓ} → Set ℓ
⟦ MkType {zero} tf [] ⟧ₜ = reify 
⟦ MkType {suc arity} tf (p ∷ ps) ⟧ₜ =
  let
    tf-tail : TypeFormer arity
    tf-tail = record { name = name ; reify = reify p }
  in ⟦ MkType tf-tail ps ⟧ₜ
--
-- note the similarity with ⟦_⟧ₑ on function symbol application
--
-- perhaps if we blur the distinction between value and type, as in dependent type theory,
-- we can simplify affairs

-- for example,

𝒩 : Type
𝒩 = MkType Nat []
  where
  Nat : TypeFormer 0
  Nat = record { name = "𝒩" ; reify = ℕ }

𝒫 : Type
𝒫 = MkType Pro []
  where
  Pro : TypeFormer 0
  Pro = record { name = "𝒫" ; reify = Set }

List : ∀ {ℓ} → TypeFormer {ℓ} 1
List = record { name = "List" ; reify = λ x → D.List ⟦ x ⟧ₜ }
  where open import Data.List as D

_≟ₜ_ : ∀ {ℓ} → Decidable {A = Type {ℓ}} _≡_
MkType tf ps ≟ₜ MkType tf' ps' with TypeFormer.name tf ≟ TypeFormer.name tf'
MkType tf ps ≟ₜ MkType tf' ps' | yes p = {!!}
MkType tf ps ≟ₜ MkType tf' ps' | no ¬p = {!!}
\end{code}
𝒩 ≟ₜ 𝒩 = yes ≡-refl
𝒩 ≟ₜ 𝒫 = no (λ ())
𝒫 ≟ₜ 𝒩 = no (λ ())
𝒫 ≟ₜ 𝒫 = yes ≡-refl

data Variable : Type → Set where
  x y z : ∀ {t} → Variable t

_≟ᵥ_ : ∀ {t s} → Variable t → Variable s → 𝔹
x ≟ᵥ x = true
x ≟ᵥ _ = false
y ≟ᵥ y = true
y ≟ᵥ _ = false
z ≟ᵥ z = true
z ≟ᵥ _ = false

State : Set₁
State = ∀ {t : Type} → Variable t → ⟦ t ⟧ₜ

_[_↦_] : State → ∀ {t} → Variable t → ⟦ t ⟧ₜ → State
_[_↦_] σ {t} v e {s} v∶s with t ≟ₜ s
_[_↦_] σ v e v∶s | yes ≡-refl = if v ≟ᵥ v∶s then e else σ v∶s
_[_↦_] σ v e v∶s | no ¬p = σ v∶s


%{{{ Function Symbol Datatype
\section{Function Symbol Datatype}

We'd like to add arbitrary function symbols to our grammar.
A first attempt would be,
\begin{spec}
record FunctionSymbol : Set where
  field
    arity : ℕ
    src tgt : Type
\end{spec}
To capture the number of arguments, their source type, and the resulting target type.

To define the semantics brackets on expressions is now made unclear since we do not know
how to interpret an arbitrary function symbol. The easiest thing to do is to place the interpretation
alongside the other data.
\begin{spec}
record InterpretedFunctionSymbol : Set₁ where
  field
    arity : ℕ
    src tgt : Type
    reify   : Vec ⟦ src ⟧ₜ arity → ⟦ tgt ⟧ₜ

open InterpretedFunctionSymbol {{...}}
\end{spec}

Now we extend the expressions datatype by a new function symbol application constructor:
\begin{spec}
data Expr : Type → Set₁ where
   ⋮
  app : (fsym : InterpretedFunctionSymbol) → Vec (Expr src) arity → Expr tgt
\end{spec}

Then the semantics definition would be extended by the clause,
\begin{spec}
⟦ app fsym args ⟧ₑ σ  = reify (map (λ e → ⟦ e ⟧ₑ σ) args)
\end{spec}
However this is a no-go since the semantics brackets are being recursively called on a
not-so-trivially structurally smaller item and so not immediately clear that result will terminate.
Hence Agda refuses the definition. One extreme, and unsafe, approach is just to
\href{http://wiki.portal.chalmers.se/agda/pmwiki.php?n=ReferenceManual.Pragmas}{disable termination checking}.
A more common approach is to case over the expressions/lists involved and do the |map|
explicitly. A middle path would be to
\href{http://blog.ezyang.com/2010/06/well-founded-recursion-in-agda/}{use well-founded recursion for writing non-structurally recursive programs in Agda.} The first option is too scary and the last
a bit technical for what we want, so we'll take the simplest, thought not general, route.

So if we now try to case over |args|, we get a unification error.
The system is not sure what to do. An immediate solution is to bring |arity|, the length of |args|,
to the top as a parameter and then case over that. So we redefine,
\begin{spec}
record InterpretedFunctionSymbol (arity : ℕ) : Set₁ where
  field
    src tgt : Type
    reify   : Vec ⟦ src ⟧ₜ arity → ⟦ tgt ⟧ₜ
\end{spec}

Okay great, we'll case over |args|.
Now how do we make a recursive call?!
Our |reify| is not defined inductively and so we cannot break it down into pieces as we would for
vectors. Let's redefine yet again,
\begin{spec}
-- Func a s t ≡ s → s → ⋯ → s → t , with a-many s arguments
Func : ∀ {ℓ} (a : ℕ) (s t : Set ℓ) → Set ℓ
Func zero s t = t
Func (suc a) s t = s → Func a s t

record InterpretedFunctionSymbol (arity : ℕ) : Set₁ where
  field
    src tgt : Type
    reify   : Func arity ⟦ src ⟧ₜ ⟦ tgt ⟧ₜ

open InterpretedFunctionSymbol {{...}}
\end{spec}
Now: an expression is a variable, a constant symbol, or a function symbol applied to other terms.
\begin{spec}
data Expr : Type → Set₁ where
  Var : ∀ {t} (v : Variable t) → Expr t
  𝒩  : ℕ → Expr 𝒩  
  𝒫  : Set → Expr 𝒫
  app : {arity : ℕ} (fsym : InterpretedFunctionSymbol arity) → Vec (Expr src) arity → Expr tgt
\end{spec}
Notice that the formal code is quiet faithful to the informal definition preceding it!

Observe that we could not have written
\begin{spec}
  app : (arity : ℕ) (fsym : InterpretedFunctionSymbol arity) → Func arity (Expr src) (Expr tgt)
\end{spec}
since datatype constructors must return the datatype being constructed and it's not immediately
clear that this is the case with |Func|, so the system rejects it.

Now: the interpretation of a variable is to look it up in the state, that of constants is to leave
them alone, and that of a function application is to reify the function and apply it to the
interpretation of its arguments.
\begin{spec}
infix 5 ⟦_⟧ₑ
⟦_⟧ₑ : ∀ {t} → Expr t → State → ⟦ t ⟧ₜ
⟦_⟧ₑ (Var v) σ = σ v
⟦_⟧ₑ (𝒩 q) σ = lift q
⟦_⟧ₑ (𝒫 p) σ = p
-- loosely: ⟦ f t₁ ⋯ tₙ ⟧ σ = (reify ⟦ t₁ ⟧ σ) ⟦ ftail t₂ ⋯ tₙ ⟧ σ
⟦_⟧ₑ (app {zero} fsym []) σ = reify
⟦_⟧ₑ (app {suc n} fsym (e ∷ args)) σ =
  let
    fsym-tail = record { src = src ; tgt = tgt ; reify = reify (⟦ e ⟧ₑ σ) }
  in
    ⟦ app {n} fsym-tail args ⟧ₑ σ
\end{spec}
Notice that the formal code is quiet faithful to the informal definition preceding it!

One more time! The substitution over a variable occurs if the variables match, substitution over
constants does nothing, and substitution over function symbol application is to just substitute
over the argument terms.
\begin{comment}
\begin{spec}
_[_/_] : ∀ {s t} (E : Expr t) (e : Expr s) (v : Variable s) → Expr t
_[_/_] {s} {t} (Var v) v₁ e' with s ≟ₜ t
Var v [ e / v₁ ] | yes ≡-refl = if v ≟ᵥ v₁ then e else Var v
Var v [ e / v₁ ] | no ¬p = Var v
𝒩 c  [ e / v  ] = 𝒩 c
𝒫 c  [ e / v  ] = 𝒫 c
app fsym args [ e / v ] = app fsym (helper args)
  where helper : ∀ {n} → Vec (Expr src) n → Vec (Expr src) n
        helper {zero } [] = []
        helper {suc n} (E ∷ ls) = E [ e / v ] ∷ helper ls
\end{spec}
\end{comment}
% the above works, the following is for show: above uses | and below uses ∣
\begin{spec}
_[_/_] : ∀ {s t} (E : Expr t) (e : Expr s) (v : Variable s) → Expr t
_[_/_] {s} {t} (Var v) v₁ e' with s ≟ₜ t
Var v [ e / v₁ ] ∣ yes ≡-refl = if v ≟ᵥ v₁ then e' else Var v
Var v [ e / v₁ ] ∣ no ¬p = Var v
𝒩 c  [ e / v  ] = 𝒩 c
𝒫 c  [ e / v  ] = 𝒫 c
app fsym args [ v / e ] = app fsym (helper args)
  where helper : ∀ {n} → Vec (Expr src) n → Vec (Expr src) n
        helper {zero } [] = []
        helper {suc n} (E ∷ ls) = E [ e / v ] ∷ helper ls
\end{spec}
Notice that the formal code is quiet faithful to the informal definition preceding it!

Exercise! Write the definition of
\begin{spec}
_[_,_/₂_,_] : ∀ {s₁ s₂ t} → Expr t → Variable s₁ → Variable s₂ → Expr s₁ → Expr s₂ → Expr t
\end{spec}
\begin{comment}
\begin{spec}
_[_,_/₂_,_] : ∀ {s₁ s₂ t} → Expr t → Expr s₁ → Expr s₂ → Variable s₁ → Variable s₂ → Expr t
_[_,_/₂_,_] {s₁} {s₂} {t} (Var v) e₁ e₂ v₁ v₂ with s₂ ≟ₜ t
Var v [ e₁ , e₂ /₂ v₁ , v₂ ] | yes ≡-refl = if v ≟ᵥ v₂ then e₂ else (Var v) [ e₁ / v₁ ]
_[_,_/₂_,_] {s₁} {s₂} {t} (Var v) e₁ e₂ v₁ v₂ | no ¬p with s₁ ≟ₜ t
Var v [ e₁ , e₂ /₂ v₁ , v₂ ] | no ¬p | yes ≡-refl = if v ≟ᵥ v₁ then e₁ else Var v
Var v [ e₁ , e₂ /₂ v₁ , v₂ ] | no ¬p₁ | no ¬p = Var v
𝒩 n [ e₁ , e₂ /₂ v₁ , v₂ ] = 𝒩 n
𝒫 b [ e₁ , e₂ /₂ v₁ , v₂ ] = 𝒫 b
(app fsym args) [ e₁ , e₂ /₂ v₁ , v₂ ] = app fsym (mapper args)
  where mapper : ∀ {n} → Vec (Expr src) n → Vec (Expr src) n
        mapper {zero} [] = []
        mapper {suc n} (E ∷ ES) = (E [ e₁ , e₂ /₂ v₁ , v₂ ]) ∷ mapper ES
\end{spec}
\end{comment}
%}}}

%{{{ Regaining Our Previous Symbols
\section{Regaining Our Previous Symbols}

The old |Expr| syntax had built-in addition, conjunction, equality, etc.; and we intend to regain
them with the power of interpreted function symbols.

We only show a few and leave the rest as exercises.

\begin{spec}
infix 10 _≈_
_≈_ : (m n : Expr 𝒩) → Expr 𝒫
m ≈ n = app symb (m ∷ n ∷ [])
  where
    sem : ⟦ 𝒩 ⟧ₜ → ⟦ 𝒩 ⟧ₜ → ⟦ 𝒫 ⟧ₜ
    sem (lift m) (lift n) = m ≡ n

    symb : InterpretedFunctionSymbol 2
    symb = record { src = 𝒩 ; tgt = 𝒫 ; reify = sem }

-- a new symbol ;)
infixr 11 _-_
_-_ : (m n : Expr 𝒩) → Expr 𝒩
m - n = app symb (m ∷ n ∷ [])
  where
    sem : ⟦ 𝒩 ⟧ₜ → ⟦ 𝒩 ⟧ₜ → ⟦ 𝒩 ⟧ₜ
    sem (lift m) (lift n) = lift (m ∸ n)

    symb : InterpretedFunctionSymbol 2
    symb = record { src = 𝒩 ; tgt = 𝒩 ; reify = sem }
\end{spec}
\begin{comment}
\begin{spec}
infix 10 _≤_
_≤_ : (m n : Expr 𝒩) → Expr 𝒫
m ≤ n = app leqsym (m ∷ n ∷ [])
  where
    r : Func 2 (Lift ℕ) Set
    r (lift a) (lift b) = a ≤ℕ b
    
    leqsym : InterpretedFunctionSymbol 2
    leqsym = record { tgt = 𝒫 ; reify = r }

infixr 9 _∧_
_∧_ : (p q : Expr 𝒫) → Expr 𝒫
p ∧ q = app symb (p ∷ q ∷ [])
  where
    symb : InterpretedFunctionSymbol 2
    symb = record { src = 𝒫 ; tgt = 𝒫 ; reify = _×_ }

infixr 11 _+_
_+_ : (m n : Expr 𝒩) → Expr 𝒩
m + n = app symb (m ∷ n ∷ [])
  where
    sem : ⟦ 𝒩 ⟧ₜ → ⟦ 𝒩 ⟧ₜ → ⟦ 𝒩 ⟧ₜ
    sem (lift m) (lift n) = lift (m +ℕ n)

    symb : InterpretedFunctionSymbol 2
    symb = record { src = 𝒩 ; tgt = 𝒩 ; reify = sem }
\end{spec}
\end{comment}

Even more so, we can form simple notions of quantification.
For example, simple summation:
\begin{spec}
-- sum over `body' with index `dummy' ranging over all natural numbers that are at most `bound'
infix 10 Σ_≤_•_
Σ_≤_•_ : Variable 𝒩 → ℕ → Expr 𝒩 → Expr 𝒩
Σ_≤_•_ dummy bound body =
         app symb (vmap (λ i → body [ 𝒩 i / dummy ]) (downFrom bound))
  where

    -- downFrom n ≡ [n , n-1, n-2, …, 1, 0]
    downFrom : (n : ℕ) → Vec ℕ (suc n)
    downFrom zero = zero ∷ []
    downFrom (suc n) = suc n ∷ downFrom n

    -- ⟨ i , n ⟩ takes n-many arguments and adds them up then adds i on top of that
    ⟨_,_⟩ : Lift ℕ → (n : ℕ) → Func n ⟦ 𝒩 ⟧ₜ ⟦ 𝒩 ⟧ₜ
    ⟨ i , zero  ⟩   = i
    ⟨ lift i , suc n ⟩ (lift j) = ⟨ lift (i +ℕ j) , n ⟩

    example : ⟨ lift 0 , 4 ⟩ (lift 100) (lift 22) (lift 31) (lift 7) ≡ lift (100 +ℕ 22 +ℕ 31 +ℕ 7)
    example = ≡-refl

    symb : InterpretedFunctionSymbol (suc bound)
    symb = record { src = 𝒩 ; tgt = 𝒩 ; reify = ⟨ lift 0 , suc bound ⟩ }    
\end{spec}
Observe that the syntax `|Σ dummy ≤ bound • body|'
is interpreted as the sum of the |body| with |dummy| ranging over the integers |0..bound|.

Let's try this out! First, let's use the notion of a default or initial state:
\begin{spec}
-- default state: numbers are 0 and booleans are false.
σ₀ : State
σ₀ {𝒩} v = lift 0
σ₀ {𝒫} v = ⊥
\end{spec}

Then, say, the sum of the successor of naturals below 3 is 10:
\begin{spec}
-- ie 10 ≡ 10
sum-test :   ⟦ Σ x ≤ 3 • Var x + 𝒩 1 ⟧ₑ σ₀
           ≡ ⟦ (𝒩 0 + 𝒩 1) + (𝒩 1 + 𝒩 1) + (𝒩 2 + 𝒩 1) + (𝒩 3 + 𝒩 1)  ⟧ₑ σ₀
sum-test = ≡-refl
\end{spec}

Notice that |Σ x ≤ 3 • Var x + 𝒩 2| is really |Σ x ≤ 3 • (Var x + 𝒩 2)| and not
|(Σ x ≤ 3 • Var x) + 𝒩 2|; which is consistent with the convention that quantifiers have scope as
large/rightwards as possible.
We've achieved this by the fixity declarations: summation is 10, which is higher priority than
the 11 of addition.

Let's consider a more complicated example; say a summation that involves variables.
\begin{spec}
σ₁ : State
σ₁ {𝒩} x = lift 1
σ₁ {𝒩} y = lift 2
σ₁ {𝒩} z = lift 3
σ₁ {𝒫} v = ⊥

sum-test2 :  ⟦ Σ x ≤ 3 • (Var x + Var y) - Var z ⟧ₑ σ₁
           ≡ ⟦ (𝒩 2 - 𝒩 3) + (𝒩 3 - 𝒩 3) + (𝒩 4 - 𝒩 3) + (𝒩 5 - 𝒩 3)  ⟧ₑ σ₁
sum-test2 = ≡-refl
\end{spec}
Notice that |x| in the sum is not treated as a global variable and so state |σ₁| is not called-upon for it.

Before we move on, let us remark that placing a bound of the form |x ∶ m … n| is tricky and I've failed to realize it
since the interpretation portion of the symbol means I need a vector of length |suc (n ∸ m)| which
is difficult to write. I've tried mimicking the Haskell definition and anyhow it's not of immediate
import. Still, it was hard and I just decided to use the case |m ≡ 0| only.

This can be remedied by adding quantifiers explicitly to the expression syntax, thereby taking in
a state for the interpretation. In particular,
\begin{spec}
⟦ SUM dummy bound range body ⟧ σ =
  reify (vmap (λ i → if ⟦ range [ dummy / 𝒩 i ] ⟧ σ then body [ dummy / 𝒩 i ] else 𝒩 0) (downFrom bound))
\end{spec}
Just something to think about.
%}}}

%{{{ unreify
\section{Unreify}

While regaining some of our function symbols, one might've noticed a general pattern and it is that
which we abstract here. That is, we will write a function that takes a real item and turns it into
an an interpreted function symbol application.

\begin{spec}
-- two argument case
unreify₂ : ∀ {s t} → (⟦ s ⟧ₜ → ⟦ s ⟧ₜ → ⟦ t ⟧ₜ) → (a b : Expr s) → Expr t
unreify₂ {s} {t} r a b = app symb (a ∷ b ∷ [])
  where
    symb : InterpretedFunctionSymbol 2
    symb = record { src = s ; tgt = t ; reify = r }

-- one argument case
unreify₁ : ∀ {s t} → (⟦ s ⟧ₜ → ⟦ t ⟧ₜ) → (Expr s → Expr t)
unreify₁ {s} {t} r a = app symb (a ∷ [])
  where
    symb : InterpretedFunctionSymbol 1
    symb = record { src = s ; tgt = t ; reify = r }

-- could be less general by making B not depend on A
lift₂ : ∀ {a b ℓ} {A : Set a} {B : A → A → Set b}
      → ((p q : A) → B p q) → ((lp lq : Lift {a} {ℓ} A) → B (lower lp) (lower lq))
lift₂ r (lift p) (lift q) = r p q

lift₁ : ∀ {a b ℓ} {A : Set a} {B : A → Set b}
      → ((p : A) → B p) → ((lp : Lift {a} {ℓ} A) → B (lower lp))
lift₁ r (lift p) = r p

-- sometimes we have an operation of the form ℕ → ℕ → ℕ and we want this to yield a
-- function symbol typed Expr 𝒩 → Expr 𝒩 → Expr 𝒩 and so we introduce yet another combinator
unreify-ℕ₂ : (ℕ → ℕ → ℕ) → Expr 𝒩 → Expr 𝒩 → Expr 𝒩
unreify-ℕ₂ r = unreify₂ (lift₂ (λ p q → lift(r p q)))
\end{spec}
Perhaps the name |unreify| is not the best..something to consider!

For example,
\begin{spec}
infixr 9 ¬_
¬_ : (p : Expr 𝒫) → Expr 𝒫
¬_ = unreify₁ NOT

infixr 8 _∨_
_∨_ : (p q : Expr 𝒫) → Expr 𝒫
_∨_ = unreify₂ _⊎_

infix 10 _>_
_>_ : (m n : Expr 𝒩) → Expr 𝒫
_>_ = unreify₂ (lift₂ (λ m n → n <ℕ m))

infix 10 _<_
_<_ : (m n : Expr 𝒩) → Expr 𝒫
m < n = n > m

infix 10 _≠_
_≠_ : (m n : Expr 𝒩) → Expr 𝒫
_≠_ = unreify₂ (lift₂ (λ a b → a ≡ b → ⊥))
\end{spec}
Excellent! Now the user need only use these combinators and worry not about the underlying
presentation of interpreted function symbols.

Let's wrap-up by looking at an intersting theorem.
\begin{spec}
-- constant expression constructor
𝒞 : ∀ {t} → ⟦ t ⟧ₜ → Expr t
𝒞 {𝒩} e = 𝒩 (lower e)
𝒞 {𝒫} e = 𝒫 e

⟦𝒞⟧ : ∀ {t} {c : ⟦ t ⟧ₜ} {σ : State} → ⟦ 𝒞 c ⟧ₑ σ ≡ c
⟦𝒞⟧ {𝒩} = ≡-refl
⟦𝒞⟧ {𝒫} = ≡-refl

inverses : ∀ {s t} (f : ⟦ s ⟧ₜ → ⟦ t ⟧ₜ) (σ : State)
         →  let reify : ∀ {s' t'} (g : Expr s' → Expr t') → (⟦ s' ⟧ₜ → ⟦ t' ⟧ₜ) 
                reify g = λ a → ⟦ g (𝒞 a) ⟧ₑ σ in
          reify (unreify₁ f) ≗ f   -- pointwise equality
inverses f σ = λ a → ≡-cong f ⟦𝒞⟧
\end{spec}
Notice that this reify differs from |InterpretedFunctionSymbol.reify| !
What of |unreify₁ (reify f) ≗ f| ? exercise.

% notice that reify plays the role of eval if we construe this as an exponential object!

%}}}

\begin{comment}
%{{{ old stuff that refers to the new expression datatype
\begin{spec}
infix 2 _⇒_
_⇒_ : Expr 𝒫 → Expr 𝒫 → Set₁
P ⇒ Q = ∀ (σ : State) → ⟦ P ⟧ₑ σ → ⟦ Q ⟧ₑ σ

True : Expr 𝒫
True = 𝒫 ⊤

everything-implies-truth : ∀ {R} → R ⇒ True
everything-implies-truth = λ _ _ → tt

False : Expr 𝒫
False = 𝒫 ⊥

false-is-strongest : ∀ {Q} → False ⇒ Q
false-is-strongest σ ()

⇒-refl : ∀ {P} → P ⇒ P
⇒-refl σ pf = pf

-- C-c C-a
⇒-trans : ∀ {P Q R} → P ⇒ Q → Q ⇒ R → P ⇒ R
⇒-trans = λ {P} {Q} {R} z₁ z₂ st z₃ → z₂ st (z₁ st z₃)

∧-sym : ∀ {P Q} → P ∧ Q ⇒ Q ∧ P
∧-sym st (p , q) = q , p

infix 7 _≔_ _,_≔₂_,_
infixr 5 _⨾_
data Program : Set₁ where
  skip  : Program
  abort : Program
  _⨾_   : Program → Program → Program
  _≔_      : ∀ {t} → Variable t → Expr t → Program
  _,_≔₂_,_ : ∀ {s t} → Variable t → Variable s → Expr t → Expr s → Program

infixr 5 _⨾⟨_⟩_
data ⟦_⟧_⟦_⟧ : Expr 𝒫 → Program → Expr 𝒫 → Set₁ where
  _⨾⟨_⟩_      : ∀ {Q S₁ P₁ P₂ S₂ R} → ⟦ Q ⟧ S₁ ⟦ P₁ ⟧ → P₁ ⇒ P₂ → ⟦ P₂ ⟧ S₂ ⟦ R ⟧ → ⟦ Q ⟧ S₁ ⨾ S₂ ⟦ R ⟧
  skip-rule  : ∀ {Q R} → Q ⇒ R → ⟦ Q ⟧ skip ⟦ R ⟧
  absurd     : ∀ {Q R} → Q ⇒ False → ⟦ Q ⟧ abort ⟦ R ⟧
  assignment : {t : Type} (v : Variable t) (e : Expr t) (Q R : Expr 𝒫)
             → Q ⇒ R [ e / v ] → ⟦ Q ⟧ v ≔ e ⟦ R ⟧ 
  simuAssign : {t₁ t₂ : Type} (v₁ : Variable t₁) (v₂ : Variable t₂) (e₁ : Expr t₁) (e₂ : Expr t₂)
               (Q R : Expr 𝒫) → Q ⇒ R [ e₁ , e₂ /₂ v₁ , v₂ ] → ⟦ Q ⟧ v₁ , v₂ ≔₂ e₁ , e₂ ⟦ R ⟧               

-- for easy copy/paste: ⟦ ? ⟧ ? ≔ ? since ? ⟦ ? ⟧
syntax assignment v e Q R Q⇒Rₑˣ = ⟦ Q ⟧ v ≔ e since Q⇒Rₑˣ ⟦ R ⟧
syntax simuAssign v₁ v₂ e₁ e₂ Q R Q⇒Rₑˣ = ⟦ Q ⟧ v₁ , v₂ ≔₂ e₁ , e₂ since Q⇒Rₑˣ ⟦ R ⟧
syntax skip-rule {Q} {R} Q⇒R = ⟦ Q ⟧⟨ Q⇒R ⟩⟦ R ⟧ -- ⟦ Q ⟧ skip-since Q⇒R ⟦ R ⟧

-- excercise in C-c C-c and C-c C-a   ^_^
strengthen : ∀ Q {P R S} → Q ⇒ P → ⟦ P ⟧ S ⟦ R ⟧ → ⟦ Q ⟧ S ⟦ R ⟧
strengthen Q {S = skip} Q⇒P (skip-rule P⇒R) = skip-rule (λ st z₁ → P⇒R st (Q⇒P st z₁))
strengthen Q {S = abort} Q⇒P (absurd P⇒False) = absurd (λ st z₁ → P⇒False st (Q⇒P st z₁))
strengthen Q {S = S₁ ⨾ S₂} Q⇒P (⟦P⟧S₁⟦P₁⟧ ⨾⟨ pf ⟩ ⟦P₂⟧S₂⟦R⟧) = strengthen Q Q⇒P ⟦P⟧S₁⟦P₁⟧ ⨾⟨ pf ⟩ ⟦P₂⟧S₂⟦R⟧
strengthen Q {S = .v ≔ .e} Q⇒P (assignment v e P R P⇒Rᵛₑ) = assignment v e Q R (λ st z₁ → P⇒Rᵛₑ st (Q⇒P st z₁))
strengthen Q {S = .v₁ , .v₂ ≔₂ .e₁ , .e₂} Q⇒P (simuAssign v₁ v₂ e₁ e₂ P R P⇒R-sub) = simuAssign v₁ v₂ e₁ e₂ Q R (λ st z₁ → P⇒R-sub st (Q⇒P st z₁))

syntax strengthen Q Q⇒P ⟦P⟧S⟦R⟧ = ⟦ Q ⟧⇒⟨ Q⇒P ⟩ ⟦P⟧S⟦R⟧
\end{spec}
%}}}
\end{comment}

%{{{ Examples
\section{Examples}

Let's produce a big example that uses all program constructs, except abort, and also makes use of the new symbols.
\begin{spec}
-- ∧ monotonicity: a nifty way to introduce implications involving ∧
∧-⇒ : ∀ {P Q P' Q'} → P ⇒ P' → Q ⇒ Q' → P ∧ Q ⇒ P' ∧ Q' 
∧-⇒ p q σ (a , b) = p σ a , q σ b

-- the refl case is so ubiquitous, let's name it.
_⨾⟨⟩_ : ∀ {Q S₁ P S₂ R} → ⟦ Q ⟧ S₁ ⟦ P ⟧ → ⟦ P ⟧ S₂ ⟦ R ⟧ → ⟦ Q ⟧ S₁ ⨾ S₂ ⟦ R ⟧
S ⨾⟨⟩ T = S ⨾⟨ (λ σ z₁ → z₁) ⟩ T -- C-c C-a

test : ⟦ True ⟧
           x ≔ 𝒩 3
         ⨾ skip
         ⨾ y ≔ 𝒩 4
         ⨾ x ≔ (Σ z ≤ 4 • Var z + Var z) -- sum of all even's that're at-most 8 = 0+2+4+6+8 = 20 
         ⨾ x , y ≔₂ Var x + Var y , Var x - Var y
       ⟦ Var x ≈ 𝒩 24 ∧ Var y ≈ 𝒩 16 ⟧
test =
        ⟦ True ⟧
            x ≔ 𝒩 3                                     since (λ σ _ → ≡-refl)
        ⟦ Var x ≈ 𝒩 3                                 ⟧
          ⨾⟨ (λ σ z₁ → z₁)  ⟩ -- ⇒-refl ⟩
        ⟦ Var x ≈ 𝒩 3                                 ⟧⟨ (λ σ z₁ → z₁) -- ⇒-refl 
        ⟩⟦ Var x ≈ 𝒩 3                                ⟧
          ⨾⟨ (λ σ _ → ≡-refl , ≡-refl) ⟩
        ⟦ 𝒩 20 + 𝒩 4 ≈ 𝒩 24 ∧ 𝒩 20 - 𝒩 4 ≈ 𝒩 16   ⟧
          y ≔ 𝒩 4                                       since (λ σ _ → ≡-refl , ≡-refl)
        ⟦ 𝒩 20 + Var y ≈ 𝒩 24 ∧ 𝒩 20 - Var y ≈ 𝒩 16 ⟧
          ⨾⟨ (λ σ z₁ → z₁) ⟩ -- ⇒-refl ⟩
        ⟦ (Σ z ≤ 4 • Var z + Var z) + Var y ≈ 𝒩 24 ∧ (Σ z ≤ 4 • Var z + Var z) - Var y ≈ 𝒩 16 ⟧
          x ≔ (Σ z ≤ 4 • Var z + Var z)                  since (λ σ z₁ → z₁) -- ⇒-refl
        ⟦ Var x + Var y ≈ 𝒩 24 ∧ Var x - Var y ≈ 𝒩 16 ⟧
          ⨾⟨⟩
        ⟦ Var x + Var y ≈ 𝒩 24 ∧ Var x - Var y ≈ 𝒩 16 ⟧
          x , y ≔₂ Var x + Var y , Var x - Var y since (λ σ z₁ → z₁) -- ⇒-refl
        ⟦ Var x ≈ 𝒩 24 ∧ Var y ≈ 𝒩 16 ⟧
\end{spec}
Notice that the type is a claim we are making about the correctness of a program
and the body is a proof of that claim.

Of course one should check that our earlier programs still work with the function symbols in-place.
Exercise ;)
\begin{comment}
\begin{spec}
prog₂ : ⟦ True ⟧ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₂ = ⟦ True         ⟧
           x ≔ 𝒩 5      since (λ _ _ → ≡-refl) 
        ⟦ Var x ≈ 𝒩 5 ⟧

swap2 : ∀ {X Y : ℕ} → ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧ x , y ≔₂ Var {𝒩} y , Var {𝒩} x ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
swap2 {X} {Y} = ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧
                  x , y ≔₂ Var y , Var x         since ∧-sym {Var x ≈ 𝒩 X} {Var y ≈ 𝒩 Y}
                ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧

prog₃ : ⟦ True ⟧ skip ⨾ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₃ =  ⟦ True ⟧⟨    ⇒-refl {True}
         ⟩⟦ True ⟧
        ⨾⟨ (λ st x₁ → x₁) ⟩
        ⟦ True ⟧
        x ≔ 𝒩 5 since (λ _ _ → ≡-refl)
        ⟦ Var x ≈ 𝒩 5 ⟧

prog₄ : ⟦ True ⟧ x ≔ 𝒩 5 ⨾ x ≔ 𝒩 3 ⟦ Var x ≈ 𝒩 3  ⟧ 
prog₄ = ⟦ True ⟧
          x ≔ 𝒩 5       since (λ _ _ → ≡-refl)
        ⟦ 𝒩 3 ≈ 𝒩 3 ⟧
        ⨾⟨ ⇒-refl {𝒩 3 ≈ 𝒩 3} ⟩
        ⟦ 𝒩 3 ≈ 𝒩 3 ⟧
          x ≔ 𝒩 3       since ⇒-refl {𝒩 3 ≈ 𝒩 3}
        ⟦ Var x ≈ 𝒩 3 ⟧

constantEx : ∀ {e : Expr 𝒩} {c : ℕ} → ⟦ Var y ≈ 𝒩 c ⟧ x ≔ e ⟦ Var y ≈ 𝒩 c ⟧
constantEx {e} {c} = ⟦ Var y ≈ 𝒩 c ⟧
                       x ≔ e       since (λ st pf → pf)
                     ⟦ Var y ≈ 𝒩 c ⟧

leq : ⟦ 𝒩 7 ≤ Var x ⟧ x ≔ Var x + 𝒩 3 ⟦ 𝒩 10 ≤ Var x  ⟧
leq = ⟦ 𝒩 7 ≤ Var x ⟧
         x ≔ Var x + 𝒩 3       since lemma1
      ⟦ 𝒩 10 ≤ Var x ⟧
      where
        lemma1 : 𝒩 7 ≤ Var x ⇒ 𝒩 10 ≤ Var x + 𝒩 3
        lemma1 σ 7≤σx = 7≤σx +-mono ≤ℕ-refl {3}
\end{spec}
\end{comment}
%% Awesome! Looks like a paper-and-pencil annotation (with proofs)!
%}}}

\section{Closing}

It seems that we've managed to get a lot done --up to chapter 9 of SoP excluding arrays--.
Hope you've enjoyed the article!


\begin{comment}
%% perhaps a future article about arrays and domain conditions?

%% add division symbol and take care about domain!

%% add array type former?

 mentioned above, in a future post, I intend to replace the explicit constructors by what interpreted
  function symbols, but it seems that I need them to be augmented with a `domain' operation.
  For example, the symbol |÷| can be interpreted as integral division with domain condition being the
  second argument is non-zero.

(
while we're at it, perhaps implement variables as ints under the hood:

data Variable : Type → Set where
  MkVar : ∀ {t} → ℕ → Variable t

_≟ᵥ_ : ∀ {t s} → Variable t → Variable s → 𝔹
(MkVar m) ≟ᵥ (MkVar n) with m ≟ℕ n
MkVar m ≟ᵥ MkVar .m | yes refl = true
MkVar m ≟ᵥ MkVar n | no ¬p = false
(MkVar m) ≟ᵥ _ = false

)
\end{comment}



% Local Variables:
% folded-file: t
% eval: (fold-set-marks "%{{{ " "%}}}")
% eval: (fold-whole-buffer)
% fold-internal-margins: 0
% end:



