module MyPrelude where

open import Level public renaming (suc to ℓsuc ; zero to ℓzero)

open import Relation.Binary.PropositionalEquality public

open import Data.Sum public

open import Data.Product public using (Σ ; proj₁ ; proj₂ ; _×_ ; _,_)
Σ∶• : ∀ {a b} (A : Set a) (B : A → Set b) → Set _
Σ∶• = Σ
infix -666 Σ∶•
syntax Σ∶• A (λ x → B) = Σ x ∶ A • B

infix 4 _∧_
_∧_ = _×_

infixr 2 _$_
_$_ : ∀ {a b} {A : Set a} {B : (i : A) → Set b} → ((i : A) → B i) → (e : A) → B e
f $ e = f e

-- Natural numbers
-- when needed, rename other items and prefix them similarly
open import Relation.Binary using (module DecTotalOrder)
open import Data.Nat public renaming (_≤_ to _≤ℕ_ ; _<_ to _<ℕ_ ; _+_ to _+ℕ_) hiding (_⊔_)
open import Data.Nat.Properties.Simple public
open import Data.Nat.Properties public
open DecTotalOrder decTotalOrder public using () renaming (refl to ≤ℕ-refl)

infix 2 _⇔_
_⇔_ : ∀ {a b} → Set a  → Set b → Set (b ⊔ a)
A ⇔ B = (A → B) × (B → A)
-- thoughht about using a tagged record, tags "to" and "from",
-- but the record-syntax is too much of an overhead; for now.

-- {-# BUILTIN REWRITE _⇔_ #-}   -- allow rewriting for _≡_ expressions
-- cannot be used as a rewriting rule since its arguments are different types!
-- heterogenous types is super lame. So we use a non-heterogenous version as well.
infix 2 _≅_
_≅_ : ∀ {a} → Set a  → Set a → Set a
A ≅ B = (A → B) × (B → A)
-- {-# BUILTIN REWRITE _≅_ #-}   -- allow rewriting for _≅_ expressions


-- postulate univ : ∀ {a} {P Q : Set a} → (P ⇔ Q) → (P ≡ Q)
-- post on agda forum :: desire to prove ≡ by using ⇔
-- postulate eq : ∀ {P Q : Set} → (P ≡ Q) ≡ {!lift {?} {?} (P ⇔ Q)!}
-- {-# REWRITE eq #-}         -- x+0=x is a permited rewrite rule

-- refl
_∎ : ∀{a} (P : Set a) → P ⇔ P
P ∎ = (λ z → z) , (λ z → z)

-- consider using preorder reasoning module

-- trans
infixr 5 _⇔⟨_⟩_
_⇔⟨_⟩_ : ∀{a b c} (P : Set a) {Q : Set b} {R : Set c}
       → P ⇔ Q → Q ⇔ R → P ⇔ R
P ⇔⟨ P⇒Q , Q⇒P ⟩ (Q⇒R , R⇒Q) = (λ z → Q⇒R (P⇒Q z)) , (λ z → Q⇒P (R⇒Q z))

⇔-sym :  ∀{a b} {P : Set a} {Q : Set b}
       → (P ⇔ Q) → (Q ⇔ P)
⇔-sym (P⇒Q , Q⇒P) = Q⇒P , P⇒Q

-- example equational proof: another form of trans
_⟨⇔⇔˘⟩_ : ∀{a b c} {A : Set a} {B : Set b} {C : Set c}
                → A ⇔ B → C ⇔ B → A ⇔ C
_⟨⇔⇔˘⟩_ {A = A} {B} {C} A⇔B C⇔B =
    A
  ⇔⟨ A⇔B ⟩
    B
  -- ⇔⟨ ⇔-sym C⇔B ⟩
  -- equivalently, we may do a subproof
  ⇔⟨    B
       ⇔⟨ ⇔-sym C⇔B ⟩
         C
       ∎
    ⟩
    C
  ∎
