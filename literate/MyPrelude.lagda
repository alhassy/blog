---
title: MyPrelude : some common exports for proving
date: March 28, 2016
tags: agda
---

This post discuss a file I intend to use for my daily proving needs in the Agda system.

In particular, this file exports the usual notions of propositional equality,
sums, products, function operations,
and a small theory of natural number arithmetic.

\begin{code}
module MyPrelude where
\end{code}

There is a prelude for *programming* needs by \href{https://github.com/UlfNorell/agda-prelude}{Ulf Norell}, but this file of mine is particularly for my own *proving* needs.

Most likely I will add on more exports as the need arises.

\section{Basics: levels, functions, sums, products, and existentials]

\begin{code}
open import Level public renaming (suc to ℓsuc ; zero to ℓzero)
open import Function public

-- forwards composition
_⨾_ : ∀ {a b c}
        {A : Set a} {B : A → Set b} {C : {x : A} → B x → Set c} →
        (g : (x : A) → B x) → (∀ {x} (y : B x) → C y) → 
        ((x : A) → C (g x))
g ⨾ f = f ∘ g

open import Data.Sum     public hiding (map)
open import Data.Product public hiding (map)

-- Z-notation
Σ∶• : ∀ {a b} (A : Set a) (B : A → Set b) → Set _
Σ∶• = Σ
infix -666 Σ∶•
syntax Σ∶• A (λ x → B) = Σ x ∶ A • B
\end{code}

\section{Bot, Top, and Decidability}
\begin{code}
open import Data.Empty public        -- ⊥ , ⊥-elim
open import Data.Unit  public using (⊤ ; tt)
open import Relation.Nullary public  -- ¬ , Dec, yes, no

-- if_then_else_ : ∀ {a c} {A : Set a} {C : Set c} → Dec C → A → A → A
-- if yes p then t else f = t
-- if no ¬p then t else f = f
\end{code}

\section{Equality and ordered algebraic structures}
\begin{code}
open import Relation.Binary.PropositionalEquality public
  renaming(sym to ≡-sym ; refl to ≡-refl ; trans to _⟨≡≡⟩_ ; cong to ≡-cong ; cong₂ to ≡-cong₂
   ; subst to ≡-subst ; subst₂ to ≡-subst₂ ; setoid to ≡-setoid)
\end{code}

Notice that we renamed transitivity to be an infix combinator.

Let us make equational-style proofs available for any type.
This is similar to what was discussed in a
\href{http://alhassy.bitbucket.org/posts/blogliterately.html#monoids}{previous post}
but much better.
\begin{code}  
module _ {i} {S : Set i} where
    open import Relation.Binary.EqReasoning (≡-setoid S) public

-- synonym for readability
definition : ∀{a} {A : Set a} {x : A} → x ≡ x
definition = ≡-refl
\end{code}

\begin{code}
open import Relation.Binary hiding (_⇒_)         public
\end{code}

The |Relation.Binary| module contains algebras for setoids, posets, and decidable variants thereof.
Moreover, the following BNF rule names the other algebras, and a teency-bit more,
\begin{spec}
["Is"] [ "Pre" ∣ [ "Dec" ] [ "Strict" ] ["Partial" ∣ "Total"] ] "Order"
\end{spec}
Moreover,
the module (in the core portion) also exports the notion of a binary relation and properties
associated with them; e.g., |Decidability|

\section{Natural numbers}
\begin{code}
-- when needed, rename other items and prefix them similarly
open import Relation.Binary using (module DecTotalOrder)
open import Data.Nat public renaming (_≤_ to _≤ℕ_ ; _<_ to _<ℕ_ ; _+_ to _+ℕ_ ; _≟_ to _≟ℕ_) hiding (_⊔_)
open import Data.Nat.Properties.Simple public
open import Data.Nat.Properties        public
open DecTotalOrder decTotalOrder       public using ()
  renaming (refl to ≤ℕ-refl ; _≤?_ to _≤ℕ?_ ; total to ℕ-total)

open import Data.Bool using (Bool ; true ; false)
⌊_⌋ : ∀ {a} {A : Set a} → Dec A → Bool
⌊ yes p ⌋ = true
⌊ no ¬p ⌋ = false
\end{code}

I have not included booleans, lists, nor vectors as they are more of a programming than a proving
item and so will import them as needed.

\begin{comment}
everything below is questionable.
\begin{code}

infix 2 _⇔_
_⇔_ : ∀ {a b} → Set a  → Set b → Set (b ⊔ a)
A ⇔ B = (A → B) × (B → A)
-- thoughht about using a tagged record, tags "to" and "from",
-- but the record-syntax is too much of an overhead; for now.

-- {-# BUILTIN REWRITE _⇔_ #-}   -- allow rewriting for _≡_ expressions
-- cannot be used as a rewriting rule since its arguments are different types!
-- heterogenous types is super lame. So we use a non-heterogenous version as well.
infix 2 _≅_
_≅_ : ∀ {a} → Set a  → Set a → Set a
A ≅ B = (A → B) × (B → A)
-- {-# BUILTIN REWRITE _≅_ #-}   -- allow rewriting for _≅_ expressions

-- postulate univ : ∀ {a} {P Q : Set a} → (P ⇔ Q) → (P ≡ Q)
-- post on agda forum :: desire to prove ≡ by using ⇔
-- postulate eq : ∀ {P Q : Set} → (P ≡ Q) ≡ {!lift {?} {?} (P ⇔ Q)!}
-- {-# REWRITE eq #-}         -- x+0=x is a permited rewrite rule

-- refl
_⇔∎ : ∀{a} (P : Set a) → P ⇔ P
P ⇔∎ = (λ z → z) , (λ z → z)

-- consider using preorder reasoning module

-- trans
infixr 5 _⇔⟨_⟩_
_⇔⟨_⟩_ : ∀{a b c} (P : Set a) {Q : Set b} {R : Set c}
       → P ⇔ Q → Q ⇔ R → P ⇔ R
P ⇔⟨ P⇒Q , Q⇒P ⟩ (Q⇒R , R⇒Q) = (λ z → Q⇒R (P⇒Q z)) , (λ z → Q⇒P (R⇒Q z))

⇔-sym :  ∀{a b} {P : Set a} {Q : Set b}
       → (P ⇔ Q) → (Q ⇔ P)
⇔-sym (P⇒Q , Q⇒P) = Q⇒P , P⇒Q

-- example equational proof: another form of trans
_⟨⇔⇔˘⟩_ : ∀{a b c} {A : Set a} {B : Set b} {C : Set c}
                → A ⇔ B → C ⇔ B → A ⇔ C
_⟨⇔⇔˘⟩_ {A = A} {B} {C} A⇔B C⇔B =
    A
  ⇔⟨ A⇔B ⟩
    B
  -- ⇔⟨ ⇔-sym C⇔B ⟩
  -- equivalently, we may do a subproof
  ⇔⟨    B
       ⇔⟨ ⇔-sym C⇔B ⟩
         C
       ⇔∎
    ⟩
    C
  ⇔∎

\end{code}
