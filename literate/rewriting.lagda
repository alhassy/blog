---
title: Rewriting in Agda
date: January 15, 2016
tags: rewriting
---

(http://www.cse.chalmers.se/~nad/listings/lib/Relation.Binary.EqReasoning.html#1)

I would like to phrase the problems posed in the program within the setting of the categorical language and then prove and implement results in the dependently typed proof assistant and programming language that is Agda.

My reasons are as follows. Categorical language can often simplify matters as it lets us ``take a step back'' and see a bigger picture which then yields a more extrinsic view of the objects of study. This lets us then see analogies with other fields and possible applications of them for our needs.
The proof-assistant Agda is so that none of our many proofs ---and I know there will be many--- will contain errors that are hard to catch, especially when we do not have someone whose only job is to check our proofs are valid. Moreover, using Agda as a programming language is like using the benefits of Haskell but with greater assurances of the correctness of our programs ---or at least they implement the specifications-as-types.


To read
* lambda-calculus and term rewriting
http://www.cs.swan.ac.uk/~csetzer/lectures/intertheo/04/intertheofinalforprinting2.pdf

* from simple to dependent types
http://www.cs.swan.ac.uk/~csetzer/lectures/intertheo/07/intertheodraft1.pdf

* Interactive Theorem Proving for Agda Users
http://www.cs.swan.ac.uk/~csetzer/lectures/intertheo/07/interactiveTheoremProvingForAgdaUsers.html

* Type families and Unification (it's a subsection)
http://oxij.org/note/BrutalDepTypes/

* 7.23. Rewrite rules
https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/rewrite-rules.html

* https://code.google.com/archive/p/agda/issues/292

* https://github.com/effectfully/random-stuff/blob/master/Fin-injective.agda

* http://agda.chalmers.narkive.com/LcWpyGbW/agda-with-the-excluded-middle-is-inconsistent

* Heterogeneous pattern matching incompatible with univalence [Re: [Agda] Agda master is anti-classical, injective type constructors are back]
https://lists.chalmers.se/pipermail/agda/2015/007402.html

* http://agda.readthedocs.org/en/latest/language/with-abstraction.html

* https://www.google.ca/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=agda+rewriting&start=20

((
Markdown to create pages and table of contents?
http://stackoverflow.com/questions/11948245/markdown-to-create-pages-and-table-of-contents

Adding Table Of Contents
http://pxqr.info/posts/2014-06-10-adding-post-navigation.html

))


"MathOverflow is for mathematicians to ask each other questions about their research. See Math.StackExchange to ask general questions in mathematics."

"This question does not appear to be about research level mathematics within the scope defined in the help center."




"
As types are judgments, and values are theorems, data constructors for a type correspond to inference rules for the corresponding proposition.

As we’re embedding these logics inside Agda, Agda’s types correspond to their judgements. Remember that if we can construct a value of a certain type, we have simultaneously constructed a proof that the theorem encoded by that type holds. As types are judgements in our embedding, and values are their proofs, data constructors for a type correspond to inference rules in the system. 
"


\begin{code}
module rewriting where
\end{code}

from Agda Digest, Vol 125, Issue 27
::
AARON STUMP
::
Starting in Agda version 2.4.2.5, the abstraction performed by "with"
has (as the CHANGELOG tells us) become more aggressive.  I am sure you
have good reasons for this change, but are you aware that you have made
it impossible to rewrite with the same assumption twice?  Consider this
self-contained example:

module test where

data _≡_ {ℓ} {A : Set ℓ} (x : A) : A → Set ℓ where
   refl : x ≡ x

{-# BUILTIN EQUALITY _≡_ #-}
{-# BUILTIN REFL refl #-}

lemma : ∀ {A : Set}(f : A → A)(a : A) → f a ≡ a → f (f a) ≡ a
lemma f a p rewrite p = p

With previous versions of Agda, this would be fine.  But starting with
2.4.2.5, this does not type check.  If we put a hole on the right-hand
side of the sole equation for lemma, we will see this goal:

Goal: f a ≡ a
————————————————————————————————————————————————————————————
p  : a ≡ a

The "rewrite p" directive has actually been applied to rewrite the type
of p itself, rendering it trivial and hence useless. 

ANDREAS ABEL
::
It seems to be easiest to change the rewrite tactic
such that

   lemma f a p rewrite p = p

behaves like

   data Protect {a} {A : Set a} : A → Set where
     protect : ∀ x → Protect x

   lemma : ∀ {A : Set} (f : A → A) (a : A) (p : f a ≡ a) → f (f a) ≡ a
   lemma f a p with protect p | f a | p
   ... | _ | _ | refl = p

which works around the more aggressive with with the just invented
Protect Pattern (TM) (C) Andreas Abel, Gothenburg University, 2016.



http://stackoverflow.com/questions/12498428/types-containing-with-rewrite-clauses-in-agda-or-how-to-use-rewrite-instead-of

To read ::
http://adam.gundry.co.uk/pub/pattern-unify/pattern-unification-2012-07-10.pdf
http://www.cse.chalmers.se/~ulfn/papers/thesis.pdf
http://stackoverflow.com/a/34849238/3550444

notice that if you use the rewrite keyword on an equation with many implicit arguments,
you may(do??) need to make them explicit.

\begin{code}
data ℕ : Set where
  z : ℕ
  s : ℕ → ℕ

infix 10 _+_
_+_ : ℕ → ℕ → ℕ
z + n = n
(s m) + n = s (m + n)

infix 5 _≡_
data _≡_ {i} {A : Set i} : (x y : A) → Set i where
  ≡-refl : ∀{x} → x ≡ x

0+x=x : ∀ {x} → z + x ≡ x
0+x=x = ≡-refl -- C-c C-a

postulate x+0=x : ∀ {x} → x + z ≡ x

claim : ∀{x} → (z + x) + z ≡ x
claim = x+0=x -- C-c C-a doesn't work
\end{code}
becuase of the definition of +, it agda rewrites the goal
(using the first clause of + defn) to obtain: ∀ {x} → x + z ≡ x,
which is then proven by |x+0=x|.

This rewriting is done automatically.

\begin{code}
postulate Vec : ℕ → Set
\end{code}
\begin{spec}
notAskable = ∀ {x : ℕ} {v : Vec (z + x)} {w : Vec (x + z)} → v ≡ w
\end{spec}
This won't even type check since |_≡_| is defined on one type and
|Vec (z+x)| is rewritten, by agda using the first clause of _+_, to
|Vec x| whereas the type of |w| is syntactically different as |Vec (x + z)|,
hence the expression |v ≡ w| is not even well-typed!

We get around this by introducing Leibniz rule,
\begin{code}
≡-subst : ∀{A : Set} (P : A → Set) {x y : A} → x ≡ y → P x → P y
≡-subst P ≡-refl p = p -- the only possible proof of |x ≡ y| is |≡-refl {x}| and so |x| and |y| are identical, hence |P y| is just |P x| and we know |p : P x| so it suffices as a solution.
\end{code}
Now, we can write
\begin{code}
askableButInelegant = ∀ {x : ℕ} {v : Vec (z + x)} {w : Vec (x + z)} → v ≡ ≡-subst Vec x+0=x w
\end{code}

We can also explicitly inform agda to rewrite the other unitlaw:
\begin{code}
{-# BUILTIN REWRITE _≡_ #-}   -- allow rewriting for _≡_ expressions
{-# REWRITE x+0=x #-}         -- x+0=x is a permited rewrite rule

-- agda rewrites this using the given rewrite rule and first clause of _+_ to
-- ∀{x} → x ≡ x, which is proven by ≡-refl
claim' : ∀{x} → (z + x) + z ≡ x 
claim' = ≡-refl -- C-c c-a does it for us, awesome!

-- cf identical to notAskable but typechecks like askableButInelegant
askable = ∀ {x : ℕ} {v : Vec (z + x)} {w : Vec (x + z)} → v ≡ w

-- note that we can have multiple rewrite rules, but only one rewrite relation
postulate bad : ∀{n} → n + n ≡ n
{-# REWRITE bad #-}

-- cannot use bad implicitly, had to use it explicitly
cannotusebad : s z + s z ≡ s z -- i.e., 2 ≡ 1
cannotusebad = bad {s z}
-- not implictialey provable with bad since definition of _+_ has higher precedence over
-- rewrite rule |bad| and so is applied first, afterwhich bad is no longer applicable.

implicitly-using-bad : ∀{n} → n + (n + n) ≡ n -- _+_ defn cannot be applied, neither clauses applicable, so other rewrite rules are checked and |bad| matches the given pattern and so applied
implicitly-using-bad = ≡-refl -- auto, C-c C-a, proves it for me :-)

-- moreover note that we first argument for the rewrite relation, in a rewrite rule,
-- must be a constructor or a defined symbol. In particular, we could not use
-- |bad' : ∀{n} → n ≡ n + n| as a rewrite rule --and rightfully so, such a left-to-right
-- rewrite never terminates!
\end{code}

We cannot have rewrite-rules that have the same pattern in the first-argument of the rewrite
relations, as in
\begin{spec}
postulate bad2 : ∀{m} → m + m ≡ s z
{-# REWRITE bad2 #-}
\end{spec}
since the pattern of first-argument of the rewrite relation would already have been given
a reduction and the system would get stuck on which reduction to use when encountered; hence
the need for uniquness of patterns in the first-argument of the rewrite relation.

However, the rewrite pragma cannot always be used, namely in situations where an equailty is an
input parameter:
\begin{code}
-- not asakable since n != m
-- again-not-askable = ∀{m n} → m ≡ n → (e : Vec m) (e' : Vec n) → e ≡ e'

-- must use subst
askable-with-subst = ∀{m n} → (eq : m ≡ n) (e : Vec m) (e' : Vec n) → ≡-subst Vec eq e ≡ e'
\end{code}

In this case, we may move to heterogenous equality:
\begin{code}
open import Relation.Binary.HeterogeneousEquality -- replace this with ≅ defn

askable-wiith-≅ = ∀{m n} → (eq : m ≡ n) (e : Vec m) (e' : Vec n) → e ≅ e'
\end{code}
Notice that we are using heterogeneous equality, which can be though of usual propositional
equality but the types are allowed to vary. We use it since the types of |e| and |e'| do not
match ---the alternative would be to use |subst| to coerce the types. Let's move on.

Since heterogeneous equality is essentially propositional equality with a bit less fuss on types,
we will use it instead. (I wonder, when is it a bad idea to use heterogeneous equality?)



For more on rewriting, see
http://hackage.haskell.org/package/Agda-2.4.2/docs/src/Agda-TypeChecking-Rewriting.html

Achieve this in emacs by |M-x customize-group RET agda2 RET| then select |program args| and
insert |--rewriting|, then |state|/save; finally kill-and-restart-agda or |C-c C-x C-r|

Another usage for the rewriting pragma is for when one desires to to make non-computing
relations appear computable, as in the [univalence of Homotopy Type Theory](http://homotopytypetheory.org/). For example,
\begin{spec}
-- level polymorphic version of propostional equality
data _≡_ {i} {A : Set i} : (x y : A) → Set i where ≡-refl : ∀{x} → x ≡ x

postulate _≃_ : ∀{i} → Set i → Set i → Set₁  -- isomorphism type former
{-# BUILTIN REWRITE _≃_ #-}   -- allow rewriting for _≃_ expressions

postulate univ : ∀{A B : Set} → (A ≃ B) ≃ (A ≡ B)
{-# REWRITE univ #-}         -- univ is a permited rewrite rule

example : ∀{A : Set} → A ≃ A
example = ≡-refl -- `refine`, C-c C-r, found the solution!
\end{spec}
Note that we can only use the |BUILTIN REWRITE| pragma once per script!


- [An example of using rewrite *pragma* to solve a currying problem](http://gallais.github.io/blog/canonical-structures-REWRITE.html)
- [Equational Reasoning vs rewrite *keyword* in Agda](http://serendependy.blogspot.ca/2014/02/equational-reasoning-vs-rewrite-syntax.html)

%% neat :: A Monad is really a Category k with an identification between k a b and a -> k () b.
%% http://web.jaguarpaw.co.uk/~tom/blog/posts/2012-09-02-what-is-a-monad-really.html


%% let record patterns where Agda 2.3.2, Nov 2012


%%% meta: things for blog setup below

% Setting up a Math/Programming Blog with Hakyll; also background colour for code blocks
% http://jdreaver.com/posts/2014-06-22-math-programming-blog-hakyll.html

% turn a literate Haskell programme (with Markdown formatted literate comments) into HTML and PDFs using Pandoc.
% https://passingcuriosity.com/2008/literate-haskell-with-markdown-syntax-hightlighting/

% http://javran.github.io/posts/2014-03-01-add-tags-to-your-hakyll-blog.html
% http://flygdynamikern.blogspot.ca/2009/03/blogging-with-pandoc-literate-haskell.html

% doing IO in agda :: http://dafoster.net/articles/2014/02/17/agda-second-impressions/

% markdown sytax et al, read this!
% http://daringfireball.net/projects/markdown/syntax


# test -1
# test 0
words here
\section{test1}
words here
\section{test2}

Agda standard library:
<http://www.cse.chalmers.se/~nad/listings/lib/Everything.html>

The equality proof has nothing to talk about. When you pattern match on a constructor, it can refine other arguments (and their types). In your case, n-n≡0 n should refine n - n, but there's no such expression and Agda gives you this (a bit puzzling) error message.

You can fix that by introducing the expression in a with:

\begin{code}
postulate A : Set
postulate F : A → A
using-with : ∀ a b → F a ≡ F b →  Set
using-with a b eq with F a | eq
using-with a b eq | .(F b) | ≡-refl = {!!}
\end{code}
versus

using-with : ∀ a b → F a ≡ F b →  Set
using-with a b eq with eq
...| h = {!!} -- cannot rewrite since: see paragraph above

this is why we have |rewrite| keyword :-)


Thanks for the full answer. The main points for me are: 1) You can only pattern match on refl if there is an expression that's equality is proven. 2) You loose information when matching, and substitution of ≡ can be used to avoid this.
