

when introduce alternatives, prove hehner's purposely unreadable binary exponentiation program:
section 4.1.2; after rendering it in an imperative style.
page 45 of http://www.cs.toronto.edu/~hehner/aPToP/aPToP.pdf

if
  x = 0 ⟶ x , y ≔ 3 , 1
 ∣ x ≠ 0 ⟶ F ; A ; E
fi 

A: while x ≠ 0
{
  x, y ≔ x-1, 7
  ; A
  ; x, y ≔ 5, 2*y
}

hmmm, maybe I cannot...

-- cf SoP2
infix 10 Σ_≤_∣_•_
Σ_≤_∣_•_ : Variable 𝒩 → ℕ → Expr 𝒫 → Expr 𝒩 → Expr 𝒩
Σ_≤_∣_•_ dummy bound range body =
         app symb (vmap (λ i → if {!⟦ range [ dummy / 𝒩 i ] ⟧ σ!} then body [ dummy / 𝒩 i ] else 𝒩 0) (downFrom bound))
  where




\begin{code}
module SoP where

open import Data.Vec hiding (foldr ; map ; sum)
open import Data.Nat renaming (_<_ to _<ℕ_ ; _≤_ to _≤ℕ_)
open import Data.Bool renaming (Bool to 𝔹 ; _∧_ to _and_ ; _∨_ to _or_)

data Type : Set where
  Nat  : Type
  Bool : Type

-- ask on stackexchange how to define this so that the second case returns: Set₀
⟦_⟧Type : Type → Set
⟦ Nat ⟧Type = ℕ
⟦ Bool ⟧Type = 𝔹

open import MyPrelude renaming (_×_ to _AND_) hiding (_∧_ ; map)
open import Relation.Binary using (Decidable)
open import Relation.Nullary renaming (¬_ to NOT)

_≟ₜ_ : Decidable {A = Type} _≡_
Nat ≟ₜ Nat = yes refl
Nat ≟ₜ Bool = no (λ ())
Bool ≟ₜ Nat = no (λ ())
Bool ≟ₜ Bool = yes refl

data Variable : Type → Set where
  x y z : ∀ {t} → Variable t

-- ask on stack exhcngage if this can be automated like in haskell: extend Eq
-- maybe reflection?
_≟ᵥ_ : ∀ {t s} → Variable t → Variable s → 𝔹
x ≟ᵥ x = true
x ≟ᵥ _ = false
y ≟ᵥ y = true
y ≟ᵥ _ = false
z ≟ᵥ z = true
z ≟ᵥ _ = false


data QOps : Type → Set where
  A E : QOps Bool
  + × : QOps Nat

data Expr : Type → Set₁ where
  Var : ∀ {t} (v : Variable t) → Expr t -- change this to be 𝒱 ? maybe add  _∶_ syntax?  might clash with dummy declaration for qunatifers..
  𝒩 : ℕ → Expr Nat  -- Number and Proposition constructors
  𝒫 : Set → Expr Bool
  _≈_ _≤_ _<_ : (e f : Expr Nat) → Expr Bool
  _∨_ _∧_ : (p q : Expr Bool) → Expr Bool
  ¬_  : (p : Expr Bool) →  Expr Bool
  -- \' , slash-prime!! .. is not allowed and so used … instead
  _′_∶_…_∣_•_ : ∀ {t} (Q : QOps t) (d : Variable t) (m n : Expr Nat) (r : Expr Bool) (b : Expr t) → Expr t
  -- only bounded quantification allowed. Why? so that we can actually realise it! see the semantics below!

-- Q ′ d ∶ m … n ∣ r • b


infix 5 _≈_
infix 3 _∧_

{-  -- need if and only if as well

  
-- 

-- just make a small program and add things as you need them!!

-- term : Expr Bool
-- term = (E ′ x ∶ Var x ≈ Var x ∶ Constant true)

-}

open import Data.List hiding (and)
-- move to MyPrelude
[_…_] : ℕ → ℕ → List ℕ
[ m … n ] = filter {!λ i → m ≤? i!} (downFrom n)



-- good thing we never actually evualte anything and so need not worry too much about state functions.
RealState = (Variable Nat → ℕ) AND (Variable Bool → 𝔹)
State = RealState AND (Variable Bool → Set)

infixr 2 _$_
_$_ : ∀ {a b} {A : Set a} {B : (i : A) → Set b} → ((i : A) → B i) → (e : A) → B e
f $ e = f e


-- function patching
_[_↦_] : ∀ {t : Type} {a} {S : Set a} → (Variable t → S) → Variable t → S → (Variable t → S)
f [ v ↦ e ] = {!!}


-- notation |≔| to be used for program assignnment.
-- when varaibles are involved, we substitute iff the variable types match; otherwise
-- we cannot even define it on the var case.
_[_/_] : ∀ {s t} → Expr t → Variable s → Expr s → Expr t
_[_/_] {s} {t} (Var v) v₁ e' with s ≟ₜ t
Var v [ v₁ / e' ] | yes refl = if v ≟ᵥ v₁ then e' else Var v
-- mathcing to obtain refl forced Agda to internally identify s and t
-- and now Var v : t and  e' : t and so a the above conditional is well-typed!
Var v [ v₁ / e' ] | no ¬p = Var v -- the only thing we can do with differntly typed vars, irrespeective of whether they're identical or not, is to leave them alone. This subtelty leads to a bit of poor items such as: |Var {t} x [ x (of type s) / e ] = Var {t} x|.
𝒩 c [ v / e' ] = 𝒩 c
𝒫 c [ v / e' ] = 𝒫 c
(p ≈ q) [ v / e' ] = (p [ v / e' ]) ≈ (q [ v / e' ])
(p ≤ q) [ v / e' ] = (p [ v / e' ]) ≤ (q [ v / e' ])
(p < q) [ v / e' ] = (p [ v / e' ]) < (q [ v / e' ])
(p ∨ q) [ v / e' ] = (p [ v / e' ]) ∨ (q [ v / e' ])
(p ∧ q) [ v / e' ] = (p [ v / e' ]) ∧ (q [ v / e' ])
(¬ p) [ v / e' ] = ¬ (p [ v / e' ])
(Q ′ d ∶ m … n ∣ r • b)[ v / e' ] = {!!}
{-
(p < q) [ v / e' ] = (p [ v / e' ]) < (q [ v / e' ])
(¬ p) [ v / e' ] = ¬ (p [ v / e' ])
(e ∨ e₁) [ v / e' ] = (e [ v / e' ]) ∨ (e₁ [ v / e' ])
_[_≔_] {Nat} {Nat} (Q ′ x₁ ∶ range ∶ body) v e' = if x₁ ≟ᵥ v
   then (Q ′ x₁ ∶ range ∶ body)
   else (Q ′ x₁ ∶ range  [ v ≔ e' ] ∶ body  [ v ≔ e' ])
_[_≔_] {Nat} {Bool} (Q ′ x₁ ∶ range ∶ body) v e' = (Q ′ x₁ ∶ range ∶ body)
_[_≔_] {Bool} {Nat} (Q ′ x₁ ∶ range ∶ body) v e' = (Q ′ x₁ ∶ range ∶ body)
_[_≔_] {Bool} {Bool} (Q ′ x₁ ∶ range ∶ body) v e' = if x₁ ≟ᵥ v
   then (Q ′ x₁ ∶ range ∶ body)
   else (Q ′ x₁ ∶ range  [ v ≔ e' ] ∶ body  [ v ≔ e' ])
-}

⟦_⟧bool : Expr Bool → RealState → 𝔹
⟦_⟧bool = {!!}

⟦_⟧ℕ : Expr Nat → RealState → ℕ
⟦_⟧ℕ (Var v) (st , st') = st v
⟦_⟧ℕ (𝒩 d) st = d
⟦_⟧ℕ (+ ′ d ∶ m … n ∣ r • b) st =
  sum $ map (λ i → ⟦ b [ d / 𝒩 i ] ⟧ℕ st) -- argh, now need to go and study how to inform agda of this, since not struuctually smaller, even though it is smaller!
  $ filter (λ i → ⟦  r [ d / (𝒩 i) ] ⟧bool st) [ ⟦ m ⟧ℕ st … ⟦ n ⟧ℕ st ]
⟦_⟧ℕ (× ′ d ∶ m … n ∣ r • b) st =
  product {!same as above!}

⟦_⟧𝔹 : Expr Bool → State → Set
⟦_⟧𝔹 (Var v) (σ , ρ) = ρ v
⟦_⟧𝔹 (𝒫 e) (σ , ρ) = e
⟦_⟧𝔹 (e ≈ e₁) (σ , ρ) = ⟦ e ⟧ℕ σ ≡ ⟦ e₁ ⟧ℕ σ
⟦_⟧𝔹 (e ≤ e₁) (σ , ρ) = ⟦ e ⟧ℕ σ ≤ℕ ⟦ e₁ ⟧ℕ σ
⟦_⟧𝔹 (e < e₁) (σ , ρ) = ⟦ e ⟧ℕ σ <ℕ ⟦ e₁ ⟧ℕ σ
⟦ p ∨ q ⟧𝔹 (σ , ρ) = ⟦ p ⟧𝔹 (σ , ρ) ⊎ ⟦ q ⟧𝔹 (σ , ρ)
⟦ p ∧ q ⟧𝔹 (σ , ρ) = ⟦ p ⟧𝔹 (σ , ρ) AND ⟦ q ⟧𝔹 (σ , ρ)
⟦ ¬ p ⟧𝔹 (σ , ρ) = NOT (⟦ p ⟧𝔹 (σ , ρ))
⟦ Q ′ d ∶ m … n ∣ r • b ⟧𝔹 (σ , ρ) = {!!}

{- to be used in-place of the above two once stackexchange shows me how to define ⟦_⟧Type
⟦_⟧ₑ : ∀ {t} → Expr t → (Variable t → ⟦ t ⟧Type) → ⟦ t ⟧Type
⟦_⟧ₑ (Var v) st = st v
⟦_⟧ₑ (Constant c) st = c
⟦_⟧ₑ (e ≈ e') st = {!!} -- (⟦ e ⟧ₑ st) and (⟦ e' ⟧ₑ st) -- maybe use set prod
-- ⟦_⟧ₑ (e ∧ e') st = (⟦ e ⟧ₑ st) and (⟦ e' ⟧ₑ st) -- maybe use set prod
-- ⟦_⟧ₑ (e ∨ e') st = (⟦ e ⟧ₑ st) or (⟦ e' ⟧ₑ st)  -- maybe use set sum
-- ⟦_⟧ₑ (Q ′ v ∶ r ∶ b) st = {!undefined!} -- don't need this, yet.
-}



-- "hide" this and leave it as an excercise to reader.
-- note that we pattern match on s2 first in order to get the convention |(x, x ≔ e,d) = x≔d|
_[_,_/₂_,_] : ∀ {s₁ s₂ t} → Expr t → Variable s₁ → Variable s₂ → Expr s₁ → Expr s₂ → Expr t
_[_,_/₂_,_] {s₁} {s₂} {t} (Var v) u v₁ e₁ e₂ with s₂ ≟ₜ t
Var v [ v₁ , v₂ /₂ e₁ , e₂ ] | yes refl = if v ≟ᵥ v₂ then e₂ else (Var v) [ v₁ / e₁ ] -- very important call at the end!
_[_,_/₂_,_] {s₁} {s₂} {t} (Var v) v₁ v₂ e₁ e₂ | no ¬p with s₁ ≟ₜ t
Var v [ v₁ , v₂ /₂ e₁ , e₂ ] | no ¬p | yes refl = if v ≟ᵥ v₁ then e₁ else Var v -- already tried |₂| and just tried |₁|, no more sunscripts to try.
_[_,_/₂_,_] {s₁} {s₂} {t} (Var v) v₁ v₂ e₁ e₂ | no ¬p₁ | no ¬p = Var v
𝒩 n [ v₁ , v₂ /₂ e₁ , e₂ ] = 𝒩 n
𝒫 b [ v₁ , v₂ /₂ e₁ , e₂ ] = 𝒫 b
(e ≈ f) [ v₁ , v₂ /₂ e₂ , e₃ ] = e [ v₁ , v₂ /₂ e₂ , e₃ ] ≈ f [ v₁ , v₂ /₂ e₂ , e₃ ]
(e ≤ f) [ v₁ , v₂ /₂ e₂ , e₃ ] = e [ v₁ , v₂ /₂ e₂ , e₃ ] ≤ f [ v₁ , v₂ /₂ e₂ , e₃ ]
(e < f) [ v₁ , v₂ /₂ e₂ , e₃ ] = e [ v₁ , v₂ /₂ e₂ , e₃ ] < f [ v₁ , v₂ /₂ e₂ , e₃ ]
(p ∨ q) [ v₁ , v₂ /₂ e₂ , e₃ ] = p [ v₁ , v₂ /₂ e₂ , e₃ ] ∨ q [ v₁ , v₂ /₂ e₂ , e₃ ]
(p ∧ q) [ v₁ , v₂ /₂ e₂ , e₃ ] = p [ v₁ , v₂ /₂ e₂ , e₃ ] ∧ q [ v₁ , v₂ /₂ e₂ , e₃ ]
(¬ p) [ v₁ , v₂ /₂ e₂ , e₃ ] = ¬ (p [ v₁ , v₂ /₂ e₂ , e₃ ])
(Q ′ d ∶ m … n ∣ r • b)[ v₁ , v₂ /₂ e₂ , e₃ ] = {!!}
infix 10 _≔_
infixr 5 _⨾_
data Program : Set₁ where
  skip  : Program
  abort : Program
  _≔_   : ∀ {t} → Variable t → Expr t → Program
  -- when there is ambiguity, my convention is to be use the last instance: |x , x ≔ e , f  EQUALS x ≔ f|, and I've implemented things so it occurs this way. In particular, we'll prove this.
  _,_≔₂_,_ : ∀ {s t} → Variable t → Variable s → Expr t → Expr s → Program
  _⨾_   : Program → Program → Program

-- the subscript 2 in simulatenous substitution is needed, otherwise there'll be parsing conflicts
-- with the notation for elementary substitution.

infix 2 _⇒_
_⇒_ : Expr Bool → Expr Bool → Set₁
P ⇒ Q = ∀ st → ⟦ P ⟧𝔹 st → ⟦ Q ⟧𝔹 st

⇒-refl : ∀ {P} → P ⇒ P
⇒-refl = λ {P} st z₁ → z₁

∧-sym : ∀ {P Q} → P ∧ Q ⇒ Q ∧ P
∧-sym st (p , q) = q , p

open import Data.Unit

True : Expr Bool
True = 𝒫 Unit

everything-implies-truth : ∀ {R} → R ⇒ True
everything-implies-truth = λ x₁ _ → unit

open import Data.Empty

False : Expr Bool
False = 𝒫 ⊥

data ⟦_⟧_⟦_⟧ : Expr Bool → Program → Expr Bool → Set₁ where
  _⨾⟨_⟩_      : ∀ {Q S₁ P₁ P₂ S₂ R} → ⟦ Q ⟧ S₁ ⟦ P₁ ⟧ → P₁ ⇒ P₂ → ⟦ P₂ ⟧ S₂ ⟦ R ⟧ → ⟦ Q ⟧ S₁ ⨾ S₂ ⟦ R ⟧
  skip       : ∀ {Q R} → Q ⇒ R → ⟦ Q ⟧ skip ⟦ R ⟧
  abort      : ∀ {Q R} → Q ⇒ False → ⟦ Q ⟧ abort ⟦ R ⟧
  assignment : {t : Type} (v : Variable t) (e : Expr t) (Q R : Expr Bool)
             → Q ⇒ R [ v  / e ] → ⟦ Q ⟧ v ≔ e ⟦ R ⟧ 
  simuAssign : {t₁ t₂ : Type} (v₁ : Variable t₁) (v₂ : Variable t₂) (e₁ : Expr t₁) (e₂ : Expr t₂)
               (Q R : Expr Bool) → Q ⇒ R [ v₁ , v₂  /₂ e₁ , e₂ ] → ⟦ Q ⟧ v₁ , v₂ ≔₂ e₁ , e₂ ⟦ R ⟧ 

infixr 5 _⨾⟨_⟩_
\end{code}

Those who know me know that I simply adore types and so might be wondering why I considered
raw untyped programs, |Program|, then correspondling
adorning each constructor with a type by considering
programs typed by their specfication.
I do this so that
- given an arbitray program, we can show it satisfies a specfication
- typed-programs can have more than one solution and so we might want to indicate
  which solution we are interested in.

\begin{code}
syntax assignment v e Q R Q⇒Rₑˣ = ⟦ Q ⟧ v ≔ e since Q⇒Rₑˣ ⟦ R ⟧
syntax simuAssign v₁ v₂ e₁ e₂ Q R Q⇒Rₑˣ = ⟦ Q ⟧ v₁ , v₂ ≔₂ e₁ , e₂ since Q⇒Rₑˣ ⟦ R ⟧

prog₀ : ⟦ 𝒩 5 ≈ 𝒩 5 ⟧ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧ 
prog₀ = ⟦ 𝒩 5 ≈ 𝒩 5 ⟧ x ≔ 𝒩 5 since (λ st → ⇒-refl {𝒩 5 ≈ 𝒩 5} st) ⟦ Var x ≈ 𝒩 5 ⟧

prog₀-alt : ⟦ 𝒩 5 ≈ 𝒩 5 ⟧ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₀-alt = assignment _ _ _ _ (λ _ _ → refl)

-- maybe I will need this version of the seuqnece later on..
-- syntax _⨾⟨_⟩_ {Q} {S₁} {P₁} {P₂} {S₂} {R} ⟦Q⟧S₁⟦P₁⟧ P₁⇒P₂ ⟦P₂⟧S₂⟦R⟧ = ⟦ Q ⟧ S₁ since ⟦Q⟧S₁⟦P₁⟧ ⟦ P₁ ⇒⟨ P₁⇒P₂ ⟩ P₂ ⟧ S₂ since ⟦P₂⟧S₂⟦R⟧ ⟦ R ⟧
-- I could not use the mixfix name since I also wanted the syntax declration above, which can only be done for simple non-mixfix identifiers.

-- assign : {t : Type} {v : Variable t} {e : Expr t} {R : Expr Bool} → ⟦ R [ v  / e ] ⟧ v ≔ e ⟦ R ⟧ 
-- assign = λ {t} {v} {e} {R} → assignment (λ {st} z₁ → z₁)

prog₁ : ⟦ True ⟧ x ≔ 𝒩 5 ⨾ x ≔ 𝒩 3 ⟦ Var x ≈ 𝒩 3  ⟧ 
prog₁ = ⟦ True ⟧
          x ≔ 𝒩 5       since (λ _ _ → refl)
        ⟦ 𝒩 3 ≈ 𝒩 3 ⟧
        ⨾⟨ ⇒-refl {𝒩 3 ≈ 𝒩 3} ⟩
        ⟦ 𝒩 3 ≈ 𝒩 3 ⟧
          x ≔ 𝒩 3       since ⇒-refl {𝒩 3 ≈ 𝒩 3}
        ⟦ Var x ≈ 𝒩 3 ⟧

-- awesome! looks like a paper-and-pencil documentation!

-- claim: if the post condition does not involve the varaible assigned, then we can ignore it: |x not free in P yields ⟦ P ⟧ x ≔ e ⟦ P ⟧|. One says, P is invaraint (wrt assignments to varaibles it does not mention).
-- here is a particular case,
constantEx : ∀ {e : Expr Nat} {c : ℕ} → ⟦ Var y ≈ 𝒩 c ⟧ x ≔ e ⟦ Var y ≈ 𝒩 c ⟧
constantEx {e} {c} = ⟦ Var y ≈ 𝒩 c ⟧
                       x ≔ e       since (λ st z₁ → z₁)
                     ⟦ Var y ≈ 𝒩 c ⟧
-- obtained by C-c C-a, then using syntactic sugar.

-- somehow remove the type Nat? ie hide it?
swap : ∀ {X Y : ℕ} → ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧ z ≔ Var {Nat} x ⨾ x ≔ Var {Nat} y ⨾ y ≔ Var {Nat} z ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
swap {X} {Y} =     ⟦ Var x ≈ 𝒩 X  ∧ Var y ≈ 𝒩 Y ⟧
                     z ≔ Var x                     since ∧-sym {Var x ≈ 𝒩 X} {Var y ≈ 𝒩 Y}
                   ⟦ Var y ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                   ⨾⟨ (λ st z₁ → z₁) ⟩
                   ⟦ Var y ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                     x ≔ Var y                     since (λ st z₁ → z₁)
                   ⟦ Var x ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                   ⨾⟨ (λ st z₁ → z₁) ⟩
                   ⟦ Var x ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                     y ≔ Var z                     since (λ st z₁ → z₁)
                   ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
-- this proof was lots of C-c C-a/r, and replacing |assignment ...| with the syntax alias.
-- neato!

-- This is a one-liner using simulatenous substitution,
swap2 : ∀ {X Y : ℕ} → ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧ x , y ≔₂ Var {Nat} y , Var {Nat} x ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
swap2 {X} {Y} = ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧
                  x , y ≔₂ Var y , Var x         since ∧-sym {Var x ≈ 𝒩 X} {Var y ≈ 𝒩 Y}
                ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧

-- ⟦ Q ⟧ v₁ , v₂ ≔₂ e₁ , e₂ since Q⇒Rₑˣ ⟦ R ⟧

{-


prog₁ : ⟦ True ⟧ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₁ = assignment (λ _ → refl)

prog₂ : ⟦ True ⟧ skip ⨾ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₂ = sequence (skip (λ {st} z₁ → z₁)) (assignment (λ _ → refl))
-}

\end{code}
