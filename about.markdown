---
title: About
---

[//]: <> ( below are comments; cf http://stackoverflow.com/questions/4823468/comments-in-markdown )
[//]: <> ( use this to update the site as follows )
[//]: <> ( pandoc -f markdown -t rst about.markdown > about.rst ; ./site rebuild )

I am an Iraqi-Canadian who enjoys mathematics such as

• algebra, topology, and number theory

• lattice theory and discrete mathematics
  
• [calculational mathematics and program derivation](http://www.mathmeth.com/read.shtml)

• [Agda](http://wiki.portal.chalmers.se/agda/pmwiki.php), formalised mathematics

• Category Theory

I also enjoy the oriental game of strategy known as Go, and am a member of the
[Canadian Go Association](https://go-canada.org/Club/Display/1)

For a very accessible introduction, see
[The Interactive Way to Go](http://playgo.to/iwtg/en/)
  
  
Interesting sites
-----------------

• [Maarten M. Fokkinga's Homepage](http://wwwhome.ewi.utwente.nl/~fokkinga/)

• [Maths of Program Construction, Backhouse](http://www.cs.nott.ac.uk/~rcb/papers/papers.html)

• [Edsger W. Dijkstra Archive](http://www.cs.utexas.edu/users/EWD/index12xx.html)

• [Graham Hutton, Publications](http://www.cs.nott.ac.uk/~gmh/bib.html)

• [Ralf Hinze, Publications](http://www.cs.ox.ac.uk/people/publications/date/Ralf.Hinze.html)

• [Jeremy Gibbons, Publications](http://www.cs.ox.ac.uk/people/publications/date/Jeremy.Gibbons.html)

•  [The Monad Reader](https://themonadreader.wordpress.com/)

Blog List
---------
• [Monadically Speaking: Benjamin's Adventures in PLT Wonderland](https://dekudekuplex.wordpress.com/)

• [A Neighborhood of Infinity](http://blog.sigfpe.com/)

• [Patterns in Functional Programming](https://patternsinfp.wordpress.com/)

• [Haskell for all](http://www.haskellforall.com/)
