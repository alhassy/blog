<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Superfluous Speculations - Why dependent types</title>
        <link rel="stylesheet" type="text/css" href="../css/default.css" />
    </head>
    <body>
        <div id="header">
            <div id="logo">
                <a href="../">Superfluous Speculations</a>
            </div>
            <div id="navigation">
                <a href="../">Home</a>
                <a href="../about.html">About</a>
                <a href="../contact.html">Contact</a>
                <a href="../archive.html">Archive</a>
            </div>
        </div>

        <div id="content">
            <h1>Why dependent types</h1>

            <div class="info">
    Posted on January 16, 2016
    
</div>

<div class="info">
    
       Tags: <a href="../tags/types.html">types</a>
    
</div>

<p>in accordance with the propositions-as-types paradigm. Thus, filling in the details of a function definition and ensuring it is type correct is no different from filling in the details of a proof and checking that it establishes the desired conclusion.</p>
<p>This post discuss why I care about dependent-types.</p>
<div class="sourceCode"><pre class="sourceCode agda"><code class="sourceCode agda"><span class="kw">module</span> whydp <span class="kw">where</span></code></pre></div>
<p>Types give us a notion of possible values. Polymorphic types are types that are indexed by other types; they are functions from types to types. Dependent types are types that are indexed by values; they are functions from values to types. Since types themselves are values of a higher type, the distinction between types and values is erased in dependently-typed theories: they are both values of specific types! –this is very different from many conventional programming languages. Alternatively, one may say ‘types are first-class citizens’ in such a language and so can be manipulated like any other value.</p>
<p>Observe that dependent types encompass the other two notions. Since simple types are just polymorphic types that ignore the index-type, we regain simple types from polymorphic types. Since types are the values of the type of ‘small types’, we regain polymorphic types from dependent types.</p>
<p>A dependent (function) type is usually written <code>π x ∶ A • B x</code> or as <code>(x : A) → B x</code>. Agda gives us the latter, but we can mimic the former via syntax declarations:</p>
<div class="sourceCode"><pre class="sourceCode agda"><code class="sourceCode agda">π∶• <span class="ot">:</span> <span class="ot">(</span>A <span class="ot">:</span> <span class="dt">Set</span><span class="ot">)</span> <span class="ot">(</span>B <span class="ot">:</span> A <span class="ot">→</span> <span class="dt">Set</span><span class="ot">)</span> <span class="ot">→</span> <span class="dt">Set</span>
π∶• A B <span class="ot">=</span> <span class="ot">(</span>x <span class="ot">:</span> A<span class="ot">)</span> <span class="ot">→</span> B x

<span class="kw">syntax</span> π∶• A <span class="ot">(λ</span> x <span class="ot">→</span> B<span class="ot">)</span> <span class="ot">=</span> π x ∶ A • B <span class="co">-- `∶` is a ghost colon obtained by `slash :` in emacs</span></code></pre></div>
<p>Why the bullet-notation? We cannot use a low dot <code>.</code> since it is one of the few reserved tokens in Agda; more importantly, I like <a href="http://www.cs.cmu.edu/~15819/zedbook.pdf">Z-notation</a> style quantification. Anyhow, this is why the main page of this blog contains a <code>Π</code>; the <code>Σ</code> is the notation for dependent product: <code>Σ x ∶ A • B x</code> has values being pairs denoted <code>(a , b)</code> with <code>a : A</code> and <code>b : B a</code>; note the <em>dependence</em> of the second component’s type on that of the first component. The dependent product type <code>Σ x ∶ A • B x</code> can be read as ‘the type of <code>x : A</code> satisfying <code>B</code>’; a sort of ‘sub-type’ of <code>A</code>.</p>
<p>A final notation for dependent function is <code>∀ (x : A) → B x</code>. The reason for this notation is that a universal-quantification can be proved true by constructing a procedure that for any element <code>x : A</code> produces a proof of <code>B x</code>, i.e., an inhabitant of the type <code>B x</code>. This is the <a href="https://en.wikipedia.org/wiki/Curry-Howard_correspondence#General_formulation">Curry-Howard Correspondence</a>: it can be stated as saying propositions coincide with types, or that Gentzen’s natural deduction is identical to Church’s lambda calculus. Loosely put, logic and computation are flip-sides of the same coin. For more on this, see <a href="http://homepages.inf.ed.ac.uk/wadler/papers/frege/frege.pdf">Proofs are programs: 19th century logic and 21st century computing</a></p>
<p>Basically, we can express a type which is inhabited precisely when the associated proposition is true: for a proposition <code>p : X → 𝔹</code>, we can construct a dependent type <code>P : (X : x) → Set</code> of ‘’proofs witnessing <code>p(x)</code>, given x’’; we can construct a value of type <code>P x</code> precisely when <code>p(x)</code> holds. <em>If <code>p(x)</code> does not hold, then we cannot construct a value of type <code>P x</code> and there is no run-time exception but a compile-time check.</em></p>
<p>Essentially, a type <code>Q</code> can be thought of as “the type of reasons, or proofs, that <code>Q</code> is true”.</p>
<h2 id="programming">Programming</h2>
<p>Usually, a program that takes input satisfying a given <code>Pre</code>condition and ensuring a given <code>Post</code>condition is usually programmed, call it <code>f</code>, then is proven correct according to the aforementioned specification by ‘verifying’ <code>∀ x • x ∈ Pre ⇒ f x ∈ Post</code> and if that is too difficult, tests are formed after-which <code>f</code> is written and checked to satisfy the given tests; so-called test-driven development. However, since types correspond to propositions, it suffices to ‘construct a correct program from its specification’ as <code>f : Pre → Post</code>; so-called type-driven programming. This is not a new idea; consider <code>abs : ℤ → ℤ</code> with a proof <code>∀ n : ℤ • abs b ≥ 0</code> versus the single program <code>abs : ℤ → ℕ</code>. However, dependent types allow us to put more complicated specification details into the types.</p>
<p>Similar examples include,</p>
<ul>
<li><p><a href="http://www.nuprl.org/MathLibrary/integer_sqrt/">integer square root</a>: <code>isqrt : ℤ → ℤ</code> with a proof of <code>∀ n : ℤ • let i = isqrt n • n ≥ 0 ⇒ i ≥ 0 ∧ i ² ≤ n &lt; (1 + i)²</code> versus the single type <code>isqrt : (n : ℕ) → Σ i ∶ ℕ • i * i ≤ n  ⋀  n &lt; (i + 1) * (i + 1)</code> —notice that the former specification says nothing about the case <code>n &lt; 0</code>.</p></li>
<li><p>Exercise: transform type <code>divrem : ℤ → ℤ → ℤ × ℤ</code> with specification <code>∀ a, b : ℤ • let (q , r) = divrem a b • b &gt; 0 ⇒ 0 ≤ r &lt; b ∧ a = b * q + r</code> into a single dependent-type that incorporates the specification.</p></li>
<li><p>Adding types removes issues. <br /> ‘Given a non-null non-empty array return its first element’. In an untyped language, we take input argument <code>arr</code> and return <code>arr[0]</code>, all the while hoping the user only calls this operation on arrays; and still, we place the specification as a comment. We could place guards checking that the input is not-null, array-like, and not-empty; but that gets in the way of the purpose of the method. The simplest solution is to move to a typed language, where we can insist that <code>arr : Array</code>, all the while hoping the user only calls it with well-defined, non-null data. Then moving to a null-free language, such as Haskell, the issue of nullity disappears. All that remains in the non-empty constraint. Now we move to a dependently-typed setting where arrays are types indexed by numbers that represent their lengths, then our operation has input type <code>∀{n : ℕ} {X : Set} → Array (Suc n) X → X</code>. Now the specification in in the type: given an array of length at-least one, we return an element of that type. (This example adapted from <a href="https://bimorphic.com/learning-idris-part-1/" class="uri">https://bimorphic.com/learning-idris-part-1/</a>)</p></li>
<li><p>Some things can be misleading. <br /> Consider <code>zipWith : ∀ {k n} {A₁ … Aₖ B} → (A₁ → A₂ → ⋯ → Aₖ → B) → Vec n A₁ → Vec n A₂ → ⋯ → Vec n Aₖ → B</code>. At a first glance it seems that the only way to write such a program is by using dependent types; however, if we use lists instead of vectors then it can actually be written without dependent types. Admittedly the <a href="http://www.brics.dk/RS/01/10/BRICS-RS-01-10.ps.gz">solution</a> is a bit clever.</p></li>
</ul>
<p>The program-then-proof approach becomes simultaneously-program-and-prove, or test-driven becomes type-driven.</p>
<p>Dependent types can be used as specifications for programs that are a part of the programs themselves and not independently elsewhere, removed from the code. Then specification satisfiability is tantamount to type-checking!</p>
<h2 id="proving">Proving</h2>
<p>I personally use the dependently-typed programming language Agda as a type checker for doing mathematics —manipulating symbols according to specified rules. Agda is used as just a mechanised mathematical notation that lets me write the mathematics in a natural way, and allows me to be confident about my proofs.</p>
<p><em>Theoretical developments can be fully mechanised and still be presented in readable calculational style, where writing is not significantly more effort than a conventional calculational presentation in LaTeX.</em> – <a href="http://link.springer.com/book/10.1007/978-3-319-06251-8">Wolfram Kahl</a></p>
<p>Moreover he goes on to say,</p>
<ul>
<li><p>Agda permits readability and writiabiliy of mathematics as it is traditionally done by paper-and-pencil —this is accomplished by unicode and mixfix operators. See the bottom of the <a href="http://alhassy.bitbucket.org/posts/blogliterately.html">previous post</a> for an example of this.</p></li>
<li><p>Since a proof is just a formal term, we can nest calculational proofs –compare this with page 2 of Fokkinga’s LaTeX <a href="ftp://ftp.dante.de/tex-archive/macros/latex/contrib/calculation/calculation.pdf">calculation environment</a>.</p></li>
<li><p>the use of Agda enables a completely natural mathematical treatment of theories, with nested calculational proofs, and their direct use as modules of executable programs.</p></li>
<li><p>formalisations in Agda have the advantage that they can be used both for theoretical reasoning and for executable implementations (by instantiating module parameters with appropriate choices of concrete structures).</p></li>
</ul>
<p>Finally, <em>proofs in Agda are fool-proof!</em> Mechanised approaches do not allow us to leave anything as ‘an exercise to the reader’ –to the extent that we always have type-checking— and check every step of our proof. Sometime we may apply a lemma is a proof-step incorrectly on paper-and-pencil proofs and it might not at all be that obvious that we failed to meet some proviso, whereas in a mechanised setting, we can rest assured that such did not occur.</p>
<p>That is, the extra work of using a real language rather than pseudo-code is the type-checking: we can make sure all the i’s are dotted and t’s crossed.</p>
<p>A more personal reason is that mathematics is a hobby for me and there’s a proverb ‘<em>I hear and forget, I see and remember, I do and understand</em>’, so formalising the math I do is to ensure my comprehension —also it’s addictive fun :-)</p>
<h2 id="further-reading">Further Reading</h2>
<p>Some pleasant introductions to dependent types, in Agda.</p>
<ul>
<li><p><a href="http://www.cse.chalmers.se/~ulfn/darcs/AFP08/LectureNotes/AgdaIntro.pdf">Agda by example, by Francesco Mazzoli</a>, a succinct and pleasant read</p></li>
<li><p>Why Dependent Types Matter: <a href="http://www.cs.nott.ac.uk/~psztxa/publ/ydtm.pdf">pdf using epigram</a>, <a href="http://www.cs.nott.ac.uk/~psztxa/talks/splst11.pdf">slides in agda</a>, <a href="https://github.com/jstolarek/why-dependent-types-matter/blob/master/WhyDependentTypesMatter.agda">code from the paper in agda</a></p></li>
<li><p><a href="http://www.cse.chalmers.se/~peterd/papers/DependentTypesAtWork.pdf">Dependent Types at Work by Ana Bove and Peter Dybjer</a></p></li>
<li><p><a href="http://www.cse.chalmers.se/~ulfn/darcs/AFP08/LectureNotes/AgdaIntro.pdf">Dependently Typed Programming in Agda by Ulf Norell</a></p></li>
<li><p><a href="http://learnyouanagda.liamoc.net/">Learn you an Agda</a></p></li>
<li><p><a href="https://www.cs.auckland.ac.nz/research/groups/CDMTCS/researchreports/056douglas.pdf">Paradise Lost, or Paradise Regained</a>, a brief overview of the history and philosophy of constructive mathematics.</p></li>
<li><p><a href="http://math.andrej.com/2012/11/08/how-to-implement-dependent-type-theory-i/">How to implement dependent type theory</a>, uses OCamal</p></li>
<li><p><a href="https://getpocket.com/a/read/1163969076">Why Program Functionally</a></p></li>
</ul>

<div id="disqus_thread"></div>
<script>
/**
* RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
* LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');

s.src = '//superfluousspeculations.disqus.com/embed.js';

s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

        </div>
        <div id="footer">
            Site proudly generated by
            <a href="http://jaspervdj.be/hakyll">Hakyll</a>
        </div>
    </body>
</html>
