I am an Iraqi-Canadian who enjoys mathematics such as

• algebra, topology, and number theory

• lattice theory and discrete mathematics

• `calculational mathematics and program
derivation <http://www.mathmeth.com/read.shtml>`__

• `Agda <http://wiki.portal.chalmers.se/agda/pmwiki.php>`__, formalised
mathematics

• Category Theory

I also enjoy the oriental game of strategy known as Go, and am a member
of the `Canadian Go
Association <https://go-canada.org/Club/Display/1>`__

For a very accessible introduction, see `The Interactive Way to
Go <http://playgo.to/iwtg/en/>`__

Interesting sites
-----------------

• `Maarten M. Fokkinga's
Homepage <http://wwwhome.ewi.utwente.nl/~fokkinga/>`__

• `Maths of Program Construction,
Backhouse <http://www.cs.nott.ac.uk/~rcb/papers/papers.html>`__

• `Edsger W. Dijkstra
Archive <http://www.cs.utexas.edu/users/EWD/index12xx.html>`__

• `Graham Hutton,
Publications <http://www.cs.nott.ac.uk/~gmh/bib.html>`__

• `Ralf Hinze,
Publications <http://www.cs.ox.ac.uk/people/publications/date/Ralf.Hinze.html>`__

• `Jeremy Gibbons,
Publications <http://www.cs.ox.ac.uk/people/publications/date/Jeremy.Gibbons.html>`__

• `The Monad Reader <https://themonadreader.wordpress.com/>`__

Blog List
---------

• `Monadically Speaking: Benjamin's Adventures in PLT
Wonderland <https://dekudekuplex.wordpress.com/>`__

• `A Neighborhood of Infinity <http://blog.sigfpe.com/>`__

• `Patterns in Functional
Programming <https://patternsinfp.wordpress.com/>`__

• `Haskell for all <http://www.haskellforall.com/>`__
