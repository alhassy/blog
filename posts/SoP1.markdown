---
title: SoP1: Assignment
date: March 31, 2016
tags: SoP
---

This post discusses the syntax and semantics of a simple imperative language with simultaneous assignment.



```agda
module SoP1 where

open import Data.Bool using(if_then_else_ ; true ; false) renaming (Bool to 𝔹 )
open import MyPrelude hiding  (_⨾_ ; swap) renaming (_×_ to _AND_)
```
The last import just brings into context the usual notions of propositional equality,
sums, products, function operations,
and a small theory of natural number arithmetic.



#DataTypes

We want to describe sets of states of program variables and to write assertions about them.
For simplicity, in our very elementary language (for now!), we only consider `𝒩`atural numbers and
`𝒫`ropositions.
```agda
data Type : Set where
  𝒩  : Type
  𝒫  : Type
```

Which are then interpreted as elements of type `Set₁`:
```agda
-- subscript t, for "t"ypes
⟦_⟧ₜ : Type → Set₁
⟦ 𝒩 ⟧ₜ = Lift ℕ
⟦ 𝒫 ⟧ₜ = Set
```
Notice that we interpret propositions constructively, since we're in Agda after-all.
Had we interpreted them as Booleans, reasoning would be a bit tricky since Booleans are not
proof-carrying data. Alternatively: a proposition describes the set of states in which it is true
and in Agda these notions coincide via Curry-Howard.

The `Lift` data type behaves, roughly, as follows: `∀ (i j : Level) → i ≤ j → Set i → Set j`.
That is, it lets us embed a small type into a larger type.
The 'equivalence' `A ≅ Lift A` is witnessed by the constructor `lift` and inversely by the eliminator
`lower`.


Finally, without any surprise, we can decide when the types coincide or not:
```agda
_≟ₜ_ : Decidable {A = Type} _≡_
𝒩 ≟ₜ 𝒩 = yes ≡-refl
𝒩 ≟ₜ 𝒫 = no (λ ())
𝒫 ≟ₜ 𝒩 = no (λ ())
𝒫 ≟ₜ 𝒫 = yes ≡-refl
```

It feels like we ought to be able to mechanically derive equality as is done in
Haskell; and indeed there is
[discussion](http://stackoverflow.com/questions/36209339/haskell-deriving-mechanism-for-agda)
on how to achieve this.






#Variables

To make assertions of program variables, we need to have variables in the first place.

We use an explicitly dedicated datatype for formal variables.
```agda
data Variable : Type → Set where
  x y z : ∀ {t} → Variable t

_≟ᵥ_ : ∀ {t s} → Variable t → Variable s → 𝔹
x ≟ᵥ x = true
x ≟ᵥ _ = false
y ≟ᵥ y = true
y ≟ᵥ _ = false
z ≟ᵥ z = true
z ≟ᵥ _ = false
```

A more generic approach would be to parameterise the module by a type for variables that is
endowed with decidable equality ---a setoid.

Note that we cannot consider using de Bruijn indices, as in [Agda by Example: λ-calculus](http://mazzo.li/posts/Lambda.html),
since our variables are not bound to any quantifier. --perhaps I will switch to begin-end
blocks introducing/quantifying over variables.




#Expressions

With types and variables in-hand, we can discuss how to form terms and how to assign meaning to
them.


##Syntax

A *term* is defined to be a constant symbol `c`, a variable `v`, or a function/relation symbol `f` applied
to terms:
```agda
term ∷= constant c ∣ variable v ∣ application f(t₁, …, tₙ)
```

For now, we take our function and relation symbols to be addition, ordering, equality, and
conjunction.
```agda
data Expr : Type → Set₁ where
  -- varaibles
  Var : ∀ {t} (v : Variable t) → Expr t

  -- constant constructors
  𝒩 : ℕ → Expr 𝒩
  𝒫 : Set → Expr 𝒫

  -- function/relation symbol application
  _+_ : (m n : Expr 𝒩) → Expr 𝒩
  _≈_ _≤_ : (e f : Expr 𝒩) → Expr 𝒫
  _∧_ : (p q : Expr 𝒫) → Expr 𝒫

infix 11 _+_
infix 10 _≈_ _≤_
infix 8 _∧_
```
Notice that we make a natural constant by using `𝒩` and the type of the resulting expression is of type `𝒩`,
with this overloading, we can loosely say `𝒩` is the 'constructor' for the 'type' `𝒩`.
Likewise for `𝒫`.

Another candidate for constant constructor is,
```agda
𝒞 : ∀ {t} → ⟦ t ⟧ₜ → Expr t

syntax 𝒞 {t} c = c ∶ t -- ghost colon
```
However, I prefer the pair ` 𝒩, 𝒫 ` as they clearly denote the type of variable I'm working with;
moreover, I cannot make them synonyms for `_ ∶ 𝒩 , _ ∶ 𝒫 ` since `𝒩 , 𝒫 ` are already defined
and Agda only permits constructor overloading.

Also, I thought about using `𝒱` instead of `Var` but the latter seems clearer and the former clashes with the aforementioned convention of the curly-letters denoting the type of the associated
constant.



## Semantics 

An expression like `Var x + 𝒩 5` can appear in a program;
when encountered, it is 'evaluated' in the current machine 'state' to produce another natural
number. What is this state? It is an association of identifiers to values.

```agda
State : Set₁
State = ∀ {t : Type} → Variable t → ⟦ t ⟧ₜ
```

It is common to denote the interpretation of an expression `e` in state `σ` by `⟦ e ⟧ σ` where
the 'semantics-brackets' are defined by
```agda
⟦ c ⟧ σ =  c  ; ⟦ v ⟧ σ = σ v ; ⟦ f(t₁, …, tₙ) ⟧ σ = ⟦ f ⟧( ⟦ t₁ ⟧ σ , …, ⟦ tₙ ⟧ σ)
```
Formally,
```agda
-- subscript e, for "e"xpressions
⟦_⟧ₑ : ∀ {t} → Expr t → State → ⟦ t ⟧ₜ
⟦_⟧ₑ (Var v)  σ = σ v
⟦_⟧ₑ (𝒩 q)   σ = lift q
⟦_⟧ₑ (𝒫 p)   σ = p
⟦_⟧ₑ (e + e₁) σ = lift $ lower (⟦ e ⟧ₑ σ) +ℕ     lower (⟦ e₁ ⟧ₑ σ)
⟦_⟧ₑ (e ≈ e₁) σ = lift (lower (⟦ e ⟧ₑ σ)) ≡ lift (lower (⟦ e₁ ⟧ₑ σ)) -- propositional equality
⟦_⟧ₑ (e ≤ e₁) σ =       lower (⟦ e ⟧ₑ σ)  ≤ℕ     lower (⟦ e₁ ⟧ₑ σ) 
⟦_⟧ₑ (e ∧ e₁) σ =              ⟦ e ⟧ₑ σ   AND            ⟦ e₁ ⟧ₑ σ    -- Cartesian product
```




The addition case works as follows: `⟦ e + e₁ ⟧ σ` is obtained by recursively obtaining the
semantics of the arguments, then using `lower` on the results to obtain the underlying naturals
(of the lifted number type), then adding them, then packing up the whole thing into the lifted
number type again. Similarly for the other cases.

Sometimes we want to update the state for a variable, say when it is reassigned, and we accomplish
this by 'function patching':
```agda
f [z ↦ y] = λ x → If x ≟ z Then y Else f x
```
Formally,

```agda
_[_↦_] : State → ∀ {t} → Variable t → ⟦ t ⟧ₜ → State
_[_↦_] σ {t} v e {s} v∶s with t ≟ₜ s
_[_↦_] σ v e v∶s ∣ yes ≡-refl = if v ≟ᵥ v∶s then e else σ v∶s
_[_↦_] σ v e v∶s ∣ no ¬p = σ v∶s
```


##Reasoning about Boolean expressions

Say proposition `Q` is *weaker* than `P` if `P ⇒ Q`; alternatively, `P` is *stronger* than `Q`.
'A stronger proposition makes more restrictions on the combinations of values its identifiers can
be associated with, a weaker proposition makes fewer.' That is, `Q` is 'less restrictive' than
`P`.
```agda
infix 2 _⇒_
_⇒_ : Expr 𝒫 → Expr 𝒫 → Set₁
P ⇒ Q = ∀ (σ : State) → ⟦ P ⟧ₑ σ → ⟦ Q ⟧ₑ σ
```

The weakest proposition is `true`, or any tautology, because it represents the set of all states
---ie it is always inhabited.
```agda
True : Expr 𝒫
True = 𝒫 ⊤

everything-implies-truth : ∀ {R} → R ⇒ True
everything-implies-truth = λ _ _ → tt
```

The strongest proposition is `false`, because it represents the set of no states
---ie it is never inhabited.
```agda
False : Expr 𝒫
False = 𝒫 ⊥

false-is-strongest : ∀ {Q} → False ⇒ Q
false-is-strongest σ ()
```

Before we move on, let us remark that `_⇒_` admits familiar properties, such as
```agda
⇒-refl : ∀ {P} → P ⇒ P
⇒-refl σ pf = pf

-- C-c C-a
⇒-trans : ∀ {P Q R} → P ⇒ Q → Q ⇒ R → P ⇒ R
⇒-trans = λ {P} {Q} {R} z₁ z₂ st z₃ → z₂ st (z₁ st z₃)

∧-sym : ∀ {P Q} → P ∧ Q ⇒ Q ∧ P
∧-sym st (p , q) = q , p
```

Using `⇒-refl` without mentioning the implicit parameter yields yellow since Agda for some reason
cannot infer them! However, if we comment out the definition of `⟦_⟧ₑ` and postulate it instead,
then no yellow :/ Send help!




#Textual Substitution

Any expression `E` can be considered as a function `E(v₁, …, vₙ)` of it's free-variables, say
`v₁, …, vₙ`, then we can form a new expression `E(e₁, …, eₙ)` for any expressions `eᵢ`.
However, this is cumbersome since the order of variables must be specified and also troublesome
when I only want to substitute a few of the free variables. This is remedied by naming the variables
to be replaced (irrespective of whether they are even in the given expression) and indicating
its replacement as well ---a sort of 'named arguments' approach.

This 'textual substitution' of variables `v` by expressions `e` is defined,
informally, by three rules:
```agda
c [ e / v ]           = e
u [ e / v ]           = IF variable u is one of the vᵢ THEN eᵢ ELSE u
f(t₁, …, tₙ) [ e / v ] = f (t₁ [ e / v ], …, tₙ [ e / v ])
```

The new term to be introduced occurs *above* the
fraction symbol,
`expression [ new_term / old_variable]`,
and we read `E [t / x] ` as 'E with t replacing x'.
As such, some would also write `E [x ╲ t]`; other notations include
`E[x ≔ t]` and `Eˣₜ`.




Warning! Simultaneous is not iteration! In general, `E [ u , v / x , y] ≠ (E [ u / x])[ v / y]`.
For example,
```agda
(x + y)[ x + y , z / x , y] = (x + y) + z ≠ (x + z) + z = (x + y)[x + y / x][z / y]
```

Anyhow, we implement the case `n = 1` and leave the case `n = 2` as an exercise for the reader.
At the moment, I do not foresee the need for the general case; nor do I see a way to make
it (quickly) syntactically appealing in Agda!

```agda
_[_/_] : ∀ {s t} (E : Expr t) (e : Expr s) (v : Variable s) → Expr t
_[_/_] {s} {t} (Var v) u e with s ≟ₜ t
Var v [ e / u ] ∣ yes ≡-refl = if v ≟ᵥ u then e else Var v
Var v [ e / u ] ∣ no ¬p = Var v
𝒩 n [ e / v ] = 𝒩 n
𝒫 p [ e / v ] = 𝒫 p
(E + F) [ e / v ] = E  [ e / v ]  +  F [ e / v ]
(E ≈ F) [ e / v ] = E  [ e / v ]  ≈  F [ e / v ]
(E ≤ F) [ e / v ] = (E [ e / v ]) ≤ (F [ e / v ])
(E ∧ F) [ e / v ] = (E [ e / v ]) ∧ (F [ e / v ])
```
Observe,

  - For the variable case, we substitute precisely when the types match; otherwise
    we cannot even define it.

  - If the types do match, then we pattern-match on the proof to obtain `≡-refl` so
    that Agda unifies them, thereby making the given conditional well-typed.

  - If the types do not match, irrespective of whether the variables are identical or not,
    the only thing we can do, while meeting the type requirements, is to leave them alone.
    This subtlety leads to a bit of poor items such as `Var {𝒩} x [ e / x (of type 𝒫) ] = Var {𝒩} x`, or formally:
```agda
subtle : ∀ {e : Expr 𝒫} → (Var {𝒩} x)[ e / x ] ≡ Var {𝒩} x
subtle = ≡-refl
```
Note that because of the substitution, the second instance of `x` has to have the same type as `e`, which
is `𝒫`. Since the two occurrences of `x` on the left have different types, the substitution does nothing.

  - The last few cases are identical; we intend to remedy this identical coding in the next post by using
    'interpreted function symbols'.

The following is an exercise for the reader, where we want them to implement the function so that
`E [e, d / v,v] = E [d / v]`.
```agda
_[_,_/₂_,_] : ∀ {s₁ s₂ t} (E : Expr t) (e₁ : Expr s₁) (e₂ : Expr s₂) (v₁ : Variable s₁) (v₂ : Variable s₂)  → Expr t
E [ e₁ , e₂ /₂ v₁ , v₂ ] = {! exercise !}
```

To close we give some lemmas dealing with textual substitution.
Their proofs would be by induction on expressions and using the definitions of substitution;
all-in-all quite a bore and so not proven here. From Chapter 4 of SoP:

   -
`σ [ v ↦ σ v ] = σ`: if we update at position `v` by doing the same thing we currently
do there, then that's the same thing as no update at all.
```agda
postulate patching-id : ∀ {t} {σ : State} {v : Variable t} → ∀ {u : Variable t} → (σ [ v ↦ σ v ]) u ≡ σ u
```

   -
Substituting an expression `e` for `v` in `E` and then evaluating in σ yields the same result as
substituting the *value* of `e` in σ for `v` and the evaluating.


```agda
-- constant helper
𝒞 : ∀ {t} → ⟦ t ⟧ₜ → Expr t
𝒞 {𝒩} c = 𝒩 (lower c)
𝒞 {𝒫} c = 𝒫 c

postulate eval-over-sub : ∀ {t s} {v : Variable t} {σ : State} {e : Expr t} {E : Expr s}
                        → ⟦ E [ e / v ]  ⟧ₑ σ ≡ ⟦ E [ 𝒞 (⟦ e ⟧ₑ σ) / v ] ⟧ₑ σ
```

   -
Evaluating `E[x / e]` in state σ is the same as evaluating `E` in σ with `x` updated to evaluation
of `e`.

```agda
postulate sub-to-state : ∀ {t} {v : Variable t} {e : Expr t} {s} {E : Expr s} {σ : State}
               → ⟦ E [ e / v ] ⟧ₑ σ ≡ ⟦ E ⟧ₑ (σ [ v ↦ ⟦ e ⟧ₑ σ ])
```
That is, evaluating a substituted expression is the same as evaluating the original expression in an
updated state. Notice that this law allows us to make substitutions part of the syntax and the
law is then how to evaluate such syntax! Is that a better approach?!

   -
   
```agda
postulate iterated-substitution : ∀ {t} {v : Variable t} {e d : Expr t} {s} {E : Expr s}
                                → (E [ e / v ]) [ d / v ] ≡ E [ e [ d / v ] / v ]
```
   
   - Substitution of a non-occurring variable: `(E[x / u])[y / v] = E [x / u[y / v] ]`, if `y` is not free in `E`. (SoP, 4.4.8)

   - 
Provided `x = (x₁, …, xₙ)` are distinct identifiers and `u = (u₁, …, uₙ)` are fresh, distinct
identifiers, we have `E[x / u][u / x] = E`.


For the curious, here is a very uninsightful proof of one of the above lemmas:
```agda
patching-id-pf : ∀ {t} {σ : State} (v : Variable t) → ∀ (u : Variable t) → (σ [ v ↦ σ v ]) u ≡ σ u
patching-id-pf {t} {σ} v u with t ≟ₜ t
patching-id-pf v u ∣ no ¬p = ≡-refl
patching-id-pf x x ∣ yes ≡-refl = ≡-refl
patching-id-pf x y ∣ yes ≡-refl = ≡-refl
patching-id-pf x z ∣ yes ≡-refl = ≡-refl
patching-id-pf y x ∣ yes ≡-refl = ≡-refl
patching-id-pf y y ∣ yes ≡-refl = ≡-refl
patching-id-pf y z ∣ yes ≡-refl = ≡-refl
patching-id-pf z x ∣ yes ≡-refl = ≡-refl
patching-id-pf z y ∣ yes ≡-refl = ≡-refl
patching-id-pf z z ∣ yes ≡-refl = ≡-refl
```


Admittedly, it could have been simplified had we implemented equality as a `Dec`idable rather than
just a Boolean.

To be fair, here's a another proof of another result: substitution is monotonic. Informally,
```agda  
  ⟦ P [ v / e ] ⟧ₑ σ
≡
  ⟦ P ⟧ₑ (σ [ v ↦ ⟦ e ⟧ₑ σ ])   -- since sub-to-state
→ 
  ⟦ Q ⟧ₑ (σ [ v ↦ ⟦ e ⟧ₑ σ ])   -- since P ⇒ Q
≡                              
  ⟦ Q [ v / e ] ⟧ₑ σ            -- since sub-to-state
```
Rather than create/import the needed syntacic sugar, let's just 'wing-it':
```agda
subst-over-⇒ : ∀ {P Q t} {v : Variable t} {e : Expr t} → P ⇒ Q → P [ e / v ] ⇒ Q [ e / v ]
subst-over-⇒ {P} {Q} {t} {v} {e} P⇒Q σ
  rewrite sub-to-state {t} {v} {e} {𝒫} {P} {σ} ` sub-to-state {t} {v} {e} {𝒫} {Q} {σ} = P⇒Q _
```




#Programming Language

The syntax of our miniature language is as follows,
```agda
data Program : Set₁ where
  skip  : Program
  abort : Program
  _⨾_   : Program → Program → Program
  _≔_      : ∀ {t} → Variable t → Expr t → Program
  _,_≔₂_,_ : ∀ {s t} → Variable t → Variable s → Expr t → Expr s → Program
```
The subscript `2` in simultaneous assignment is needed, otherwise there'll be parsing conflicts
with the notation for elementary assignment.


As in usual programming languages, the precedence for the program constructors cannot be greater than that of expressions;
otherwise, we'll need many parentheses.
```agda
infix 7 _≔_
infixr 5 _⨾_
```

Now we quickly turn to the program semantics ---we do not give 'operational semantics' but instead use 'Hoare Triples'.
The traditional notation for Hoare Triples is `{Q} S {R}`, with assertions enclosed in braces.
However, since braces are one of the few reserved symbols in Agda, we instead employ the semantics-brackets
yet again. This is not too much of a leap since the conditions are seen to *define* the program and so assign
it meaning ---the ideology mentioned in SoP is that one begins with the pre- and post-conditions then derives the program.
```agda
infixr 5 _⨾⟨_⟩_

data ⟦_⟧_⟦_⟧ : Expr 𝒫 → Program → Expr 𝒫 → Set₁ where
  _⨾⟨_⟩_      : ∀ {Q S₁ P₁ P₂ S₂ R} → ⟦ Q ⟧ S₁ ⟦ P₁ ⟧ → P₁ ⇒ P₂ → ⟦ P₂ ⟧ S₂ ⟦ R ⟧ → ⟦ Q ⟧ S₁ ⨾ S₂ ⟦ R ⟧
  skip-rule  : ∀ {Q R} → Q ⇒ R → ⟦ Q ⟧ skip ⟦ R ⟧
  absurd     : ∀ {Q R} → Q ⇒ False → ⟦ Q ⟧ abort ⟦ R ⟧
  assignment : {t : Type} (v : Variable t) (e : Expr t) (Q R : Expr 𝒫)
             → Q ⇒ R [ e  / v ] → ⟦ Q ⟧ v ≔ e ⟦ R ⟧ 
  simuAssign : {t₁ t₂ : Type} (v₁ : Variable t₁) (v₂ : Variable t₂) (e₁ : Expr t₁) (e₂ : Expr t₂)
               (Q R : Expr 𝒫) → Q ⇒ R [ e₁ , e₂ /₂ v₁ , v₂ ] → ⟦ Q ⟧ v₁ , v₂ ≔₂ e₁ , e₂ ⟦ R ⟧ 
```

Those who know me know that I simply adore types and so might be wondering why I considered
raw untyped programs, `Program`, then correspondingly
adorned each constructor with a type by considering
programs typed by their specification.
I do this so that

  - given an arbitrary program, we can show it satisfies a specification

  - typed-programs can have more than one solution and so we might want to indicate
  which solution we are interested in.

Anyhow, some remarks are in order:

  - sequential composition is executed by doing the first part then the second part:
    if we want to establish `R` from `Q` by executing `S₁ ⨾ S₂` then necessarily we
    establish an intermediate `P` from `Q` by executing `S₁` and from that
    we reach `R` via `S₂`. Does composition order matter? Nope, we have
    `(S₁ ⨾ S₂) ⨾ S₃ = S₁ ⨾ (S₂ ⨾ S₃)` ---just as `(m × n) × k = m × (n × k)`.

       ''
         Be aware of the role of the semicolon; it is used to *combine* adjacent,
         independent commands into a single command, much the same way it is used
         in English to combine independent clauses. (For an example of its use
         in English, see the previous sentence.)
       ''

       In particular, the semicolon is not a statement terminator;
       which is great since we haven't even defined a notion of statement!

  - execution of `skip` does nothing; some languages denote it by whitespace of by `;`.
    It is an explicit way to say nothing is done ---which is just its definition:
    given `Q` we wish to establish `R` by doing nothing, and this is only possible if
    `Q ⇒ R`. Interestingly, just as `1 × n = n × 1 = n` we have `skip ⨾ S = S ⨾ skip = S`.

  - `abort` should never be executed, since it can only be executed in a state satisfying
    `False` and no state satisfies `False`! If it is ever executed, then the program (and its proof)
    is in error and abortion is called for. Which fits in nicely with the law
    `abort ⨾ S = S ⨾ abort = abort` ---similar to `0 × n = n × 0 = 0`.

  - After execution of `x ≔ e` we have that `x` will contain the value `e` and so
  `R` can be established precisely if `R`, with the value of `x` replaced by the value of `e`
  can be established before the execution. That is, if we can show that the condition with updated
  variables can be established, then making the update establishes the condition.

  - Assignment ''`x` becomes `e`'' should really have proof obligation
  `Q   ⇒  ( domain e  cand  Rˣₑ )`
  where `domain` is a predicate that describes the set of all states in which `e` is well-defined
  and so may be evaluated, and `cand` is just lazy-conjunction: `x cand y = if x then y else false`.
  
  As mentioned above, in a future post, I intend to replace the explicit constructors by what interpreted
  function symbols, but it seems that I need them to be augmented with a 'domain' operation.
  For example, the symbol `÷` can be interpreted as integral division with domain condition being the
  second argument is non-zero.
  For now, all of our expressions are well-defined. In the future, we intend to use
  'interpreted function symbols with domain'.

  In practice, the `domain` condition is omitted altogether since assignments should be always
  written in contexts in which the expressions can be properly evaluated.
  If we are deriving our program and know not the context, then the full definition is quiet
  useful.

We now introduce some combinators to make our annotated programs look more like those presented in
the literature.
```agda
-- for easy copy/paste: ⟦ ? ⟧ ? ≔ ? since ? ⟦ ? ⟧
syntax assignment v e Q R Q⇒Rₑˣ = ⟦ Q ⟧ v ≔ e since Q⇒Rₑˣ ⟦ R ⟧
syntax simuAssign v₁ v₂ e₁ e₂ Q R Q⇒Rₑˣ = ⟦ Q ⟧ v₁ , v₂ ≔₂ e₁ , e₂ since Q⇒Rₑˣ ⟦ R ⟧
syntax skip-rule {Q} {R} Q⇒R = ⟦ Q ⟧⟨ Q⇒R ⟩⟦ R ⟧ -- ⟦ Q ⟧ skip-since Q⇒R ⟦ R ⟧
```

For example, ---Example 1, page 118 of SoP---
```agda
prog₀ : ⟦ 𝒩 5 ≈ 𝒩 5 ⟧ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₀ = assignment _ _ _ _ (λ _ _ → ≡-refl)
```
Now with out handy-dandy syntax:
```agda
prog₁ : ⟦ 𝒩 5 ≈ 𝒩 5 ⟧ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧ 
prog₁ = ⟦ 𝒩 5 ≈ 𝒩 5 ⟧ x ≔ 𝒩 5 since (λ st → ⇒-refl {𝒩 5 ≈ 𝒩 5} st) ⟦ Var x ≈ 𝒩 5 ⟧
```
Before we move on, we know from propostional logic that all theorems can be replaced with `true`.
In particular, reflexitvity of equality does not give us much (in our toy language) and so
we can replace it with true which was defined earlier as the unit type.
```agda
prog₂ : ⟦ True ⟧ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₂ = ⟦ True         ⟧
           x ≔ 𝒩 5      since (λ _ _ → ≡-refl) 
        ⟦ Var x ≈ 𝒩 5 ⟧
```





#Examples

We now port over some simple programs from Gries' text and show that readability is still maintained!
We begin with the most complicated one, that is expressible in the language as described so far.

```agda
swap : ∀ {X Y : ℕ} → ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧
                             z ≔ Var {𝒩} x
                           ⨾ x ≔ Var {𝒩} y
                           ⨾ y ≔ Var {𝒩} z
                      ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
swap {X} {Y} =
                   ⟦ Var x ≈ 𝒩 X  ∧ Var y ≈ 𝒩 Y ⟧
                     z ≔ Var x                     since ∧-sym {Var x ≈ 𝒩 X} {Var y ≈ 𝒩 Y}
                   ⟦ Var y ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                   ⨾⟨ (λ st z₁ → z₁) ⟩
                   ⟦ Var y ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                     x ≔ Var y                     since (λ st z₁ → z₁)
                   ⟦ Var x ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                   ⨾⟨ (λ st z₁ → z₁) ⟩
                   ⟦ Var x ≈ 𝒩 Y ∧ Var z ≈ 𝒩 X ⟧
                     y ≔ Var z                     since (λ st z₁ → z₁)
                   ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
```

Some things to note:

  - We needed to mention the type 𝒩 since the variables are in-fact always independent; eg, the first `x`
    is not at all related to any second occurrence of `x`. This is like the numerals: one 4 does not affect
    another 4.

  - The proof was really just `C-c C-a/r`, auto and refinements! What I did manually was replace
    the `assignment` constructor with the preferred syntax.

  - finally, equality syntax, as defined so far, is only well-defined for 𝒩 and so we could not
    prove `swap` for all types.

Swapping becomes a one-liner using simultaneous assignment.
```agda
swap2 : ∀ {X Y : ℕ} → ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧ x , y ≔₂ Var {𝒩} y , Var {𝒩} x ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
swap2 {X} {Y} = ⟦ Var x ≈ 𝒩 X ∧ Var y ≈ 𝒩 Y ⟧
                  x , y ≔₂ Var y , Var x         since ∧-sym {Var x ≈ 𝒩 X} {Var y ≈ 𝒩 Y}
                ⟦ Var x ≈ 𝒩 Y ∧ Var y ≈ 𝒩 X ⟧
```
One popular language that offers such a command is Python.

Another example of sequence ---many C-c C-r!
```agda
prog₃ : ⟦ True ⟧ skip ⨾ x ≔ 𝒩 5 ⟦ Var x ≈ 𝒩 5  ⟧
prog₃ =  ⟦ True ⟧⟨    ⇒-refl {True}
         ⟩⟦ True ⟧
        ⨾⟨ (λ st x₁ → x₁) ⟩
        ⟦ True ⟧
        x ≔ 𝒩 5 since (λ _ _ → ≡-refl)
        ⟦ Var x ≈ 𝒩 5 ⟧
```


Multiple assignments to the same variable mean only the last assignment is realized.
```agda
prog₄ : ⟦ True ⟧ x ≔ 𝒩 5 ⨾ x ≔ 𝒩 3 ⟦ Var x ≈ 𝒩 3  ⟧ 
prog₄ = ⟦ True ⟧
          x ≔ 𝒩 5       since (λ _ _ → ≡-refl)
        ⟦ 𝒩 3 ≈ 𝒩 3 ⟧
        ⨾⟨ ⇒-refl {𝒩 3 ≈ 𝒩 3} ⟩
        ⟦ 𝒩 3 ≈ 𝒩 3 ⟧
          x ≔ 𝒩 3       since ⇒-refl {𝒩 3 ≈ 𝒩 3}
        ⟦ Var x ≈ 𝒩 3 ⟧
```
Awesome! Looks like a paper-and-pencil annotation (with proofs)!


If the post condition does not involve the variable assigned, then we can ignore it: `x not free in P yields ⟦ P ⟧ x ≔ e ⟦ P ⟧`.

Here is a particular case, obtained by C-c C-a then using syntactic sugar --page 119--,
```agda
constantEx : ∀ {e : Expr 𝒩} {c : ℕ} → ⟦ Var y ≈ 𝒩 c ⟧ x ≔ e ⟦ Var y ≈ 𝒩 c ⟧
constantEx {e} {c} = ⟦ Var y ≈ 𝒩 c ⟧
                       x ≔ e       since (λ st pf → pf)
                     ⟦ Var y ≈ 𝒩 c ⟧
```

Sometimes we'll need lemmas in our annotations.
For example, If I want `x` to be at least 10 after increasing it by 3,
then I necessairly need `x` to be at least 7 before the increment.
[Page 118, but no negative numbers.]
```agda
leq : ⟦ 𝒩 7 ≤ Var x ⟧ x ≔ Var x + 𝒩 3 ⟦ 𝒩 10 ≤ Var x  ⟧
leq = ⟦ 𝒩 7 ≤ Var x ⟧
         x ≔ Var x + 𝒩 3       since lemma1
      ⟦ 𝒩 10 ≤ Var x ⟧
      where
        lemma1 : 𝒩 7 ≤ Var x ⇒ 𝒩 10 ≤ Var x + 𝒩 3
        lemma1 σ 7≤σx = 7≤σx +-mono ≤ℕ-refl {3}
```
where we have used the fact that addition preserves order,
`_+-mono_ : ∀ {x y u v : ℕ} → x ≤ℕ y → u ≤ℕ v → x +ℕ u ≤ y +ℕ v`.




#Program Equality

Earlier, when introducing the programming language semantics, we mentioned
certain equalities; such as, `skip` is identity of composition:
`skip ⨾ S = S ⨾ skip = S`. We never defined program equality! Let's fix that.

Two programs are considered equal precisely when they satisfy the same specifications,
```agda
infix 4 _≈ₚ_
_≈ₚ_ : Program → Program → Set₁
S ≈ₚ T = ∀ {Q R} → ⟦ Q ⟧ S ⟦ R ⟧ ⇔ ⟦ Q ⟧ T ⟦ R ⟧
```
Where `x ⇔ y` just means a pair of functions `x → y` and `y → x`.


Let us prove the aforementioned unity law. We begin with an expected result, then the needed pieces.
```agda
strengthen : ∀ Q {P R S} → Q ⇒ P → ⟦ P ⟧ S ⟦ R ⟧ → ⟦ Q ⟧ S ⟦ R ⟧
strengthen = {! exercise in C-c C-c and C-c C-a   ^_^ !}
```
What about?
`weaken : ∀ {Q P} R {S} → P ⇒ R → ⟦ Q ⟧ S ⟦ P ⟧ → ⟦ Q ⟧ S ⟦ R ⟧` ?

Let's add some syntacic sugar ---good ol' sweet stuff ^_^
```agda
syntax strengthen Q Q⇒P ⟦P⟧S⟦R⟧ = ⟦ Q ⟧⇒⟨ Q⇒P ⟩ ⟦P⟧S⟦R⟧
```



```agda
tada : ∀ {S Q R} →  ⟦ Q ⟧ skip ⨾ S ⟦ R ⟧ → ⟦ Q ⟧ S ⟦ R ⟧
tada {S} {Q} {R} (skip-rule Q⇒P₁ ⨾⟨ P₁⇒P₂ ⟩ ⟦P₂⟧S⟦R⟧) =
  ⟦ Q ⟧⇒⟨ (λ σ z₁ → P₁⇒P₂ σ (Q⇒P₁ σ z₁))⟩ ⟦P₂⟧S⟦R⟧


tada˘ : ∀ {S} {Q R}  → ⟦ Q ⟧ S ⟦ R ⟧ →  ⟦ Q ⟧ skip ⨾ S ⟦ R ⟧
tada˘ {S} {Q} {R} ⟦Q⟧S⟦R⟧ =
  ⟦ Q ⟧⟨ ⇒-refl {Q}
  ⟩⟦ Q ⟧
  ⨾⟨ ⇒-refl {Q} ⟩
  ⟦Q⟧S⟦R⟧

skip-left-unit : ∀ {S} → skip ⨾ S ≈ₚ S
skip-left-unit = tada , tada˘
```

These abstract-proofs are not as clear as the examples, but that's to be expected since
the syntax was made with concrete code annotation in mind.




#Closing

It seems that we've managed to get a lot done --up to chapter 9 of SoP excluding arrays,
quantification, and a few other function and relation symbols. We intend to remedy the situation
in future posts! Hope you've enjoyed the article!







